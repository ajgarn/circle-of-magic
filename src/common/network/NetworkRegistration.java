package common.network;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import model.GameHeader;
import model.gamesettings.FFAGameSettings;
import model.spells.SpellSlot;
import services.network.NetworkSettings;

import com.badlogic.gdx.math.Vector2;
import com.esotericsoftware.kryonet.rmi.ObjectSpace;
import common.network.holders.IGameHeadersHolder;

public class NetworkRegistration {
	/**
	 * Registers the classes to be sent over the network, between client and
	 * server.
	 */
	public static NetworkSettings getNetworkSettings() {
		NetworkSettings settings = new NetworkSettings();
		
		// Register classes for sending over network.
		
		// Important to register version control first.
		try {
			settings.registerClass(
					Class.forName("server.network.IVersionControlService"), 0);
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		
		// Kryonet classes
		settings.registerClass(ObjectSpace.InvokeMethod.class);
		settings.registerClass(ObjectSpace.InvokeMethodResult.class);
		
		// General classes
		settings.registerClass(ArrayList.class);
		settings.registerClass(byte[].class);
		settings.registerClass(CopyOnWriteArrayList.class);
		settings.registerClass(HashMap.class);
		settings.registerClass(HashSet.class);
		settings.registerClass(Inet4Address.class);
		settings.registerClass(Inet6Address.class);
		settings.registerClass(LinkedList.class);
		settings.registerClass(Map.class);
		settings.registerClass(SpellSlot[].class);
		settings.registerClass(Vector2.class);
		
		// Register all classes that implements INetworkObject in the following packages.
		// TODO: Put these packages in an external file.
//		registerPackage(settings, Package.getPackage("model"));
//		registerPackage(settings, Package.getPackage("model.gamesettings"));
		// Package is not loaded before, thus referring from GameHeader class.
		registerPackage(settings, Package.getPackage("services.network"));
		registerPackage(settings, GameHeader.class.getPackage());
		registerPackage(settings, FFAGameSettings.class.getPackage());
		registerPackage(settings, IGameHeadersHolder.class.getPackage());
		registerPackage(settings, Package.getPackage("server.network"));
		
		return settings;
	}
	
	private static void registerPackage(NetworkSettings settings, Package pkg) {
		if (pkg != null) {
			settings.registerPackage(pkg);
		} else {
			throw new NullPointerException("Package not loaded. " +
					"Cannot retrieve content for registration.");
		}
	}
}
