package common.network.holders;

import java.util.Collection;

import services.network.INetworkObject;

import model.GameHeader;


/**
 * Holder for available games on a server.
 * @author Anton Jansson
 *
 */
public interface IGameHeadersHolder extends INetworkObject {
	/**
	 * Gets all games available on the current host.
	 * @return A collection of GameHeaders.
	 */
	public Collection<GameHeader> getAvailableGames();
	/**
	 * Gets all lobbies available on the current host.
	 * @return A collection of GameHeaders.
	 */
	public Collection<GameHeader> getAvailableLobbies();
}
