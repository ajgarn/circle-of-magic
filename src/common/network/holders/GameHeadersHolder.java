package common.network.holders;

import java.util.ArrayList;
import java.util.Collection;

import model.AbstractGame;
import model.GameHeader;
import model.Lobby;

/**
 * Holder for the available games of a server, that can be accessed by clients.
 * @author Anton Jansson
 *
 */
public class GameHeadersHolder implements IGameHeadersHolder {
	
	private final Collection<? extends AbstractGame> games;
	private final Collection<Lobby> lobbies;
	
	@SuppressWarnings("unused")
	private GameHeadersHolder() {
		this(null, null);
	}

	/**
	 * Create a GamesNetHolder.
	 * @param games The collection of available games.
	 */
	public GameHeadersHolder(Collection<? extends AbstractGame> games,
			Collection<Lobby> lobbies) {
		super();
		this.games = games;
		this.lobbies = lobbies;
	}
	
	@Override
	public Collection<GameHeader> getAvailableGames() {
		Collection<GameHeader> headers = new ArrayList<GameHeader>();
		for (AbstractGame game : games) {
			headers.add(game.getHeader());
		}
		return headers;
	}
	
	@Override
	public Collection<GameHeader> getAvailableLobbies() {
		Collection<GameHeader> headers = new ArrayList<GameHeader>();
		for (Lobby lobby : lobbies) {
			headers.add(lobby.getHeader());
		}
		return headers;
	}
}
