package common.network.holders;

import model.GameHeader;
import services.network.INetworkObject;

/**
 * Holder for connecting a game.
 * @author Anton Jansson
 */
public class JoinLobbyHolder implements INetworkObject {

	private final GameHeader game;
	
	@SuppressWarnings("unused")
	private JoinLobbyHolder() {
		this(null);
	}
	
	public JoinLobbyHolder(GameHeader game) {
		this.game = game;
	}
	
	public GameHeader getGameHeader() {
		return game;
	}
}
