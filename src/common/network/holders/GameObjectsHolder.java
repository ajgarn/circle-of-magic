package common.network.holders;

import java.util.HashMap;
import java.util.Map;

import model.core.IGameObject;
import services.network.NetHolder;

/**
 * Holder for GameObjects that must be updated on clients.
 * @author Anton Jansson
 */
public class GameObjectsHolder extends NetHolder<Map<Integer, ? extends IGameObject>> {
	
	private GameObjectsHolder() {
		super(null);
	}
	
	public GameObjectsHolder(Map<Integer,  ? extends IGameObject> gameObjects) {
		super(new HashMap<Integer, IGameObject>(gameObjects));
	}
}
