package common.network.holders;

import java.util.ArrayList;
import java.util.Collection;

import model.Player;
import services.network.NetHolder;

/**
 * Holder for players that must be updated on clients.
 * @author Anton Jansson
 */
public class PlayersHolder extends NetHolder<Collection<Player>> {
	
	private PlayersHolder() {
		super(null);
	}
	
	public PlayersHolder(Collection<Player> players) {
		super(new ArrayList<Player>(players));
	}
}
