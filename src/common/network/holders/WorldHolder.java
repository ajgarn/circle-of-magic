package common.network.holders;

import java.util.ArrayList;
import java.util.Collection;

import services.network.INetworkObject;

import model.AbstractGame.GameState;
import model.AbstractGameSettings;
import model.Player;
import model.World;

/**
 * Holder for a {@link World} to be sent over network.
 * @author Anton Jansson
 *
 */
public class WorldHolder implements INetworkObject {
	private final World world;
	private final Collection<Player> players;
	private final GameState state;
	private final AbstractGameSettings settings;
	
	@SuppressWarnings("unused")
	private WorldHolder() {
		this(null, null, null, null);
	}
	
	public WorldHolder(World world, Collection<Player> players, GameState state, 
			AbstractGameSettings settings) {
		super();
		this.world = world;
		this.state = state;
		this.settings = settings;
		this.players = new ArrayList<Player>();
		if (players != null) {
			for (Player p : players) {
				this.players.add(p.clone());
			}
		}
	}
	
	/**
	 * Get the players in this game.
	 */
	public Collection<Player> getPlayers() {
		return players;
	}
	
	/**
	 * Get the {@link World}.
	 */
	public World getWorld() {
		return world;
	}

	/**
	 * Get the settings for the game.
	 */
	public AbstractGameSettings getSettings() {
		return settings;
	}

	/**
	 * Get the state of the game.
	 */
	public GameState getState() {
		return state;
	}
}
