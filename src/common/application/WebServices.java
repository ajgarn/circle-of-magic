package common.application;

/**
 * Configuration for web services used by application.
 * @author Anton Jansson
 *
 */
public class WebServices {

	public static final String AUTHENTICATION = 
			"http://circleofmagic.yellowend.com/services/login/?data=";
	public static final String LOGGING = 
			"http://circleofmagic.yellowend.com/services/logs/";
	
	
	/**
	 * Constants for network security.
	 * @author Anton Jansson
	 *
	 */
	public class Security {
		// Security
		public static final String IV = "DB8IGx9QtCoBmX4H";
		public static final String SECRET_KEY = "M2OV70Ic1RgcBxA8";
		
		// Control key
		public static final int CONTROL_KEY_LENGTH = 8;
	}

}
