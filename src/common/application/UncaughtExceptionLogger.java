package common.application;

import java.lang.Thread.UncaughtExceptionHandler;

import services.logger.Log;

/**
 * Sends uncaught exceptions to the log.
 * @author Anton Jansson
 *
 */
public class UncaughtExceptionLogger implements UncaughtExceptionHandler {

	@Override
	public void uncaughtException(Thread t, Throwable e) {
		Log.fatal("Uncaught exception in thread " + t.getName(), e);
	}

}
