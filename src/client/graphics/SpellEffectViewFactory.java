package client.graphics;

import model.core.SpellEffect;
import services.graphics.particles.ParticleSystemFactory;
import services.graphics.renderables.IRenderable3D;

/**
 * A factory used to create IRenderableObejcts for SpellEffects. Used by
 * GameObjectView
 * 
 * @author robin
 * 
 */
public class SpellEffectViewFactory {
	public static IRenderable3D create(SpellEffect spellEffect) {
		switch (spellEffect.getSpellName()) {
		case FROSTBOLT:
			return ParticleSystemFactory.createFrostbolt(2, 0.5f);
		case FIREBALL:
			return ParticleSystemFactory.createFire(2, 0.5f);
		case LIGHTNINGBOLT:
			return ParticleSystemFactory.createLightning(2, 2);
		default:
			return null;
		}
	}
}
