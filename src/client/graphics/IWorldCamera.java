package client.graphics;

import model.core.IGameObject;
import services.math.IVector2;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector3;

/**
 * This class represents a world camera
 * 
 * @author Robin Gronberg
 * 
 */
public interface IWorldCamera {
	/**
	 * Convert from world coordinates to screen coordinates
	 * @param worldPos The coordinates in the world
	 * @return The position in on the screen
	 */
	public IVector2 getScreenPosition(Vector3 worldPos);
	/**
	 * Convert from screen coordinates to world coordinates
	 * @param screenPos The coordinates of the screen
	 * @return The position in the world along the xy-plane
	 */
	public IVector2 getWorldPos(IVector2 screenPos);
	/**
	 * Updates this IWorldCamera. 
	 * @param dTime
	 */
	public void update(float dTime);

	public void setAspectRatio(float aspectRatio);
	/**
	 * Sets the zoom of this IWorldCamera
	 * @param zoom 
	 */
	public void setZoom(float zoom);

	public float getZoom();

	/**
	 * Set the speed this camera will move during the update
	 * 
	 * @param speed
	 *            The amount to move the camera along the ground.
	 */
	public void setSpeed(IVector2 speed);

	/**
	 * Get this IWorldCamera speed
	 * 
	 * @return
	 */
	public IVector2 getSpeed();

	/**
	 * Center this camera at position
	 * 
	 * @param position
	 *            The position in 2d coordinates along the ground.
	 */
	public void lookAt(IVector2 position);

	/**
	 * Lock this camera at a specific game object
	 * 
	 * @param gameObject
	 *            The gameObject to lock at
	 */
	public void lockAtGameObject(IGameObject gameObject);

	/**
	 * If this camera is locked at a specific gameobject, unlock it.
	 */
	public void unlockAtGameObject();

	/**
	 * Get if this camera is locked at a specific gameObject
	 * 
	 * @return true if locked, false otherwise
	 */
	public boolean isLockedAtGameObject();

	/**
	 * The the camera of this WorldCamera
	 * 
	 * @return The camera
	 */
	public Camera getCamera();

	/**
	 * Get this cameras position in the world.
	 * 
	 * @return This camera's position in the 3d-world along the ground as a
	 *         2d-vector
	 */
	public IVector2 getPosition();
}
