package client.graphics;


import services.graphics.models.Model;
import services.graphics.particles.ParticleSystem;
import services.graphics.particles.ParticleSystemFactory;
import services.graphics.util.ContentManager;
import services.math.LinearFunction;
import client.graphics.map.GroundTexture;
import client.graphics.map.IMap;
import client.graphics.map.Map;
import client.graphics.map.MovableGroundTexture;

/**
 * A factory that creates differnet Maps. (graphical representation only)
 * @author Robin Gronberg
 *
 */
public class MapFactory {
	public static IMap create(){
		ContentManager c = ContentManager.getInstance();
		
		GroundTexture g0 = new GroundTexture(c.getTexture("/maps/rock.png"), 10,false);
		GroundTexture g1 = new GroundTexture(c.getTexture("maps/mask.png"), 
				c.getTexture("maps/grass.png"), 30,false);
		GroundTexture g2 = new MovableGroundTexture(c.getTexture("/maps/lava.png"), 10,true,
				0.03f,new LinearFunction(new float[]{-7.5f,7.5f}));
		Model model = new Model(c.getModel("maps/map.md2"),c.getTexture("maps/map.png"),true,false,false,false);
		ParticleSystem s = ParticleSystemFactory.createLavaFlowEffect(5, c.getTexture("particles/lava.png"));
		IMap m = new Map(150,150,new GroundTexture[]{g0,g1,g2},70,model);
		m.attachParticleSystem(s);
		return m;
		
	}
}
