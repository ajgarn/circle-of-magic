package client.graphics;

import model.core.IGameObject;
import model.core.Vector2;
import services.math.IVector2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Plane;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;
/**
 * This class is used to render a WorldView.
 * @author Robin Gronberg
 *
 */
public class WorldCamera implements IWorldCamera{
	private static final float CAMERA_HEIGHT = 20;
	private static final float CAMERA_DEAPTH = 15;
	
	private Camera camera;
	private float zoom = 5;
	private IVector2 cameraSpeed = new Vector2(0,0);
	private IGameObject lockedGameObject;
	
	private final IVector2 cameraOffset;
	
	/**
	 * Create a camera that displays the world. 
	 * @param worldView The world this camera should display.
	 */
	public WorldCamera(){
		cameraOffset = new Vector2(0, 0);
		lookAt(new Vector2(0, 0));
	}
	public void setAspectRatio(float aspectRatio){
		this.camera = new PerspectiveCamera(15, 2f * aspectRatio, 2f);
		this.camera.near = 1f;
		this.camera.translate(0, CAMERA_HEIGHT * zoom,CAMERA_DEAPTH* zoom);
		this.camera.lookAt(0, 0, 0);
		this.camera.far = 200; 
	}
	private void move(IVector2 dPosition){
		cameraOffset.setX(cameraOffset.getX() + dPosition.getX());
		cameraOffset.setY(cameraOffset.getY() + dPosition.getY());
	} 
	@Override
	public void setSpeed(IVector2 speed) {
		cameraSpeed = speed;
	}
	@Override
	public IVector2 getSpeed() {
		return cameraSpeed;
	}
	/**
	 * Center this camera at position
	 * @param position The position in 2d coordinates along the ground.
	 */
	public void lookAt(IVector2 position){
		cameraOffset.setX(position.getX());
		cameraOffset.setY(position.getY());
	}
	/**
	 * Update this WorldCamera. 
	 * IMPTORTANT this method does not update the WorldView this camera projects.
	 */
	public void update(float dTime){
		camera.update();
		camera.apply(Gdx.gl10);
		this.camera.position.x = cameraOffset.getX();
		this.camera.position.z = cameraOffset.getY() + CAMERA_DEAPTH * zoom;
		this.camera.position.y = CAMERA_HEIGHT * zoom;
		if(lockedGameObject != null){
			move(lockedGameObject.getPosition().sub(cameraOffset).mul(1/10f));
		}else{
			move(cameraSpeed.cpy().mul(dTime));
		}
	}
	/**
	 * Get the "screenPosition" coordinate on the screen as a vector2 in the 3d world space on the ground plane.
	 * @param screenPos The position on the screen.
	 * @return The position on the ground plane as a Vector2.
	 */
	public IVector2 getWorldPos(IVector2 screenPos){
		Vector3 pos = new Vector3();
		Ray pickRay = camera.getPickRay(screenPos.getX(), screenPos.getY());
		Intersector.intersectRayPlane(pickRay, new Plane(new Vector3(0, 1, 0), 0), pos);
		return new Vector2(pos.x, pos.z);
	}
	@Override
	public IVector2 getScreenPosition(Vector3 worldPos) {
		worldPos = new Vector3(worldPos.x, worldPos.z, worldPos.y);
		camera.project(worldPos);
		IVector2 pos = new Vector2(worldPos.x,worldPos.y);
		return pos;
	}
	@Override
	public void setZoom(float zoom) {
		this.zoom = zoom;
		
	}
	@Override
	public float getZoom() {
		return zoom;
	}
	@Override
	public void lockAtGameObject(IGameObject gameObject) {
		lockedGameObject = gameObject;
		
	}
	@Override
	public void unlockAtGameObject() {
		lockedGameObject = null;
	}
	@Override
	public boolean isLockedAtGameObject() {
		return lockedGameObject != null;
	}
	@Override
	public Camera getCamera() {
		return camera;
	}
	public IVector2 getPosition(){
		return cameraOffset.cpy();
	}
}
