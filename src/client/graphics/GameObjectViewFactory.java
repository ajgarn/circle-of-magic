package client.graphics;

import model.ICharacter;
import model.IMage;
import model.core.IGameObject;
import model.core.State;
import model.spells.ISpellObject;
import services.graphics.core.FrameInterval;
import services.graphics.models.Model;
import services.graphics.models.ModelAnimation;
import services.graphics.particles.ParticleSystemFactory;
import services.graphics.particles.StateParticleSystem;
import services.graphics.renderables.AttachableRenderable;
import services.graphics.renderables.AttachableRenderable3D;
import services.graphics.renderables.IRenderable;
import services.graphics.util.ContentManager;
import services.math.IVector2;
import client.gui.screens.components.HealthBar;
import client.gui.screens.components.RenderableTable;

import com.badlogic.gdx.math.Vector3;

/**
 * A class used to create GameObjectViews
 * 
 * @author Robin Gronberg
 * 
 */
public class GameObjectViewFactory {

	/**
	 * Create a GameObjectView that is suitable for the specified gameObject
	 * 
	 * @param gameObject
	 * @return The GameObjectView
	 */
	@SuppressWarnings("incomplete-switch")
	public static GameObjectView create(IGameObject gameObject,IWorldCamera camera) {
		if (gameObject instanceof IMage) {
			return createMage(gameObject,camera);
		} else if (gameObject instanceof ISpellObject) {
			ISpellObject o = (ISpellObject) gameObject;
			switch (o.getSpellName()) {
			case FIREBALL:
				return createFireBall(gameObject);
			case FIRESPRAY:
				return createFireSpray(gameObject);
			case FROSTBOLT:
				return createFrostbolt(gameObject);
			case BOOMERANG:
				return createBoomerang(gameObject);
			case ENFLAME:
				return createEnflame(gameObject);
			case BOUNCING:
				return createBouncing(gameObject);
			case LIGHTNINGBOLT:
				return createLightningBolt(gameObject);
			}
		}
		throw new IllegalArgumentException(
				"cannot create a GameObjectView of that type of gameObject");
	}

	private static GameObjectView createBoomerang(IGameObject gameObject) {
		ContentManager content = ContentManager.getInstance();
		GameObjectView w = new GameObjectView(gameObject);
		Model model = new Model(content.getModel("spells/boomerang.md2"),
				content.getTexture("spells/boomerang.png"), true, true, true,
				true, 0, new Vector3(gameObject.getPosition().getX(),
						gameObject.getPosition().getY(), 2));
		w.attach(model);
		model.setScale(0.7f, 1, 0.7f);
		ModelAnimation animation = new ModelAnimation(new FrameInterval(1, 29),
				new FrameInterval[] { new FrameInterval(1, 28) }, 0.5f,
				"standing", false);
		model.addAnimation("standing", animation);
		model.playAnimation("standing", true);
		return w;
	}

	private static GameObjectView createFrostbolt(IGameObject gameObject) {
		GameObjectView w = new GameObjectView(gameObject);
		w.attach(new StateParticleSystem(State.RUNNING.toString(),
				ParticleSystemFactory.createFrostbolt(2f, 10)));
		return w;
	}

	private static GameObjectView createLightningBolt(IGameObject gameObject) {
		GameObjectView w = new GameObjectView(gameObject);
		w.attach(new StateParticleSystem(State.RUNNING.toString(),
				ParticleSystemFactory.createLightning(2f, 10)));
		return w;
	}

	private static GameObjectView createBouncing(IGameObject gameObject) {
		GameObjectView w = new GameObjectView(gameObject);
		w.attach(new StateParticleSystem(State.RUNNING.toString(),
				ParticleSystemFactory.createBouncing(1.5f, 20)));
		return w;
	}

	private static GameObjectView createFireSpray(IGameObject gameObject) {
		GameObjectView w = new GameObjectView(gameObject);
		w.attach(new StateParticleSystem(State.RUNNING.toString(),
				ParticleSystemFactory.createFire(2.5f, 2)));
		return w;
	}

	private static GameObjectView createEnflame(IGameObject gameObject) {
		GameObjectView w = new GameObjectView(gameObject);
		w.attach(ParticleSystemFactory.createEnflameExplostion(3));
		return w;
	}

	private static GameObjectView createMage(IGameObject gameObject,IWorldCamera camera) {
		ContentManager content = ContentManager.getInstance();
		IMage mage = (IMage) gameObject;
		Model model = new Model(content.getModel("mage/model.md2"),
				content.getTexture("mage/texture" + mage.getColor() + ".png"),
				true, true, true, false, gameObject.getDirection(),
				new Vector3(gameObject.getPosition().getX(), gameObject
						.getPosition().getY(), 0));
		// Add animations
		ModelAnimation animation = new ModelAnimation(new FrameInterval(1, 30),
				new FrameInterval[] { new FrameInterval(1, 30) }, 1,
				"standing", false);
		model.addAnimation("standing", animation);
		animation = new ModelAnimation(new FrameInterval(31, 40),
				new FrameInterval[] { new FrameInterval(1, 30) }, 1, "running",
				true);
		model.addAnimation("startRunning", animation);
		animation = new ModelAnimation(new FrameInterval(41, 95),
				new FrameInterval[] { new FrameInterval(40, 41) }, 1,
				"running", false);
		model.addAnimation("running", animation);

		animation = new ModelAnimation(new FrameInterval(96, 105),
				new FrameInterval[] { new FrameInterval(80, 81) }, 1,
				"standing", true);
		model.addAnimation("stopRunning0", animation);
		animation = new ModelAnimation(new FrameInterval(106, 115),
				new FrameInterval[] { new FrameInterval(60, 61) }, 1,
				"standing", true);
		model.addAnimation("stopRunning1", animation);
		animation = new ModelAnimation(new FrameInterval(116, 125),
				new FrameInterval[] { new FrameInterval(40, 41) }, 1,
				"standing", true);
		model.addAnimation("stopRunning2", animation);

		animation = new ModelAnimation(new FrameInterval(126, 160),
				new FrameInterval[] { new FrameInterval(1, 95) }, 1,
				"standing", false);
		model.addAnimation("castTargetSpell0", animation);
		animation = new ModelAnimation(new FrameInterval(161, 195),
				new FrameInterval[] { new FrameInterval(1, 95) }, 1,
				"standing", false);
		model.addAnimation("castTargetSpell1", animation);

		animation = new ModelAnimation(new FrameInterval(196, 220),
				new FrameInterval[] { new FrameInterval(1, 30) }, 1.5f,
				"standing", false);
		model.addAnimation("castNoTargetSpell", animation);
		animation = new ModelAnimation(new FrameInterval(221, 260),
				new FrameInterval[] { new FrameInterval(1, 30) }, 1.2f,
				"standing", false);
		model.addAnimation("castAOESpell", animation);
//		model.playAnimation("castTargetSpell0", true);

		RenderableTable table = new RenderableTable(camera,new Vector3(0, 0, 4));
		HealthBar healthBar = new HealthBar(0.005f);
		healthBar.setCharacter((ICharacter)gameObject);
		table.add(healthBar).width(100).height(15);
		
		GameObjectView mageView = new GameObjectView(gameObject);
		mageView.attach(model);
		mageView.attach(table);
		return mageView;
	}

	/**
	 * Create a new screen cursor in the world
	 * 
	 * @param position
	 *            The position in the world this moveCursor is located
	 * @return The GameObjectView with the specific cursor.
	 */
	public static IRenderable createMoveCursor(IVector2 position) {
		ContentManager content = ContentManager.getInstance();
		AttachableRenderable gw = new AttachableRenderable3D();
		for (int i = 0; i < 4; i++) {
			Model m = new Model(content.getModel("arrow/arrow" + i + ".md2"),
					null, false, false, true, true, 0, new Vector3(
							position.getX(), position.getY(), 0));
			m.setColor(0, 1, 0, 1);
			m.setScale(0.7f, 0.7f, 0.7f);
			ModelAnimation a = new ModelAnimation(new FrameInterval(1, 24),
					new FrameInterval[] { new FrameInterval(1, 24) }, 1, "",
					true);
			m.addAnimation("standing", a);
			m.playAnimation("standing", true);
			gw.attach(m);
		}
		return gw;
	}

	private static GameObjectView createFireBall(IGameObject gameObject) {
		GameObjectView view = new GameObjectView(gameObject);
		view.attach(new StateParticleSystem(State.RUNNING.toString(),
				ParticleSystemFactory.createFire(2, 3)));
		view.attach(new StateParticleSystem(State.DYING.toString(),
				ParticleSystemFactory.createExplostion(2)));
		return view;
	}
}
