package client.graphics;

import model.core.IGameObject;
import services.graphics.renderables.IRenderable3D;
/**
 * An interface that represents GameObject as
 * a graphical representation.
 * @author Robin Gronberg
 *
 */
public interface IGameObjectView extends IRenderable3D{
	/**
	 * Kills this gameObjectView as well as its
	 * attached renderables.
	 */
	public void kill();
	/**
	 * Returns true if completely dead.
	 * It's completely dead if all renderables are dead
	 * as well as the gameObject this View represents.
	 * @return
	 */
	public boolean isDead();
	/**
	 * Update this view as well as all the Renderables
	 * @param dTime
	 */
	public void update(float dTime);
	/**
	 * Get the gameObject this GameObjectView is 
	 * basing its data on
	 * @return
	 */
	public IGameObject getGameObject();
}
