package client.graphics.map;

import services.graphics.util.MeshFactory;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureWrap;

/**
 * A ground texture is a texture that should be rendered 
 * along the ground. 
 * @author Robin Gronberg
 *
 */
public class GroundTexture {
	private final Texture mask,texture;
	private final int xRepeat,yRepeat;
	private Mesh mesh, maskMesh;
	private final boolean outsideCircle;
	private float OUTSIDE_CIRCLE_HEIGHT = -3;
	/**
	 * Create a new GroundTexture
	 * @param mask The mask to use when rendering the texture.
	 * total while will apply 100% of the pixel to the framebuffer.
	 * total black will apply 0% of the pixel to the framebuffer
	 * @param texture The texture pattern to use when rendering.
	 * This texture is repeated 
	 * @param repeat How many times the texture should repeat itself
	 */
	public GroundTexture(Texture mask,Texture texture,int repeat,boolean outsideCircle){
		this.mask = mask;
		this.texture = texture;
		this.xRepeat = repeat;
		this.yRepeat = repeat;
		this.outsideCircle = outsideCircle;
		texture.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
		createMesh(1, 1);
	}
	public boolean isOutsideCircle(){
		return outsideCircle;
	}
	/**
	 * Create a new groundtexture without alpha mask
	 * @param texture The texture pattern to use when rendering.
	 * This texture is repeated 
	 * @param repeat How many times the texture should repeat itself
	 */
	public GroundTexture(Texture texture,int repeat,boolean outsideCircle){
		this(null,texture,repeat,outsideCircle);
	}
	public void createMesh(float width, float height){
		mesh = MeshFactory.createRectangle(width, height, 0, 0, xRepeat, yRepeat);
		maskMesh = MeshFactory.createRectangle(width, height, 0, 0, 1, 1);
	}
	public void render(boolean opaque) {
		if(opaque){
			if(outsideCircle){
				Gdx.gl11.glPushMatrix();
				Gdx.gl11.glEnable(GL10.GL_DEPTH_TEST);
				Gdx.gl11.glTranslatef(0, OUTSIDE_CIRCLE_HEIGHT, 0);
				Gdx.gl11.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
				texture.bind();
				mesh.render(GL10.GL_TRIANGLES);
				Gdx.gl11.glDisable(GL10.GL_DEPTH_TEST);
				Gdx.gl11.glPopMatrix();
			}else{	
				Gdx.gl10.glEnable(GL10.GL_BLEND);
				if(mask == null){
					Gdx.gl11.glBlendFunc(GL10.GL_DST_ALPHA, GL10.GL_ONE_MINUS_DST_ALPHA);
					texture.bind();
					mesh.render(GL10.GL_TRIANGLES);
				}else{
					// Next, we want a blendfunc that doesn't change the color of any pixels,
					// but rather replaces the framebuffer alpha values with values based
					// on the whiteness of the mask. In other words, if a pixel is white in the mask,
					// then the corresponding framebuffer pixel's alpha will be set to 1.
					//Gdx.gl11.glBlendFuncSeparate(GL10.GL_ZERO, GL10.GL_ONE, GL10.GL_SRC_COLOR, GL10.GL_ZERO);
					Gdx.gl11.glBlendFunc(GL10.GL_DST_COLOR, GL10.GL_ZERO);
					mask.bind();
					maskMesh.render(GL10.GL_TRIANGLES);
					
					Gdx.gl11.glBlendFunc(GL10.GL_DST_ALPHA, GL10.GL_ONE_MINUS_DST_ALPHA);
					texture.bind();
					mesh.render(GL10.GL_TRIANGLES);
				}
			}			
		}
	}

	public void update(float dTime) {}

}
