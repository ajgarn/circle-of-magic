package client.graphics.map;

import java.util.ArrayList;
import java.util.List;

import services.graphics.models.Model;
import services.graphics.particles.CircleEmitter;
import services.graphics.particles.ParticleSystem;
import services.graphics.util.MeshFactory;
import services.math.StaticFunction;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Mesh;

public class Map implements IMap {
	private Mesh meshCircle;
	private final GroundTexture[] groundTextures;
	private float radius = 30,width,height;
	private Model edgeModel;
	public float getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}
	private List<ParticleSystem> particleSystems;

	public Map(float width,float height,GroundTexture[] groundTextures,float radius,Model edgeModel) {
		this.particleSystems = new ArrayList<ParticleSystem>();
		this.groundTextures = groundTextures;
		generateMesh();
		this.radius = radius;
		for(GroundTexture g: groundTextures){
			if(g.isOutsideCircle()){
				g.createMesh(width, height);
			}else{
				g.createMesh(radius, radius);
			}
		}
		this.edgeModel = edgeModel;
		this.width = width;
		this.height = height;
	}
	
	public void attachParticleSystem(ParticleSystem p){
		particleSystems.add(p);
	}
	
	@Override
	public void update(float dTime) {
		//radius -=0.004f * dTime;
		edgeModel.setScale(radius/1.04f, 1, radius/1.04f);
		for(ParticleSystem p: particleSystems){
			p.update(dTime);
		}
		for(GroundTexture g: groundTextures){
			g.update(dTime);
		}
	}
	public void render(boolean opaque) {
		Gdx.gl10.glPushMatrix();
		Gdx.gl10.glDepthMask(true);
		Gdx.gl10.glTranslatef(0, -.3f, 0);
		Gdx.gl10.glColor4f(1, 1, 1, 1);
		Gdx.gl10.glScalef(radius*2, 0, radius*2);
		Gdx.gl10.glDisable(GL10.GL_TEXTURE_2D);
		Gdx.gl10.glDisable(GL10.GL_LIGHTING);
		meshCircle.render(GL10.GL_TRIANGLES);
		Gdx.gl10.glDisable(GL10.GL_DEPTH_TEST);
		Gdx.gl10.glDepthMask(false);
		Gdx.gl10.glEnable(GL10.GL_BLEND);
		Gdx.gl10.glEnable(GL10.GL_TEXTURE_2D);
		Gdx.gl10.glPopMatrix();			
		
		for (GroundTexture g : groundTextures) {
			g.render(true);
		}
		Gdx.gl10.glDisable(GL10.GL_LIGHTING);
		Gdx.gl10.glEnable(GL10.GL_DEPTH_TEST);
		Gdx.gl11.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		Gdx.gl10.glDepthMask(true);
		for(ParticleSystem p: particleSystems){
			p.render(false);
		}
		Gdx.gl10.glPushMatrix();
		Gdx.gl10.glTranslatef(0, -.3f, 0);
		edgeModel.render(true);
		Gdx.gl10.glPopMatrix();
		
		Gdx.gl10.glDisable(GL10.GL_BLEND);
	}

	private void generateMesh() {
		meshCircle = MeshFactory.createCircle(1, 100);
		
	}
	@Override      
	public void setRadius(float r){
		radius = r;
		for(ParticleSystem p: particleSystems){
			((CircleEmitter)p.getEmitter()).setRadius(r);
			p.getEmitter().setIntensity(new StaticFunction(radius / 20));        
		}
	}


	
}
