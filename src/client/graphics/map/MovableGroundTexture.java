package client.graphics.map;

import model.core.Constants;
import services.math.IFunction;
import services.math.SinFunction;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

public class MovableGroundTexture extends GroundTexture {
	private final float speed;
	private final IFunction function;

	private float deltaX, time;
	
	public MovableGroundTexture(Texture texture, int repeat,
			boolean outsideCircle) {
		this(texture, repeat, outsideCircle,1,new SinFunction(1, 1));
	}
	public MovableGroundTexture(Texture texture, int repeat,
			boolean outsideCircle,float speed,IFunction function) {
		super(texture, repeat, outsideCircle);
		this.function = function;
		this.speed = speed;
	}
	@Override
	public void update(float dTime) {
		super.update(dTime);
		time += speed * dTime / Constants.FPS;
		time%=1;
		deltaX = function.getValue(time);
	}
	@Override
	public void render(boolean opaque) {
		Gdx.gl10.glPushMatrix();
		Gdx.gl10.glTranslatef(deltaX, 0, 0);
		super.render(opaque);
		Gdx.gl10.glPopMatrix();
	}
}
