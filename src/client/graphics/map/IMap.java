package client.graphics.map;

import services.graphics.particles.ParticleSystem;

public interface IMap{
	/**
	 * ParticleEffect to this map
	 * @param p
	 */
	public void attachParticleSystem(ParticleSystem p);
	/**
	 * Sets the raius of the map
	 * @param r The new radius
	 */
	public void setRadius(float r);
	
	public float getWidth();
	public float getHeight();
	public void update(float dTime);
	public void render(boolean opaque);
	
}
