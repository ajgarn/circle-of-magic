package client.graphics;


import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import model.World;
import model.core.IGameObject;
import model.core.Vector2;
import services.graphics.renderables.IRenderable;
import services.graphics.renderer.IRenderer;
import services.graphics.renderer.Renderer2D;
import services.graphics.renderer.Renderer3D;
import services.graphics.renderer.RendererAttacher;
import services.math.IVector2;
import client.graphics.map.IMap;

/**
 * A world view is used to render a world properly.
 * @author Robin Gronberg
 *
 */
public class WorldView implements IRenderer{
	
	private RendererAttacher renderer;
	
	private boolean refreshWorld = false;
	private World world;
	private IWorldCamera camera;
	private List<GameObjectView> gameObjectViews;
	private IMap map;

	/**
	 * Create an empty WorldView with no world.
	 * And a worldcamera
	 * @param camera The camera to use
	 */
	public WorldView(IWorldCamera camera){
		gameObjectViews = new CopyOnWriteArrayList<GameObjectView>();
		this.map = MapFactory.create();
		this.camera = camera;
		this.renderer = new RendererAttacher();
		renderer.attach(new Renderer3D());		
		renderer.attach(new Renderer2D());		
	}
	/**
	 * Create a new WorldView with a world to render
	 * @param world The world this WorldView should render
	 * @param camera The worldCamera this world should use when rendering
	 */
	public WorldView(World world,IWorldCamera camera){
		this(camera);
		worldReceived(world);
	}

	/**
	 * Get the gameObjectView that has a gameObject with a certain
	 * ID
	 * @param gameObject The GameObject the searched GameObjectView
	 * shoudl have as GameObject
	 * @return The GameObjectView that has that gameObject
	 * as gameObject, null if no GameObjectView is found
	 */
	private GameObjectView getGameObjectView(IGameObject gameObject) {
		for(GameObjectView g: gameObjectViews){
			if(g.getGameObject().equals(gameObject)){
				return g;
			}
		}
		return null;
	}
	/**
	 * Refreshes the world with a new one. All gameObjectViews in this worldView is
	 * relinked to its GameObject in the specified world. It removes all the GameObjectViews
	 * that doesn't have a GameObject in the refreshed world anymore. It also creates 
	 * a GameObjectView for each GameObject that didn't exist in the old world.
	 * @param world
	 */
	private void refreshWorld(World world){
		refreshWorld = false;
		//loops through the world game objects and refreshes the views
		for(IGameObject gameObject: world.getGameObjects()){
			GameObjectView renderable = getGameObjectView(gameObject);
			//if object didn't exist before
			if(renderable == null){
				renderable = GameObjectViewFactory.create(gameObject,camera);
				addRenderableObject(renderable);
			}
		}
		//loop through the view and delete the view where the object is gone
		for(GameObjectView g: gameObjectViews){
			//the objects model was removed on the server, remove the view as well         
			if(!world.getGameObjects().contains(g.getGameObject())){
				g.kill();
			}
		}
	}
	/**
	 * Renders all the gameObjectViews
	 */
	public void render(){
		map.setRadius(world.getRadius());
		map.render(true);
		renderer.render();
	}
	/**
	 * Updates all the gameObjectViews. If a new world was sent from the network since
	 * the last update, refresh the world
	 * @param dTime
	 */
	public void update(float dTime){
		renderer.update(dTime);
		map.update(dTime);
		handleCameraBounds();
		if(refreshWorld){
			refreshWorld(world);
		}
		
	}
	/**
	 * Fixes so the WorldCamera is inside the map bounds.
	 * If not, it's placed as far of as it can.
	 */
	private void handleCameraBounds(){
		IVector2 camPos = camera.getPosition();
		float x = camPos.getX() > map.getWidth()/2? map.getWidth()/2:camPos.getX();
			   x = camPos.getX() < -map.getWidth()/2? -map.getWidth()/2:x;
		float y = camPos.getY() > map.getHeight()/2? map.getHeight()/2:camPos.getY();
			   y = camPos.getY() < -map.getHeight()/2? -map.getHeight()/2:y;
		camera.lookAt(new Vector2(x,y));
	}
	/**
	 * When a world is received from the network. Get that world and refresh next update.
	 */
	public void worldReceived(World world) {
		clearRenderableObjects();
		this.world = world;
		this.refreshWorld = true;
	}
	public void changedGameObjectsReceived(Map<Integer, ? extends IGameObject> map2,
			long timeSent) {
		refreshWorld = true;
	}
	public void resize(int width, int height) {
		camera.setAspectRatio((float)width/(float)height);
		renderer.resize(width, height);
	}
	@Override
	public void removeRenderableObject(IRenderable renderable) {
		renderer.removeRenderableObject(renderable);
		if(renderable instanceof GameObjectView){
			gameObjectViews.remove((GameObjectView)renderable);
		}
	}
	@Override
	public void addRenderableObject(IRenderable renderable) {
		renderer.addRenderableObject(renderable);
		if(renderable instanceof GameObjectView){
			gameObjectViews.add((GameObjectView)renderable);
		}
	}
	@Override
	public Collection<IRenderable> getRenderableObjects() {
		return renderer.getRenderableObjects();
	}
	@Override
	public void clearRenderableObjects() {
		renderer.clearRenderableObjects();
	}
}
