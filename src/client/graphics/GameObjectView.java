package client.graphics;

import java.util.HashMap;
import java.util.Map;

import model.ICharacter;
import model.core.GameObjectListener;
import model.core.IAction;
import model.core.IGameObject;
import model.core.SpellEffect;
import model.core.SpellNames;
import model.core.State;
import model.spells.TeleportAction;
import services.graphics.core.Attachable;
import services.graphics.models.Model;
import services.graphics.particles.StateParticleSystem;
import services.graphics.renderables.AttachableRenderable;
import services.graphics.renderables.IRenderable;
import services.graphics.renderables.IRenderable2D;
import services.graphics.renderables.IRenderable3D;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;



/**
 * This class is used to render a gameObject in the world
 * @author Robin Gronberg
 *
 */
class GameObjectView extends AttachableRenderable<IRenderable> implements IGameObjectView, GameObjectListener,Attachable<IRenderable>,IRenderable2D,IRenderable3D{
	
	private AttachableRenderable<IRenderable3D> renderable3Dattachor;
	private AttachableRenderable<IRenderable2D> renderable2Dattachor;
	private AttachableRenderable<Model> models;
	private AttachableRenderable<StateParticleSystem> particleSystems;
	
	private IGameObject gameObject;
	private Map<SpellNames, IRenderable3D> spellEffectRenderables;
	/**
	 * Create a new GameObjectView with a gameObject and a representative 3d model.
	 * @param obj The data to be used for rendering
	 */
	public GameObjectView(IGameObject obj){
		setGameObject(obj);
		models = new AttachableRenderable<Model>();
		particleSystems = new AttachableRenderable<StateParticleSystem>();
		renderable3Dattachor = new AttachableRenderable<IRenderable3D>();
		renderable2Dattachor = new AttachableRenderable<IRenderable2D>();
		spellEffectRenderables = new HashMap<SpellNames, IRenderable3D>();
	}
	@Override
	public void attach(IRenderable r) {
		super.attach(r);
		r.setPosition(gameObject.getPosition());
		if(r instanceof IRenderable2D){
			renderable2Dattachor.attach((IRenderable2D)r);
		}
		if(r instanceof IRenderable3D){
			renderable3Dattachor.attach((IRenderable3D)r);
		}
		if(r instanceof Model){
			models.attach((Model)r);
		}
		if(r instanceof StateParticleSystem){
			particleSystems.attach((StateParticleSystem)r);
		}
	}
	@Override
	public void detach(IRenderable r) {
		super.detach(r);
		if(r instanceof IRenderable2D){
			renderable2Dattachor.detach((IRenderable2D)r);
		}
		if(r instanceof IRenderable3D){
			renderable3Dattachor.detach((IRenderable3D)r);
		}
		if(r instanceof Model){
			models.detach((Model)r);
		}
		if(r instanceof StateParticleSystem){
			particleSystems.detach((StateParticleSystem)r);
		}
	}
	/**
	 * Get the gameObject this model is using
	 * @return
	 */
	public IGameObject getGameObject(){
		return gameObject;
	}
	/**
	 * refresh this objects data model
	 * @param gameObj
	 */
	public void setGameObject(IGameObject gameObj){
		gameObject = gameObj;
		gameObj.addListener(this);
	}
	/**
	 * Updates the 3d models animation and change the animation if 
	 * the data model has changed its state. If the gameObject this
	 * view is based on is dead, kill this object as well.
	 * @param dTime
	 */
	public void update(float dTime){
		for(StateParticleSystem p: particleSystems.get()){
			if(!p.getState().equals(getGameObject().getState().toString())){
				p.getEmitter().pause();
			}else{
				p.getEmitter().resume();
			}
		}
		for(IRenderable r: get()){
			r.movePosition(gameObject.getPosition(),0.3f);
			r.setAngle(gameObject.getDirection());
		}
		super.update(dTime);
	}

	/**
	 * Check if this view is completely dead.
	 * @return true if gameObject this view renders is dead
	 * as well as all the attached ParticleSystems
	 */
	public boolean isDead(){
		return gameObject.isDead() && super.isDead();
	}
	@Override
	public void effectAdded(IGameObject source, SpellEffect effect) {
		if(spellEffectRenderables.containsKey(effect.getSpellName())){
			detach(spellEffectRenderables.get(effect.getSpellName()));
//			System.out.println("detached");
		}
		IRenderable3D spellEffectView = SpellEffectViewFactory.create(effect);
		if(spellEffectView != null){
			spellEffectRenderables.put(effect.getSpellName(), spellEffectView);
			attach(spellEffectView);
//			System.out.println("Added" + effect.getSpellName());
		}
	}
	@Override
	public void effectRemoved(IGameObject source, SpellEffect effect) {
		IRenderable spellEffectView = spellEffectRenderables.get(effect.getSpellName());
		//remove is it exists and is the last effect of that spell.
		if(spellEffectView != null && 
				!((ICharacter)gameObject).isAffectedBySpell(effect.getSpellName())){
			spellEffectView.kill();
			spellEffectRenderables.remove(effect.getSpellName());
			System.out.println("Removed" + effect.getSpellName());
		}
	}
	@Override
	public void actionAdded(IGameObject source, IAction action) {
		if(action instanceof TeleportAction){
			for(IRenderable r: get()){
				r.setPosition(((TeleportAction) action).getTarget());
			}
		}
	}
	@Override
	public void actionRemoved(IGameObject source, IAction action) {
		// TODO Auto-generated method stub
		
	} 
	@Override
	public void stateChanged(IGameObject source, State oldState, State newState) {
		//Change animation on every model
		String newAnimation = newState.toString();
		for(Model model: models.get()){
			if(!newAnimation.equals(model.getCurrentAnimationName())){
				model.playAnimation(newAnimation,false);
			}
		}
	}
	@Override
	public void killed(IGameObject killer, IGameObject killed) {
		//Kill this view if dataModel is dead
	}
	@Override
	public void damaged(IGameObject damager, IGameObject damaged, int damage) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void died(IGameObject source) {
		kill(); 
	}
	@Override
	public void render(boolean opaque) {
		for(IRenderable3D r : renderable3Dattachor.get()){
			r.render(opaque);
		}
	}
	@Override
	public void render(SpriteBatch batch) {
		for(IRenderable2D r : renderable2Dattachor.get()){
			r.render(batch);
		}
		
	}
}
