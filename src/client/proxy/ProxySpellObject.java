package client.proxy;

import model.core.IGameObject;
import model.core.SpellNames;
import model.spells.ISpellObject;
import model.spells.SpellObject;

public class ProxySpellObject extends ProxyGameObject implements ISpellObject{
	private SpellObject spellObject;
	public ProxySpellObject(SpellObject source){
		super(source);
		this.spellObject = source;
	}
	@Override
	public SpellNames getSpellName() {
		return spellObject.getSpellName();
	}

	@Override
	public IGameObject getCaster() {
		return spellObject.getCaster();
	}
	@Override
	public void refresh(IGameObject o) {
		// TODO Auto-generated method stub
		super.refresh(o);
	}
}
