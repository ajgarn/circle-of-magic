package client.proxy;

import model.IMage;
import model.Mage;
import model.core.ColorNames;
import model.core.IGameObject;

public class ProxyMage extends ProxyCharacter implements IMage {
	private Mage mage;
	public ProxyMage(Mage mage){
		super(mage);
		this.mage = mage;
	}
	@Override
	public ColorNames getColor() {
		return mage.getColor();
	}
	@Override
	public void refresh(IGameObject o){
		super.refresh(o);
	}

}
