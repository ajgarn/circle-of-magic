package client.proxy;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import model.Character;
import model.ICharacter;
import model.core.IGameObject;
import model.core.SpellEffect;
import model.core.SpellNames;

public class ProxyCharacter extends ProxyGameObject implements ICharacter {
	private Character character;
	private List<SpellEffect> spellEffectsAdded;
	private List<SpellEffect> spellEffectsRemoved;
	public ProxyCharacter(){}
	public ProxyCharacter(Character character){
		super(character);
		this.character = character;
		spellEffectsAdded = new CopyOnWriteArrayList<SpellEffect>();
		spellEffectsRemoved = new CopyOnWriteArrayList<SpellEffect>();
	}
	@Override
	public int getHealth() {
		return character.getHealth();
	}

	@Override
	public List<SpellEffect> getSpellEffects() {
		return character.getSpellEffects();
	}

	@Override
	public int getMaxHealth() {
		return character.getMaxHealth();
	}
	@Override
	public void update(float time) {
		for(SpellEffect spell : spellEffectsAdded){
			character.spellEffectAdded(this,spell);
		}
		spellEffectsAdded.clear();
		for(SpellEffect spell : spellEffectsRemoved){
			character.spellEffectRemoved(this,spell);
		}
		spellEffectsRemoved.clear();
		super.update(time);
	}
	@Override
	public void refresh(IGameObject o) {
		List<SpellEffect> newSpellEffects = ((ICharacter)o).getSpellEffects();
		List<SpellEffect> oldSpellEffects = getSpellEffects();
		//Check removed spellEffects
		for(SpellEffect spell: oldSpellEffects){
			if(!newSpellEffects.contains(spell)){
				spellEffectsRemoved.add(spell);
			}
		}
		//Check added spellEffects
		for(SpellEffect spell: newSpellEffects){
			if(!oldSpellEffects.contains(spell)){
				spellEffectsAdded.add(spell);
			}
		}
		super.refresh(o);
	}
	@Override
	public boolean isAffectedBySpell(SpellNames spellName) {
		return character.isAffectedBySpell(spellName);
	}
}
