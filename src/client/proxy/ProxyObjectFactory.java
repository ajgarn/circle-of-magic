package client.proxy;

import model.Character;
import model.Mage;
import model.World;
import model.core.GameObject;
import model.core.IGameObject;
import model.spells.SpellObject;

/**
 * Used to determine the proxy object to create on the client.
 * @author Olliver
 * @author Anton Jansson
 */
public class ProxyObjectFactory {
	/**
	 * Creates the correct ProxyGameObject of the passed GameObject.
	 * All reference pointers that may be wrong is updated to objects
	 * in the world.
	 * @param go The GameObject to create a proxy of.
	 * @param world The world to get reference pointers from.
	 * @return The created ProxyGameObject.
	 */
	public static ProxyGameObject create(IGameObject go, World world){
		if (go instanceof ProxyGameObject) {
			return (ProxyGameObject) go;
		} else {
			GameObject temp = (GameObject)go;
			if (temp instanceof Mage) {
				return new ProxyMage((Mage) temp);
			} else if (temp instanceof Character) {
				return new ProxyCharacter((Character) temp);
			} else if (temp instanceof SpellObject) {
				// Set the correct caster reference for spell objects.
				SpellObject spellObj = (SpellObject) temp;
				int casterId = world.getGameObjectIndex(ProxyObjectFactory.create(
						spellObj.getCaster(), world));
				spellObj.setCaster(world.getGameObject(casterId));
				return new ProxySpellObject(spellObj);
			} else {
				return new ProxyGameObject(temp);
			}
		}
	}
}
