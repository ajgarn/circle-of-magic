package client.proxy;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import services.math.IVector2;

import model.core.GameObject;
import model.core.GameObjectListener;
import model.core.IAction;
import model.core.IGameObject;
import model.core.State;

public class ProxyGameObject implements IGameObject {
	private GameObject gameObject;
	private List<IAction> added = new CopyOnWriteArrayList<IAction>();
	private List<IAction> removed = new CopyOnWriteArrayList<IAction>();

	protected ProxyGameObject() {
		// TODO Auto-generated constructor stub
	}

	public ProxyGameObject(GameObject gameObject) {
		this.gameObject = gameObject;
	}

	@Override
	public float getRotationSpeed() {
		return gameObject.getRotationSpeed();
	}

	@Override
	public IVector2 getPosition() {
		return gameObject.getPosition();
	}

	@Override
	public float getDirection() {
		return gameObject.getDirection();
	}

	@Override
	public State getState() {
		return gameObject.getState();
	}

	@Override
	public List<IAction> getActions() {
		return gameObject.getActions();
	}

	@Override
	public float getSpeed() {
		return gameObject.getSpeed();
	}

	@Override
	public void update(float time) {
		gameObject.update(time);
		for (IAction a : added) {
			gameObject.actionAdded(this, a);
		}
		for (IAction a : removed) {
			gameObject.actionRemoved(this, a);
		}
		added.clear();
		removed.clear();
	}

	@Override
	public boolean isDead() {
		return gameObject.isDead();
	}

	@Override
	public void refresh(IGameObject o) {
		for (IAction a : o.getActions()) {
			if (!getActions().contains(a)) {
				added.add(a);
			}
		}
		for (IAction a : gameObject.getActions()) {
			if (!o.getActions().contains(a)) {
				removed.add(a);
			}
		}
		gameObject.refresh(o);
	}

	@Override
	public void addListener(GameObjectListener l) {
		gameObject.addListener(l);
	}

	@Override
	public void removeListener(GameObjectListener l) {
		gameObject.removeListener(l);
	}

	public IGameObject getGameObject() {
		return gameObject;
	}
	@Override
	public boolean equals(Object obj) {
		if(obj == null || getClass() != obj.getClass()){
			return false;
		}else if(this == obj){
			return true;
		}else{
			return gameObject.equals(((ProxyGameObject)obj).gameObject);
		}
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode() + gameObject.hashCode();
	}
}
