 package client;

import client.controller.Application;

/**
 * Class launching the application.
 * 
 * @author Alexander H�renstam.
 */
public class Main {
	public static void main(String[] args) {
		new Application();
	}
}
