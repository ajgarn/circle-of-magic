package client.gui;

import java.util.HashMap;
import java.util.Map;

import model.core.Constants;
import services.graphics.util.Graphics;
import client.gui.screens.Screen;
import client.gui.screens.SplashScreenDesktop;
import client.model.ClientSettings;
import client.model.Event;
import client.sound.SoundHandler;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Pixmap;

/**
 * Main application, deciding which state to render.
 * 
 * @author Alexander H�renstam.
 *
 */
public class ApplicationFrame implements ApplicationListener{
	private final Map<ApplicationState,Screen> guiViews;
	private final Map<CursorType,Cursor> cursors;
	private LwjglApplicationConfiguration cfg;
	private LwjglApplication lwjglApp;
	private final StateHistory history;
	private ApplicationState applicationState = ApplicationState.LOGIN;
	private ApplicationState desiredApplicationState;
	private Event applicationStateChangedEvent;
	
	private static ApplicationFrame appFrame;

	private ApplicationFrame(){
		guiViews = new HashMap<ApplicationState, Screen>();
		cursors = new HashMap<CursorType, Cursor>();
		history = new StateHistory();
	}
	
	public static synchronized ApplicationFrame getInstance(){
		if(appFrame==null){
			appFrame=new ApplicationFrame();
		}
		return appFrame;
	}
	
	@Override
	public void create() {
		for(ApplicationState state:guiViews.keySet()){
			guiViews.get(state).create();
		}
		//add cursors
		cursors.put(CursorType.STANDARD, new Cursor(1, 1, new Pixmap(Gdx.files.internal("res/cursor.png"))));
		cursors.put(CursorType.CASTSPELL, new Cursor(16, 16, new Pixmap(Gdx.files.internal("res/cursorTarget.png"))));
		displayCursor(CursorType.STANDARD);
		
		changeApplicationState(ApplicationState.LOGIN);
		validate();
		//end splashscreen
		SplashScreenDesktop.getInstance().endSplash();
		//Application.getInstance().startApp();
	}

	public void displayCursor(CursorType cursor) {
		cursors.get(cursor).displayCursor();
	}

	@Override
	public void dispose() {
		for(ApplicationState state: guiViews.keySet()){
			guiViews.get(state).dispose();
		}
	}

	@Override
	public void pause() {
		guiViews.get(applicationState).pause();
	}

	@Override
	public void render() {
		if(desiredApplicationState != applicationState){
			setApplicationState(desiredApplicationState);
		}
		Graphics.clear();
		guiViews.get(applicationState).render(Gdx.graphics.getDeltaTime()*Constants.FPS);
		//applicationListeners.get(applicationState).resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	}

	@Override
	public void resize(int width, int height) {
        //original
		for(ApplicationState state: guiViews.keySet()){
			guiViews.get(state).resize(width, height);
		}
	}

	@Override
	public void resume() {
		guiViews.get(applicationState).resume();
	}
	
	public void start() {
		cfg = new LwjglApplicationConfiguration();
		cfg.title = "Circle of Magic Client";
		cfg.width = ClientSettings.getInstance().getScreenWidthGui();
		cfg.height = ClientSettings.getInstance().getScreenHeightGui();
		//TODO include icons for all operating systems. .ico not supported?
		//possible to add multiple icons manually?
		cfg.addIcon("res/icon.png",FileType.Internal);
		//cfg.addIcon("res/gui/icon_mac.hqx",FileType.Internal);
		//com.apple.eawt.Application apple = com.apple.eawt.Application.getApplication();
		//apple.setDockIconImage();
		cfg.useGL20 = false;
		lwjglApp = new LwjglApplication(this, cfg);
	}
	
	public void exit(){
		lwjglApp.exit();
		//System.exit(0);
	}
	
	public void validate(){
		ClientSettings c = ClientSettings.getInstance();
		if(applicationState == ApplicationState.GAMEPLAY){
			Gdx.graphics.setDisplayMode(c.getScreenWidthGame(),
					c.getScreenHeightGame(),c.getFullscreenGameplay());
		}else{
			Gdx.graphics.setDisplayMode(c.getScreenWidthGui(),
					c.getScreenHeightGui(),c.getFullscreenGui());
		}
	}
	
	/**
	 * Go back in the history one step, changing the application state.
	 */
	public void back() {
		back(1);
	}
	
	/**
	 * Go back in the history.
	 * @param steps The number of steps to go back.
	 */
	public void back(int steps) {
		changeApplicationState(history.back(steps));
	}

	/**
	 * Clear the history.
	 */
	public void clearHistory() {
		history.clearHistory();
	}
	
	/**
	 * Go forward in the history one step, changing the application state.
	 */
	public void forward() {
		forward(1);
	}
	
	/**
	 * Go forward in the history.
	 * @param steps The number of steps to go forward.
	 */
	public void forward(int steps) {
		changeApplicationState(history.forward(steps));
	}
	
	private void setApplicationState(ApplicationState newState){
		history.set(newState);		// Save state to history
		guiViews.get(applicationState).hide();
		if(applicationState == ApplicationState.GAMEPLAY){
			applicationState = newState;
			validate();
		}
		applicationState = newState;
		if(guiViews.get(newState) != null){
			guiViews.get(newState).show(applicationStateChangedEvent);
			Gdx.input.setInputProcessor(guiViews.get(newState).getInputProcessor());
		}
		if(newState == ApplicationState.GAMEPLAY){
			validate();
			SoundHandler.getInstance().playGameplayMusic();
		}else{
			SoundHandler.getInstance().playMainMenuMusic();
		}
	}
	/**
	 * Request this frame to change the applicationState
	 * The application state will change the next update
	 * @param appState The new application state to change to
	 */
	public void changeApplicationState(ApplicationState appState){
		changeApplicationState(appState, null);
	}
	/**
	 * Request this frame to change the applicationState
	 * The application state will change the next update
	 * @param appState The new application state to change to
	 * @param event If something should be passed to the next
	 * applicationState. This event should contain that information
	 */
	public void changeApplicationState(ApplicationState appState, Event event){
		desiredApplicationState=appState;
		applicationStateChangedEvent = event;
	}
	public void addGUIView(Screen guiView,ApplicationState applicationState){
		guiViews.put(applicationState,guiView);
	}
}
