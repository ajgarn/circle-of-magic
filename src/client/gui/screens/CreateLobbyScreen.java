/**
 * 
 */
package client.gui.screens;


import model.AbstractGameSettings;
import server.model.CoMServer;
import client.gui.screens.components.DropdownComponent;
import client.gui.screens.components.LabelComponent;
import client.gui.screens.components.ListComponent;
import client.gui.screens.components.TextButtonComponent;
import client.gui.screens.components.TextFieldComponent;
import client.model.Action;
import client.model.Event;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;


/**
 * Class defining how to render gui's "creating a lobby screen" in client.
 * @author Alexander H�renstam.
 * 
 */
public class CreateLobbyScreen extends AbstractMenuScreen {
	private Table topLeftTable;
	private TextButtonComponent hostButton;
	private TextFieldComponent gameNameField;
	private ListComponent gamemodesList;
	private LabelComponent gamenameLabel;
	private LabelComponent roundsLabel;
	private Table gamemodesTable;
	private AbstractGameSettings[] gamemodes;
	private DropdownComponent roundsDropdown;
	
	@Override
	public void create(){
		super.create();
		String[] stringArray = new String[10];
		for(int i=1; i<11; i++){
			stringArray[i-1]=""+i;
		}
		backButton = new TextButtonComponent("Back");
		topLeftTable = new Table();
		hostButton = new TextButtonComponent("Host");
		gamenameLabel = new LabelComponent("Gamename");
		gameNameField = new TextFieldComponent();
		gamemodesTable = new Table();
		gamemodes = CoMServer.getInstance().getGameSettings()
				.toArray(new AbstractGameSettings[0]);
		gamemodesList = new ListComponent(gamemodes);
		roundsDropdown = new DropdownComponent(stringArray);
		roundsLabel = new LabelComponent("Amount of rounds");
		
		gamemodesTable.setFillParent(true);
		topLeftTable.setFillParent(true);
		
		gamemodesTable.bottom().pad(100);
		gamemodesTable.debug();
		
		stage.addActor(topLeftTable);
		stage.addActor(gamemodesTable);
		gamemodesTable.add(gamemodesList);
		topLeftTable.top().left().pad(50);
		topLeftTable.add(backButton);
		centerTable.add(roundsLabel);
		centerTable.row();
		centerTable.add(roundsDropdown).width(200).height(50);
		centerTable.row();
		centerTable.add(gamemodesList);
		centerTable.row();
		centerTable.add(gamenameLabel);
		centerTable.row();
		centerTable.add(gameNameField);
		centerTable.row();
		centerTable.add(hostButton);
		
		hostButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				AbstractGameSettings mode = gamemodes[gamemodesList.getSelectedIndex()];
				mode.setRounds(Integer.parseInt(roundsDropdown.getSelection()));
				perform(new Event(Action.NEXT_CLICKED, new Object[] {gameNameField.getText(), mode}));
			}
		});
		backButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event(Action.BACK_CLICKED));
			}
		});
	}
	@Override
	public void resize(int width, int height) {
		// TODO validate components
		super.resize(width, height);
	}
}
