/**
 * 
 */
package client.gui.screens;


import model.Lobby;
import model.Player;
import services.logger.Log;
import client.gui.screens.components.ButtonComponent;
import client.gui.screens.components.LabelComponent;
import client.gui.screens.components.LobbyPlayerButtonComponent;
import client.gui.screens.components.TextButtonComponent;
import client.model.Action;
import client.model.CoMClient;
import client.model.Event;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;


/**
 * Abstract parent class responsible for defining how to render Lobby in graphical user interface.
 * @author Alexander H�renstam.
 * 
 */
public abstract class AbstractLobbyScreen extends AbstractMenuScreen {
	private LabelComponent screenNameLabel;
	private LabelComponent ownerLabel;
	private Lobby lobby;
	//move storage of lobby?
	private boolean refreshLobby;
	
	private LabelComponent settingsLabel;
	
	private Table playersTable;
	protected Table bottomRightTable;
	private Table topTable;
	private Table topLeftTable;
	private Table settingsTable;
	private Table backgroundTable;
	
	private Table settingsHolderTable;
	private Table playersHolderTable;
	
	/**
	 * Empty constructor.
	 */
	public AbstractLobbyScreen(){
	}
	@Override
	public void create(){
		super.create();
		backButton = new TextButtonComponent("Back");
		screenNameLabel=new LabelComponent("Lobby");
		ownerLabel = new LabelComponent("OWNER MISSING");
		settingsLabel = new LabelComponent("Settings");
		
		playersTable = new Table();
		topLeftTable = new Table();
		topTable = new Table();
		bottomRightTable = new Table();
		settingsTable = new Table();
		backgroundTable = new Table();
		
		settingsHolderTable = new Table();
		playersHolderTable = new Table();
				
		stage.addActor(backgroundTable);
		backgroundTable.pad(200);
		//addTable(backgroundTable);
		addTable(topLeftTable);
		addTable(topTable);
		addTable(bottomRightTable);
		
		addTable(settingsHolderTable);
		addTable(playersHolderTable);
				
		settingsHolderTable.add(settingsTable);
		playersHolderTable.add(playersTable);
		
		topTable.add(screenNameLabel);
		topTable.row();
		topTable.add(ownerLabel);
		
		topLeftTable.add(backButton);
		
		centerTable.row();
				
		settingsTable.add(settingsLabel);
		topLeftTable.top().left().pad(50);
		topTable.top();
		bottomRightTable.bottom().right().pad(50);
		
		playersHolderTable.top().pad(100);
		settingsHolderTable.top().right().pad(50);
		
		settingsTable.setBackground(tableBorderDrawable);
		playersTable.setBackground(tableBorderDrawable);
		backgroundTable.pad(100);
		//backgroundTable.setBackground(ninePatchDrawable);
		//playersTable.setBackground(ninePatchDrawable);
				
		backButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event(Action.BACK_CLICKED));
			}
		});
	}
	
	@Override
	public void render(float dTime) {
		super.render(dTime);
		if(refreshLobby){
			this.refreshLobby();
		}
	}
	
	@Override
	public void resize(int width, int height) {
		// TODO validate components
		super.resize(width, height);
	}
	/**
	 * Creates buttons for each player in Lobby every time a player has left or joined.
	 */
	public final void refreshLobbyPlayers(){
		playersTable.clear();
		for(final Player player: lobby.getPlayers()){
			ButtonComponent component;
			if(lobby.getOwner().equals(player)){
				component = new LobbyPlayerButtonComponent(player,true);
			}
			else{
				component = new LobbyPlayerButtonComponent(player);
			}
			component.addListener(new ChangeListener() {
				public void changed(ChangeEvent event, Actor actor) {
					perform(new Event(Action.REMOVE_PLAYER, player));
				}
			});
			if(CoMClient.getInstance().getPlayer().equals(player)){
				component.setColor(Color.MAGENTA);
			}
			playersTable.add(component).fillY();
		}
	}
	/**
	 * Sets text on label containing player info.
	 * @param lobby
	 */
	public final void setLobby(Lobby lobby) {
		this.lobby = lobby;
		refreshLobby = true;
		screenNameLabel.setText("Lobby - " + lobby.getHeader().getName());
		ownerLabel.setText("Owner: "
				+ (lobby.getOwner() != null ? lobby.getOwner().getName()
						: "N/A"));
	}
	/**
	 * Returns Lobby.
	 * @return
	 */
	public final Lobby getLobby(){
		return this.lobby;
	}

	/**
	 * Run each time Lobby has been updated. 
	 * Used in render() method.
	 */
	public final void refreshLobby(){
		refreshLobby=false;		// Must be called before refresh methods.
		this.refreshLobbyPlayers();
		this.refreshLobbySettings();
	}
	/**
	 * Refreshes settingsTable each time lobby has been updated.
	 */
	public final void refreshLobbySettings(){
		settingsTable.clear();
		settingsTable.add(new LabelComponent("Settings"));
		settingsTable.row();
		Image image = new Image(cm.getTexture("volcanoMap.png"));
		settingsTable.add(image).width(200).height(200).align(Align.center);
		settingsTable.row();
		settingsTable.add(new LabelComponent("Volcano Map"));
		settingsTable.row();
		settingsTable.add(new LabelComponent("Name: "+lobby.getHeader().getName())).align(Align.left);
		settingsTable.row();
		settingsTable.add(new LabelComponent("Hoster: "
						+ ((lobby.getOwner() != null) ? lobby.getOwner()
								.getName() : ""))).align(Align.left);
		settingsTable.row();
		settingsTable.add(new LabelComponent("Gamemode: "+lobby.getGameSettings().toString())).align(Align.left);
		settingsTable.row();
		settingsTable.add(new LabelComponent("Rounds: "+lobby.getGameSettings().getTotalRounds())).align(Align.left);
		settingsTable.row();
		settingsTable.add(new LabelComponent("PLAYERS CONNECTED: "+lobby.getPlayers().size())).align(Align.left);
		settingsTable.row();
		// TODO: Add this message to the GUI
		Log.info(lobby.getGameSettings().getMessage());
	}
}
