/**
 * 
 */
package client.gui.screens;


import client.gui.screens.components.LabelComponent;
import client.gui.screens.components.TextButtonComponent;
import client.model.Action;
import client.model.Event;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;


/**
 * Class defining how to render gui's "about screen" in client.
 * @author Alexander H�renstam.
 * 
 */
public class AboutScreen extends AbstractMenuScreen{
	
	private Table topLeftTable;
	private Table companyTable;
	private Table aboutLabelTable;
	
	private LabelComponent aboutLabel;
	private Image companyImage;
	
	@Override
	public void create(){
		super.create();
		
		backButton = new TextButtonComponent("Back");
		//TODO Implement support for reading text to label with ContentManager.
		aboutLabel = new LabelComponent(Gdx.files.internal("res/text/aboutText.txt").readString());
		companyImage = new Image(cm.getTexture("SHEEPSTUDIOStestonly.png"));
		
		topLeftTable = new Table();
		companyTable = new Table();
		aboutLabelTable = new Table();
		
		topLeftTable.setFillParent(true);
		companyTable.setFillParent(true);
		
		stage.addActor(companyTable);
		stage.addActor(topLeftTable);
		
		topLeftTable.add(backButton);
		centerTable.add(aboutLabelTable);
		companyTable.add(companyImage);
		
		aboutLabelTable.add(aboutLabel);
		
		topLeftTable.pad(50);
		topLeftTable.top().left();
		companyTable.bottom();
		
		aboutLabelTable.setBackground(tableBorderDrawable);
		
		backButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event(Action.BACK_CLICKED));
			}
		});
	}
}
