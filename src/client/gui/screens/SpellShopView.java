package client.gui.screens;

import java.util.ArrayList;
import java.util.List;

import model.Player;
import model.SpellShop;
import model.spells.SpellSlot;
import client.gui.screens.components.LabelComponent;
import client.gui.screens.components.SpellSlotButton;
import client.gui.screens.components.TextButtonComponent;
import client.model.Action;
import client.model.CoMClient;
import client.model.Event;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;


public class SpellShopView extends AbstractScreen{
	private Table spellTable;
	private SpellShop spellShop;
	private int spellIndex;
	private List<SpellSlotButton> spellSlotButtons;
	
	public SpellShopView(SpellShop spellShop,int index){
		this.spellShop = spellShop;
		this.spellIndex = index;
		spellSlotButtons = new ArrayList<SpellSlotButton>();
	}
	public SpellShopView(SpellShop spellShop){
		this(spellShop,-1);
	}
	@Override
	public void create() {
		super.create();
		spellTable = new Table();
		TextButton backButton = new TextButtonComponent("");
		
		spellTable.add();
		spellTable.add();
		spellTable.add(new LabelComponent("Spell Shop"));
		spellTable.add();
		spellTable.add(backButton).width(30).height(30).right();
		spellTable.row();
		
		spellTable.setBackground(new NinePatchDrawable(new NinePatch(
				cm.getTexture("gui/components/windowBackground.png"),5,5,5,5)));
		centerTable.add(spellTable);
		addSpellButtons();
		
		backButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent arg0,Actor arg1) {
				perform(new client.model.Event(Action.CLOSE_OVERLAY));
			}
		});
	}
	private void addSpellButtons(){
		int i = 1;
		for(final SpellSlot spellSlot:spellShop.getSpellSlots()){
			final int index = i;
			SpellSlotButton spellSlotButton = new SpellSlotButton(spellSlot, 0, 0);
			spellSlotButtons.add(spellSlotButton);
			Player player = CoMClient.getInstance().getPlayer();
			if(player.getSpellPoints() < spellSlot.getSpellRecipe().getCost() ||
					player.hasSpellSlot(spellSlot)){
				spellSlotButton.setDisabled(true);
			}
			spellSlotButton.addListener(new ChangeListener() {
				@Override
				public void changed(
						com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent arg0,
						Actor arg1) {
					buySpell(spellSlot);
					
				}
			});
			spellTable.add(spellSlotButton).width(100).height(100);
			if(i++ % 5 == 0){
				spellTable.row();
			}
		}
	}
	private void buySpell(SpellSlot spellSlot){
		perform(new Event(Action.BUY_SPELL, new Object[]{spellSlot,(Integer) spellIndex}));
		Player player = CoMClient.getInstance().getPlayer();
		for(SpellSlotButton spellSlotButton: spellSlotButtons){
			if(spellSlotButton.hasSpellSlot()){
				if(player.getSpellPoints() < 
						spellSlotButton.getSpellSlot().getSpellRecipe().getCost() ||
						player.hasSpellSlot(spellSlotButton.getSpellSlot())){
					spellSlotButton.setDisabled(true);
				}
			}
		}
	}
}
