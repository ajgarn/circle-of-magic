/**
 * 
 */
package client.gui.screens;

import client.gui.screens.components.LabelComponent;
import client.gui.screens.components.TextButtonComponent;
import client.model.Action;
import client.model.Event;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

/**
 * Class defining how to render gui's main menu in client.
 * @author Alexander H�renstam.
 * @author Robin Gr�nberg
 */
public class OnlineScreen extends AbstractMenuScreen {
	
	private LabelComponent headerLabel;	
	private TextButtonComponent joinButton;
	private TextButtonComponent hostButton;
	private TextButtonComponent findGameButton;
	private Table topLeftTable;
	
	public OnlineScreen(){
	}
	@Override
	public void create(){
		super.create();
		
		headerLabel = new LabelComponent("ONLINE");
		findGameButton = new TextButtonComponent("Quick match");
		joinButton = new TextButtonComponent("Join Custom Game");
		hostButton = new TextButtonComponent("Host Custom Game");
		backButton = new TextButtonComponent("Back");
		topLeftTable = new Table();
		
		topLeftTable.setFillParent(true);
		stage.addActor(topLeftTable);
		topLeftTable.top().left().pad(50);
		topLeftTable.add(backButton);
		
		centerTable.add(headerLabel).width(400).height(100).pad(5);
		centerTable.row();
		centerTable.add(findGameButton).width(400).height(100).pad(5);
		centerTable.row();
		centerTable.add(joinButton).width(400).height(100).pad(5);
		centerTable.row();
		centerTable.add(hostButton).width(400).height(100).pad(5);
		
		
		findGameButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event(Action.FIND_GAME));
			}
		});
		
		joinButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event( Action.JOIN_GAME_CLICKED));
			}
		});
		
		hostButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event(Action.HOST_GAME_CLICKED));
			}
		});
		backButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event(Action.BACK_CLICKED));
			}
		});
	}
	public void resize(int width, int height) {
		super.resize(width, height);
	}
}
