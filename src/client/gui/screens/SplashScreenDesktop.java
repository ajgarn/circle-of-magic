package client.gui.screens;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;

/**
 * Class displaying a splashscreen for a set amount of time.
 * Tutorial: http://www.java-tips.org/java-se-tips/javax.swing/how-to-implement-a-splash-screen-for-an-applic-2.html
 * @author Alexander H�renstam.
 *
 */
public class SplashScreenDesktop extends JWindow {
    
	private static SplashScreenDesktop splashScreenMod;

    public static SplashScreenDesktop getInstance(){
    	if(splashScreenMod==null){
    		splashScreenMod=new SplashScreenDesktop();
    	}
    	return splashScreenMod;
    }
    
    // A simple little method to show a title screen in the center
    // of the screen for the amount of time given in the constructor
    public void showSplash() {
        
        JPanel content = (JPanel)getContentPane();
        
        // Set the window's bounds, centering the window
        int width = 400;
        int height =383;
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (screen.width-width)/2;
        int y = (screen.height-height)/2;
        setBounds(x,y,width,height);
        
        // Build the splash screen
        ImageIcon ii = new ImageIcon("res/splash.png");
        JLabel label = new JLabel(ii);
        content.add(label, BorderLayout.CENTER);
        Color borderColor = Color.BLACK;
        content.setBorder(BorderFactory.createLineBorder(borderColor, 5));
        
        // Display it
        setVisible(true);        
    }
    
    /**
     * Set splash screen invisible.
     */
    public void endSplash(){
    	setVisible(false);
    }
}
