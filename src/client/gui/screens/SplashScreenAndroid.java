package client.gui.screens;


import services.graphics.util.ContentManager;
import client.model.Action;
import client.model.Event;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
 
/**
 * Class defining how to render splash screen on an Android device.
 * @author Alexander H�renstam.
 *
 */
public class SplashScreenAndroid extends AbstractScreen{
    private Texture logo;
    private SpriteBatch spriteBatch;
    
    @Override
    public void show()
    {
    	super.show();
    	logo.setEnforcePotImages(false);
        logo = ContentManager.getInstance().getTexture("splash.png");
        spriteBatch = new SpriteBatch();
    }
 
    @Override
    public void render(float delta)
    {
        handleInput();
 
        GL10 gl = Gdx.graphics.getGL10();        
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
 
        spriteBatch.begin();
        spriteBatch.draw(logo, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        spriteBatch.end();
    }
 
    private void handleInput()
    {
        if(Gdx.input.justTouched())
        {	
        	perform(new Event(Action.NEXT_CLICKED));
        }
    }
 
    @Override
    public void resize(int width, int height) {
    }
    @Override
    public void hide() {
    }
    @Override
    public void pause() {
    }
    @Override
    public void resume() {
    }
    @Override
    public void dispose() {	
    }
}