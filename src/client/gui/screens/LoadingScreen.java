/**
 * 
 */
package client.gui.screens;


import services.graphics.util.ContentManager;
import client.gui.screens.components.LabelComponent;

import com.badlogic.gdx.scenes.scene2d.ui.Table;

/**
 * Class defining how to render gui's "in lobby, joining a game" in client.
 * @author Alexander H�renstam.
 * 
 */
public class LoadingScreen extends AbstractScreen{
	
	private Table tab;
	
	private LabelComponent progressLabel;
	
	private LabelComponent textLabel;
	
	@Override
	public void create(){
		super.create();
		textureBackground = cm.getTexture("gui/backgrounds/sparksbg.png");
		tab = new Table();
		tab.setFillParent(true);
		stage.addActor(tab);
		textLabel = new LabelComponent("Loading...");
		progressLabel=new LabelComponent(""+ContentManager.getInstance().getProgress()*100+"%");
		tab.add(textLabel).top();
		tab.row();
		tab.add(progressLabel).center();
	}
	@Override
	public void render(float dTime) {
		progressLabel.setText(""+ContentManager.getInstance().getProgress()*100+"%");
		super.render(dTime);
	}
}
