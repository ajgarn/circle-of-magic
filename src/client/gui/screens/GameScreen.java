package client.gui.screens;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import model.AbstractGame.GameState;
import model.Player;
import model.core.IGameObject;
import services.graphics.renderables.IRenderable;
import services.graphics.util.Graphics;
import client.graphics.IWorldCamera;
import client.graphics.WorldCamera;
import client.graphics.WorldView;
import client.model.Action;
import client.model.ClientGame;
import client.model.ClientSettings;
import client.model.Event;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;

public class GameScreen extends InputMultiplexer implements Screen {

	private final List<GUIListener> listeners;
	private WorldView worldView;
	private IWorldCamera camera;
	private ClientGame game;
	private GameHudView gameHudView;
		
	
	/**
	 * This screen is rendered over the actual screen
	 */
	private Screen overlayScreen;
	
	public GameScreen(){
		listeners = new ArrayList<GUIListener>(1);
		gameHudView = new GameHudView();
	}
	@Override
	public void create(){
		camera = new WorldCamera();
		gameHudView.create();
		worldView = new WorldView(camera);
		addProcessor(gameHudView.getInputProcessor());
		Graphics.setupGL();
	}

	@Override
	public void dispose() {}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void render(float dTime) {
		game.update(dTime);
		camera.update(dTime);
		worldView.update(dTime);
		worldView.render();	
		gameHudView.render(dTime);
		if(overlayScreen != null){
			overlayScreen.render(dTime);
		}
		Gdx.input.setInputProcessor(this);
	}

	@Override
	public void resize(int width, int height) {
		worldView.resize(width,height);
		gameHudView.resize(width, height);
		if(overlayScreen != null){
			overlayScreen.resize(width, height);
		}
	}

	public void perform(Event event){
		for(GUIListener l:listeners){
			l.actionPerformed(event);
		}
	}

	@Override
	public void show() {
		show(null);
	}
	@Override
	public void show(Event event) {
		perform(new Event(Action.SCREEN_SHOWED, 
				(event != null) ? event.getValue() : null));
	}

	@Override
	public void hide() {
		perform(new Event( Action.SCREEN_HID));
	}
	
	@Override
	public InputProcessor getInputProcessor() {
		return this;
	}
	public IWorldCamera getCamera(){
		return camera;
	}
	public ClientGame getGame(){
		return game;
	}
	public void setGame(ClientGame game){
		this.game = game;
		gameHudView.setGame(game);
	}
	/**
	 * Reset the GameScreen after a game
	 */
	public void reset(){
		gameHudView.reset();
		hideOverlayScreen();
	}
	public void addRenderableObject(IRenderable renderable){
		worldView.addRenderableObject(renderable);
	}
	@Override
	public void addGuiListener(GUIListener listener){
		listeners.add(listener);
		gameHudView.addGuiListener(listener);
	}
	/**
	 * Hide the overlay screen, if any
	 */
	public void hideOverlayScreen(){
		if(overlayScreen != null){
			overlayScreen.hide();
			removeProcessor(overlayScreen.getInputProcessor());
		}
		overlayScreen = null;
	}
	/**
	 * Get whenever this gamescreen has an overlay
	 * @return true if it has an overlay, false otherwise.
	 */
	public boolean hasOverlayScreen(){
		return overlayScreen != null;
	}
	/**
	 * Show the specific screen as a overlay screen.
	 * @param overlayScreen The specific screen to show.
	 */
	public void showOverlayScreen(Screen overlayScreen){
		this.overlayScreen = overlayScreen;
		overlayScreen.create();
		overlayScreen.show();
		overlayScreen.resize(ClientSettings.getInstance().getScreenWidthGame(), ClientSettings.getInstance().getScreenHeightGame());
		addProcessor(0,overlayScreen.getInputProcessor());
		for(GUIListener l: listeners){
			overlayScreen.addGuiListener(l);
		}
	}
	public void receivedGameObjects(Map<Integer,? extends IGameObject> gameObjects, long timeSent) {
		worldView.changedGameObjectsReceived(gameObjects, timeSent);
		gameHudView.changedGameObjectsReceived(gameObjects, timeSent);
	}
	public void receivedPlayer(Player player, long timeSent){
		game.receivedPlayer(player, timeSent);
		gameHudView.changedPlayerReceived(player, timeSent);
	}
	public void receivedWorld() {
		worldView.worldReceived(game.getWorld());
		gameHudView.refresh();
		if(camera.isLockedAtGameObject()){
			camera.lockAtGameObject(game.getPlayerCharacter());
		}
		
		if (game.getState() == GameState.ENDED) {
			perform(new Event( Action.NEXT_CLICKED));
		} else if(game.getPlayerCharacter() != null){
			camera.lookAt(game.getPlayerCharacter().getPosition());
		}
			
	}
	public Object getOverlayScreen() {
		return overlayScreen;
	}
	/**
	 * Show a message on the top of the gameScreen.
	 * __________________
	 * |				|
	 * |	message		|
	 * |				|
	 * |				|
	 * |________________|
	 * @param message The message to show
	 * @param time The time the text will show, in seconds
	 */
	public void showMessage(String message, float time){
		gameHudView.showMessage(message, time);
	}
}
