package client.gui.screens;

import services.graphics.util.ContentManager;
import client.gui.screens.components.TextButtonComponent;
import client.model.Action;
import client.model.Event;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

/**
 * Class defining how to render main menu in game. Used in GUI.
 * @author Alexander H�renstam.
 *
 */
public class InGameMenuScreen extends AbstractScreen {
	private TextButtonComponent exitButton;
	private TextButtonComponent returnMenuButton;
	private TextButtonComponent optionsButton;
	private TextButtonComponent resumeButton;
	private Table buttonTable;
	private ContentManager cm;

	@Override
	public void create() {
		super.create();
		cm = ContentManager.getInstance();
		optionsButton = new TextButtonComponent("Options");
		returnMenuButton = new TextButtonComponent("Return to Main Menu");
		exitButton = new TextButtonComponent("Exit Game");
		resumeButton = new TextButtonComponent("Resume");
		buttonTable = new Table();
				
		centerTable.add(buttonTable);
		
		buttonTable.add(resumeButton).fillX().pad(25);
		buttonTable.row();
		buttonTable.add(optionsButton).fillX().pad(25);
		buttonTable.row();
		buttonTable.add(returnMenuButton).fillX().pad(25);
		buttonTable.row();
		buttonTable.add(exitButton).fillX().pad(25);
		
		buttonTable.setBackground(new NinePatchDrawable(new NinePatch(cm.getTexture("gui/components/windowBackground.png"),5,5,5,5)));
		
		resumeButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new client.model.Event(Action.CLOSE_OVERLAY));
			}
		});
		
		returnMenuButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event(Action.BACK_CLICKED));
			}
		});
		
		exitButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event(Action.EXIT_CLICKED));
			}
		});
	}
}
