/**
 * 
 */
package client.gui.screens;

import client.gui.screens.components.TextButtonComponent;
import client.model.Action;
import client.model.Event;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

/**
 * Class defining how to render gui's main menu in client.
 * @author Alexander H�renstam.
 * @author Robin Gr�nberg
 */
public class MainMenuScreen extends AbstractMenuScreen {
		
	private TextButtonComponent onlineButton;
	private TextButtonComponent lanButton;
	private TextButtonComponent optionsButton;
	private TextButtonComponent aboutButton;
	private TextButtonComponent exitButton;
	//private TextButtonComponent backButton;
	
	private NinePatchDrawable ninePatchDrawable;
	
	//private Table centerPosTable;
	private Table mainMenuTable;
	
	public MainMenuScreen(){
	}
	@Override
	public void create(){
		super.create();
		//textureBackground = cm.getTexture("gui/backgrounds/sparksbg.png");
		
		onlineButton = new TextButtonComponent("Play Online");
		lanButton = new TextButtonComponent("LAN");
		optionsButton = new TextButtonComponent("Options");
		aboutButton = new TextButtonComponent("About");
		exitButton = new TextButtonComponent("Exit");
		backButton = new TextButtonComponent("Back");
		mainMenuTable = new Table();
		
		ninePatchDrawable= new NinePatchDrawable(new NinePatch(cm.getTexture("gui/components/windowBackground.png"),5,5,5,5));
		
		centerTable.add(mainMenuTable);
		mainMenuTable.setBackground(ninePatchDrawable);
		
		mainMenuTable.add(onlineButton).width(400).height(100).pad(5);
		mainMenuTable.row();
		mainMenuTable.add(lanButton).width(400).height(100).pad(5);
		mainMenuTable.row();
		mainMenuTable.add(optionsButton).width(400).height(100).pad(5);
		mainMenuTable.row();
		mainMenuTable.add(aboutButton).width(400).height(100).pad(5);
		mainMenuTable.row();
		mainMenuTable.add(exitButton).width(400).height(100).pad(5);
		
		onlineButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event( Action.ONLINE_CLICKED));
			}
		});
		
		lanButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event(Action.LAN_CLICKED));
			}
		});
		optionsButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event(Action.OPTIONS_CLICKED));
			}
		});
		aboutButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event(Action.ABOUT_CLICKED));
			}
		});
		exitButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event( Action.EXIT_CLICKED));
			}
		});
		backButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event(Action.BACK_CLICKED));
			}
		});
	}
	public void resize(int width, int height) {
		super.resize(width, height);
	}
}
