/**
 * 
 */
package client.gui.screens;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import model.GameHeader;
import client.gui.screens.components.LabelComponent;
import client.gui.screens.components.ScrollPaneComponent;
import client.gui.screens.components.SelectGameButtonComponent;
import client.gui.screens.components.TextButtonComponent;
import client.model.Action;
import client.model.Event;
import client.model.SearchGamesModel;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;


/**
 * Class defining how to render gui's "Select game screen" in client.
 * @author Alexander H�renstam.
 * 
 */
public class SelectGameScreen extends AbstractMenuScreen{

	private Table topLeftTable;
	private Table gamesTable;
	private Table topTable;
	private SearchGamesModel selectGameModel;
	private Collection<GameHeader> games;
	private ScrollPaneComponent scrollPane;
	private LabelComponent selectGameLabel;
	
	public SelectGameScreen(){
	}
	@Override
	public void create(){
		super.create();
		selectGameModel = SearchGamesModel.getInstance();
		
		games = new ArrayList<GameHeader>();
		gamesTable=new Table();
		topTable = new Table();
		backButton = new TextButtonComponent("Back");
		topLeftTable = new Table();
		scrollPane = new ScrollPaneComponent(gamesTable);
		selectGameLabel = new LabelComponent("Select Game");
		
		topLeftTable.setFillParent(true);
		topTable.setFillParent(true);
		
		topLeftTable.add(backButton).top().left();
		stage.addActor(topLeftTable);
		stage.addActor(topTable);
		topTable.top().pad(50);
		topLeftTable.top().left().pad(50);
		topTable.add(selectGameLabel);
		centerTable.add(scrollPane);
		
		backButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event(Action.BACK_CLICKED));
			}
		});
	}
	public void resize(int width, int height) {
		super.resize(width, height);
	}
	

	@Override
	public void show() {
		super.show();
		gamesTable.clear();
		//TODO fetch games and display to user
	}
	@Override
	public void render(float dTime) {
		super.render(dTime);
		for(final GameHeader game: selectGameModel.getAvailableGames()){
			//add new games
			if(!games.contains(game)){
				SelectGameButtonComponent gameButton=new SelectGameButtonComponent(game);
				gamesTable.add(gameButton);
				gameButton.addListener(new ChangeListener() {
					public void changed(ChangeEvent event, Actor actor) {
						perform(new Event( Action.JOIN_GAME_CLICKED, game.hashCode()));
					}
				});
				games.add(game);
			}else{ //Update the old one
				findButton(game).setGameHeader(game);
			}
		}
		Iterator<GameHeader> gameIterator = games.iterator();
		//Remove old ones if they do not exist anymore
		while(gameIterator.hasNext()){
			GameHeader gameHeader = gameIterator.next();
			if(!selectGameModel.getAvailableGames().contains(gameHeader)){
				gameIterator.remove();
				gamesTable.removeActor(findButton(gameHeader));
			}
		}
	}
	private SelectGameButtonComponent findButton(GameHeader game){
		for(Actor a: gamesTable.getChildren()){
			if(a instanceof SelectGameButtonComponent){
				SelectGameButtonComponent button = (SelectGameButtonComponent)a;
				if(game.equals(button.getGameHeader())){
					return button;
				}
			}
		}
		return null;
	}
}
