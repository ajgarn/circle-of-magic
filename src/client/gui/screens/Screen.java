package client.gui.screens;


import client.model.Event;

import com.badlogic.gdx.InputProcessor;

/**
 * A new Graphical view in the Application should implement this interface.
 * @author Robin Gronberg
 *
 */
public interface Screen extends com.badlogic.gdx.Screen {
	/**
	 * This method loads the screen.
	 */
	public void create();
	/**
	 * Get this GUIview's Input listener
	 * @return
	 */
	public InputProcessor getInputProcessor();
	/**
	 * Add a GUIlistener to this View
	 * @param listener The GUIListener that should listen to this GUIView
	 */
	public void addGuiListener(GUIListener listener);
	/**
	 * Called when this screen becomes the current screen for a Game.
	 * Passes an {@link Event} to the controller.
	 * @see #show()
	 * @param event The event to pass to the controller.
	 */
	void show(Event event);
}
