/**
 * 
 */
package client.gui.screens;


import client.gui.screens.components.TextButtonComponent;
import client.model.Action;
import client.model.Event;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;


/**
 * Class defining how to render the hosting player's Lobby screen in client.
 * @author Alexander H�renstam.
 * 
 */
public class HostLobbyScreen extends AbstractLobbyScreen {
	private TextButtonComponent startButton;
	/**
	 * Empty constructor.
	 */
	public HostLobbyScreen(){
	}
	@Override
	public void create(){
		super.create();
		startButton = new TextButtonComponent("Start");
		bottomRightTable.row();
		bottomRightTable.add(startButton);
		
		startButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event( Action.START_GAME_CLICKED));
			}
		});
	}
}
