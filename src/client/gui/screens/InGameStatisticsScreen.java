/**
 * 
 */
package client.gui.screens;

import java.util.Collection;

import model.Player;
import model.PlayerStatistics;
import services.graphics.util.ContentManager;
import client.gui.screens.components.LabelComponent;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

/**
 * Class defining how to render gui's statistics in client.
 * @author Alexander H�renstam.
 */
public class InGameStatisticsScreen extends AbstractScreen {
	private LabelComponent screenNameLabel;
	private Table statisticsTable;
	
	private ContentManager cm;
	//private boolean refreshStatistics;
	
	public InGameStatisticsScreen(){
	}
	@Override
	public void create(){
		super.create();
		cm = ContentManager.getInstance();
		
		screenNameLabel = new LabelComponent("Statistics");
		statisticsTable = new Table();
		
		centerTable.add(statisticsTable);
		statisticsTable.add(screenNameLabel);
		statisticsTable.row();
		
		NinePatchDrawable ninePatchDrawable= new NinePatchDrawable(new NinePatch(cm.getTexture("gui/components/windowBackground.png"),5,5,5,5));
		statisticsTable.setBackground(ninePatchDrawable		);
	}
	/**
	 * Creates labels for each player in Game every time a statistics screen is shown.
	 */
	public void refreshPlayerStatistics(Collection<Player> players){
		statisticsTable.clear();
		for (Player player : players) {
			PlayerStatistics ps = player.getStatistics();
			LabelComponent component = new LabelComponent(player.getName()+"; Killing Blows: "+ps.getKillingBlows()+" Deaths: "+ps.getDeaths()+" Damage Done: "+ps.getDamageDone());
			statisticsTable.add(component);
			System.out.println(ps.getDamageDone());
			statisticsTable.row();
		}
	}
}
