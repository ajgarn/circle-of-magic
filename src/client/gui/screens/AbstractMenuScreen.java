package client.gui.screens;

import client.gui.screens.components.TextButtonComponent;

/**
 * Parent class for a menu screen in the GUI. Extending AbstractScreen.
 * @author Alexander H�renstam.
 *
 */
public abstract class AbstractMenuScreen extends AbstractScreen {
	protected TextButtonComponent backButton;
	
	@Override
	public void create(){
		super.create();
		textureBackground = cm.getTexture("gui/backgrounds/gray_background.png");
	}
}
