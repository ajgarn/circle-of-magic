package client.gui.screens;


import client.gui.screens.components.CheckBoxComponent;
import client.gui.screens.components.LabelComponent;
import client.gui.screens.components.TextButtonComponent;
import client.gui.screens.components.TextFieldComponent;
import client.model.Action;
import client.model.Event;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;


/**
 * Class defining how to render gui login client.
 * @author Alexander H�renstam.
 * 
 */
public class LoginScreen extends AbstractScreen{
        
        private LabelComponent labelName;
        private LabelComponent labelPassword;
        private TextFieldComponent textFieldName;
        private TextFieldComponent textFieldPassword;
        private TextButtonComponent loginButton;
        private LabelComponent copyrightLabel;
        private CheckBoxComponent rememberCheckBox;
        private CheckBoxComponent lanCheckBox;
        private LabelComponent rememberLabel;
        private LabelComponent lanLabel;
        
        private Image logoImage;
        
        private Table loginBoxTable;
        private Table bottomTable;
        private Table logoTable;
        
        private Animator animator;

        public LoginScreen(){
        }
        @Override
        public void create(){
                super.create();
                
                labelName = new LabelComponent("Username");
                labelPassword = new LabelComponent("Password");
                //textFieldName = new TextFieldComponent(System.getProperty("user.name"));
                textFieldName = new TextFieldComponent("test");
                textFieldPassword = new TextFieldComponent("qwerty");
                loginButton = new TextButtonComponent("Login");
                lanCheckBox = new CheckBoxComponent();
                logoImage = new Image(cm.getTexture("logo.png"));
                copyrightLabel = new LabelComponent("Copyright 2013 SHEEPSTUDIOS. All Rights Reserved.");
                rememberCheckBox = new CheckBoxComponent();
                rememberLabel = new LabelComponent("Remember username.");
                lanLabel = new LabelComponent("LAN mode");
                
                loginBoxTable = new Table();
                logoTable = new Table();
                bottomTable = new Table();
                
                addTable(logoTable);
                addTable(bottomTable);
                
               textureBackground = cm.getTexture("gui/backgrounds/magebg.png");
                
                centerTable.add(loginBoxTable);
                loginBoxTable.bottom();
                centerTable.bottom().pad(200);
                
                bottomTable.add(copyrightLabel);
                //animator = new Animator();
                //animator.create();
                
                loginBoxTable.add(labelName).pad(10).colspan(2);
                loginBoxTable.row();
                loginBoxTable.add(textFieldName).expandX().fill().pad(10).colspan(2);
                loginBoxTable.row();
                loginBoxTable.add(labelPassword).pad(10).colspan(2);
                loginBoxTable.row();
                loginBoxTable.add(textFieldPassword).expandX().fill().pad(10).colspan(2);
                loginBoxTable.row();
                loginBoxTable.add(loginButton).expandX().fill().colspan(2).pad(10);
                loginBoxTable.row();
                loginBoxTable.add(lanCheckBox).align(Align.left);
                loginBoxTable.add(lanLabel).align(Align.left);
                loginBoxTable.row();
                loginBoxTable.add(rememberCheckBox).align(Align.left);
                loginBoxTable.add(rememberLabel);
                
                logoTable.add(logoImage).height(400).width(400);
                bottomTable.bottom();
                
                logoTable.top().left().pad(50);

                loginButton.addListener(new ChangeListener() {
                    public void changed(ChangeEvent event, Actor actor) {
                        // Passes player name as a event value.
                        perform(new Event(Action.NEXT_CLICKED, new Object[] 
                        		{textFieldName.getText(), textFieldPassword.getText(),
                        		lanCheckBox.isChecked()}));
                    }
                });
                lanCheckBox.addListener(new ChangeListener() {
                    public void changed(ChangeEvent event, Actor actor) {
                        boolean checked = lanCheckBox.isChecked();
                        labelPassword.setVisible(!checked);
                        textFieldPassword.setVisible(!checked);
                    }
                });
                //TODO:add listener on textbox
                
                NinePatchDrawable ninePatch = new NinePatchDrawable(
                                new NinePatch(new TextureRegion(
                                                cm.getTexture("gui/components/windowBackground.png")),32,32,32,32));
                loginBoxTable.setBackground(ninePatch);
                //loginBoxTable.debug();

        }
        @Override
        public void render(float dTime) {
        // TODO Auto-generated method stub
        	super.render(dTime);
        	//animator.render();
        }
        public void resize(int width, int height) {
                super.resize(width, height);
        }
}
