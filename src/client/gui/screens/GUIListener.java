package client.gui.screens;

import client.model.Event;



/**
 * Any controller for a GUIView should implement this 
 * Interface. The GUIView that implement this.
 * @author Robin Gronberg
 *
 */
public interface GUIListener {
	public void actionPerformed(Event event);
}
