package client.gui.screens;


import java.util.ArrayList;
import java.util.List;

import services.graphics.util.ContentManager;
import services.graphics.util.GraphicsConstants;
import client.model.Action;
import client.model.Event;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

/**
 * Parent class for a screen in the GUI.
 * @author Robin Gr�nberg.
 *
 */
public abstract class AbstractScreen implements Screen {
	protected final List<GUIListener> listeners;

	public AbstractScreen(){
		listeners = new ArrayList<GUIListener>(1);
	}
	@Override
	public void create(){
		cm = ContentManager.getInstance();
		batch = new SpriteBatch();
		stage = new Stage();
		centerTable = new Table();
		centerTable.setFillParent(true);
		stage.addActor(centerTable);
		tableBorderDrawable = new NinePatchDrawable(new NinePatch(cm.getTexture("gui/components/windowBackground.png"),5,5,5,5));
		//centerTable.debug();
	}
	/**
	 * Adding a GUIListener to AbstractScreen object.
	 */
	public void addGuiListener(GUIListener listener){
		listeners.add(listener);
	}
	
	protected Stage stage;
	protected SpriteBatch batch;
	protected OrthographicCamera camera = new OrthographicCamera();
	protected Texture textureBackground;
	protected Table centerTable;
	protected ContentManager cm;
	protected NinePatchDrawable tableBorderDrawable;
	
	@Override
	public void dispose() {
		stage.dispose();
		batch.dispose();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}
	@Override
	public void render(float dTime) {
		Gdx.input.setInputProcessor(getInputProcessor());
		camera.update();
		batch.begin();
		if(textureBackground != null){
			batch.draw(textureBackground,0,0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		}
		batch.end();
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
		
		Table.drawDebug(stage); // This is optional, but enables debug lines for
							// tables.
	}

	@Override
	public void resize(int width, int height) {
		camera.setToOrtho(false, width, height);
		stage.setViewport(width, height, true);
		
		stage.setViewport(GraphicsConstants.PREFERRED_SCREEN_WIDTH, 
				GraphicsConstants.PREFERRED_SCREEN_HEIGHT, true);
		batch.setProjectionMatrix(camera.combined);
	}

	@Override
	public void resume() {}


	@Override
	public InputProcessor getInputProcessor() {
		return stage;
	}
	
	@Override
	public void show() {
		//Notify controller that this is is the active screen
		show(null);
	}
	@Override
	public void show(Event event) {
		//Notify controller that this is is the active screen
		perform(new Event(Action.SCREEN_SHOWED, (event != null) ? event.getValue() : null));
	}
	@Override
	public void hide() {
		//Notify controller that this is is the active screen
		perform(new Event(Action.SCREEN_HID));
	}
	/**
	 * Notifies all GUIListener objects that something has happened.
	 * @param event what has happened in the view.
	 */
	public void perform(Event event){
		for(GUIListener l:listeners){
			l.actionPerformed(event);
		}
	}
	/**
	 * Sets setFillParent() to true and uses addActor() to add Table to Stage properly.
	 * @param table
	 */
	protected void addTable(Table table){
		table.setFillParent(true);
		stage.addActor(table);
	}
}
