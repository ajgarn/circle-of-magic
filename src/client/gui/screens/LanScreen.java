/**
 * 
 */
package client.gui.screens;

import client.gui.screens.components.LabelComponent;
import client.gui.screens.components.TextButtonComponent;
import client.model.Action;
import client.model.Event;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

/**
 * Class defining how to render gui's main menu in client.
 * @author Alexander H�renstam.
 * @author Robin Gr�nberg
 */
public class LanScreen extends AbstractMenuScreen {
	private LabelComponent headerLabel;	
	private TextButtonComponent joinButton;
	private TextButtonComponent hostButton;
	private Table topLeftTable;
	
	public LanScreen(){
	}
	@Override
	public void create(){
		super.create();
		
		headerLabel=new LabelComponent("LAN");
		joinButton = new TextButtonComponent("Join Game");
		hostButton = new TextButtonComponent("Host Game");
		backButton = new TextButtonComponent("Back");
		topLeftTable = new Table();
		
		topLeftTable.setFillParent(true);
		stage.addActor(topLeftTable);
		topLeftTable.top().left().pad(50);
		topLeftTable.add(backButton);
		
		centerTable.add(headerLabel).width(400).height(100).pad(5);
		centerTable.row();
		centerTable.add(joinButton).width(400).height(100).pad(5);
		centerTable.row();
		centerTable.add(hostButton).width(400).height(100).pad(5);
		centerTable.row();
		
		joinButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event( Action.JOIN_GAME_CLICKED));
			}
		});
		
		hostButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event(Action.HOST_GAME_CLICKED));
			}
		});
		backButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event(Action.BACK_CLICKED));
			}
		});
	}
	public void resize(int width, int height) {
		super.resize(width, height);
	}
}
