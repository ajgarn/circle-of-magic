/**
 * 
 */
package client.gui.screens;

import java.util.Collection;

import model.Player;
import model.PlayerStatistics;
import client.gui.screens.components.LabelComponent;
import client.gui.screens.components.TextButtonComponent;
import client.model.Action;
import client.model.Event;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

/**
 * Class defining how to render gui's statistics in client.
 * @author Alexander H�renstam.
 * @author Robin Gr�nberg.
 */
public class StatisticsScreen extends AbstractScreen {
		
	private TextButtonComponent mainMenuButton;
	private LabelComponent screenNameLabel;
	private Table statisticsTable;
	private Table topTable;
	//private boolean refreshStatistics;
	
	public StatisticsScreen(){
	}
	@Override
	public void create(){
		super.create();
		textureBackground = cm.getTexture("gui/backgrounds/sparksbg.png");
		
		mainMenuButton = new TextButtonComponent("Return to Main Menu");
		screenNameLabel = new LabelComponent("Statistics");
		statisticsTable = new Table();
		topTable = new Table();
		
		topTable.setFillParent(true);
		statisticsTable.setFillParent(true);
		
		stage.addActor(statisticsTable);
		stage.addActor(topTable);
		topTable.add(screenNameLabel);
		centerTable.add(mainMenuButton);
		
		centerTable.bottom().pad(100);
		statisticsTable.center();
		topTable.top();

		mainMenuButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event( Action.MAIN_MENU_CLICKED));
			}
		});
	}
	@Override
	public void render(float dTime) {
		// TODO Auto-generated method stub
		super.render(dTime);
	}
	public void resize(int width, int height) {
		super.resize(width, height);
	}
	/**
	 * Creates buttons for each player in Lobby every time a player has left or joined.
	 */
	public void refreshPlayerStatistics(Collection<Player> players){
		statisticsTable.clear();
		for (Player player : players) {
			PlayerStatistics ps = player.getStatistics();
			LabelComponent component = new LabelComponent(player.getName()+"; Killing Blows: "+ps.getKillingBlows()+" Deaths: "+ps.getDeaths()+" Damage Done: "+ps.getDamageDone());
			statisticsTable.add(component);
			statisticsTable.row();
			/*component.addListener
			 * (new ChangeListener() {
				public void changed(ChangeEvent event, Actor actor) {
					perform(new Event(Action.REMOVE_PLAYER, player));
				}
			});*/
		}
		//refreshStatistics=false;
	}
}
