package client.gui.screens;

import client.gui.screens.components.TextButtonComponent;
import client.model.Action;
import client.model.CoMClient;
import client.model.Event;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

/**
 * Class responsible for rendering Lobby in graphical user interface for a joining player's client.
 * @author Alexander H�renstam.
 *
 */
public class JoinLobbyScreen extends AbstractLobbyScreen{
	private TextButtonComponent readyButton;
	
	@Override
	public void create() {
		super.create();
		readyButton = new TextButtonComponent("Ready!");
		
		bottomRightTable.add(readyButton);
		
		readyButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event( Action.READY_CLICKED));
			}
		});
	}	
	
	/**
	 * Sets readyButton to Color.GREEN.
	 */
	public void updateReadyButton(){
		switch(CoMClient.getInstance().getPlayer().getStatus()){
		case READY:
			readyButton.setColor(Color.GREEN);
		break;
		case WAITING:
			readyButton.setColor(Color.RED);
			break;
		default:
		}
	}
}
