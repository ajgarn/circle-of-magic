/**
 * 
 */
package client.gui.screens;

import java.util.ArrayList;
import java.util.List;

import client.gui.screens.components.CheckBoxComponent;
import client.gui.screens.components.LabelComponent;
import client.gui.screens.components.ListComponent;
import client.gui.screens.components.TextButtonComponent;
import client.model.Action;
import client.model.ClientSettings;
import client.model.Event;
import client.model.Resolution;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;


/**
 * Class defining how to render gui's "option menu" in client.
 * @author Alexander H�renstam.
 * 
 */
public class OptionsScreen extends AbstractMenuScreen {
	
	private Table topLeftTable;
	private CheckBoxComponent checkBoxFullscreenGui;
	private CheckBoxComponent checkBoxFullscreenGame;
	private CheckBoxComponent checkBoxMuteSound;
	private CheckBoxComponent checkBoxMuteGameplayMusic;
	private CheckBoxComponent checkBoxMuteMenuMusic;
	private ListComponent resolutionList;
	
	private TextButtonComponent applyButton;
	
	private Table optionsHolderTable;
	private Table bottomRightTable;
	
	public OptionsScreen(){

	}

	@Override
	public void create(){
		super.create();
		DisplayMode display = Gdx.graphics.getDesktopDisplayMode();
		int height = display.height;
		int width = display.width;
		final List<Resolution> supportedResolutions = Resolution.getSupportedResolutions();
		final List<String> supportedResolutionStrings = new ArrayList<String>(supportedResolutions.size());
		for(Resolution r : supportedResolutions){
			supportedResolutionStrings.add(r.toString());
		}
		backButton = new TextButtonComponent("Back");
		topLeftTable = new Table();
		checkBoxFullscreenGui = new CheckBoxComponent();
		checkBoxFullscreenGame = new CheckBoxComponent();
		checkBoxMuteSound = new CheckBoxComponent();
		checkBoxMuteGameplayMusic = new CheckBoxComponent();
		checkBoxMuteMenuMusic = new CheckBoxComponent();
		applyButton = new TextButtonComponent("Apply");
		resolutionList = new ListComponent(supportedResolutionStrings.toArray());
		
		optionsHolderTable = new Table();
		bottomRightTable = new Table();
		
		ClientSettings settings = ClientSettings.getInstance();
		checkBoxFullscreenGui.setChecked(settings.getFullscreenGui());
		checkBoxFullscreenGame.setChecked(settings.getFullscreenGameplay());
		checkBoxMuteGameplayMusic.setChecked(settings.getIsGameplayAudioMuted());
		checkBoxMuteMenuMusic.setChecked(settings.getIsMenuAudioMuted());
		
		topLeftTable.top().left().pad(50);
		topLeftTable.add(backButton);
		topLeftTable.setFillParent(true);
		centerTable.add(optionsHolderTable);
		stage.addActor(topLeftTable);
		
		addTable(bottomRightTable);
		bottomRightTable.bottom().right();
		
		bottomRightTable.add(applyButton).pad(50);
		
		optionsHolderTable.add(new LabelComponent("Use Fullscreen in menu:"));
		optionsHolderTable.add(checkBoxFullscreenGui);
		optionsHolderTable.row();
		optionsHolderTable.add(new LabelComponent("Use Fullscreen in gameplay:"));
		optionsHolderTable.add(checkBoxFullscreenGame);
		optionsHolderTable.row();
		optionsHolderTable.add(new LabelComponent("Mute SoundEffects: "));
		optionsHolderTable.add(checkBoxMuteSound);
		optionsHolderTable.row();
		optionsHolderTable.add(new LabelComponent("Mute Music during Gameplay: "));
		optionsHolderTable.add(checkBoxMuteGameplayMusic);
		optionsHolderTable.row();
		optionsHolderTable.add(new LabelComponent("Mute MenuMusic: "));
		optionsHolderTable.add(checkBoxMuteMenuMusic);
		optionsHolderTable.row();
		optionsHolderTable.add(new LabelComponent("Resolution: "));
		optionsHolderTable.add(resolutionList);
		
		optionsHolderTable.setBackground(tableBorderDrawable);
		
		checkBoxFullscreenGui.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event( Action.OPTIONS_FULLSCREEN_GUI_CLICKED));
			}
		});
		checkBoxFullscreenGame.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event(Action.OPTIONS_FULLSCREEN_GAME_CLICKED));
			}
		});
		checkBoxMuteGameplayMusic.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event(Action.OPTIONS_GAMEPLAY_MUSIC_CLICKED));
			}
		});
		checkBoxMuteMenuMusic.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				perform(new Event(Action.OPTIONS_MENU_MUSIC_CLICKED));
			}
		});
		backButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				ClientSettings.getInstance().setScreenWidthGame((int)(supportedResolutions.get(resolutionList.getSelectedIndex()).getWidth()));
				ClientSettings.getInstance().setScreenHeightGame((int)(supportedResolutions.get(resolutionList.getSelectedIndex()).getHeight()));
				perform(new Event(Action.BACK_CLICKED));
			}
		});
		applyButton.addListener(new ChangeListener() {
			public void changed(ChangeEvent event, Actor actor) {
				ClientSettings.getInstance().setScreenWidthGame((int)(supportedResolutions.get(resolutionList.getSelectedIndex()).getWidth()));
				ClientSettings.getInstance().setScreenHeightGame((int)(supportedResolutions.get(resolutionList.getSelectedIndex()).getHeight()));
				perform(new Event(Action.BACK_CLICKED));
			}
		});
	}
	public void resize(int width, int height) {
		super.resize(width, height);
	}
	@Override
	public void show() {
		super.show();
	}

}
