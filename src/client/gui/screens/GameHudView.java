package client.gui.screens;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import model.ICharacter;
import model.Player;
import model.core.IGameObject;
import client.gui.screens.components.HealthBar;
import client.gui.screens.components.LabelComponent;
import client.gui.screens.components.LabelTimeoutComponent;
import client.gui.screens.components.SpellSlotButton;
import client.model.Action;
import client.model.ClientGame;
import client.model.ClientSettings;
import client.model.CoMClient;
import client.model.Event;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
/**
 * This class holds the Grafical representation
 * of the HUD.
 * Used by GameScreen
 * @author robin
 *
 */
public class GameHudView extends AbstractScreen {
	
	private CharacterHud characterHudTable;
	private ClientGame game;
	private LabelComponent pingLabel;
	private LabelComponent fpsLabel;
	private LabelComponent timeLabel;
	private boolean refreshPlayerHud;
	private List<LabelTimeoutComponent> textToShow;
	
	private Table upperRightTable;
	private Table showTextTable;
	private Table timeTable;
	private Table topHolderTable;
	
	/**
	 * Standard constructor setting 
	 * textToShow = new ArrayList<LabelTimeoutComponent>(2).
	 */
	public GameHudView(){
		textToShow = new ArrayList<LabelTimeoutComponent>(2);
	}
	/**
	 * Returns a Game.
	 * @return ClientGame active on client.
	 */
	public ClientGame getGame() {
		return game;
	}
	public void setGame(ClientGame game) {
		this.game = game;
		characterHudTable.setPlayer(CoMClient.getInstance().getPlayer());
	}
	/**
	 * Refresh player hud. Can only be called from
	 * the OpenGL Thread
	 */
	private void refreshPlayerHud(){
		if(game != null){
			characterHudTable.setCharacter(game.getPlayerCharacter());
			characterHudTable.refreshSpellSlots();
		}
		refreshPlayerHud = false;
	}
	
	@Override
	public void create() {
		super.create();
		stage.setCamera(new OrthographicCamera());
		characterHudTable = new CharacterHud();
		pingLabel=new LabelComponent("PING");
		fpsLabel=new LabelComponent("FPS");
		timeLabel = new LabelComponent("TIME");
		
		upperRightTable = new Table();
		showTextTable = new Table();
		timeTable = new Table();
		topHolderTable = new Table();
		
		addTable(characterHudTable);
		addTable(upperRightTable);
		addTable(showTextTable);
		addTable(topHolderTable);
		
		upperRightTable.add(fpsLabel);
		upperRightTable.row();
		upperRightTable.add(pingLabel).pad(20);
		timeTable.add(timeLabel);
		topHolderTable.add(timeTable).height(35);
		
		topHolderTable.top();
		characterHudTable.bottom();
		upperRightTable.top().right();
		showTextTable.top().pad(200);
		
		timeTable.setBackground(tableBorderDrawable);
		
		//spellTable.debug();
		//table.debug();
	}
	/**
	 * Updates ping and fps labels. 
	 * Used in render() method.
	 */
	public void updateHudLabels(){
		pingLabel.setText("Ping: "+CoMClient.getInstance().getPing());
		fpsLabel.setText("Fps: "+Gdx.graphics.getFramesPerSecond());
		timeLabel.setText(game.getState().toString()+" "+getFormattedTime((int) game.getSettings()
				.getRoundTime()));
	}
	/**
	 * Returns the time in the format MM:SS.
	 * @param seconds The time in seconds to format.
	 * @return A string of the time.
	 */
	private String getFormattedTime(int seconds) {
		return String.format("%02d", seconds / 60) + ":" + 
				String.format("%02d", seconds % 60);
	}
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		if(stage.getCamera() != null){
			OrthographicCamera c = (OrthographicCamera)stage.getCamera();
			c.setToOrtho(false, stage.getWidth(),stage.getHeight());
		}
	}
	@Override
	public void render(float dTime) {
		if(refreshPlayerHud){
			refreshPlayerHud();
		}
		if(!textToShow.isEmpty() && textToShow.get(0).isTimedOut()){
			changeTextMessage();
		}
		stage.act(Gdx.graphics.getDeltaTime());
		updateHudLabels();
		stage.draw();
		Table.drawDebug(stage);
	}
	/**
	 * A class that holds a number of spellSlots. 
	 * @author robin
	 *
	 */
	private class CharacterHud extends Table {
		private SpellSlotButton[] spellButtons;
		private HealthBar healthBar;
		private Player player;
		private ICharacter playerCharacter;
		
		/**
		 * Add the hud elements to the hud that is related
		 * to the player character. Adds a Healthbar, 
		 * the spellSlotButton from player character
		 * and refreshes the hotkeys on each spellslot. 
		 */
		public CharacterHud(){
			healthBar = new HealthBar();
			spellButtons = new SpellSlotButton[8];
			//Add healthbar
			healthBar = new HealthBar();
			add(healthBar).width(800).height(50).colspan(8);
			row();
			for(int i = 0; i < spellButtons.length; i++){
				final int index = i;
				final char hotkey = (char)ClientSettings.getInstance().getKeyBinds().getUseSpell(index);
				//TODO fetch spell from player character. 
				spellButtons[i] = new SpellSlotButton(hotkey,index);
				add(spellButtons[i]).size(100);
				spellButtons[i].addListener(new ChangeListener(){
					@Override
					public void changed(ChangeEvent arg0, Actor arg1) {
						spellClicked(index);
					}
				});
			}
				
		}
		public void setPlayer(Player player){
			this.player = player;
		}
		public void setCharacter(ICharacter character){
			this.playerCharacter = character;
			healthBar.setCharacter(playerCharacter);
		}
		public void refreshSpellSlots(){
			for(int i = 0; i < spellButtons.length; i++){
				spellButtons[i].setSpellSlot(player.getSpellSlot(i));
			}			
		}
		public void spellClicked(int index){
			SpellSlotButton spellSlotButton = spellButtons[index];
			//Show spell shop
			if(player.getSpellPoints() > 0 && !spellSlotButton.hasSpellSlot()){
				perform(new Event(Action.SHOW_SPELLSHOP,index));
			}else{
				//prepare spell
				perform(new Event(Action.PREPARE_SPELL,spellSlotButton.getIndex()));
			}
		}
	}
	public void changedGameObjectsReceived(
			Map<Integer, ? extends IGameObject> gameObjects, long timeSent) {}
	public void refresh() {
		refreshPlayerHud = true;
		
	}
	public void changedPlayerReceived(Player player, long timeSent) {
		refreshPlayerHud = true;
	}
	/**
	 * Show a message on the top of the gameScreen.
	 * __________________
	 * |				|
	 * |	message		|
	 * |				|
	 * |				|
	 * |________________|
	 * @param message The message to show
	 * @param time The time the text will show, in seconds
	 */
	public void showMessage(String message, float time){
		LabelTimeoutComponent actor = new LabelTimeoutComponent(message, time);
		if(textToShow.isEmpty()){
			showTextTable.add(actor);
		}
		textToShow.add(actor);
	}
	private void changeTextMessage(){
		showTextTable.clear();
		textToShow.remove(0);
		if(!textToShow.isEmpty()){
			showTextTable.add(textToShow.get(0));
		}
	}
	/**
	 * Reset the Hud after a game
	 * Clears all shown texts. 
	 */
	public void reset() {
		for(SpellSlotButton s : characterHudTable.spellButtons){
			s.setSpellSlot(null);
		}
		characterHudTable.setPlayer(null);
		textToShow.clear();
		showTextTable.clear();
	}
}
