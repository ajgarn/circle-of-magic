package client.gui.screens.components;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public class ProgressWheel extends Table {
	private ShapeRenderer shapeRenderer;
	private float percent;
	
	public ProgressWheel(){
		this(new ShapeRenderer());
	}
	public ProgressWheel(ShapeRenderer shapeRenderer){
		super();
		setShapeRenderer(shapeRenderer);
	}
	
	public void setShapeRenderer(ShapeRenderer shapeRenderer){
		this.shapeRenderer = shapeRenderer;
	}
	@Override
	public void draw(SpriteBatch spriteBatch, float arg1) {
		shapeRenderer.setProjectionMatrix(getStage().getCamera().combined);
		float x = getX() + getParent().getX();
		float y = getY() + getParent().getY();
		spriteBatch.flush();
		shapeRenderer.begin(ShapeType.FilledTriangle);
		shapeRenderer.setColor(0, 0, 0, 0.5f);
		
		//Draw the first 1/8 of the square with 100% if it hasn't past last corner
		//Otherwise draw the first 1/8 of the square depending on how much cooldown is left
		float percentOfLength = 1 - getPercentage(1/8f, 0, percent); 
		//used to determine how much that should be rendered to the rectangle
		shapeRenderer.filledTriangle(x + getWidth() / 2, y + getHeight() / 2, 
				x + getWidth() / 2, y + getHeight(), 
				x + getWidth() / 2 * percentOfLength, y + getHeight());

		percentOfLength = 1 - getPercentage(3/8f, 1/8f, percent);		
		shapeRenderer.filledTriangle(x + getWidth() / 2, y + getHeight() / 2, 
				x, y + getHeight(),x, y + getHeight() * percentOfLength);
		
		percentOfLength = getPercentage(5/8f, 3/8f, percent);		
		shapeRenderer.filledTriangle(x + getWidth() / 2, y + getHeight() / 2, 
				x, y,x + getWidth() * percentOfLength, y);
		percentOfLength = getPercentage(7/8f, 5/8f, percent);		
		shapeRenderer.filledTriangle(x + getWidth() / 2, y + getHeight() / 2, 
				x + getWidth(), y,x + getWidth(), y + getHeight() * percentOfLength);
		percentOfLength = 1 - getPercentage(1, 7/8f, percent);		
		shapeRenderer.filledTriangle(x + getWidth() / 2, y + getHeight() / 2, 
				x + getWidth(), y + getHeight(),
				x + getWidth() / 2 + getWidth() / 2 * percentOfLength, y + getHeight());
		
		Gdx.gl10.glEnable(GL10.GL_BLEND);
//		Gdx.gl10.glDepthMask(true);
		Gdx.gl10.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		Gdx.gl10.glDisable(GL10.GL_TEXTURE_2D);
		shapeRenderer.end();
//		Gdx.gl10.glDepthMask(true);
		Gdx.gl10.glDisable(GL10.GL_BLEND);
		Gdx.gl10.glEnable(GL10.GL_TEXTURE_2D);
		super.draw(spriteBatch, arg1);
	}
	public void setPercent(float percent){
		this.percent = percent;
	}
	@Override
	protected void setStage(Stage stage) {
		super.setStage(stage);
		if(getStage() != null){
			shapeRenderer.setProjectionMatrix(getStage().getCamera().combined);
		}
	}
	private float getPercentage(float max, float min, float value){
		if(value > max) return 1;
		if(value < min) return 0;
		return (value - min) / (max - min);
	}
}
