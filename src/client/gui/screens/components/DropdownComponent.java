package client.gui.screens.components;

import services.graphics.util.ContentManager;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

/**
 * Class defining a dropdown menu box.
 * 
 * @author Alexander H�renstam
 * 
 */
public class DropdownComponent extends SelectBox {
	public DropdownComponent(Object[] objectList) {
		super(objectList, getListStyle());
	}

	private static SelectBoxStyle getListStyle() {
		ContentManager cm = ContentManager.getInstance();
		SelectBoxStyle style;
		// Font.
		BitmapFont font = cm.getFont("gui/fonts/menu.fnt", "gui/fonts/menu.png");
		TextureRegion upTR = new TextureRegion(cm.getTexture("gui/components/dropdown2.png"));
		TextureRegion overTR = new TextureRegion(cm.getTexture("gui/components/button.png"));
		TextureRegion downTR = new TextureRegion(cm.getTexture("gui/components/button.png"));
		TextureRegion selectTR = new TextureRegion(cm.getTexture("gui/components/hscrollbarknob.png"));
		TextureRegion listbgTR = new TextureRegion(cm.getTexture("gui/components/textSelection.png"));
        NinePatchDrawable up = new NinePatchDrawable(new NinePatch(upTR, 0, 0, 0, 0));
        NinePatchDrawable down = new NinePatchDrawable(new NinePatch(downTR, 0, 0, 0, 0));
        NinePatchDrawable over = new NinePatchDrawable(new NinePatch(overTR,0, 0, 0, 0));
        NinePatchDrawable select = new NinePatchDrawable(new NinePatch(selectTR, 0, 0, 0, 0));
        NinePatchDrawable listBG = new NinePatchDrawable(new NinePatch(listbgTR,0,0,0,0));
        style=new SelectBoxStyle(font,Color.BLACK,up,down,over);
        style.listSelection = select;
        style.listBackground = listBG;
		return style;
	}
}
