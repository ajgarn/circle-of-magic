package client.gui.screens.components;

import model.ICharacter;
import services.graphics.util.ContentManager;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

public class HealthBar extends ProgressBar {
	private ICharacter character;
	public HealthBar(){
		this(0.1f);
	}
	public HealthBar(float speed){
		super(speed);
		setProgressImageDrawable(new NinePatchDrawable(new NinePatch(
				ContentManager.getInstance().getTexture("gui/components/healthBar.png"), 0,0,0,0)));
		setProgressImageRemovedDrawable(new NinePatchDrawable(new NinePatch(
				ContentManager.getInstance().getTexture("gui/components/healthBarLost.png"), 0,0,0,0)));
		setBackground(new NinePatchDrawable(new NinePatch(
				ContentManager.getInstance().getTexture("gui/components/barBackground.png"), 2, 2,2,2)));
		
	}
	@Override
	public void act(float delta) {
		if(character != null){
			setPercent((float)character.getHealth() / character.getMaxHealth());
		}
		super.act(delta);
	}
	public void setCharacter(ICharacter character){
		this.character = character;
	}
}
