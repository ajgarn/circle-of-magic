package client.gui.screens.components;
/**
 * Something that can be timed out
 * @author Robin
 *
 */
public interface Timeoutable {
	/**
	 * Get whenever this Object is timed out.
	 * @return
	 */
	public boolean isTimedOut();
}
