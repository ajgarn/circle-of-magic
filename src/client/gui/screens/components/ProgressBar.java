package client.gui.screens.components;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

public class ProgressBar extends Table {
	private final float speed;
	private final Image progressImage;
	private final Image progressImageRemoved;
	private float desiredPercent = 1,actualPercent = 1;
	
	public ProgressBar(float speed){
		progressImage = new Image();
		progressImageRemoved = new Image();
		Stack stack = new Stack();
		add(stack).expand().fill();
		stack.add(progressImageRemoved);
		stack.add(progressImage);
		this.speed = speed;
	}
	public void setPercent(float percent){
		this.desiredPercent = percent;
		progressImage.setScaleX(percent > 0? (percent < 1? percent : 1): 0);
	}
	public void setProgressImageDrawable(Drawable drawable){
		progressImage.setDrawable(drawable);
	}
	public void setProgressImageRemovedDrawable(Drawable drawable){
		progressImageRemoved.setDrawable(drawable);
	}
	@Override
	public void act(float dTime) {
		float percent = actualPercent;
		progressImageRemoved.setScaleX(percent > 0? (percent < 1? percent : 1): 0);
		if(desiredPercent > actualPercent){
			actualPercent += speed * dTime;
			if(desiredPercent < actualPercent){
				actualPercent = desiredPercent;
			}
		}
		if(desiredPercent < actualPercent){
			actualPercent -= speed * dTime;
			if(desiredPercent > actualPercent){
				actualPercent = desiredPercent;
			}
		}
		super.act(dTime);
	}
}
