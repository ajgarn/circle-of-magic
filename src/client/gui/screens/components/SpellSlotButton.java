package client.gui.screens.components;

import model.spells.SpellSlot;
import services.graphics.util.ContentManager;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class SpellSlotButton extends TextButton {

	private SpellSlot spellSlot;
	private LabelComponent hotKeyLabel;
	private LabelComponent cooldownLabel;
	private ProgressWheel cooldownClockwheel;
	private int index;
	
	public SpellSlotButton(SpellSlot spellSlot,int keycode, int index) {
		super("",getTextFieldStyle(spellSlot == null? "":spellSlot.getSpellRecipe().getName().toString()));
		setSpellSlot(spellSlot);
		this.index = index;
		hotKeyLabel = new LabelComponent(getHotkey(keycode));
		cooldownLabel = new LabelComponent("",36);
		cooldownClockwheel = new ProgressWheel();
		//cooldownLabel.setFillParent(true);
		cooldownClockwheel.setFillParent(true);
		add(cooldownClockwheel).left().bottom();
		cooldownClockwheel.add(cooldownLabel).expand().center();
		cooldownClockwheel.row();
		cooldownClockwheel.add(hotKeyLabel).bottom().left();
	}
	public SpellSlotButton(int keycode, int index) {
		this(null,keycode,index);
		
	}
	public SpellSlot getSpellSlot() {
		return spellSlot;
	}
	public int getIndex(){
		return index;
	}
	public String getSpellName(){
		return spellSlot == null? "":spellSlot.getSpellRecipe().getName().toString();
	}
	/**
	 * Set the spellslot. Load graphics if the new spellslot is a new spellslot. 
	 * @param spellSlot The new Spellslot
	 */
	public void setSpellSlot(SpellSlot spellSlot) {
		if(this.spellSlot == null){
			this.spellSlot = spellSlot;
		}else if (spellSlot == null){
			this.spellSlot = null;
			cooldownClockwheel.setPercent(0);
			cooldownLabel.setText("");
		}else{
			this.spellSlot.refresh(spellSlot);
		}
		setStyle(getTextFieldStyle(getSpellName()));
	}
	/**
	 * If this spellSlotButton has a spell
	 * @return
	 */
	public boolean hasSpellSlot(){
		return spellSlot != null;
	}
	@Override
	public void act(float delta) {
		if(spellSlot != null){
			cooldownLabel.setText(spellSlot.isAvaiable()?"":(int)(spellSlot.getRemainingCooldown() + 1) + "");
			cooldownClockwheel.setPercent(spellSlot.getRemainingCooldown() / 
					spellSlot.getSpellRecipe().getCooldown());
		}
		super.act(delta);
	}
	private static TextButtonStyle getTextFieldStyle(String spellName) {
         ContentManager cm = ContentManager.getInstance();
         TextButtonStyle style = new TextButtonStyle();
         // Font.
         style.font = cm.getFont("gui/fonts/menu.fnt","gui/fonts/menu.png");
         // Color on font.
         style.fontColor = Color.WHITE;
         TextureRegion texRegionUp = new TextureRegion(cm.getTexture("gui/spellSlot/spellSlot" + spellName + ".png"), 0, 0, 256, 256);
         TextureRegion texRegionDown = new TextureRegion(cm.getTexture("gui/spellSlot/spellSlot" + spellName + ".png"), 512, 0, 256, 256);
         TextureRegion texRegionOver = new TextureRegion(cm.getTexture("gui/spellSlot/spellSlot" + spellName + ".png"), 256, 0, 256, 256);
         TextureRegion texRegionDisabled = new TextureRegion(cm.getTexture("gui/spellSlot/spellSlot" + spellName + ".png"), 768, 0, 256, 256);
         style.up = new TextureRegionDrawable(texRegionUp);
         style.down = new TextureRegionDrawable(texRegionDown);
         style.over = new TextureRegionDrawable(texRegionOver);
         style.disabled = new TextureRegionDrawable(texRegionDisabled);
         return style;
    }
	private String getHotkey(int keycode){
		switch(keycode){
			case Keys.Q : return "Q"; 
			case Keys.W : return "W"; 
			case Keys.E : return "E"; 
			case Keys.R : return "R"; 
			case Keys.A : return "A"; 
			case Keys.S : return "S"; 
			case Keys.D : return "D"; 
			case Keys.F : return "F"; 
			default: return "";
			
			//TODO:Add more keys or fix a more elegant way
		}
	}

}
