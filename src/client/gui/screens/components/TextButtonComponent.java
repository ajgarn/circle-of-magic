package client.gui.screens.components;


import services.graphics.util.ContentManager;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

/**
 * Class defining a TextField.
 * 
 * @author Alexander H�renstam
 * 
 */
public class TextButtonComponent extends TextButton{

        public TextButtonComponent(String text) {
                super(text,getTextFieldStyle());
        }

        private static TextButtonStyle getTextFieldStyle() {
            ContentManager cm = ContentManager.getInstance();
            TextButtonStyle style = new TextButtonStyle();
            // Font.
            style.font = cm.getFont("gui/fonts/menu.fnt","gui/fonts/menu.png");
            // Color on font.
            style.fontColor = Color.WHITE;
            TextureRegion texRegionUp = new TextureRegion(cm.getTexture("gui/components/button.png"), 0, 0, 128, 128);
            TextureRegion texRegionDown = new TextureRegion(cm.getTexture("gui/components/button.png"), 256, 0, 128, 128);
            TextureRegion texRegionOver = new TextureRegion(cm.getTexture("gui/components/button.png"), 128, 0, 128, 128);
            style.up = new NinePatchDrawable(new NinePatch(texRegionUp, 16, 16, 16, 16));
            style.down = new NinePatchDrawable(new NinePatch(texRegionDown, 16, 16, 16, 16));
            style.over = new NinePatchDrawable(new NinePatch(texRegionOver, 16, 16, 16, 16));
            return style;
        }
}