package client.gui.screens.components;

import services.graphics.util.ContentManager;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox.CheckBoxStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * Class defining a TextField.
 * 
 * @author Alexander H�renstam
 * 
 */
public class CheckBoxComponent extends Button{

	public CheckBoxComponent() {
		super(getTextFieldStyle()); 
	}
	public CheckBoxComponent(String s){
		this();
		this.add(s);
	}

	private static CheckBoxStyle getTextFieldStyle() {
		ContentManager cm = ContentManager.getInstance();
		CheckBoxStyle checkBoxStyle = new CheckBoxStyle();
		// Color on font.
		//style.fontColor = Color.WHITE;
		checkBoxStyle.up = new TextureRegionDrawable(new TextureRegion(cm.getTexture("gui/components/checkBoxUp.png")));
		checkBoxStyle.down = new TextureRegionDrawable(new TextureRegion(cm.getTexture("gui/components/checkBoxDown.png")));
		checkBoxStyle.checked=new TextureRegionDrawable(new TextureRegion(cm.getTexture("gui/components/checkBoxChecked.png")));
		checkBoxStyle.checkedFontColor = Color.GREEN;
		checkBoxStyle.checkedOverFontColor = Color.GREEN;
		checkBoxStyle.downFontColor = Color.ORANGE;
		return checkBoxStyle;
	}
}
