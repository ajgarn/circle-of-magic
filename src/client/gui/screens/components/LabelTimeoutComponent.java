package client.gui.screens.components;

import services.graphics.util.ContentManager;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
/**
 * This is a label that times out after 
 * some time. Used by GameScreen when
 * showing message on the screen.
 * @author robin
 *
 */
public class LabelTimeoutComponent extends Label implements Timeoutable{
	private float timeLeft;
	public LabelTimeoutComponent(CharSequence text, float time){
		super(text,new LabelStyle(ContentManager.getInstance().getFont(
						"gui/fonts/gameMessageFont.fnt", 
						"gui/fonts/gameMessageFont.png"), 
						new Color(Color.WHITE)));
		timeLeft = time;
	}
	@Override
	public void act(float delta) {
		timeLeft -= delta;
		if(timeLeft <= 0){
			getStyle().fontColor.a-= .02;
		}
		super.act(delta);
	}
	@Override
	public boolean isTimedOut() {
		return getStyle().fontColor.a <= 0.06;
	}

}
