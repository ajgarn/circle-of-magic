package client.gui.screens.components;

import model.Player;
import services.graphics.util.ContentManager;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

/**
 * Class defining a component used for a player in a lobby. 
 * Extending TextButtonComponent.
 * 
 * @author Alexander H�renstam
 * 
 */
public class LobbyPlayerButtonComponent extends ButtonComponent {
	ContentManager cm;
	
	public LobbyPlayerButtonComponent(Player player) {
		this(player,false);
	}
	public LobbyPlayerButtonComponent(Player player, boolean isOwner) {
		super();
		cm=ContentManager.getInstance();
		if(isOwner){
			Image image = new Image(new NinePatch(cm.getTexture("gui/lobbyOwnerCrown.png")));
			this.add(image).width(60).height(60);
			this.row();
		}
		this.add(new LabelComponent(player.getName()));
		this.row();
		LabelComponent colorLabel = new LabelComponent(player.getColorName().toString());
		colorLabel.setColor(player.getColorName().getColor());
		this.add(colorLabel);
		this.row();
		this.add(new LabelComponent(player.getStatus().toString()));
	}
}