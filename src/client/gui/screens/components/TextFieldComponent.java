package client.gui.screens.components;

import services.graphics.util.ContentManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * Class defining a TextField.
 * 
 * @author Alexander H�renstam
 * 
 */
public class TextFieldComponent extends TextField {

	/**
	 * Texture for white picture, text field.
	 */
	private final static Texture textureTextField = new Texture(
			Gdx.files.internal("res/gui/components/textField.png"));

	public TextFieldComponent() {
		this("");
	}

	public TextFieldComponent(String text) {
		super(text, getTextFieldStyle());
	}

	private static TextFieldStyle getTextFieldStyle() {
		ContentManager cm = ContentManager.getInstance();
		TextFieldStyle textFieldStyle = new TextFieldStyle();
		// Font.
		textFieldStyle.font = cm.getFont("gui/fonts/text.fnt","gui/fonts/text.png");;
		// Color on font.
		textFieldStyle.fontColor = Color.BLACK;
		// Color on text field.
		textFieldStyle.background = new TextureRegionDrawable(
				new TextureRegion(textureTextField, 0, 0, 256, 64));
		// Color when you mark text.
		textFieldStyle.selection = new TextureRegionDrawable(new TextureRegion(
				cm.getTexture("gui/components/textSelection.png")));
		//Cursor blipper
		textFieldStyle.cursor = new TextureRegionDrawable(new TextureRegion(
				cm.getTexture("gui/components/textSelection.png"),1,1));
		return textFieldStyle;
	}
}
