package client.gui.screens.components;

import services.graphics.util.ContentManager;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

/**
 * Class defining a Label.
 * 
 * @author Alexander H�renstam
 * 
 */
public class LabelComponent extends Label{
	/**
	 * Standard constructor, taking a text as parameter.
	 * @param text
	 */
	public LabelComponent(CharSequence text) {
		super(text,new LabelStyle(
				ContentManager.getInstance().getFont(
						"gui/fonts/text.fnt", 
						"gui/fonts/text.png"), 
						new Color(Color.WHITE)));
	}
	public LabelComponent(CharSequence text, int size) {
		super(text,new LabelStyle(
				ContentManager.getInstance().getFont(
						"gui/fonts/text" + size + ".fnt", 
						"gui/fonts/text" + size + ".png"), 
						new Color(Color.WHITE)));
	}
}
