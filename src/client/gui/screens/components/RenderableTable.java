package client.gui.screens.components;

import services.graphics.renderables.IRenderable2D;
import services.math.IVector2;
import client.graphics.IWorldCamera;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
/**
 * This class is Table but can be handled as a IRenderable2D object.
 * Used to attach a Table to IRenderable objects. 
 * @author Robin
 *
 */
public class RenderableTable extends Table implements IRenderable2D{
	
	private final IWorldCamera camera;
	private boolean alive = true;
	private Vector3 position;
		
	public RenderableTable(IWorldCamera camera){
		this(camera, new Vector3(0,0,0));
	}
	public RenderableTable(IWorldCamera camera, Vector3 position){
		this.camera = camera;
		this.position = position;
		
	}
	@Override
	public void kill() {
		alive = false;
	}

	@Override
	public boolean isDead() {
		return !alive;
	}
	
	public void movePosition(IVector2 pos, float percent) {
		position.x = (position.x * (1 - percent) + pos.getX() * percent);
		position.y = (position.y * (1 - percent) + pos.getY() * percent);
	}
	
	@Override
	public void setPosition(IVector2 pos) {
		movePosition(pos, 1);	
	}
	@Override
	public void setAngle(float angle) {}

	@Override
	public void update(float dTime) {
		super.act(dTime);
		IVector2 screenPosition = camera.getScreenPosition(position.cpy());
		this.setX(screenPosition.getX());
		this.setY(screenPosition.getY());
	}

	@Override
	public void render(SpriteBatch batch) {
		super.draw(batch,1);
		
	}

}
