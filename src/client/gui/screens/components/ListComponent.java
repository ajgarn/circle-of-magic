package client.gui.screens.components;

import services.graphics.util.ContentManager;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * Class defining a TextField.
 * 
 * @author Alexander H�renstam
 * 
 */
public class ListComponent extends List{

	public ListComponent(Object[] objectList) {
		super(objectList,getListStyle());
	}

	private static ListStyle getListStyle() {
		ContentManager cm = ContentManager.getInstance();
		ListStyle style = new ListStyle();
		// Font.
		style.font = cm.getFont("gui/fonts/menu.fnt","gui/fonts/menu.png");
		// Color on font.
		style.selection = new TextureRegionDrawable(
				new TextureRegion(cm.getTexture("gui/components/button.png"), 256,
				0, 128, 128));
		style.fontColorSelected = Color.WHITE;
		style.fontColorUnselected = Color.ORANGE;
		return style;
	}
}
