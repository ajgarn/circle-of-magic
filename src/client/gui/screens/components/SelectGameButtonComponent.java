package client.gui.screens.components;

import model.GameHeader;

/**
 * Class defining a component used for showing available games. Extending
 * TextButton.
 * 
 * @author Alexander H�renstam
 * 
 */
public class SelectGameButtonComponent extends TextButtonComponent {
	private GameHeader gameHeader;
	
	public SelectGameButtonComponent(GameHeader gameHeader) {
		super(gameHeader.getName());
		sourceIpLabel = new LabelComponent("");
		amountOfPlayersLabel = new LabelComponent("");
		this.add(sourceIpLabel);
		this.row();
		this.add(amountOfPlayersLabel);
		setGameHeader(gameHeader);
	}
	LabelComponent sourceIpLabel;

	public String getSourceIpLabel() {
		return sourceIpLabel.getText().toString();
	}

	public void setSourceIpLabel(String text) {
		sourceIpLabel.setText(text);
	}

	LabelComponent amountOfPlayersLabel;

	public String getAmountOfPlayersLabel() {
		return amountOfPlayersLabel.getText().toString();
	}

	public void setAmountOfPlayersLabel(String text) {
		amountOfPlayersLabel.setText(text);
	}

	public GameHeader getGameHeader() {
		return gameHeader;
	}

	public void setGameHeader(GameHeader gameHeader) {
		this.gameHeader = gameHeader;
		super.setText(gameHeader.getName());
		sourceIpLabel.setText(gameHeader.getHost().getHostName());
		amountOfPlayersLabel.setText(gameHeader.getPlayers().size() + " players");
	}
}