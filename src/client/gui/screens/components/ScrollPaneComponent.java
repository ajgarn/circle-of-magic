package client.gui.screens.components;

import services.graphics.util.ContentManager;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

/**
 * Class defining a ScrollPane.
 * 
 * @author Alexander H�renstam
 * 
 */
public class ScrollPaneComponent extends ScrollPane{
	/**
	 * Requires a Table as a parameter.
	 * @param table
	 */
	public ScrollPaneComponent(Table table) {
		super(table);
		this.setStyle(getScrollPaneStyle());
	}
	private static ScrollPaneStyle getScrollPaneStyle() {
        ContentManager cm = ContentManager.getInstance();
        NinePatchDrawable vscroll = new NinePatchDrawable(new NinePatch((cm.getTexture("gui/components/vscroll.png"))));
        NinePatchDrawable vscrollbarknob = new NinePatchDrawable(new NinePatch(cm.getTexture("gui/components/vscrollbarknob.png")));
        NinePatchDrawable hscroll = new NinePatchDrawable(new NinePatch(cm.getTexture("gui/components/hscroll.png")));
        NinePatchDrawable hscrollbarknob = new NinePatchDrawable(new NinePatch(cm.getTexture("gui/components/hscrollbarknob.png")));
        NinePatchDrawable scrollbarbg = new NinePatchDrawable(new NinePatch(cm.getTexture("gui/components/scrollbarbg.png")));
        ScrollPaneStyle style = new ScrollPaneStyle(scrollbarbg,hscroll,hscrollbarknob,vscroll,vscrollbarknob);
        return style;
    }
	//TODO add implementation for the scrollbar's texture.
}
