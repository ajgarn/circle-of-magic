package client.gui;

import java.util.LinkedList;
import java.util.List;

/**
 * Stores the navigation history of the GUI screens. Uses the
 * {@link ApplicationState} for each screen.
 * @author Anton Jansson
 */
class StateHistory {
	private int currentIndex = -1;
	private List<ApplicationState> history;
	
	public StateHistory() {
		history = new LinkedList<ApplicationState>();
	}
	
	/**
	 * Go back in the history one step, changing the application state.
	 * @return The new {@link ApplicationState}.
	 */
	public ApplicationState back() {
		return back(1);
	}
	
	/**
	 * Go back in the history.
	 * @param steps The number of steps to go back.
	 * @return The new {@link ApplicationState}.
	 */
	public ApplicationState back(int steps) {
		setIndex(currentIndex - steps);
		return getCurrent();
	}

	/**
	 * Clear the history.
	 */
	public void clearHistory() {
		history.clear();
		currentIndex = -1;
	}
	
	/**
	 * Go forward in the history one step, changing the application state.
	 * @return The new {@link ApplicationState}.
	 */
	public ApplicationState forward() {
		return forward(1);
	}
	
	/**
	 * Go forward in the history.
	 * @param steps The number of steps to go forward.
	 * @return The new {@link ApplicationState}.
	 */
	public ApplicationState forward(int steps) {
		setIndex(currentIndex + steps);
		return getCurrent();
	}
	
	/**
	 * Get the current state.
	 * @return The {@link ApplicationState} for the current screen or 
	 * <code>null</code> if there are no states in the history.
	 */
	public ApplicationState getCurrent() {
		return (currentIndex >= 0) ? history.get(currentIndex) : null;
	}
	
	/**
	 * Sets the new current state.
	 * @param state The new state.
	 */
	public void set(ApplicationState state) {
		if (currentIndex < 0 || getCurrent() != state) {
			int size = history.size();
			currentIndex++;
			for (int i = 0; i < size - currentIndex; i++) {
				history.remove(currentIndex);
			}
			history.add(state);
		}
	}
	
	private void setIndex(int index) {
		if (index < 0) {
			currentIndex = 0;
		} else if (index >= history.size()) {
			currentIndex = history.size() - 1;
		} else {
			currentIndex = index;
		}
	}
}
