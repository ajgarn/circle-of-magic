package client.gui;

/**
 * Enums for each application state.
 * 
 * @author Alexander H�renstam.
 *
 */
public enum ApplicationState {
	LOGIN,MAIN_MENU,GAMEPLAY,OPTIONS,JOINLOBBY,HOSTLOBBY,SELECTGAME,ABOUT,SPLASHSCREEN,CREATELOBBY,LOADING,STATISTICS,LAN,ONLINE;
}
