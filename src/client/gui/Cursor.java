package client.gui;

import java.nio.IntBuffer;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Mouse;

import com.badlogic.gdx.graphics.Pixmap;
 
/**
 * 
 * @author Robin Gronberg
 * @author Alexander H�renstam
 *
 */
public class Cursor{
	org.lwjgl.input.Cursor cursor;
	/**
	   * Creates a non-animated native cursor (Desktop only). 
	   * <p>
	   * Notes: For maximum portability use a cursor size of 32x32 pixels. Only use alpha values 0x00 and 0xff as 
	   * translucent cursor images are not supported on all platforms.
	   * 
	   * @param xHotspot  The x hotspot location for the cursor.
	   * @param yHotspot  The y hotspot location for the cursor.
	   * @param image  The cursor image.
	   * @return  The cursor or null if not supported.
	   */
	  public Cursor(int xHotspot, int yHotspot, Pixmap image) {
	    this(xHotspot, yHotspot, new Pixmap[] { image }, null);
	  }

	  /**
	   * Creates a native cursor (Desktop only). 
	   * <p>
	   * Notes: For maximum portability use a cursor size of 32x32 pixels. Only use alpha values 0x00 and 0xff as 
	   * translucent cursor images are not supported on all platforms.
	   * 
	   * @param xHotspot  The x hotspot location for the cursor.
	   * @param yHotspot  The y hotspot location for the cursor.
	   * @param images  The cursor images. Supply more than 1 image for non-animated cursors. All images need to have
	   *                the same size.
	   * @param delays  If more than one image is supplied (animated cursor), give the image delays in milliseconds. 
	   *                Note that the size of the delays array will need to much the number of images supplied.
	   * @return  The cursor or null if not supported.
	   */
	  public Cursor(int xHotspot, int yHotspot, Pixmap[] images, int[] delays) {
	    // create the image buffer
	    int w = images[0].getWidth();
	    int h = images[0].getHeight();
	    IntBuffer bufImages = IntBuffer.allocate(images.length * w * h);
	    for (int i = 0; i < images.length; i++) {
	      for (int y = (h - 1); y >= 0; y--) {
	        for (int x = 0; x < w; x++) {
	          int rgba = images[i].getPixel(x, y);
	          int argb = (rgba >>> 8) | (rgba << 24);
	          bufImages.put(argb);
	        }
	      }
	    }
	    bufImages.position(0);
	    
	    // create the delay buffer if we have an animated cursor (delays != null)
	    IntBuffer bufDelays = null;
	    if (delays != null) {
	      bufDelays = IntBuffer.wrap(delays);
	      bufDelays.position(0);
	    }
	    
	    // create & return the cursor object (y is reversed under LWJGL!)
	    org.lwjgl.input.Cursor cursor = null;
	    try {
	      cursor = new org.lwjgl.input.Cursor(w, h, xHotspot, h - yHotspot, images.length, bufImages, bufDelays);
	    }
	    catch (LWJGLException e) {
	      //Gdx.app.error("Cursor", "Cannot create cursor via LWJGL.", e);
	    }
	    this.cursor = cursor;
	  }
	  
	  /**
	   * Displays this cursor as the active one.
	   * 
	   * @param cursor  The cursor.
	   */
	  public void displayCursor() {
	    try {
	      Mouse.setNativeCursor(cursor);
	    }
	    catch (LWJGLException e) {
	    }
	  }
}