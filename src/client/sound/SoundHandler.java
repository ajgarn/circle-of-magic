package client.sound;

import model.core.SpellNames;
import client.model.ClientSettings;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class SoundHandler {
	private Music mainMenuMusic;
	private Music gameplayMusic;
	private Sound fireballSound;
	private static SoundHandler soundHandler;
	private SoundHandler(){
		mainMenuMusic=Gdx.audio.newMusic(Gdx.files.internal("res/gui/sound/Dexter_Britain_-_01_-_The_Time_To_Run_Finale.mp3"));
		gameplayMusic=Gdx.audio.newMusic(Gdx.files.internal("res/gui/sound/AnalogByNature_-_Analog_By_Nature_-_C_Mon.mp3"));
		fireballSound=Gdx.audio.newSound(Gdx.files.internal("res/gui/sound/fireballsound.mp3"));
	}
	public static SoundHandler getInstance(){
		if(soundHandler==null){
			soundHandler=new SoundHandler();
		}
		return soundHandler;
	}
	public void getMusic(){
		//TODO missing implementation.
	}
	/**
	 * Stops gameplay music if it is playing and starts playing menu music.
	 * But, only if ClientSettings hasn't muted it and it already isn't playing.
	 */
	public void playMainMenuMusic(){
		gameplayMusic.stop();
		if(!mainMenuMusic.isPlaying() && ClientSettings.getInstance().getIsMenuAudioMuted()==false){
			mainMenuMusic.setVolume(ClientSettings.getInstance().getMenuAudioLevel());
			mainMenuMusic.setLooping(true);
			mainMenuMusic.play();
		}
		else if(ClientSettings.getInstance().getIsMenuAudioMuted()==true && mainMenuMusic.isPlaying()){
			mainMenuMusic.stop();
		}
	}
	/**
	 * Stops main menu music if it is playing and starts playing gameplay music.
	 * But, only if ClientSettings hasn't muted it and it already isn't playing.
	 */
	public void playGameplayMusic(){
		mainMenuMusic.stop();
		if(!gameplayMusic.isPlaying() && !ClientSettings.getInstance().getIsGameplayAudioMuted()) {
			gameplayMusic.setVolume(ClientSettings.getInstance().getGameplayAudioLevel());
			gameplayMusic.play();
		}
		else if(ClientSettings.getInstance().getIsMenuAudioMuted()==true){
			gameplayMusic.stop();
		}
	}
	private void playFireballSound(){
		fireballSound.play(ClientSettings.getInstance().getGameplaySoundLevel());
	}
	public void playSpellSound(SpellNames spell){
		switch(spell){
			case FIREBALL:
				playFireballSound();
			break;
		default:
				//System.out.println("That spell doesn't have a sound yet");
			break;
		}
	}
}
