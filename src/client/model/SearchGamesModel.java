package client.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import model.GameHeader;

/**
 * Searches for available games to join and stores this data for easy access.
 * Used by SelectGameScreen and controllers.
 * @author Anton Jansson
 */
public class SearchGamesModel {

	private static SearchGamesModel instance;
	
	private Collection<GameHeader> games;
	private AvailableGamesThread searchGamesThread;
	private String hostname;
	
	private SearchGamesModel() {
		games = new HashSet<GameHeader>();
	}

	/**
	 * Get the best lobby for the current player to join, i.e. the lobby 
	 * with the most joined players.
	 * @param hostname The host where to search.
	 * @return The best lobby or <code>null</code> if there are no lobbies found.
	 */
	public GameHeader findBestGame(String hostname) {
		List<GameHeader> games = new ArrayList<GameHeader>(
				CoMClient.getInstance().getAvailableGames(hostname));
		if (!games.isEmpty()) {
			// Get the lobby with most joined players.
			Collections.sort(games, new Comparator<GameHeader>() {
				@Override
				public int compare(GameHeader g1, GameHeader g2) {
					return g2.getPlayers().size() - g1.getPlayers().size();
				}
			});
			return games.get(0);
		}
		return null;
	}

	/**
	 * Get available games that has been retrieved by the searching thread.
	 * <p><b>Note</b>: For this collection to be filled, {@link #start()} must
	 * be called to begin the search.
	 * @return A collection of GameHeaders.
	 * @see #start()
	 * @see #setHostname(String)
	 * @see #setLAN()
	 */
	public synchronized Collection<GameHeader> getAvailableGames() {
		return new HashSet<GameHeader>(games);
	}
	
	public static synchronized SearchGamesModel getInstance() {
		if (instance == null) {
			instance = new SearchGamesModel();
		}
		return instance;
	}
	
	/**
	 * Get game within this list of available games.
	 * @param hashCode The hash code of the game.
	 * @return The game or <code>null</code> if no match is found.
	 */
	public synchronized GameHeader getGameHeader(int hashCode){
		for(GameHeader game: games){
			if(game.hashCode()==hashCode){
				return game;
			}
		}
		return null;
	}
	
	private synchronized void setGames(Collection<GameHeader> gameHeaders) {
		games.clear();
		games.addAll(gameHeaders);
	}
	
	/**
	 * Set searching for available games on the specified hostname.
	 * If hostname is <code>null</code> the thread will search on LAN.
	 * This method does not start the search thread.
	 * <p>The collection of available games is cleared if hostname is
	 * different from before.
	 * @param hostname The host where to search for games.
	 * @see #setLAN()
	 * @see #start()
	 */
	public synchronized void setHostname(String hostname) {
		if (this.hostname != null && !this.hostname.equals(hostname)
				|| hostname != null && !hostname.equals(this.hostname)) {
			games.clear();
		}
		this.hostname = hostname;
	}
	
	/**
	 * Set searching for available games on LAN.
	 * This method does not start the search thread.
	 * @see #setHostname(String)
	 * @see #start()
	 */
	public synchronized void setLAN() {
		setHostname(null);
	}
	
	/**
	 * Start the searching thread.
	 * <p>Set where to search by using the methods
	 * {@link #setHostname(String)} and {@link #setLAN()}.
	 * @see #stop()
	 */
	public synchronized void start() {
		searchGamesThread = new AvailableGamesThread();
		searchGamesThread.start();
	}
	
	/**
	 * Stops the running search thread.
	 * @see #start()
	 */
	public synchronized void stop() {
		searchGamesThread = null;
	}
	
	
	/**
	 * Thread that searches for available games and updates
	 * {@link SearchGamesModel}s games.
	 * @author Anton Jansson
	 */
	private class AvailableGamesThread extends Thread {
		
		/**
		 * Start a new thread searching for available games.
		 */
		public AvailableGamesThread() {
			super();
			setDaemon(true);
		}
		
		@Override
		public void run() {
			CoMClient client = CoMClient.getInstance();
			while (searchGamesThread == Thread.currentThread()) {
				if (hostname != null) {
					setGames(client.getAvailableGames(hostname));	// Search on host.
				} else {
					setGames(client.getAvailableGames());			// Search on LAN.
				}
				try {
					Thread.sleep(0);		// TODO: Save constant: how much time to wait before searching again.
				} catch (InterruptedException e) {
					return; 
				}
			}
		}
	}
}
