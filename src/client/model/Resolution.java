package client.model;

import java.awt.GraphicsEnvironment;
import java.util.ArrayList;
import java.util.List;

/**
 * A class that keeps track of different screen resolutions. 
 * Used by OptionsScreen to determine what resolutions
 * to show up when changing
 * @author Robin
 *
 */
public class Resolution {
	private int width;
	private int height;
	private static final Resolution screenResolution = new Resolution(
			GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getWidth(),
			GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getHeight());
	private static final Resolution[] allResolutions = new Resolution[]{
		new Resolution(640, 480),
		new Resolution(800, 600),		
		new Resolution(1024, 600),		
		new Resolution(1024, 768),		
		new Resolution(1280, 720),		
		new Resolution(1280, 800),		
		new Resolution(1280, 1024),			
		new Resolution(1366, 768),			
		new Resolution(1400, 1050),		
		new Resolution(1440, 900),		
		new Resolution(1600, 900),		
		new Resolution(1600, 1200),		
		new Resolution(1680, 1050),		
		new Resolution(1920, 1080),		
		new Resolution(1920, 1200),		
		new Resolution(2048, 1152),		
		new Resolution(2560, 1440),		
		new Resolution(2560, 1600),		
		new Resolution(2880, 1800),		
	};
	
	public Resolution(int width, int height){
		setHeight(height);
		setWidth(width);
	}
	
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public float getAspectRatio(){
		return (float)width / height;
	}
	public static List<Resolution> getSupportedResolutions(){
		ArrayList<Resolution> resolutions = new ArrayList<Resolution>();
		for(Resolution r: allResolutions){
			if(r.getAspectRatio() == screenResolution.getAspectRatio() &&
					r.getWidth() <= screenResolution.getWidth() &&
					r.getHeight() <= screenResolution.getHeight()){
				resolutions.add(r);
			}
		}
		if (!resolutions.contains(screenResolution)) {
			resolutions.add(screenResolution);
		}
		return resolutions;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		Resolution r = (Resolution) obj;
		return height == r.height && width == r.width;
	}
	@Override
	public int hashCode() {
		return 13 * height + 19 * width;
	}
	@Override
	public String toString() {
		return width + "x" + height;
	}
}
