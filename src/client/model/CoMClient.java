package client.model;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Collection;

import model.AbstractGameSettings;
import model.Account;
import model.GameHeader;
import model.Lobby;
import model.Player;
import model.core.Constants;
import model.core.IAction;
import server.network.IVersionControlService;
import services.logger.Log;
import services.network.ClientNetHandler;
import services.network.DisconnectRequest;
import services.network.IClassNetworkReceivable;
import services.network.IClientNetHandler;
import client.network.ClientNetReceiver;
import client.network.IClientNetworkListener;

import com.esotericsoftware.kryonet.rmi.TimeoutException;
import common.network.holders.JoinLobbyHolder;

/**
 * Client for Circle of Magic. Has a {@link ClientNetHandler} and handles events sent by clients.
 * @author Anton Jansson
 */
public class CoMClient {
	
	private static CoMClient instance;

	private final ClientNetReceiver netHandler;		// TODO: Change to IClientNetHandler?
	private Account account;

	private long lastPing;
	
	private CoMClient() {
		netHandler = ClientNetReceiver.getInstance();
	}
	
	public static synchronized CoMClient getInstance() {
		if (instance == null) {
			instance = new CoMClient();
		}
		return instance;
	}
	
	/**
	 * Tries to authenticate the user on the server.
	 * @param username The username.
	 * @param password The password.
	 * @return Whether the user is authenticated or not.
	 */
	public boolean authenticate(String username, String password) {
		setAccount(netHandler.authenticate(username, password));
		return account != null && !account.isLANAccount();
	}

	/**
	 * Sets the account to <code>null</code>.
	 */
	private void clearAccount() {
		account = null;
	}

	/**
	 * Sets the current account to a not authenticated account,
	 * allowed for LAN playing. If the client tries to connect 
	 * to an online lobby the server will require further
	 * authentication.
	 */
	public void createLANAccount(String name) {
		setAccount(new Account(-1, name));
	}

	/**
	 * Create a lobby on a specific server.
	 * @param title The title of the game.
	 * @param settings The settings for the game.
	 * @param hostname The host where the server should be created.
	 */
	public void createLobby(String title, AbstractGameSettings settings, 
			String hostname) {
		try {
			netHandler.connectHost(hostname);
			if (!netHandler.isOnline()) {
				setAccount(netHandler.addAccount(account));
			}
			Lobby lobby = new Lobby(new GameHeader(-1, title, null), account, 
					settings);
			// TODO: Remove line below..?
			//sendJoinLobby(lobby.getHeader());		// Add player to connections on server.
			sendLobby(lobby);
		} catch (IOException e) {}
	}
	
	/**
	 * Gets the games for all available hosts in LAN. 
	 * If no hosts are found the list of games is empty.
	 * @return The collection of games, empty if there are no games.
	 */
	public Collection<GameHeader> getAvailableGames() {
		return netHandler.getAvailableGames();
	}
	/**
	 * Gets the games for a specific host.
	 * @param host IP address of host.
	 * @return The collection of games, empty if there are no games.
	 */
	public Collection<GameHeader> getAvailableGames(InetAddress host) {
		return netHandler.getAvailableGames(host);
	}
	/**
	 * Gets the games for a specific host.
	 * @param hostname Host address as a string.
	 * @return The collection of games, empty if there are no games.
	 */
	public Collection<GameHeader> getAvailableGames(String hostname) {
		return netHandler.getAvailableGames(hostname);
	}
	
	/**
	 * Returns the HolderReceivable to register holder receivers.
	 * @return An IHolderReceivable.
	 */
	public IClassNetworkReceivable<IClientNetworkListener<?>> getNetworkReceivable() {
		return netHandler;
	}
	
	/**
	 * Returns the last ping response time for the current connection.
	 * <p><b>Note</b>: The ping is automatically refreshed while calling this method,
	 * but the response is not given immediately.
	 * @return The ping time in milliseconds.
	 * @see #refreshPing()
	 */
	public int getPing() {
		refreshPing();
		return netHandler.getPing();
	}
	
	/**
	 * Get the player this client should control.
	 * @return The current player this client is controlling. 
	 */
	public Player getPlayer(){
		return account.getPlayer();
	}
	
	/**
	 * Returns an object from the remote connection.
	 * 
	 * @param objectID The ID of the remote object.
	 * @param iface The type of the remote object as an interface.
	 * @return The remote object
	 * @throws TimeoutException
	 * 
	 * @see {@link IClientNetHandler#getRemoteObject(int, Class)}
	 */
	public <T> T getRemoteObject(int objectID, Class<T> iface) {
		// TODO: Handle TimeoutException
		return netHandler.getRemoteObject(objectID, iface);
	}
	
	/**
	 * Connects this client to a specific lobby on the server.
	 * <p><i>If the client is not connected to the host that is
	 * the owner of the game, it will be automatically connected.</i>
	 * @param gameHeader The game lobby to connect to.
	 */
	public void joinLobby(GameHeader gameHeader) {
		try {
			netHandler.connectHost(gameHeader.getHost());
			if (!netHandler.isOnline()) {
				setAccount(netHandler.addAccount(account));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}
		sendJoinLobby(gameHeader);
	}

	/**
	 * Clears the current account and sends a disconnect request 
	 * to the server, removing the active connection from the 
	 * server if specified.
	 * @param notifyServer If a disconnect request should be sent 
	 * to the server, to remove it from active connections.
	 */
	public void logout(boolean notifyServer) {
		clearAccount();
		if (notifyServer) {
			try {
				netHandler.connectHost(Constants.ONLINE_SERVER_HOSTNAME);
				netHandler.send(new DisconnectRequest());
			} catch (IOException e) {}
		}
	}
	
	/**
	 * Update the ping response time by sending a ping request to 
	 * the current connection.
	 * @see #getPing()
	 */
	private void refreshPing() {
		long now = System.currentTimeMillis();
		if (now >= lastPing + Constants.PING_INTERVAL) {
			netHandler.updatePing();
			lastPing = now;
		}
	}
	
	/**
	 * Returns whether the client must be updated or not.
	 * @return <code>true</code> if the client must be updated, <code>false</code>
	 * otherwise.
	 */
	public boolean requiresUpdate() {
		try {
			netHandler.connectHost(Constants.ONLINE_SERVER_HOSTNAME);
			IVersionControlService service = netHandler.getRemoteObject(Constants.IDs.VERSION_CONTROL_SERVICE, IVersionControlService.class);
			return service.requiresUpdate(Constants.CLIENT_VERSION);
		} catch (IOException e) {
		} catch (TimeoutException e) {
			Log.fatal("Cannot check version due to VersionControlService timeout.", e);
		}
		return false;
	}
	
	/**
	 * Resets the player so its as it was before any game
	 */
	public void resetPlayer(){
		account.getPlayer().reset();
	}
	
	/**
	 * Sends an object to the server.
	 * @param object The object to send to the server.
	 */
	public <T> void send(T object) {
		netHandler.send(object);
	}

	/**
	 * Sends an action to the connected server host.
	 * 
	 * <p>If no connection is active the action is dismissed.
	 * @param action The action to be sent.
	 */
	public void sendAction(IAction action) {
		if (action != null) {
			send(action);
		}
	}
	
	/**
	 * Send the JoinLobbyHolder to the connected host.
	 * @param gameHeader The GameHeader to send in the holder.
	 */
	private void sendJoinLobby(GameHeader gameHeader) {
		send(new JoinLobbyHolder(gameHeader));
	}
	
	/**
	 * Send a lobby to update it on the server. Used for updating settings
	 * on a lobby. If current player not is owner of the lobby this call is
	 * dismissed by the server.
	 * @param lobby The lobby to update.
	 * @see #createLobby(String, AbstractGameSettings, String)
	 */
	public void sendLobby(Lobby lobby) {
		send(lobby);
	}
	
	/**
	 * Sets the account if the argument not is <code>null</code>.
	 * @param account The new account.
	 */
	private void setAccount(Account account) {
		if (account != null) {
			this.account = account;
		}
	}

	/**
	 * Update the player this client should control.
	 * @param players All players in the active game.
	 */
	public void updateCurrentPlayer(Collection<Player> players) {
		for (Player p : players) {
			if (p.equals(account.getPlayer())) {
				account.setPlayer(p);
				break;
			}
		}
	}
}
