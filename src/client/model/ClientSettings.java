package client.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import model.core.Vector2;



import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;

public class ClientSettings implements Serializable {
	public static ClientSettings getInstance() {
		if (instance == null) {
			instance = new ClientSettings();
		}
		return instance;
	}

	private ClientSettings() {
		screenWidthGame = Resolution.getSupportedResolutions().get(0).getWidth();
		screenHeightGame = Resolution.getSupportedResolutions().get(0).getHeight();
		resolution = new Vector2(screenWidthGame,screenHeightGame);
	}

	private static ClientSettings instance;
	private static final long serialVersionUID = 1L;

	private boolean fullscreenGui = false;
	private boolean fullscreenGameplay = false;
	private boolean muteMenuAudio=true;
	private boolean muteGameplayAudio=true;
	private boolean disableMoveCameraAtEdges;
	private transient Vector2 resolution;
	private float menuAudioLevel = 0.4f;
	private float gameplayAudioLevel = 0.5f;
	private float gameplaySoundLevel = 0.6f;
	private int screenWidthGame = 800;
	private int screenHeightGame = 600;
	private int screenWidthGui = 800;
	private int screenHeightGui = 600;
	
	private KeyBinds keyBinds = new KeyBinds();

	public void setFullscreenGui(boolean b) {
		fullscreenGui = b;
	}

	/**
	 * @return the screenWidthGame
	 */
	public int getScreenWidthGame() {
		return screenWidthGame;
	}

	/**
	 * @param screenWidthGame
	 *            the screenWidthGame to set
	 */
	public void setScreenWidthGame(int screenWidthGame) {
		this.screenWidthGame = screenWidthGame;
	}

	/**
	 * @return the screenHeightGame
	 */
	public int getScreenHeightGame() {
		return screenHeightGame;
	}

	/**
	 * @param screenHeightGame
	 *            the screenHeightGame to set
	 */
	public void setScreenHeightGame(int screenHeightGame) {
		this.screenHeightGame = screenHeightGame;
	}

	/**
	 * @return the screenWidthGui
	 */
	public int getScreenWidthGui() {
		return screenWidthGui;
	}

	/**
	 * @param screenWidthGui
	 *            the screenWidthGui to set
	 */
	public void setScreenWidthGui(int screenWidthGui) {
		this.screenWidthGui = screenWidthGui;
	}

	/**
	 * @return the screenHeightGui
	 */
	public int getScreenHeightGui() {
		return screenHeightGui;
	}

	/**
	 * @param screenHeightGui
	 *            the screenHeightGui to set
	 */
	public void setScreenHeightGui(int screenHeightGui) {
		this.screenHeightGui = screenHeightGui;
	}

	public boolean getFullscreenGui() {
		return fullscreenGui;
	}

	public void setFullscreenGameplay(boolean b) {
		fullscreenGameplay = b;
	}

	public boolean getFullscreenGameplay() {
		return fullscreenGameplay;
	}

	/**
	 * Sets volume level of the gameplay audio. Must be in the scale [0, 1].
	 * 
	 * @param i
	 */
	public void setGameplayAudioLevel(float i) {
		if (i <= 1 && i > 0) {
			gameplayAudioLevel = i;
		}
	}

	/**
	 * Sets volume level of the gameplay audio. Will be in the scale [0, 1].
	 * 
	 * @return
	 */
	public float getGameplayAudioLevel() {
		return gameplayAudioLevel;
	}

	/**
	 * Sets volume level of the menu audio. Must be in the scale [0, 1].
	 * 
	 * @param i
	 */
	public void setMenuAudioLevel(float i) {
		if (i <= 1 && i > 0) {
			menuAudioLevel = i;
		}
	}

	/**
	 * Sets volume level of the menu audio. Will be in the scale [0, 1].
	 * 
	 * @return
	 */
	public float getMenuAudioLevel() {
		return menuAudioLevel;
	}

	/**
	 * @return the gameplaySoundLevel
	 */
	public float getGameplaySoundLevel() {
		return gameplaySoundLevel;
	}

	/**
	 * @param gameplaySoundLevel the gameplaySoundLevel to set
	 */
	public void setGameplaySoundLevel(float gameplaySoundLevel) {
		if (gameplaySoundLevel <= 1 && gameplaySoundLevel > 0) {
			this.gameplaySoundLevel = gameplaySoundLevel;
		}
	}

	/**
	 * Mute the gameplay audio.
	 * 
	 * @param b
	 */
	public void setGameplayAudioMuted(boolean b) {
		muteGameplayAudio = b;
	}

	/**
	 * Returns whether the gameplay audio is muted.
	 * 
	 * @return
	 */
	public boolean getIsGameplayAudioMuted() {
		return muteGameplayAudio;
	}

	/**
	 * Mute the menu audio.
	 * 
	 * @param b
	 */
	public void setMenuAudioMuted(boolean b) {
		muteMenuAudio = b;
	}

	/**
	 * Returns whether the menu audio is muted.
	 * 
	 * @return
	 */
	public boolean getIsMenuAudioMuted() {
		return muteMenuAudio;
	}

	public KeyBinds getKeyBinds() {
		return keyBinds;
	}

	public boolean isDisableMoveCameraAtEdges() {
		return disableMoveCameraAtEdges;
	}

	public void setDisableMoveCameraAtEdges(boolean disableMoveCameraAtEdges) {
		this.disableMoveCameraAtEdges = disableMoveCameraAtEdges;
	}

	public class KeyBinds implements Serializable {
		private static final long serialVersionUID = -6919099015532446955L;
		public int getUseSpell(int slot) {
			return useSpell[slot];
		}


		public void setUseSpell(int useSpell,int slot) {
			this.useSpell[slot] = useSpell;
		}


		public int getMoveCameraUp() {
			return moveCameraUp;
		}


		public void setMoveCameraUp(int moveCameraUp) {
			this.moveCameraUp = moveCameraUp;
		}


		public int getMoveCameraDown() {
			return moveCameraDown;
		}


		public void setMoveCameraDown(int moveCameraDown) {
			this.moveCameraDown = moveCameraDown;
		}


		public int getMoveCameraLeft() {
			return moveCameraLeft;
		}


		public void setMoveCameraLeft(int moveCameraLeft) {
			this.moveCameraLeft = moveCameraLeft;
		}


		public int getMoveCameraRight() {
			return moveCameraRight;
		}


		public void setMoveCameraRight(int moveCameraRight) {
			this.moveCameraRight = moveCameraRight;
		}
		private final transient Map<Integer, Event> keyBinds;
		
		private int[] useSpell = new int[8];{
		useSpell[0] = Keys.Q;
		useSpell[1] = Keys.W;
		useSpell[2] = Keys.E;
		useSpell[3] = Keys.R;
		useSpell[4] = Keys.A;
		useSpell[5] = Keys.S;
		useSpell[6] = Keys.D;
		useSpell[7] = Keys.F;}
		
		private int moveCameraUp = Keys.UP;
		private int moveCameraDown = Keys.DOWN;
		private int moveCameraLeft = Keys.LEFT;
		private int moveCameraRight = Keys.RIGHT;
		
		private int useButton = Buttons.LEFT;
		private int moveButton = Buttons.RIGHT;
		
		private int centerCameraAtCharacterHold = Keys.SPACE;
		private int centerCameraAtCharacterToggle = Keys.Y;
		
		private int showGameStatistics = Keys.TAB;
		private int showGameMenu = Keys.ESCAPE;
		private int showSpellShop = Keys.P;
		
		private KeyBinds() {
			keyBinds = new HashMap<Integer, Event>();
			updateKeybindings();
		}


		public Event getEvent(int keycode) {
			return keyBinds.get(keycode);
		}
		/**
		 * Rebind all keys with its proper Events;
		 */
		public void updateKeybindings(){
			keyBinds.clear();
			keyBinds.put(useSpell[0], new Event(Action.PREPARE_SPELL, 0));
			keyBinds.put(useSpell[1], new Event(Action.PREPARE_SPELL, 1));
			keyBinds.put(useSpell[2], new Event(Action.PREPARE_SPELL, 2));
			keyBinds.put(useSpell[3], new Event(Action.PREPARE_SPELL, 3));
			keyBinds.put(useSpell[4], new Event(Action.PREPARE_SPELL, 4));
			keyBinds.put(useSpell[5], new Event(Action.PREPARE_SPELL, 5));
			keyBinds.put(useSpell[6], new Event(Action.PREPARE_SPELL, 6));
			keyBinds.put(useSpell[7], new Event(Action.PREPARE_SPELL, 7));
			
			keyBinds.put(moveCameraUp, new Event(Action.MOVE_CAMERA, 
					new Vector2(0,-1)));
			keyBinds.put(moveCameraDown, new Event(Action.MOVE_CAMERA, 
					new Vector2(0,1)));
			keyBinds.put(moveCameraRight, new Event(Action.MOVE_CAMERA, 
					new Vector2(1,0)));
			keyBinds.put(moveCameraLeft, new Event(Action.MOVE_CAMERA, 
					new Vector2(-1,0)));
			
			keyBinds.put(useButton, new Event(Action.USE_CLICKED));
			keyBinds.put(moveButton, new Event(Action.MOVE_CHARACTER));
			keyBinds.put(centerCameraAtCharacterHold, new Event(Action.CENTER_CAMERA_CHARACTER_HOLD));
			keyBinds.put(centerCameraAtCharacterToggle, new Event(Action.CENTER_CAMERA_CHARACTER_TOGGLE));
			keyBinds.put(showGameStatistics, new Event(Action.SHOW_GAME_STATISTICS));
			keyBinds.put(showGameMenu, new Event(Action.CLOSE_OVERLAY));
			keyBinds.put(showSpellShop, new Event(Action.SHOW_SPELLSHOP,-1));
		}
	}
	public Vector2 getResolution(){
		return resolution;
	}
}
