package client.model;

public class Timer{
	private long start;
	private long stop;
	private boolean running;
	public Timer(){
	}
	public void start(){
		start = System.nanoTime();
		running=true;
	}
	public void stop(){
		stop = System.nanoTime();
		running=false;
	}
	/**
	 * Returns time elapsed since start() method was used. 
	 * @return elapsed time in nano sec.
	 */
	public long getElapsedTime(){
		long elapsed;
		if(running){
			elapsed=System.nanoTime()-start;
		}
		else{
			elapsed=stop-start;
		}
		return elapsed;
	}
	/**
	 * Returns time elapsed since start() method was used. 
	 * @return elapsed time in seconds.
	 */
	public long getElapsedTimeSecs(){
		return this.getElapsedTime()/1000000000;
	}
}
