package client.model;




public class Event {
	public Event( Action action){
		this(action,null);
	}
	public Event(Action action,Object value){
		this.action = action;
		this.value = value;
	}
	private Action action;
	private Object value;
	public Action getAction(){
		return action;
	}
	public Object getValue(){
		return value;
	}
}
