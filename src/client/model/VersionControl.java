package client.model;

import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import model.core.Constants;
import services.logger.Log;

/**
 * Verifies the currently installed client version and checks for updates.
 * 
 * @author Anton Jansson
 */
public class VersionControl {

	/**
	 * Checks for updates at the server. Displays a message if the application
	 * needs an update. This method can exit this application process.
	 */
	public static void checkVersion() {
		if (CoMClient.getInstance().requiresUpdate()) {
			// TODO: Add message with link to download latest client version.
        	Log.warning("Client needs to be updated. Current version: "
					+ Constants.CLIENT_VERSION);
        	try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (ClassNotFoundException e) {
			} catch (InstantiationException e) {
			} catch (IllegalAccessException e) {
			} catch (UnsupportedLookAndFeelException e) {}
        	
			int result = JOptionPane.showConfirmDialog(null,
					"Please update your Circle of Magic application." +
					" (OK has currently no effect)",
					"Update is required", JOptionPane.OK_CANCEL_OPTION,
					JOptionPane.INFORMATION_MESSAGE);
			
        	switch (result) {
			case JOptionPane.OK_OPTION:
				// TODO: Update
				break;
			case JOptionPane.CANCEL_OPTION:
				System.exit(0);
			}
		} else {
			Log.info("Checking for updates: No update found (current version: "
					+ Constants.CLIENT_VERSION + ")");
		}
	}
}
