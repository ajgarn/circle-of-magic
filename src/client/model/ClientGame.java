package client.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import model.AbstractGame;
import model.AbstractGameSettings;
import model.GameHeader;
import model.ICharacter;
import model.Player;
import model.World;
import model.core.IAction;
import model.core.IGameObject;
import model.spells.CastSpellAction;
import model.spells.SpellSlot;

import client.proxy.ProxyGameObject;
import client.proxy.ProxyObjectFactory;

/**
 * A client representation of a game, extension of {@link AbstractGame}.
 * Refreshes the world when a new world is received from the server.
 * @author Anton Jansson
 */
public class ClientGame extends AbstractGame {
	/**
	 * Used to remember what spell to use when clicked. If prepared spell is null
	 * no spell is prepared
	 */
	private SpellSlot preparedSpell = null;
	/** Holds the time each GameObjects last was updated. */
	private final Map<Integer, Long> gameObjectsUpdatedTime;
	private final CoMClient client = CoMClient.getInstance();

	/**
	 * @see AbstractGame#AbstractGame(GameHeader)
	 */
	public ClientGame(GameHeader header) {
		super(header);
		gameObjectsUpdatedTime = new HashMap<Integer, Long>();
		client.updateCurrentPlayer(header.getPlayers());
	}
	
	/**
	 * Adds an proxy object to the world.
	 * @param index The index of the game object.
	 * @param gameObject The game obect to add an proxy of.
	 */
	private void addProxyObject(int index, IGameObject gameObject) {
		ProxyGameObject proxy = ProxyObjectFactory.create(gameObject, getWorld());
		getWorld().setGameObject(index, proxy);
	}
	
	/**
	 * Returns the character of the current player.
	 */
	public ICharacter getPlayerCharacter(){
		return (ICharacter) getWorld().getGameObject(client.getPlayer().getCharacterID());
	}
	
	/**
	 * Returns the prepared spell or <code>null</code> if no spell is prepared.
	 */
	public SpellSlot getPreparedSpell() {
		return preparedSpell;
	}
	
	/**
	 * Adds or updates the specified game object as its proxy.
	 * @param index The index of the game object.
	 * @param receivedObject The received game obect.
	 */
	private void receivedGameObject(int index, IGameObject receivedObject) {
		IGameObject oldProxy = getWorld().getGameObject(index);
		if (oldProxy == null) {
			addProxyObject(index, receivedObject);
		} else {
			refreshProxyObject(index, receivedObject, oldProxy);
		}
	}

	public void receivedGameObjects(Map<Integer, ? extends IGameObject> gameObjects, long timeSent) {
		for (Integer index : gameObjects.keySet()) {
			if (gameObjectsUpdatedTime.get(index) == null
					|| timeSent > gameObjectsUpdatedTime.get(index)) {
				receivedGameObject(index, gameObjects.get(index));
				gameObjectsUpdatedTime.put(index, timeSent);
			}
		}
	}
	
	public void receivedPlayer(Player player, long timeSent){
		for (Player p : getPlayers()) {
			if (p.equals(player)) {
				p.refresh(player);
				break;
			}
		}
	}
	
	public void receivedPlayers(Collection<Player> players, float timeSent){
		getHeader().setPlayers(players);
	}
	
	public void receivedWorld(World world, Collection<Player> players, GameState state, 
			AbstractGameSettings settings, long timeSent) {
		setWorld(new World());
		setState(state);
		setSettings(settings);
		receivedGameObjects(world.getGameObjectsMap(), timeSent);
		gameObjectsUpdatedTime.clear();
		setPreparedSpell(null);
		getHeader().setPlayers(players);
		client.updateCurrentPlayer(getPlayers());
	}
	
	/**
	 * Refreshes the specified proxy with the values from the received game object.
	 * @param index The index of the game object.
	 * @param receivedObject The received game obect.
	 * @param oldProxy The proxy for the received game obect, which is in the world.
	 */
	private void refreshProxyObject(int index, IGameObject receivedObject,
			IGameObject oldProxy) {
		oldProxy.refresh(receivedObject);
		// Refresh spellslots on CastSpellActions if any.
		Player p = getPlayer(index);
		if (p != null) {
			for (IAction action : oldProxy.getActions()) {
				if (action instanceof CastSpellAction) {
					((CastSpellAction) action).updateSpellSlot(p.getSpellSlots());
				}
			}
		}
	}

	/**
	 * Sets the prepared spell.
	 * @param spell The spell to set as prepared.
	 */
	public void setPreparedSpell(SpellSlot spell) {
		this.preparedSpell = spell;
	}
	
	@Override
	public void update(float dTime) {
		for(Player p : getPlayers()){
			p.update(dTime);
		}
		super.update(dTime);
	}
}
