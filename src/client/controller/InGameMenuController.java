package client.controller;

import model.Player.Status;
import client.gui.ApplicationFrame;
import client.gui.ApplicationState;
import client.gui.screens.GUIListener;
import client.gui.screens.InGameMenuScreen;
import client.model.CoMClient;
import client.model.Event;

public class InGameMenuController implements GUIListener{
	private ApplicationFrame applicationFrame;
	private InGameMenuScreen view;
	public InGameMenuController(ApplicationFrame a,InGameMenuScreen view) {
		this.view = view;
		this.applicationFrame = a;
		view.addGuiListener(this);
	}

	@Override
	public void actionPerformed(Event event) {
		switch(event.getAction()){
		case EXIT_CLICKED:
			CoMClient.getInstance().send(Status.DISCONNECTED);
			applicationFrame.exit();
			break;
		case BACK_CLICKED:
			CoMClient.getInstance().send(Status.DISCONNECTED);
			applicationFrame.clearHistory();
			applicationFrame.changeApplicationState(ApplicationState.MAIN_MENU);
			break;
		case SCREEN_SHOWED:
		case SCREEN_HID:
			break;
			default:
				//throw new IllegalArgumentException("State " + event.getAction() + " is not supported");
		}
	}

}
