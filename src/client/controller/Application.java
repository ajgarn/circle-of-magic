package client.controller;

import common.application.UncaughtExceptionLogger;
import common.application.WebServices;

import services.logger.ConsoleLogger;
import services.logger.Log;
import services.logger.WebLogger;
import services.logger.Log.Level;
import services.network.DisconnectRequest;
import services.security.Base64AESCrypter;
import client.gui.ApplicationFrame;
import client.gui.ApplicationState;
import client.gui.screens.SplashScreenDesktop;
import client.model.CoMClient;
import client.model.VersionControl;
import client.network.IClientNetworkListener;

public class Application {

	private static Application app;
	private ApplicationFrame appFrame;
	
	public Application() {
        // Show SplashScreen first
        SplashScreenDesktop.getInstance().showSplash();
        
        // Add logging to client.
		Log.addLogger(new ConsoleLogger());
		Log.addLogger(new WebLogger(Level.INFO, "Client", WebServices.LOGGING,
				1000, new Base64AESCrypter(WebServices.Security.SECRET_KEY,
						WebServices.Security.IV)));
        
		// Log uncaught exceptions
        Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionLogger());
        
        VersionControl.checkVersion();
        
        //Load app resources
		appFrame = ApplicationFrame.getInstance();//new ApplicationFrame();
		new GameScreenController(appFrame);
		new HostLobbyController(appFrame);
		new JoinLobbyController(appFrame);
		new LoginController(appFrame);
		new MainMenuController(appFrame);
		new OptionsController(appFrame);
		new SelectGameController(appFrame);
		//new SplashScreenController(appFrame);
		new AboutScreenController(appFrame);
		new CreateLobbyController(appFrame);
		new LoadingScreenController(appFrame);
		new StatisticsScreenController(appFrame);
		new OnlineScreenController(appFrame);
		new LanScreenController(appFrame);

		// Add listeners
		CoMClient.getInstance().getNetworkReceivable().addListener(DisconnectRequest.class,
				logoutListener);

		startApp();
	}
	
	// TODO: Class is not singleton, due to public constructor! Should it be? //Anton
	public static synchronized Application getInstance(){
		if(app==null){
			app=new Application();
		}
		return app;
	}
	
	/**
	 * Logs out the user, changing the screen to LOGIN as well as 
	 * clearing the navigation history.
	 */
	private void logout() {
		CoMClient.getInstance().logout(false);
		appFrame.clearHistory();
		appFrame.changeApplicationState(ApplicationState.LOGIN);
	}
	
	public void startApp(){
		appFrame.start();
	}
	
	
	/**
	 * Listener for making the server able to force client logout.
	 */
	private IClientNetworkListener<DisconnectRequest> logoutListener =
			new IClientNetworkListener<DisconnectRequest>() {
		@Override
		public void networkReceived(DisconnectRequest object, long timeSent) {
			logout();
		}
	};
}
