package client.controller;

import client.gui.ApplicationFrame;
import client.gui.ApplicationState;
import client.gui.screens.SplashScreenAndroid;
import client.model.Event;

public class SplashScreenController extends AbstractScreenController {

	public SplashScreenController(ApplicationFrame a) {
		super(a);
		view = new SplashScreenAndroid();
		a.addGUIView(view, ApplicationState.SPLASHSCREEN);
		view.addGuiListener(this);
	}

	private SplashScreenAndroid view;

	@Override
	public void actionPerformed(Event event) {
		switch(event.getAction()){
		case NEXT_CLICKED:
			applicationFrame.changeApplicationState(ApplicationState.LOGIN);
			break;
		case SCREEN_SHOWED:
			//Testing if version number on client is same as on server.
			//if(VersionHandler.VERSIONCONSTANT==0.3){
			//}
			break;
		case SCREEN_HID:
			break;
			default:
				throw new IllegalArgumentException("State " + event.getAction() + " is not supported");
		}
	}

}
