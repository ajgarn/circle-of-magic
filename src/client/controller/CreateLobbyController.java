package client.controller;

import model.AbstractGameSettings;
import client.gui.ApplicationFrame;
import client.gui.ApplicationState;
import client.gui.screens.CreateLobbyScreen;
import client.gui.screens.GUIListener;
import client.model.CoMClient;
import client.model.Event;

public class CreateLobbyController extends AbstractScreenController implements GUIListener {
	public CreateLobbyController(ApplicationFrame a) {
		super(a);
		view = new CreateLobbyScreen();
		a.addGUIView(view, ApplicationState.CREATELOBBY);
		view.addGuiListener(this);
	}

	private CreateLobbyScreen view;
	private String hostname;

	@Override
	public void actionPerformed(Event event) {
		switch(event.getAction()){
		case BACK_CLICKED:
			applicationFrame.back();
			break;
		case NEXT_CLICKED:
			Object[] value = (Object[]) event.getValue();
			String gameTitle = (String) value[0];
			AbstractGameSettings gameMode = (AbstractGameSettings) value[1];
			CoMClient client = CoMClient.getInstance();
			client.createLobby(gameTitle, gameMode, hostname);
			applicationFrame.changeApplicationState(ApplicationState.HOSTLOBBY);
			break;
		case SCREEN_SHOWED:
			hostname = (String) event.getValue();
		case SCREEN_HID:
			break;
			default:
				throw new IllegalArgumentException("State " + event.getAction() + " is not supported");
		}
	}

}
