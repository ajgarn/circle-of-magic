package client.controller;

import model.Lobby;
import model.Player;
import model.Player.Status;
import client.gui.ApplicationFrame;
import client.gui.ApplicationState;
import client.gui.screens.AbstractLobbyScreen;
import client.gui.screens.HostLobbyScreen;
import client.model.CoMClient;
import client.model.Event;
/**
 * The controller that controls the Host lobby screen.
 * Used by Application.
 * @author Alexander Härenstam
 */
public class HostLobbyController extends AbstractLobbyController {
	private HostLobbyScreen screen;
	public HostLobbyController(ApplicationFrame a) {
		super(a);
		screen = new HostLobbyScreen();
		a.addGUIView(screen, ApplicationState.HOSTLOBBY);
		screen.addGuiListener(this);
	}

	@Override
	public void actionPerformed(Event event) {
		CoMClient client = CoMClient.getInstance();
		switch(event.getAction()){
		case BACK_CLICKED:
			client.send(Status.DISCONNECTED);
			applicationFrame.back(2);
			break;
		case START_GAME_CLICKED:
			Lobby lobby = screen.getLobby();
			lobby.loadGame();
			client.sendLobby(lobby);
			break;
		case REMOVE_PLAYER:
			Player player = (Player) event.getValue();
			lobby = screen.getLobby();
			lobby.getPlayers().remove(player);
			client.sendLobby(lobby);
			break;
		default:
			super.actionPerformed(event);
		}
	}

	@Override
	public AbstractLobbyScreen getScreen() {
		return screen;
	}
	@Override
	protected void lobbyReceived() {
		super.lobbyReceived();
		if(CoMClient.getInstance().getPlayer().getStatus()!=Status.READY){
			client.send(Status.READY);
		}
	}
}
