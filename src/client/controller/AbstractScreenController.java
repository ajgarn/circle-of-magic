package client.controller;

import client.gui.ApplicationFrame;
import client.gui.screens.GUIListener;

/**
 * An abstract controller for a GUIView. It holds the applicationFrame
 * @author Robin Gronberg
 * Used by Application
 */
public abstract class AbstractScreenController implements GUIListener {
	protected ApplicationFrame applicationFrame;
	public AbstractScreenController(ApplicationFrame a){
		applicationFrame = a;
	}
	
}
