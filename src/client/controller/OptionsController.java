package client.controller;

import client.gui.ApplicationFrame;
import client.gui.ApplicationState;
import client.gui.screens.OptionsScreen;
import client.model.ClientSettings;
import client.model.Event;
import client.sound.SoundHandler;

public class OptionsController extends AbstractScreenController {

	public OptionsController(ApplicationFrame a) {
		super(a);
		view = new OptionsScreen();
		a.addGUIView(view, ApplicationState.OPTIONS);
		view.addGuiListener(this);
	}

	private OptionsScreen view;

	@Override
	public void actionPerformed(Event event) {
		ClientSettings settings = ClientSettings.getInstance();
		switch(event.getAction()){
		case OPTIONS_FULLSCREEN_GUI_CLICKED:
			settings.setFullscreenGui(!settings.getFullscreenGui());
			break;
		case OPTIONS_FULLSCREEN_GAME_CLICKED:
			settings.setFullscreenGameplay(!settings.getFullscreenGameplay());
			break;
		case OPTIONS_GAMEPLAY_MUSIC_CLICKED:
			settings.setGameplayAudioMuted(!settings.getIsGameplayAudioMuted());
			//SoundHandler.getInstance().playGameplayMusic();
			break;
		case OPTIONS_MENU_MUSIC_CLICKED:
			settings.setMenuAudioMuted((!settings.getIsMenuAudioMuted()));
			SoundHandler.getInstance().playMainMenuMusic();
			break;
		case BACK_CLICKED:
			applicationFrame.back();
			applicationFrame.validate();
			break;
		case SCREEN_SHOWED:
		case SCREEN_HID:
			break;
			default:
				throw new IllegalArgumentException("State " + event.getAction() + " is not supported");
		}
	}

}
