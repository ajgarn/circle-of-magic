package client.controller;

import client.gui.ApplicationFrame;
import client.gui.ApplicationState;
import client.gui.screens.MainMenuScreen;
import client.model.Event;

public class MainMenuController extends AbstractScreenController {

	public MainMenuController(ApplicationFrame a) {
		super(a);
		view = new MainMenuScreen();
		a.addGUIView(view, ApplicationState.MAIN_MENU);
		view.addGuiListener(this);
	}

	private MainMenuScreen view;

	@Override
	public void actionPerformed(Event event) {
		switch(event.getAction()){
		case ONLINE_CLICKED:
			applicationFrame.changeApplicationState(ApplicationState.ONLINE);
			break;
		case LAN_CLICKED:
			applicationFrame.changeApplicationState(ApplicationState.LAN);
			break;
		case OPTIONS_CLICKED:
			applicationFrame.changeApplicationState(ApplicationState.OPTIONS);
			break;
		case ABOUT_CLICKED:
			applicationFrame.changeApplicationState(ApplicationState.ABOUT);
			break;
		case EXIT_CLICKED:
			applicationFrame.exit();
			break;
		case BACK_CLICKED:
			applicationFrame.back();
			break;
		case SCREEN_SHOWED:
		case SCREEN_HID:
			break;
			default:
				throw new IllegalArgumentException("State " + event.getAction() + " is not supported");
		}
	}

}
