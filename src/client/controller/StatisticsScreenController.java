package client.controller;

import java.util.Collection;

import common.network.holders.PlayersHolder;

import model.Player;
import client.gui.ApplicationFrame;
import client.gui.ApplicationState;
import client.gui.screens.StatisticsScreen;
import client.model.CoMClient;
import client.model.Event;
import client.network.IClientNetworkListener;

public class StatisticsScreenController extends AbstractScreenController
		implements IClientNetworkListener<PlayersHolder> {

	public StatisticsScreenController(ApplicationFrame a) {
		super(a);
		view = new StatisticsScreen();
		a.addGUIView(view, ApplicationState.STATISTICS);
		view.addGuiListener(this);
	}

	private StatisticsScreen view;

	@SuppressWarnings("unchecked")
	@Override
	public void actionPerformed(Event event) {
		switch(event.getAction()){
		case MAIN_MENU_CLICKED:
			applicationFrame.clearHistory();
			applicationFrame.changeApplicationState(ApplicationState.MAIN_MENU);
			CoMClient.getInstance().resetPlayer();
			break;
		case SCREEN_SHOWED:
			CoMClient.getInstance().getNetworkReceivable()
					.addListener(PlayersHolder.class, this);
			view.refreshPlayerStatistics((Collection<Player>) event.getValue());
			break;
		case SCREEN_HID:
			CoMClient.getInstance().getNetworkReceivable()
					.removeListener(PlayersHolder.class, this);
			break;
			default:
				throw new IllegalArgumentException("State " + event.getAction() + " is not supported");
		}
	}

	@Override
	public void networkReceived(PlayersHolder holder, long timeSent) {
		view.refreshPlayerStatistics(holder.get());
	}

}
