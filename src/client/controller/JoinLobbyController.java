package client.controller;

import model.GameHeader;
import model.Player.Status;
import client.gui.ApplicationFrame;
import client.gui.ApplicationState;
import client.gui.screens.AbstractLobbyScreen;
import client.gui.screens.JoinLobbyScreen;
import client.model.CoMClient;
import client.model.Event;

public class JoinLobbyController extends AbstractLobbyController {

	private JoinLobbyScreen screen;
	public JoinLobbyController(ApplicationFrame a) {
		super(a);
		screen = new JoinLobbyScreen();
		a.addGUIView(screen, ApplicationState.JOINLOBBY);
		screen.addGuiListener(this);
	}

	@Override
	public void actionPerformed(Event event) {
		CoMClient client = CoMClient.getInstance();
		switch(event.getAction()){
		case READY_CLICKED:
			Status status = (client.getPlayer().getStatus() != Status.READY) ? 
					Status.READY : Status.WAITING;
			client.send(status);
			break;
		case BACK_CLICKED:
			client.send(Status.DISCONNECTED);
			applicationFrame.back();
			break;
		case SCREEN_SHOWED:
			super.actionPerformed(event);
			GameHeader game = (GameHeader) event.getValue();
			client.joinLobby(game);
			break;
			default:
				super.actionPerformed(event);
		}
	}
	@Override
	protected void lobbyReceived() {
		super.lobbyReceived();
		screen.updateReadyButton();
	}

	@Override
	public AbstractLobbyScreen getScreen() {
		return screen;
	}
}
