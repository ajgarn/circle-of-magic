package client.controller;

import model.GameHeader;
import model.core.Constants;
import client.gui.ApplicationFrame;
import client.gui.ApplicationState;
import client.gui.screens.OnlineScreen;
import client.model.Action;
import client.model.Event;
import client.model.SearchGamesModel;

public class OnlineScreenController extends AbstractScreenController {

	public OnlineScreenController(ApplicationFrame a) {
		super(a);
		view = new OnlineScreen();
		a.addGUIView(view, ApplicationState.ONLINE);
		view.addGuiListener(this);
	}

	private OnlineScreen view;

	@Override
	public void actionPerformed(Event event) {
		switch(event.getAction()){
		case FIND_GAME:
			quickMatch();
			break;
		case JOIN_GAME_CLICKED:
			SearchGamesModel.getInstance().setHostname(Constants.ONLINE_SERVER_HOSTNAME);
			applicationFrame.changeApplicationState(ApplicationState.SELECTGAME);
			break;
		case HOST_GAME_CLICKED:
			applicationFrame.changeApplicationState(ApplicationState.CREATELOBBY,
					new Event(null, Constants.ONLINE_SERVER_HOSTNAME));
			break;
		case OPTIONS_CLICKED:
			applicationFrame.changeApplicationState(ApplicationState.OPTIONS);
			break;
		case ABOUT_CLICKED:
			applicationFrame.changeApplicationState(ApplicationState.ABOUT);
			break;
		case EXIT_CLICKED:
			applicationFrame.exit();
			break;
		case BACK_CLICKED:
			applicationFrame.back();
			break;
		case SCREEN_SHOWED:
			break;
		case SCREEN_HID:
			break;
			default:
				throw new IllegalArgumentException("State " + event.getAction() + " is not supported");
		}
	}

	private void quickMatch() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				GameHeader game = SearchGamesModel.getInstance()
						.findBestGame(Constants.ONLINE_SERVER_HOSTNAME);
				if (game != null) {
					applicationFrame.changeApplicationState(ApplicationState.JOINLOBBY, 
							new Event(null, game));
				} else {
					// Create new lobby.
					actionPerformed(new Event(Action.HOST_GAME_CLICKED));
				}
			}
		}).start();
	}
}
