package client.controller;

import client.gui.ApplicationFrame;
import client.gui.ApplicationState;
import client.gui.screens.LoginScreen;
import client.model.CoMClient;
import client.model.Event;

public class LoginController extends AbstractScreenController {

	public LoginController(ApplicationFrame a) {
		super(a);
		view = new LoginScreen();
		a.addGUIView(view, ApplicationState.LOGIN);
		view.addGuiListener(this);
	}

	private LoginScreen view;

	@Override
	public void actionPerformed(Event event) {
		switch(event.getAction()){
		case NEXT_CLICKED:
			Object[] login = (Object[]) event.getValue();
			login((String) login[0], (String) login[1], (boolean) login[2]);
			break;
		case SCREEN_SHOWED:
		case SCREEN_HID:
			break;
			default:
				throw new IllegalArgumentException("State " + event.getAction() + " is not supported");
		}
	}
	
	private void login(String username, String password, boolean lanMode) {
		CoMClient client = CoMClient.getInstance();
		if (lanMode) {		// LAN - Do not authenticate user.
			client.createLANAccount(username);
			nextScreen();
		} else {			// Online - Authenticate user.
			if (client.authenticate(username, password)) {	
				nextScreen();
			} else {
				// TODO: Print error message (failed to login).
				System.out.println("Failed to login.");
			}
		}
	}
	
	private void nextScreen() {
		applicationFrame.changeApplicationState(ApplicationState.MAIN_MENU);
	}

}
