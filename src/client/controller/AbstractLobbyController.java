package client.controller;

import model.Lobby;
import model.Player.Status;
import client.gui.ApplicationFrame;
import client.gui.ApplicationState;
import client.gui.screens.AbstractLobbyScreen;
import client.gui.screens.GUIListener;
import client.model.CoMClient;
import client.model.Event;
import client.network.IClientNetworkListener;
/**
 * Parent controller class for Lobby. Used in GUI.
 * @author Alexander H�renstam.
 *
 */
public abstract class AbstractLobbyController extends AbstractScreenController 
		implements GUIListener {
	
	public AbstractLobbyController(ApplicationFrame a) {
		super(a);
	}

	//protected AbstractLobbyScreen view;
	protected CoMClient client = CoMClient.getInstance();

	@Override
	public void actionPerformed(Event event) {
		switch(event.getAction()){
		case BACK_CLICKED:
			client.send(Status.DISCONNECTED);
			break;
		case SCREEN_SHOWED:
			client.getNetworkReceivable().addListener(Lobby.class, lobbyListener);
			break;
		case SCREEN_HID:
			client.getNetworkReceivable().removeListener(Lobby.class, lobbyListener);
			break;
		case REMOVE_PLAYER:
			break;
		default:
			throw new IllegalArgumentException("State " + event.getAction() + " is not supported");
		}
	}
	/**
	 * Method used for updating readyButton's color.
	 */
	protected void lobbyReceived(){
	}
	
	public abstract AbstractLobbyScreen getScreen();
	
	private IClientNetworkListener<Lobby> lobbyListener = new IClientNetworkListener<Lobby>() {		
		private long lastSent;
		
		@Override
		public void networkReceived(Lobby lobby, long timeSent) {
			if (timeSent > lastSent) {
				lastSent = timeSent;
				
				client.updateCurrentPlayer(lobby.getPlayers());
				if (!lobby.getPlayers().contains(client.getPlayer())) {
					applicationFrame.back();
				}
				getScreen().setLobby(lobby);
				if (lobby.isLoading()) {
					applicationFrame.changeApplicationState(ApplicationState.LOADING, 
							new Event(null, lobby));
				}
				lobbyReceived();
			}
		}
	};
}
