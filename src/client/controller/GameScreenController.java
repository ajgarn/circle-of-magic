package client.controller;

import model.Player;
import model.SpellShop;
import model.core.IAction;
import model.core.MoveAction;
import model.core.Vector2;
import model.spells.CastSpellAction;
import model.spells.CastSpellActionFactory;
import model.spells.SpellSlot;
import services.math.IVector2;
import client.graphics.GameObjectViewFactory;
import client.graphics.IWorldCamera;
import client.gui.ApplicationFrame;
import client.gui.ApplicationState;
import client.gui.CursorType;
import client.gui.screens.GameScreen;
import client.gui.screens.InGameMenuScreen;
import client.gui.screens.InGameStatisticsScreen;
import client.gui.screens.SpellShopView;
import client.model.ClientGame;
import client.model.ClientSettings;
import client.model.CoMClient;
import client.model.Event;
import client.network.IClientNetworkListener;
import client.sound.SoundHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import common.network.holders.GameObjectsHolder;
import common.network.holders.PlayersHolder;
import common.network.holders.WorldHolder;

public class GameScreenController extends AbstractScreenController {
	private GameScreen view;
	private CoMClient handler;
	private ClientGame game;
	private InputController inputController;

	public GameScreenController(ApplicationFrame a) {
		super(a);
		view = new GameScreen();
		inputController = new InputController();
		a.addGUIView(view, ApplicationState.GAMEPLAY);
		view.addGuiListener(this);
	}

	/**
	 * When something has happened in the GameScreen. Call this method so that
	 * this GameController knows what has happened in on the screen
	 * 
	 * @param event
	 *            What event the screen should send to this controller.
	 */
	@Override
	public void actionPerformed(Event event) {
		switch (event.getAction()) {
		case SCREEN_SHOWED:
			// Game is start�ng
			game = (ClientGame) event.getValue();
			handler = CoMClient.getInstance();
			// reset before starting new game
			view.reset(); 
			view.setGame(game);
			view.receivedWorld();
			view.showMessage("Welcome to Circle of Magic!", 4);
			handler.getNetworkReceivable().addListener(WorldHolder.class,
					worldHolderListener);
			handler.getNetworkReceivable().addListener(GameObjectsHolder.class,
					gameObjectsListener);
			handler.getNetworkReceivable().addListener(Player.class,
					playerListener);
			handler.getNetworkReceivable().addListener(PlayersHolder.class,
					playersListener);
			view.addProcessor(inputController);
			break;
		case SCREEN_HID:
			applicationFrame.displayCursor(CursorType.STANDARD);
			break;
		case NEXT_CLICKED: // Game has ended
			applicationFrame.clearHistory();
			applicationFrame.changeApplicationState(ApplicationState.STATISTICS,
					new Event(null, game.getPlayers()));
			break;
		default:
			// run input controller. If nothing happened there, something is
			// wrong
			if (!inputController.pressed(event)) {
				// throw new IllegalArgumentException("State " +
				// event.getAction() + " is not supported");
			}
		}

	}

	/**
	 * This class handles key and button inputs and convert them to actions that
	 * is implemented in the game
	 * 
	 * @author Robin Gronberg
	 * @author Olliver Mattsson
	 * 
	 */
	private class InputController implements InputProcessor {
		@Override
		public boolean keyDown(int keycode) {
			return inputController.pressed(ClientSettings.getInstance()
					.getKeyBinds().getEvent(keycode));
		}

		@Override
		public boolean keyUp(int keycode) {
			return inputController.released(ClientSettings.getInstance()
					.getKeyBinds().getEvent(keycode));
		}

		@Override
		public boolean keyTyped(char character) {
			return false;
		}

		@Override
		public boolean mouseMoved(int x, int y) {
			ClientSettings settings = ClientSettings.getInstance();
			if (!settings.isDisableMoveCameraAtEdges()) {
				IVector2 camSpeed = view.getCamera().getSpeed();
				if (x >= Gdx.graphics.getWidth() - 10) {
					camSpeed.setX(1);
				} else if (x <= 10) {
					camSpeed.setX(-1);
				} else {
					// Only reset speed in x direction if the buttons for moving
					// the
					// camera isn't pressed
					if (!(Gdx.input.isKeyPressed(settings.getKeyBinds()
							.getMoveCameraRight()) || Gdx.input
							.isKeyPressed(settings.getKeyBinds()
									.getMoveCameraLeft()))) {
						camSpeed.setX(0);
					}
				}
				if (y >= Gdx.graphics.getHeight() - 10) {
					camSpeed.setY(1);
				} else if (y <= 10) {
					camSpeed.setY(-1);
				} else {
					// Only reset speed in y direction if the buttons for moving
					// the
					// camera isn't pressed
					if (!(Gdx.input.isKeyPressed(settings.getKeyBinds()
							.getMoveCameraDown()) || Gdx.input
							.isKeyPressed(settings.getKeyBinds()
									.getMoveCameraUp()))) {
						camSpeed.setY(0);
					}
				}
			}
			return false;
		}

		@Override
		public boolean scrolled(int amount) {
			IWorldCamera camera = view.getCamera();
			camera.setZoom(camera.getZoom() + (float) amount / 10);
			return true;
		}

		@Override
		public boolean touchDown(int x, int y, int pointer, int button) {
			return inputController.pressed(ClientSettings.getInstance()
					.getKeyBinds().getEvent(button));
		}

		@Override
		public boolean touchDragged(int x, int y, int pointer) {
			return false;
		}

		@Override
		public boolean touchUp(int x, int y, int pointer, int button) {
			return inputController.released(ClientSettings.getInstance()
					.getKeyBinds().getEvent(button));
		}

		/**
		 * When a key or button is pressed
		 * 
		 * @param event
		 *            The event this input controller should process.
		 * @return Whenever this key/button press made an impact to the game
		 */
		public boolean pressed(Event event) {
			if (event != null) {
				IVector2 worldPosition = view.getCamera().getWorldPos(
						new Vector2(Gdx.input.getX(), Gdx.input.getY()));
				switch (event.getAction()) {
				case USE_CLICKED:
					/*
					 * If an spell is prepared, cast that spell to mouse
					 * location
					 */
					applicationFrame.displayCursor(CursorType.STANDARD);
					if (view.getGame().getPreparedSpell() != null) {
						IAction action = CastSpellActionFactory.create(view
								.getGame().getPreparedSpell(), worldPosition);
						SoundHandler.getInstance().playSpellSound(
								view.getGame().getPreparedSpell()
										.getSpellRecipe().getName());
						view.getGame().setPreparedSpell(null);
						handler.sendAction(action);
					} else {
						return false;
					}
					break;
				case PREPARE_SPELL:
					SpellSlot preparedSpell = CoMClient.getInstance()
							.getPlayer()
							.getSpellSlot((Integer) event.getValue());
					// If spellslot is empty, skip
					if (preparedSpell != null) {
						prepareSpell((Integer) event.getValue());
					} else {
						// return cursor to normal if spellslot with no spell is
						// selected
						applicationFrame.displayCursor(CursorType.STANDARD);
					}
					break;
				case MOVE_CAMERA:
					IWorldCamera camera = view.getCamera();
					camera.setSpeed(camera.getSpeed().add(
							(IVector2) event.getValue()));
					break;
				case MOVE_CHARACTER:
					view.getGame().setPreparedSpell(null);
					applicationFrame.displayCursor(CursorType.STANDARD);
					IAction action = new MoveAction(worldPosition);
					view.addRenderableObject(GameObjectViewFactory
							.createMoveCursor(worldPosition));
					handler.sendAction(action);
					break;
				case CENTER_CAMERA_CHARACTER_HOLD:
					camera = view.getCamera();
					camera.lockAtGameObject(game.getPlayerCharacter());
					break;
				case CENTER_CAMERA_CHARACTER_TOGGLE:
					camera = view.getCamera();
					if (camera.isLockedAtGameObject()) {
						camera.unlockAtGameObject();
					} else {
						camera.lockAtGameObject(game.getPlayerCharacter());
					}
					break;
				case SHOW_SPELLSHOP:
					if (view.hasOverlayScreen()) {
						view.hideOverlayScreen();
					}else{
						int index = (Integer)event.getValue();
						SpellShopView spellShopView = new SpellShopView(
								new SpellShop(),index);
						view.showOverlayScreen(spellShopView);
						new SpellShopController(spellShopView);
					}
					break;
				case CLOSE_OVERLAY:
					if (view.hasOverlayScreen()) {
						view.hideOverlayScreen();
					} else {
						// TODO: show in game menu
						InGameMenuScreen inGameMenuView = new InGameMenuScreen();
						new InGameMenuController(applicationFrame, inGameMenuView);
						view.showOverlayScreen(inGameMenuView);
					}
					break;
				case SHOW_GAME_STATISTICS:
					if (!view.hasOverlayScreen()) {
						view.hideOverlayScreen();
						InGameStatisticsScreen inGameStatistics = new InGameStatisticsScreen();
						new InGameStatisticsController(applicationFrame, inGameStatistics);
						view.showOverlayScreen(inGameStatistics);
						inGameStatistics.refreshPlayerStatistics(game
								.getPlayers());
					}
					break;
				default:
					// nothing made impact to the world, return false
					return false;
				}
				return true;
			} else {
				// event is null, nothing happens:
				return false;
			}
		}

		/**
		 * When a key or button is released
		 * 
		 * @param event
		 *            The event this input controller should process.
		 * @return Whenever this key/button release made an impact to the game
		 */
		public boolean released(Event event) {
			if (event != null) {
				switch (event.getAction()) {
				case MOVE_CAMERA:
					IWorldCamera camera = view.getCamera();
					IVector2 v = (IVector2) event.getValue();
					// set camera speed to 0 if desired speed is not 0
					if (v.getX() != 0) {
						camera.getSpeed().setX(0);
					}
					if (v.getY() != 0) {
						camera.getSpeed().setY(0);
					}
					break;
				case CENTER_CAMERA_CHARACTER_HOLD:
					camera = view.getCamera();
					camera.unlockAtGameObject();
					break;
				case SHOW_GAME_STATISTICS:
					if (view.hasOverlayScreen()) {
						if (view.getOverlayScreen().getClass() == InGameStatisticsScreen.class) {
							view.hideOverlayScreen();
						}
					}
					break;
				default:
					return false;
				}
				return true;
			} else {
				return false;
			}
		}
	}

	/**
	 * Prepare this clients player to cast a spell. The spell is casted if no
	 * preparation is needed. If the player tries to use a spell. A spell must
	 * first be prepared with this method
	 * 
	 * @param spellSlotIndex
	 */
	private void prepareSpell(int spellSlotIndex) {
		SpellSlot preparedSpell = CoMClient.getInstance().getPlayer()
				.getSpellSlot(spellSlotIndex);
		model.ICharacter character = game.getPlayerCharacter();
		boolean same = false;
		// Checks if the prepared spell is already in the actionlist of the
		// character
		if (preparedSpell.isAvaiable() && character != null) {
			for (IAction action : character.getActions()) {
				if (action instanceof CastSpellAction) {
					CastSpellAction cast = (CastSpellAction) action;
					if (preparedSpell.getSpellRecipe().getName() == cast
							.getSpellRecipe().getName()) {
						same = true;
						view.getGame().setPreparedSpell(null);
						break;
					}
				}
			}
			if (!same) {
				view.getGame().setPreparedSpell(preparedSpell);
				// If the spell does not need to prepare before casting
				if (!preparedSpell.getSpellRecipe().getRequiresAim()) {
					IAction action = CastSpellActionFactory.create(
							preparedSpell, character.getPosition());
					handler.sendAction(action);
					view.getGame().setPreparedSpell(null);
				} else {
					applicationFrame.displayCursor(CursorType.CASTSPELL);
				}
			}
		}
	}

	private IClientNetworkListener<WorldHolder> worldHolderListener = new IClientNetworkListener<WorldHolder>() {
		private float lastTime;

		@Override
		public void networkReceived(WorldHolder object, long timeSent) {
			if (timeSent > lastTime) {
				switch (object.getState()) {
				case BUYING:
					view.showMessage("Time to purchase spells!", 3);
					break;
				case PLAYING:
					view.showMessage("Fight!", 2);
					break;
				default:
					break;

				}
				
				game.receivedWorld(object.getWorld(), object.getPlayers(),
						object.getState(), object.getSettings(), timeSent);
				view.receivedWorld();
			}
		}
	};
	private IClientNetworkListener<GameObjectsHolder> gameObjectsListener = new IClientNetworkListener<GameObjectsHolder>() {
		@Override
		public void networkReceived(GameObjectsHolder object, long timeSent) {
			game.receivedGameObjects(object.get(), timeSent);
			view.receivedGameObjects(object.get(), timeSent);
		}
	};
	private IClientNetworkListener<Player> playerListener = new IClientNetworkListener<Player>() {
		@Override
		public void networkReceived(Player player, long timeSent) {
			view.receivedPlayer(player, timeSent);
		}
	};
	private IClientNetworkListener<PlayersHolder> playersListener = new IClientNetworkListener<PlayersHolder>() {
		private long lastTime;

		@Override
		public void networkReceived(PlayersHolder players, long timeSent) {
			if (timeSent > lastTime) {
				handleStatisticsMessages(players);
				lastTime = timeSent;
				game.receivedPlayers(players.get(), timeSent);
			}
		}
	};

	/**
	 * When statistics are received from server. Show it to the player
	 * 
	 * @param statistics
	 *            The statistics that was received from the server
	 */
	private void handleStatisticsMessages(PlayersHolder players) {
		// Check if someone was killed,
		// show message on screen
		String killerName = "someone";
		String killedName = "Someone";
		boolean hasBeenKilled = false;
		for (Player newPlayer : players.get()) {
			for (Player oldPlayer : game.getPlayers()) {
				if (oldPlayer.equals(newPlayer)) {
					// player was killed
					if (oldPlayer.getStatistics().getDeaths() < newPlayer
							.getStatistics().getDeaths()) {
						killedName = oldPlayer.getName();
						hasBeenKilled = true;
					}
					if (oldPlayer.getStatistics().getKillingBlows() < newPlayer
							.getStatistics().getKillingBlows()) {
						killerName = oldPlayer.getName();
						hasBeenKilled = true;
					}
				}
			}
		}
		if (hasBeenKilled) {
			view.showMessage(killedName + " has been killed by " + killerName,
					2);
		}
	}
}
