package client.controller;

import server.model.CoMServer;
import client.gui.ApplicationFrame;
import client.gui.ApplicationState;
import client.gui.screens.LanScreen;
import client.model.Event;
import client.model.SearchGamesModel;

public class LanScreenController extends AbstractScreenController {

	public LanScreenController(ApplicationFrame a) {
		super(a);
		view = new LanScreen();
		a.addGUIView(view, ApplicationState.LAN);
		view.addGuiListener(this);
	}

	private LanScreen view;

	@Override
	public void actionPerformed(Event event) {
		switch(event.getAction()){
		case JOIN_GAME_CLICKED:
			SearchGamesModel.getInstance().setLAN();
			applicationFrame.changeApplicationState(ApplicationState.SELECTGAME);
			break;
		case HOST_GAME_CLICKED:
			CoMServer.getInstance().start();		// TODO: Remove (temporary). Start server to be able to create game on localhost.
			applicationFrame.changeApplicationState(ApplicationState.CREATELOBBY,
					new Event(null, "localhost"));
			break;
		case BACK_CLICKED:
			applicationFrame.back();
			break;
		case SCREEN_SHOWED:
		case SCREEN_HID:
			break;
			default:
				throw new IllegalArgumentException("State " + event.getAction() + " is not supported");
		}
	}

}
