package client.controller;

import model.GameHeader;
import client.gui.ApplicationFrame;
import client.gui.ApplicationState;
import client.gui.screens.SelectGameScreen;
import client.model.Event;
import client.model.SearchGamesModel;

public class SelectGameController extends AbstractScreenController {

	public SelectGameController(ApplicationFrame a) {
		super(a);
		view = new SelectGameScreen();
		a.addGUIView(view, ApplicationState.SELECTGAME);
		view.addGuiListener(this);
	}

	private SelectGameScreen view;

	@Override
	public void actionPerformed(Event event) {
		SearchGamesModel searchGames = SearchGamesModel.getInstance();
		switch(event.getAction()){
		case JOIN_GAME_CLICKED:
			int gameHashCode = (Integer)event.getValue(); 
			GameHeader game = searchGames.getGameHeader(gameHashCode);
			// Pass GameHeader to JOINLOBBY controller.
			if (game != null) {
				searchGames.stop();
				applicationFrame.changeApplicationState(ApplicationState.JOINLOBBY, new Event(null, game));
			}
			break;
		case BACK_CLICKED:
			applicationFrame.back();
			break;
		case SCREEN_SHOWED:
			searchGames.start();
	        break;
		case SCREEN_HID:
			searchGames.stop();
			break;
			default:
				throw new IllegalArgumentException("State " + event.getAction() + " is not supported");
		}
	}

}
