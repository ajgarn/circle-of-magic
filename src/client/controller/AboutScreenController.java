package client.controller;

import client.gui.ApplicationFrame;
import client.gui.ApplicationState;
import client.gui.screens.AboutScreen;
import client.model.Event;
/**
 * A screenController that controls the AboutScreen.
 * @author Alexander Härenstam
 * Used by Application
 */
public class AboutScreenController extends AbstractScreenController {

	public AboutScreenController(ApplicationFrame a) {
		super(a);
		view = new AboutScreen();
		a.addGUIView(view, ApplicationState.ABOUT);
		view.addGuiListener(this);
	}

	private AboutScreen view;

	@Override
	public void actionPerformed(Event event) {
		switch(event.getAction()){
		case BACK_CLICKED:
			applicationFrame.back();
		break;
		case SCREEN_SHOWED:
		case SCREEN_HID:
			break;
			default:
				throw new IllegalArgumentException("State " + event.getAction() + " is not supported");
		}
	}

}
