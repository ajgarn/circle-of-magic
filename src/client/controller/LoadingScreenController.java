package client.controller;

import common.network.holders.WorldHolder;

import model.Lobby;
import model.Player.Status;
import client.gui.ApplicationFrame;
import client.gui.ApplicationState;
import client.gui.screens.LoadingScreen;
import client.model.ClientGame;
import client.model.CoMClient;
import client.model.Event;
import client.network.IClientNetworkListener;

public class LoadingScreenController extends AbstractScreenController {

	public LoadingScreenController(ApplicationFrame a) {
		super(a);
		view = new LoadingScreen();
		a.addGUIView(view, ApplicationState.LOADING);
		view.addGuiListener(this);
	}

	private LoadingScreen view;
	private Lobby lobby;
	private CoMClient client = CoMClient.getInstance();

	@Override
	public void actionPerformed(Event event) {
		switch(event.getAction()){
		case SCREEN_SHOWED:
			lobby = (Lobby) event.getValue();
			// TODO: Make sure all game stuff has loaded completely.
			client.getNetworkReceivable().addListener(Lobby.class, lobbyListener);
			client.getNetworkReceivable().addListener(WorldHolder.class, worldListener);
			client.send(Status.LOADED);
			break;
		case SCREEN_HID:
			client.getNetworkReceivable().removeListener(Lobby.class, lobbyListener);
			client.getNetworkReceivable().removeListener(WorldHolder.class, worldListener);
			break;
			default:
				throw new IllegalArgumentException("State " + event.getAction() + " is not supported");
		}
	}

	private IClientNetworkListener<Lobby> lobbyListener = new IClientNetworkListener<Lobby>() {		
		@Override
		public void networkReceived(Lobby lobby, long timeSent) {
			LoadingScreenController.this.lobby = lobby;
		}
	};
	private IClientNetworkListener<WorldHolder> worldListener = new IClientNetworkListener<WorldHolder>() {		
		@Override
		public void networkReceived(WorldHolder holder, long timeSent) {
			lobby.getHeader().setPlayers(holder.getPlayers());
			ClientGame game = new ClientGame(lobby.getHeader());
			game.receivedWorld(holder.getWorld(), holder.getPlayers(), holder.getState(), 
					holder.getSettings(), timeSent);
			applicationFrame.changeApplicationState(ApplicationState.GAMEPLAY, 
					new Event(null, game));
		}
	};
}
