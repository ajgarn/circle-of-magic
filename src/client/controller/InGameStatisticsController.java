package client.controller;

import client.gui.ApplicationFrame;
import client.gui.screens.GUIListener;
import client.gui.screens.InGameStatisticsScreen;
import client.model.Event;

/**
 * Controller defining how to handle events on statistics during gameplay. Used in GameScreen.
 * @author Alexander H�renstam.
 *
 */
public class InGameStatisticsController implements GUIListener{
	private ApplicationFrame applicationFrame;
	private InGameStatisticsScreen view;
	public InGameStatisticsController(ApplicationFrame a,InGameStatisticsScreen view) {
		this.view = view;
		this.applicationFrame = a;
		view.addGuiListener(this);
	}

	@Override
	public void actionPerformed(Event event) {
		switch(event.getAction()){
		case SCREEN_SHOWED:
			//view.refreshPlayerStatistics((GameStatistics)event.getValue());
			break;
		case SCREEN_HID:
			break;
			default:
				//throw new IllegalArgumentException("State " + event.getAction() + " is not supported");
		}
	}

}
