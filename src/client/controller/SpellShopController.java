package client.controller;

import model.Player;
import model.spells.SpellSlot;
import client.gui.screens.GUIListener;
import client.gui.screens.SpellShopView;
import client.model.CoMClient;
import client.model.Event;

public class SpellShopController implements GUIListener{
	private SpellShopView view;

	public SpellShopController(SpellShopView view){
		this.view = view;
		view.addGuiListener(this);
	}

	@Override
	public void actionPerformed(Event event) {
		switch (event.getAction()) {
		case BUY_SPELL:
			Object[] values = (Object[])event.getValue();
			SpellSlot spellSlot = (SpellSlot)values[0];
			int index = (Integer)values[1];
			Player player = CoMClient.getInstance().getPlayer();
			//if no index was specified. Find first empty spellSlot
			if(player.getSpellPoints() >= spellSlot.getSpellRecipe().getCost()){
				if(index < 0){
					index = player.getFirstEmptySpellSlotIndex();
					if(index < 0){
						//No spellslot are empty.
						break;
					}
				}
				player.buy(spellSlot,index);
				CoMClient.getInstance().send(player);
			}
			//TODO: do something when spell is bought etc.
			break;
		default:
			break;
		}
	}
}
