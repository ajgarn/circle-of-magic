package client.network;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import model.Account;
import model.GameHeader;
import model.core.Constants;
import server.network.IAuthenticationService;
import services.logger.Log;
import services.network.AbstractClassNetReceiver;
import services.network.ClientNetHandler;
import services.network.IClientNetHandler;
import services.network.NetworkSettings;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.rmi.TimeoutException;

import common.network.NetworkRegistration;
import common.network.holders.IGameHeadersHolder;

/**
 * Network receivable customized for the client side of Circle of Magic. Hides
 * the network connection ID for listeners and making
 * {@link IClientNetworkListener} to register.
 * 
 * @author Anton Jansson
 * 
 * @see AbstractClassNetReceiver
 * @see IClientNetworkListener
 * @see IClientNetHandler
 */
public class ClientNetReceiver extends
		AbstractClassNetReceiver<IClientNetworkListener<?>> implements
		IClientNetHandler {

	private static ClientNetReceiver instance;
	private final IClientNetHandler handler;
		
	private ClientNetReceiver(NetworkSettings settings) {
		super(ClientNetHandler.getInstance(settings));
		handler = ClientNetHandler.getInstance(settings);
	}
	
	/**
	 * Tries to authenticate the user.
	 * @param username The username.
	 * @param password The password.
	 * @return The account if the user is authenticated, <code>null</code> otherwise.
	 */
	public Account authenticate(String username, String password) {
		try {
			handler.connectHost(Constants.ONLINE_SERVER_HOSTNAME);
			return getAuthenticationService().login(username, password,
					handler.getCurrentClient().getID());
		} catch (IOException e) {
			// TODO Auto-generated catch block
		} catch (TimeoutException e) {
			Log.fatal(e.getMessage(), e);
		}
		return null;
	}

	@Override
	public void connectHost(InetAddress host) throws IOException {
		handler.connectHost(host);
	}

	@Override
	public void connectHost(String host) throws IOException {
		handler.connectHost(host);
	}
	
	/**
	 * Adds an account to the currently connected host's authentication service.
	 * @param account The account to add.
	 * @param host The host where to add the account.
	 * @return The added account or <code>null</code> if the connection
	 * fails or the account with the specified name already exists.
	 */
	public Account addAccount(Account account) {
		try {
			return getAuthenticationService().addAccount(account,
					handler.getCurrentClient().getID());
		} catch (TimeoutException e) {
			Log.fatal(e.getMessage(), e);
		}
		return null;
	}

	@Override
	public void disconnect() {
		handler.disconnect();
	}
	
	private IAuthenticationService getAuthenticationService()
			throws TimeoutException {
		return getRemoteObject(Constants.IDs.AUTHENTICATION_SERVICE,
				IAuthenticationService.class);
	}
	
	/**
	 * Gets the games for all available hosts. 
	 * If no hosts are found the list of games is empty.
	 * 
	 * <p>This method only searches for hosts in LAN.
	 * @return The collection of games, empty if there are no games.
	 */
	public Collection<GameHeader> getAvailableGames() {
		final Collection<GameHeader> games = new HashSet<GameHeader>();
		Collection<InetAddress> hosts = getAvailableHosts();
		Collection<Thread> threads = new ArrayList<Thread>();
		
		for (final InetAddress host : hosts) {
			Thread thread = new Thread(new Runnable() {
				@Override
				public void run() {
					games.addAll(getAvailableGames(host));
				}
			});
			thread.start();
			threads.add(thread);
		}
		
		for (Thread thread : threads) {
			try {
				thread.join();
			} catch (InterruptedException e) {}
		}
		return games;
	}
	
	/**
	 * Gets the games for a specific host.
	 * @param host IP address of host.
	 * @return The collection of games, empty if there are no games.
	 */
	public Collection<GameHeader> getAvailableGames(InetAddress host) {
		Client c = getClient(host);
		Collection<GameHeader> games = getAvailableGames(c);
		return games;
	}
	
	/**
	 * Gets the games for a specific host.
	 * @param hostname Host address as a string.
	 * @return The collection of games, empty if there are no games.
	 */
	public Collection<GameHeader> getAvailableGames(String hostname) {
		Client c = getClient(hostname);
		Collection<GameHeader> games = getAvailableGames(c);
		return games;
	}
	
	/**
	 * Gets the games from a connected client's host.
	 * @param client The client that will connect to the host.
	 * @return The collection of games, empty if there are no games.
	 */
	private Collection<GameHeader> getAvailableGames(Client client) {
		IGameHeadersHolder holder = handler.getRemoteObject(client, 
				Constants.IDs.GAMES_HOLDER, IGameHeadersHolder.class);
		Collection<GameHeader> games;
		try {
			games = holder.getAvailableLobbies();
		} catch (TimeoutException e) {
			return new ArrayList<GameHeader>();
		}
		Iterator<GameHeader> it = games.iterator();
		while (it.hasNext()) {
			GameHeader g = it.next();
			if (g != null) {
				g.setHost(client.getRemoteAddressTCP().getAddress());
			} else {
				it.remove();
			}
		}
		return games;
	}

	public Collection<InetAddress> getAvailableHosts() {
		return handler.getAvailableHosts();
	}

	@Override
	public Client getClient(InetAddress host) {
		return handler.getClient(host);
	}

	@Override
	public Client getClient(String host) {
		return handler.getClient(host);
	}

	public Client getCurrentClient() {
		return null;
	}
	
	public static synchronized ClientNetReceiver getInstance() {
		if (instance == null) {
			instance = new ClientNetReceiver(
					NetworkRegistration.getNetworkSettings());
		}
		return instance;
	}

	@Override
	public int getPing() {
		return handler.getPing();
	}

	@Override
	public <T> T getRemoteObject(int objectID, Class<T> iface) throws TimeoutException {
		return handler.getRemoteObject(objectID, iface);
	}
	
	/**
	 * Returns whether the current client is connected to the online server or not.
	 * @return <code>true</code> if connected to online, <code>false</code> otherwise.
	 */
	public boolean isOnline() {
		return handler.getCurrentClient() != null
				&& handler.getCurrentClient().getRemoteAddressTCP()
						.getHostString()
						.equals(Constants.ONLINE_SERVER_HOSTNAME);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected <T> void networkReceived(IClientNetworkListener<?> listener,
			int connectionID, T object, long time) {
		((IClientNetworkListener<T>) listener).networkReceived(object, time);
	}

	@Override
	public <T> void send(T object) {
		handler.send(object);
	}

	@Override
	public void updatePing() {
		handler.updatePing();
	}
}
