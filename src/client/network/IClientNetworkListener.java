package client.network;

/**
 * Listener for objects received from the network on the client side. 
 * The type that this will listen for is specified by the generic params.
 * @author Anton Jansson
 *
 * @param <T> The object type to listen for.
 */
public interface IClientNetworkListener<T> {
	/**
	 * An object of type {@link T} is received on this client.
	 * @param object The object received.
	 * @param timeSent The time the object was sent.
	 */
	void networkReceived(T object, long timeSent);
}
