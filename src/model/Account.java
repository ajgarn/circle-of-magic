package model;

import services.network.INetworkObject;

/**
 * Account for authenticated users within the application. 
 * @author Anton Jansson
 *
 */
public class Account implements INetworkObject {

	private final int id;
	private final String name;
	private Player player;
	
	@SuppressWarnings("unused")
	private Account() {
		this(0, null);
	}
	
	/**
	 * Creates a new Account. 
	 * <p>For a LAN account, specify an ID below 0. 
	 * @param id The ID of the account. If below 0, the account will be a LAN account.
	 * @param name The display name of the account.
	 */
	public Account(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	private Player createPlayer() {
		return new Player(getName(), id);
	}
	
	/**
	 * Two {@link Account}s are equal if they have the same ID,
	 * or have the same display name if they are LAN accounts.
	 * <p>
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		} else if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		Account acc = (Account) obj;
		return id == acc.id
				|| (isLANAccount() && acc.isLANAccount() && name
						.equals(acc.name));
	}
	
	public int getID() {
		return id;
	}
	
	public String getName() {
		return name;
	}

	public Player getPlayer() {
		if (player == null) {
			player = createPlayer();
		}
		return player;
	}
	
	@Override
	public int hashCode() {
		return (isLANAccount()) ? 29 * name.hashCode() : 23 * id;
	}
	
	public boolean isLANAccount() {
		return id < 0;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + "[" + name + ", id=" + id +"]";
	}
}
