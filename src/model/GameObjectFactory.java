package model;

import model.core.ColorNames;
import model.core.State;
import services.math.IVector2;

/**
 * Factory for creating GameObjects.
 * @author Anton Jansson
 *
 */
public class GameObjectFactory {

	public static Character createMage(ColorNames color, IVector2 position) {
		Mage m = new Mage(position, 0, State.STANDING, 100, 6f, color);
		return m;
	}
}
