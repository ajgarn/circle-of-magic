package model;

import model.core.ColorNames;
import model.core.IGameObject;
import model.core.State;
import services.math.IVector2;

/**
 * A representation of a Mage. Holds all the data for the Mage.
 * @author Olliver
 *
 */
public class Mage extends Character implements IMage {
	private ColorNames color;
	protected Mage() {
		super();
	}
	
	public Mage(IVector2 position, float direction, State state, int health,float speed, ColorNames color){
		super(position,direction,state,health,speed, 10);
		this.color = color;
	}
	/* (non-Javadoc)
	 * @see model.IMage#getColor()
	 */
	@Override
	public ColorNames getColor(){
		return color;
	}
	@Override
	public void update(float dTime){
		super.update(dTime);
		
	}

	public void refresh(IGameObject o){
		super.refresh(o);
		if(o instanceof Mage){
			IMage m = (IMage)o;
			this.color = m.getColor();
		}
	}
}
