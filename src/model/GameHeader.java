package model;

import java.net.InetAddress;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import services.network.INetworkObject;


/**
 * General information of a game.
 * @author Anton Jansson
 *
 */
public class GameHeader implements INetworkObject {
	private final int id;
	private transient InetAddress host;
	private final String name;
	private final String originalHost;
	private final List<Player> players;
	
	@SuppressWarnings("unused")
	private GameHeader(){
		this(0, null, null);
	}
	
	public GameHeader(int id, String name, InetAddress host) {
		this.id = id;
		this.name = name;
		this.host = host;
		this.players = new CopyOnWriteArrayList<Player>();
		originalHost = (host != null) ? host.toString() : null;
	}
	
	/**
	 * Returns whether all players have the specified status.
	 * @return <code>true</code> if all players have the status, 
	 * <code>false</code> otherwise.
	 */
	public boolean allPlayersHaveStatus(Player.Status status) {
		for (Player player : getPlayers()) {
			if (player.getStatus() != status) {
				return false;
			}
		}
		return true;
	}
	
	public InetAddress getHost() {
		return host;
	}
	
	public int getID() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public Collection<Player> getPlayers() {
		return players;
	}
	
	/**
	 * Returns whether the two games are the same, 
	 * thus have the same host and game ID.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		
		GameHeader gh = (GameHeader) obj;
		return id == gh.id && originalHost.equals(gh.originalHost);
	}
	
	@Override
	public int hashCode() {
		return id * 13 + originalHost.hashCode() * 7;
	}
	
	@Override
	public String toString() {
		return "GameHeader[" + name + " (" + id + ") @ " + host + "]";
	}

	public void setHost(InetAddress host) {
		this.host = host;
	}
	public void setPlayers(Collection<Player> players) {
		for(Player p : players){
			int index = this.players.indexOf(p);
			if(index >= 0){
				this.players.get(index).refresh(p);
			}else{
				this.players.add(p);
			}
		}
	}
}
