package model.core;

import java.awt.Shape;

import services.math.IVector2;


/**
 * Interface for a shape to test intersection.
 * @author Anton Jansson
 */
public interface IShape {
	Shape getShape();
	/**
	 * Tests if the interior of the IShape intersects the interior of a 
	 * specified IShape.
	 * @param shape The shape to compare with.
	 * @return <code>true</code> if the interior of the IShape and the 
	 * interior of the passed IShape intersect; false otherwise.
	 */
	boolean intersects(IShape shape);

	/**
	 * Set the position of the shape.
	 * @param position The center position of an object in the world.
	 */
	void setPosition(IVector2 position);
}
