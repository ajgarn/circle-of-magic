package model.core;

import services.network.INetworkObject;

/**
 * A spellEffect is an effect that affects the character. It comes from being hit by a spell.
 * @author Olliver
 *
 */
public abstract class SpellEffect implements INetworkObject{
	private static long idGenerator = 0;
	private final long id;
	protected transient IGameObject caster;
	protected float duration;
	private boolean alreadyPerformed =false;
	private SpellNames spellName;
	public SpellEffect(){id = 0;}
	public SpellEffect(SpellNames spellName,float duration){
		this(spellName,duration, null);
	}
	public SpellEffect(SpellNames spellName,float duration, IGameObject caster){
		this.duration = duration;
		this.caster = caster;
		this.spellName = spellName;
		id = idGenerator++;
	}
	public abstract void perform(GameObject object, float dTime);
	public abstract boolean isDone();
	public void timePassed(float dTime){
		duration -= (dTime/60);
	}
	public float getDuration(){
		return duration;
	}
	public void alreadyPerformed(){
		alreadyPerformed = true;
	}
	public boolean isAlreadyPerformed(){
		return alreadyPerformed;
	}
	public void addCaster(IGameObject caster){
		this.caster = caster;
	}
	public abstract SpellEffect copy();
	public SpellNames getSpellName(){
		return spellName;
	}
	@Override
	public boolean equals(Object obj) {
		if(obj == null || this.getClass() != obj.getClass()){
			return false;
		}else if( this == obj){
			return true;
		}else{
			SpellEffect effect = (SpellEffect)obj;
			return id == effect.id;
		}
	}
}
