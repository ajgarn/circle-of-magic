package model.core;

import java.util.List;

import services.math.IVector2;
/**
 * An interface for the gameobjects in the world. Holds the methods that are the same
 * @author Olliver
 *
 */
public interface IGameObject {
	/**
	 * Gets how fast this gameObject can rotate
	 * @return The rotationSpeed.
	 */
	public abstract float getRotationSpeed();
	/**
	 * Get the gameObjects position coordinates
	 * in the world.
	 * @return The position of this gameObject
	 */
	public abstract IVector2 getPosition();
	/**
	 * Get the direction in degrees, that
	 * this gameObject is currently facing
	 * @return The direction.
	 */
	public abstract float getDirection();
	/**
	 * Get the state this gameObject current has
	 * @return The current state this gameObject has
	 */
	public abstract State getState();
	/**
	 * Get all actions this gameObject current has
	 * @return A collection of actions, where 
	 * index 0 is the performing action and
	 * last index is the last action 
	 */
	public abstract List<IAction> getActions();
	/**
	 * Get the speed of this gameObject
	 * @return The speed.
	 */
	public abstract float getSpeed();

	/**
	 * A method to update the GameObject
	 * @param time The time since the last update
	 */
	public abstract void update(float time);

	/**
	 * Answers if the object should be removed from the world
	 * @return true if it should be removed false otherwise
	 */
	public abstract boolean isDead();
	/**
	 * Refreshes this gameObjects values with o's
	 * values. Used to sync two gameObjects without
	 * trashing the references.
	 * @param o The gameObject this gameObject
	 * should refresh to. 
	 */
	public abstract void refresh(IGameObject o);
	/**
	 * Adds a listener to this gameObject
	 * @param l The listener that shall listen
	 * to this gameObejct
	 */
	public abstract void addListener(GameObjectListener l);
	/**
	 * Removes the gameObjectListener
	 * @param l The gameObjectlistener that
	 * should be removed
	 */
	public abstract void removeListener(GameObjectListener l);

}