package model.core;

import java.awt.Shape;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.RectangularShape;

import services.math.IVector2;


/**
 * Circular shape for testing intersection.
 * @author Anton Jansson
 */
public class CircleShape implements IShape {
	
	private final RectangularShape shape;
	private final float radius;
	
	public CircleShape(float diameter) {
		shape = new Ellipse2D.Float(0, 0, diameter, diameter);
		radius = diameter / 2;
	}

	@Override
	public boolean intersects(IShape shape) {
		Area area = new Area(getShape());
		area.intersect(new Area(shape.getShape()));
		return !area.isEmpty();
	}

	@Override
	public void setPosition(IVector2 position) {
		shape.setFrame(position.getX() - radius, position.getY() - radius, 
				shape.getWidth(), shape.getHeight());
	}

	@Override
	public Shape getShape() {
		return shape;
	}

}
