package model.core;

public abstract class Action implements IAction {
	private final boolean clearLowerPriority;
	private final boolean clearActions;
	private final boolean canInterruptAction;
	private final int priority;
	public Action() {
		this.clearActions = false;
		this.clearLowerPriority = false;
		this.canInterruptAction = false;
		this.priority = 1;
	}
	/**
	 * Creates a new action with some specific attributes
	 * @param clearLowerPriority Set this as true if you want to clear all lower priority actions.
	 * @param clearActions Set this as true if you would like to clear all but the first action.
	 * @param canInterruptAction Set this as true if you want this action to be able to interrupt the active action.
	 * @param priority Set the priority of this action i.e. where it should be put in the list. Higher priorities is put in the beginning lower in the end.
	 */
	public Action(boolean clearLowerPriority, boolean clearActions, boolean canInterruptAction, int priority){
		this.clearLowerPriority = clearLowerPriority;
		this.clearActions = clearActions;
		this.canInterruptAction = canInterruptAction;
		this.priority = priority;
	}

	@Override
	public abstract void perform(float dTime, GameObject gameObject);

	@Override
	public abstract boolean isDone(IGameObject gameObject);

	@Override
	public int getPriority() {
		// TODO Auto-generated method stub
		return priority;
	}

	@Override
	public boolean clearActions() {
		// TODO Auto-generated method stub
		return clearActions;
	}

	@Override
	public boolean canInterruptAction() {
		// TODO Auto-generated method stub
		return canInterruptAction;
	}

	@Override
	public boolean clearLowerPriority() {
		// TODO Auto-generated method stub
		return clearLowerPriority;
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode() + 3 * priority;
	}
	@Override
	public boolean equals(Object obj) {
		if(this == obj){
			return true;
		}else if(obj == null || obj.getClass() != getClass()){
			return false;
		}else{
			Action a = (Action)obj;
			return clearActions == a.clearActions &&
					canInterruptAction == a.canInterruptAction &&
					clearLowerPriority == a.clearLowerPriority &&
					priority == a.priority;
		}
	}

}
