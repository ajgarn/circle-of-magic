package model.core;

import services.network.INetworkObject;



/**
 * An action that can be performed in the game
 * @author Olliver
 *
 */
public interface IAction extends INetworkObject {
	/**
	 * What happens when this action is performed
	 * @param dTime	The time since the last action
	 * @param gameObject The object wich performs the action
	 */
	public void perform(float dTime, GameObject gameObject);
	/**
	 * Checks if an action is done
	 * @param gameObject The object wich holds the action
	 * @return True if it's done false otherwise
	 */
	public boolean isDone(IGameObject gameObject);
	/**
	 * Returns the priority of this action. High numbers mean high priority
	 * @return the priority
	 */
	public int getPriority();
	/**
	 * Clears all actions except the first
	 * @return True if it should false otherwise
	 */
	public boolean clearActions();
	/**
	 * If you can remove the first element
	 * @return True if you can remove first element false otherwise
	 */
	public boolean canInterruptAction();
	/**
	 * Clears all lower priority actions
	 * @return True if it should false otherwise
	 */
	public boolean clearLowerPriority();
}
