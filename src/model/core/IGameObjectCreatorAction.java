package model.core;

import java.util.List;


/**
 * Interface for classes having objects to be created in the World.
 * @author Anton Jansson
 *
 */
public interface IGameObjectCreatorAction {
	/**
	 * Returns all GameObjects that should be added to the world,
	 * and clears them from the list.
	 * @return List of GameObjects.
	 */
	List<? extends GameObject> pullGameObjects();
}
