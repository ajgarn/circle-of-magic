package model.core;

/**
 * If something should be notified when something happens
 * to a specific gameObject. That should implement this
 * interface
 * @author Robin Grönberg
 *
 */
public interface GameObjectListener {
	/**
	 * When a spellEffect is added to the game object,
	 * notify all the listeners that the specific effect
	 * has been added.
	 * @param effect The effect that has been added
	 */
	public void effectAdded(IGameObject source, SpellEffect effect);
	/**
	 * When a spellEffect is removed to the game object,
	 * notify all the listeners that the specific effect
	 * has been removed.
	 * @param effect The effect that has been removed
	 */
	public void effectRemoved(IGameObject source, SpellEffect effect);
	/**
	 * When an Action is added to the game object,
	 * notify all the listeners that the specific Action
	 * has been added.
	 * @param action The Action that has been added
	 */
	public void actionAdded(IGameObject source, IAction action);
	/**
	 * When an Action is removed to the game object,
	 * notify all the listeners that the specific Action
	 * has been added.
	 * @param action The Action that has been removed
	 */
	public void actionRemoved(IGameObject source, IAction action);
	/**
	 * When the state has changed on the specific
	 * GameObject, notify all listeners
	 * @param oldState The old state this gameObject had.
	 * @param newState The new state this gameObject has.
	 */
	public void stateChanged(IGameObject source, State oldState, State newState);
	/**
	 * Is called when a character is killed
	 * @param killer The GameObject that killed this
	 * @param killed The GameObject that died
	 */
	public void killed(IGameObject killer, IGameObject killed);
	/**
	 * Is called when a character is killed
	 * @param damager The GameObject that did the damage
	 * @param damaged The GameObject that took damage
	 */
	public void damaged(IGameObject damager, IGameObject damaged, int damage);
	/**
	 * This gameObject has died, notify the listeners
	 * @param source The object that died
	 */
	public void died(IGameObject source);
}
