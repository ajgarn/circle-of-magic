package model.core;


/**
 * Interface for all objects that are able to collide.
 * @author Anton Jansson
 */
public interface ICollidable {
	/**
	 * Check if the passed object collides with current object.
	 * This method is not symmetric.
	 * @param object The collidable object to check.
	 * @return Whether the object collides with current object.
	 */
	boolean isColliding(ICollidable object);
	/**
	 * Handle what to do when a collision has occurred.
	 * @param object The other object in the collision.
	 */
	void collision(ICollidable object);
	/**
	 * Get the shape of the object.
	 * @return IShape for this object.
	 */
	IShape getShape();
}
