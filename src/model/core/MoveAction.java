package model.core;

import services.math.IVector2;

/** 
 * A move action that can be performed in the game. Allows the character to move around in the world
 * @author Olliver
 *
 */
public class MoveAction extends Action{
	protected IVector2 newPosition;
	private IVector2 velocity;
	
	@SuppressWarnings("unused")
	private MoveAction() {}
	
	public MoveAction(IVector2 newPosition){
		super(false,true,true,0);
		this.newPosition = newPosition;
	}
	@Override
	/**
	 * Calculates the new position to wich the player will move
	 */
	public void perform(float dTime, GameObject gameObject) {
		if(!(newPosition.equals(gameObject.getPosition()))){//checks if the player has reached its goal
			velocity = new Vector2(newPosition);
			velocity.sub(gameObject.getPosition());//finds the vector between the newPosition and the objects position
			velocity.nor();
			velocity.mul(gameObject.getSpeed());
			if(velocity.angle() != gameObject.getDirection()){//Checks if we should keep rotating
				gameObject.lookAt(velocity.angle());
			}
			IVector2 temp = gameObject.getPosition();
			velocity.mul(dTime/60);
			temp.add(velocity);
			gameObject.setPosition(temp);//sets the new position with the position were the object should be now
			gameObject.setState(State.RUNNING);
		}

	}
	@Override
	public boolean isDone(IGameObject gameObject){
		Vector2 temp = new Vector2(newPosition);
		temp.sub(gameObject.getPosition());
		float length = temp.len();
		boolean b = length < gameObject.getSpeed()/100;//The object is done if the length is smaller than the speed divided by 100
		return b;
		
	}
	@Override
	public boolean equals(Object o){
		return super.equals(o) && newPosition.equals(((MoveAction)o).newPosition) ;
	}
	@Override
	public int hashCode() {
		return super.hashCode() + newPosition.hashCode();
	}
}
