package model.core;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import services.math.IVector2;
import services.network.INetworkObject;

/**
 * An abstract representation of a GameObject in the world. Holds the data for
 * the GameObjects
 * 
 * @author Olliver
 */
public abstract class GameObject implements INetworkObject, IGameObject {

	private IVector2 position;
	private float direction;
	private float speed;
	private transient State state;
	private List<IAction> actions;
	private float rotationspeed;
	private long id;
	protected transient Set<GameObjectListener> listeners = new HashSet<GameObjectListener>();

	protected GameObject() {
		state = State.STANDING;
	}

	public GameObject(IVector2 position, float direction, State state,
			float speed, float rotation) {
		this.position = position;
		this.direction = direction;
		this.state = state;
		this.actions = new ArrayList<IAction>();
		this.speed = speed;
		this.rotationspeed = rotation;
		id = System.nanoTime();
	}

	@Override
	public float getRotationSpeed() {
		return rotationspeed;
	}

	public void setPosition(IVector2 position) {
		this.position.setX(position.getX());
		this.position.setY(position.getY());
	}

	@Override
	public IVector2 getPosition() {
		return position.cpy();
	}

	public void setDirection(float direction) {
		this.direction = direction;
	}

	@Override
	public float getDirection() {
		return direction;
	}

	public void setState(State state) {
		for (GameObjectListener l : listeners) {
			l.stateChanged(this, this.state, state);
		}
		this.state = state;
	}

	@Override
	public State getState() {
		return state;
	}

	/**
	 * Adds a new action to the current list of actions. If action has priority
	 * 0 adds this to the second position of the list and clears everything
	 * after. Unless the action that is first in the list also has priority 0
	 * then removes that action and adds this instead. Priority 2 clears list
	 * and doesn't let anyone else add themselves. Priority 3 clears list of all
	 * priority 0's and adds itself last. All other priorities is put last in
	 * the list unless the other action has priority 0 then puts this first and
	 * the 0 priority action after.
	 * 
	 * @param action
	 *            The action to be added
	 */
	public void addAction(IAction action) {
		// TODO: Look over priority cast spell after moveaction done?
		// Action might be used by a separated thread.
		synchronized (actions) {
			if (action != null) {
				if (actions.isEmpty()) {
					actions.add(action);
					actionAdded(action);
				} else {
					if (action.clearLowerPriority()) {// if this action should
														// clear all lower
														// priority action does
														// this
						Iterator<IAction> i = actions.iterator();
						while (i.hasNext()) {
							IAction temp = i.next();
							if (temp.getPriority() < action.getPriority()) {
								i.remove();
								actionRemoved(temp);
							}
						}
					}
					if (action.clearActions()) {
						IAction temp = actions.get(0);
						actions.clear();
						actions.add(temp);
					}
					if (action.canInterruptAction()) {// if action can interrupt
														// actions
														// removes(interrupts)
														// the first actions in
														// the list
						if (action.getPriority() >= actions.get(0)
								.getPriority()) {
							actionRemoved(actions.remove(0));
						}
					}
					for (IAction a : actions) {// in the ned loop through all to
												// add itself before all lower
												// priority actions
						if (a.getPriority() < action.getPriority()) {
							actions.add(actions.indexOf(a), action);
							actionAdded(action);
							break;
						}
					}
					if (!actions.contains(action)) {// if not higher than any in
													// the list adds itself last
						actions.add(action);
						actionAdded(action);
					}
				}
			}
		}
	}

	public void removeAction(IAction action) {
		actions.remove(action);
	}

	@Override
	public List<IAction> getActions() {
		return actions;
	}

	@Override
	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	@Override
	public void update(float time) {
		if (!getActions().isEmpty()) {
			IAction a = getActions().get(0);
			if (a.isDone(this)) {
				removeAction(a);
				actionRemoved(a);
			} else {
				a.perform(time, this);
			}
		}
	}

	/**
	 * This gameObject turns towards direction with its rotationSpeed.
	 * 
	 * @param direction
	 *            the direction to be looked at
	 */
	public void lookAt(float direction) {
		float temp = direction - this.direction;
		while (temp <= -180) {
			temp += 360;
		}
		while (temp > 180) {
			temp -= 360;
		}
		if (temp < rotationspeed && temp > -rotationspeed) {
			return;
		}
		this.direction += temp / 13;
	}

	@Override
	public abstract boolean isDead();

	/**
	 * Kill this object. This object will be removed from the world once it's
	 * dead.
	 */
	public void kill() {
		for (GameObjectListener l : listeners) {
			l.died(this);
		}
		actions.clear();
	}

	public abstract void damage(int damage, IGameObject damager);

	public abstract void heal(int heal);

	@Override
	public void refresh(IGameObject o) {
		this.position = o.getPosition();
		this.direction = o.getDirection();
		this.state = o.getState();
		this.actions = o.getActions();
		this.speed = o.getSpeed();
		this.rotationspeed = o.getRotationSpeed();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o == null || getClass() != o.getClass()) {
			return false;
		}
		return id == ((GameObject) o).id;
	}

	@Override
	public int hashCode() {
		return 19 * ((Long) id).hashCode();
	}

	public void addListener(GameObjectListener listen) {
		if (!listeners.contains(listen)) {
			listeners.add(listen);
		}
	}

	public void removeListener(GameObjectListener listen) {
		listeners.remove(listen);
	}

	private void actionRemoved(IAction action) {
		actionRemoved(this, action);
	}

	public void actionRemoved(IGameObject source, IAction a) {
		for (GameObjectListener l : listeners) {
			l.actionRemoved(source, a);
		}
	}

	private void actionAdded(IAction action) {
		actionAdded(this, action);
	}

	public void actionAdded(IGameObject source, IAction a) {
		for (GameObjectListener l : listeners) {
			l.actionAdded(source, a);
		}
	}
}
