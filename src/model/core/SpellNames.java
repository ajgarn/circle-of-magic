package model.core;

import services.network.INetworkObject;

/**
 * Enum for each spell in game.
 * @author Alexander H�renstam
 *
 */
public enum SpellNames implements INetworkObject {
	FIREBALL("Fireball"),FROSTBOLT("Frostbolt"),LIGHTNINGBOLT("Lightningbolt"),HEAL("Heal"),REFLECT("Reflect"),TRANSPARENCY("Transparency"),
	GRAVITYBOLT("Gravitybolt"),HOMING("Homing"),FIRESPRAY("Fire Spray"),BLINK("Blink"),GRIP("Grip"),STONEPOLE("Stone Pole"),BOOMERANG("Boomerang"),
	LIGHTNINGTRAP("Lightning Trap"),BOWLINGROCK("Bowling Rock"),ENFLAME("Enflame"),BOUNCING("Bouncer");
	
	private String s;
	
	private SpellNames(String s){
		this.s=s;
	}
	@Override
	public String toString(){
		return s;
	}
}
