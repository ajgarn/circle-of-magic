package model.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import services.network.INetworkObject;

import com.badlogic.gdx.graphics.Color;
/**
 * An Enum for colors in the game
 * 
 * @author Olliver.
 * @author Anton Jansson
 */
public enum ColorNames implements INetworkObject {
	BLACK("Black", Color.BLACK), WHITE("White", Color.WHITE), VIOLET(
			"Violet", Color.MAGENTA), LIGHTBLUE("LightBlue",
			Color.CYAN), BLUE("Blue", Color.BLUE), GREEN(
			"Green", Color.GREEN), RED("Red", Color.RED), YELLOW(
			"Yellow", Color.YELLOW);
	private String colorName;
	private Color color;

	private ColorNames(String s, Color color) {
		colorName = s;
		this.color=color;
	}

	public String toString() {
		return colorName;
	}

	/**
	 * Returns a random ColorName.
	 * @return A ColorName.
	 */
	public static ColorNames getRandomColor() {
		return values()[(int) (Math.random() * values().length)];
	}
	
	/**
	 * Returns a random ColorName. The arguments specify colors to
	 * exclude from result, i.e. cannot be returned from this method.
	 * If the argument collection contains all ColorNames values, it is ignored.
	 * @param excludeColors Colors to exclude from result.
	 * @return A ColorName not in the excludeColors collection, see description above.
	 */
	public static ColorNames getRandomColor(Collection<ColorNames> excludeColors) {
		Collection<ColorNames> colors = new ArrayList<ColorNames>(Arrays.asList(values()));
		if (excludeColors.size() < colors.size()) {
			colors.removeAll(excludeColors);
		}
		ColorNames[] c = colors.toArray(new ColorNames[0]);
		return c[(int) (Math.random() * c.length)];
	}

	public Color getColor() {
		return color;
	}
}
