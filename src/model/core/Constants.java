package model.core;

/**
 * Class holding constants.
 * 
 * @author Alexander H�renstam
 * @author Anton Jansson
 *
 */
public class Constants {
	public static final float CLIENT_VERSION = 0.32f;
	
	public static final int FPS = 60;
	public static final int SERVER_FPS = 60;
	/** Interval between ping refreshes, in milliseconds. */
	public static final int PING_INTERVAL = 1500;
	public static final String ONLINE_SERVER_HOSTNAME = "hem.yellowend.com";
	
	public static final int NUM_OF_SPELLSLOT = 8;
	
	/**
	 * Remote method invocation identifiers.
	 * @author Anton Jansson
	 */
	public static class IDs {
		public static final int VERSION_CONTROL_SERVICE = 10;
		public static final int AUTHENTICATION_SERVICE = 20;
		public static final int GAMES_HOLDER = 30;
	}
}
