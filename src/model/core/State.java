package model.core;

import services.network.INetworkObject;

/**
 * An enum for the states of the GameObjects
 * @author Olliver
 *
 */
public enum State implements INetworkObject{
	RUNNING("running"),STANDING("standing"),STARTRUNNING("startRunning"),STOPRUNNING0("stopRunning0"),STOPRUNNING1("stopRunning1"),
	STOPRUNNING2("stopRunning2"),CASTTARGETSPELL0("castTargetSpell0"),CASTTARGETSPELL1("castTargetSpell1"),CASTNOTARGETSPELL("castNoTargetSpell"),
	CASTAOESPELL("castAOESpell"),DYING("dying");
	
	private String s;
	
	private State(String s){
		this.s=s;
	}
	public String toString(){
		return s;
	}
}
