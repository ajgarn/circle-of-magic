package model.core;

import services.math.IVector2;
import services.network.INetworkObject;

/**
 * A Vector2 to be represented in the model
 * @author Olliver
 *
 */
public class Vector2 implements IVector2, INetworkObject{
	private com.badlogic.gdx.math.Vector2 data;
	public Vector2(IVector2 v){
		this(v.getX(),v.getY());
	}
	public Vector2(float x, float y)
	{
		data = new com.badlogic.gdx.math.Vector2(x, y);
	}
	public Vector2()
	{
		data = new com.badlogic.gdx.math.Vector2();
	}
	@Override
	public float getX() {
		return data.x;
	}

	@Override
	public float getY() {
		return data.y;
	}

	@Override
	public void setX(float x) {
		data.x = x;
	}

	@Override
	public void setY(float y) {
		data.y = y;
	}

	@Override
	public void nor() {
		data.nor();
	}

	@Override
	public float angle() {
		return data.angle();
	}

	@Override
	public IVector2 cpy() {
		return new Vector2(this);
	}
	@Override
	public IVector2 add(IVector2 v) {
		data.add(v.getX(), v.getY());
		return this;
	}
	@Override
	public IVector2 sub(IVector2 v) {
		data.sub(v.getX(), v.getY());
		return this;
	}
	@Override
	public IVector2 mul(float m) {
		data.mul(m);
		return this;
	}
	@Override
	public float len() {
		return data.len();
	}
	@Override
	public IVector2 setAngle(float angle) {
		data.setAngle(angle);
		return this;
	}
	public IVector2 rotate(float angle){
		data.rotate(angle);
		return this;
	}	
	@Override
	public String toString() {
		return "x: " + getX() + " y: " + getY();
	}
	@Override
	public boolean equals(Object obj) {
		if(obj == this){
			return true;
		}else if(obj == null || obj.getClass() != getClass()){
			return false;
		}else{
			Vector2 v = (Vector2)obj;
			return v.getX() == getX() && v.getY() == v.getY();
		}
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return (int)(getX() * 3 + getY() * 5);
	}
}
