package model.spells;

import model.core.GameObject;
import model.core.IGameObject;
import model.core.SpellEffect;
import model.core.SpellNames;
/**
 * A spelleffect that damages over time
 * @author Olliver
 *
 */
public class DoTEffect extends SpellEffect {
	private float initialDuration;
	private int damage;
	private float timeElapsed;
	private float intervall;
	@SuppressWarnings({"unused" })
	private DoTEffect() {
		// TODO Auto-generated constructor stub
	}

	public DoTEffect(SpellNames spellName,float duration, int damage, float intervall) {
		this(spellName,duration,damage,intervall,0,null);
	}
	public DoTEffect(SpellNames spellName,float duration, int damage, float intervall, IGameObject caster) {
		this(spellName,duration,damage,intervall,0,caster);
	}
	public DoTEffect(SpellNames spellName,float duration, int damage, float intervall, float timeElapsed, IGameObject caster) {
		super(spellName,duration,caster);
		this.damage = damage;
		this.intervall = intervall;
		this.timeElapsed = timeElapsed;
		this.initialDuration = duration;
	}

	@Override
	public void perform(GameObject object, float dTime) {
		timeElapsed += dTime/60;
		timePassed(dTime);
		if(timeElapsed > intervall){
			object.damage(damage/((int)(initialDuration/intervall)),caster);
			timeElapsed = 0;
		}

	}

	@Override
	public boolean isDone() {
		return duration <= 0;
	}

	@Override
	public SpellEffect copy() {
		return new DoTEffect(getSpellName(),duration,damage,intervall,timeElapsed, caster);
	}

}
