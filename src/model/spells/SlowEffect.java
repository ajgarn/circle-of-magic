package model.spells;

import model.core.GameObject;
import model.core.IGameObject;
import model.core.SpellEffect;
import model.core.SpellNames;
/**
 * A spelleffect that slows
 * @author Olliver
 *
 */
public class SlowEffect extends SpellEffect {

	
	float slowPercent;
	public SlowEffect() {
	}
	public SlowEffect(SpellNames spellName,float duration,float slowPercent) {
		this(spellName,duration,slowPercent,null);
	}
	public SlowEffect(SpellNames spellName,float duration,float slowPercent, IGameObject caster) {
		super(spellName,duration,caster);
		this.slowPercent = slowPercent;
	}

	@Override
	public void perform(GameObject object, float dTime) {
		if(!isAlreadyPerformed()){
			float temp = object.getSpeed()*slowPercent;
			object.setSpeed(temp);
			timePassed(dTime);
			alreadyPerformed();
		}else{
			timePassed(dTime);
			if(getDuration() < 0){
				float temp = object.getSpeed()/slowPercent;
				object.setSpeed(temp);
			}
		}

	}

	@Override
	public boolean isDone() {
		return getDuration() < 0;
	}

	@Override
	public SpellEffect copy() {
		return new SlowEffect(getSpellName(),duration, slowPercent,caster);
	}

}
