package model.spells;

import java.util.LinkedList;
import java.util.List;

import model.core.IAction;
import model.core.ICollidable;
import model.core.IGameObject;
import model.core.MoveAction;
import model.core.SpellEffect;
import model.core.State;
import services.math.IVector2;
/**
 * A class that represents a Bouncingspellobject
 * @author Olliver
 *
 */
public class BouncingSpellObject extends SpellObject {


	private transient IGameObject previouslyBounced;
	public BouncingSpellObject() {
		// TODO Auto-generated constructor stub
	}

	public BouncingSpellObject(SpellRecipe spell, IGameObject caster,
			IAction action) {
		super(spell, caster, action);
	}

	public BouncingSpellObject(SpellRecipe spell, IGameObject caster,
			IVector2 target) {
		super(spell, caster, target);
		// TODO Auto-generated constructor stub
	}

	public BouncingSpellObject(SpellRecipe spell, IGameObject caster) {
		super(spell, caster);
		// TODO Auto-generated constructor stub   
	}
	@Override
	public void collision(ICollidable object){
		if (object instanceof ISpellAffectable) {
			bounce(object);
		} else if (object instanceof ISpellObject) {
			SpellObject so = (SpellObject) object;
			int compare = spell.compareTo(so.spell);
			if (compare < 0) {
				this.kill();
			} else if (compare > 0) {
				so.kill();
			} else {
				this.kill();
				so.kill();
			}
		}
	}
	@Override
	public boolean isColliding(ICollidable object) {
		if (getState() == State.DYING) {
			return false;
		}
		return getShape().intersects(object.getShape());
	}
	public void bounce (ICollidable object){
		if(object == caster && previouslyBounced == null){}
		else if(object == caster && previouslyBounced != null){
			IVector2 temp = previouslyBounced.getPosition().cpy();
			temp.sub(caster.getPosition());//gets the vector that is the vector between the target and the characters position
			temp.nor();
			temp.mul(spell.getRange());//multiplies it so that it is the range it gets double the range unless divided by 2
			temp.add(caster.getPosition());//acquires the new target
			addAction(new MoveAction(temp));
		}else if(object == caster){
		}else{
			List<SpellEffect> effects = new LinkedList<SpellEffect>();
			for(SpellEffect e : spell.getSpellEffects()){
				SpellEffect effect = e.copy();
				effect.addCaster(caster);
				effects.add(effect);
			}
			((ISpellAffectable) object).addSpellEffect(effects);
			previouslyBounced = (IGameObject)object;
			IVector2 temp = caster.getPosition().cpy();
			temp.sub(this.getPosition());//gets the vector that is the vector between the target and the characters position
			temp.nor();
			temp.mul(spell.getRange());//multiplies it so that it is the range it gets double the range unless divided by 2
			temp.add(((IGameObject)object).getPosition());//acquires the new target
			addAction(new MoveAction(temp));
		}
		
	}

}
