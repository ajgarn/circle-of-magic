package model.spells;

import services.network.INetworkObject;
/**
 * A model representation of a spellslot. Manages the cooldown mostly
 * @author Olliver
 *
 */
public class SpellSlot implements INetworkObject{	
	private SpellRecipe spellRecipe;
	private float cooldownRemaining;
	public SpellSlot(){	}
	public SpellSlot(SpellRecipe spellRecipe){
		this.spellRecipe = spellRecipe;
		cooldownRemaining = -1;
	}
	public SpellSlot(SpellSlot slot){
		this.spellRecipe = slot.getSpellRecipe();
		this.cooldownRemaining = slot.getRemainingCooldown();
	}
	/**
	 * Updates the cooldown on this SpellSlot
	 * @param dTime
	 */
	public void update(float dTime){
		if(cooldownRemaining > 0){
			cooldownRemaining -= dTime / 60;
		}
	}
	/**
	 * Get whenever the spellRecipe on this spellslot can be casted
	 * @return
	 */
	public boolean isAvaiable(){
		return cooldownRemaining <= 0;
	}
	
	/**
	 * Get the ramaining cooldown on this Spell
	 * @return The remaining cooldows in seconds.
	 */
	public float getRemainingCooldown(){
		return cooldownRemaining;
	}
	
	/**
	 * Use the spellRecipe on this spellSlot
	 * This puts the spell on cooldown
	 */
	public void useSpell(){
		cooldownRemaining = spellRecipe.getCooldown();
	}
	/**
	 * Reset the cooldown on the spell on this spellslot
	 */
	public void resetCooldown(){
		cooldownRemaining = 0;
	}
	/**
	 * Get the spellRecipe on this spell
	 * @return The spellRecipe on this spell
	 */
	public SpellRecipe getSpellRecipe(){
		return spellRecipe;
	}
	/**
	 * When a new spellSlot is received from
	 * the server, refresh the spellSlot,
	 * @param spellSlot
	 */
	public void refresh(SpellSlot spellSlot){
		this.spellRecipe = spellSlot.spellRecipe;
		this.cooldownRemaining = spellSlot.cooldownRemaining;
	}
}
