package model.spells;

import model.core.GameObject;
import model.core.IGameObject;
import model.core.State;
import services.math.IVector2;

/**
 * An action for Projectile Spells
 * @author Olliver
 *
 */
public class ProjectileAction extends CastSpellAction {

	private int count = 0;
	private float delayLeft;
	private float angle;
	private float changedAngle;
	public ProjectileAction() {
		// TODO Auto-generated constructor stub
	}

	public ProjectileAction(SpellSlot spell, IVector2 target) {
		super(spell, target,false,false,false,1);
		angle = 0;
	}

	@Override
	public void perform(float dTime, GameObject gameObject) {
		gameObject.setState(getSpellRecipe().getState()); 
		if(angle == 0){
			setAngle(gameObject);//Sets the angle to be the angle of the vector 
			//between the target and the object who cast it
			changedAngle = angle;
			if(getSpellRecipe().getNumberOf() > 1){//if more than one spellobject will be created make it so that we start from one side and move to the other
				changedAngle = changedAngle + (getSpellRecipe().getAngelIntervall()/2);
			}
		}
		setTimeLeft(getTimeLeft()-dTime/60);
		gameObject.lookAt(angle);
		if(getTimeLeft() <= 0f){
			for(int i = 0; i < getSpellRecipe().getNumberOf(); i++){//Creates the number of spellobjects as indicated in the constructor
				if(delayLeft <=0 && count != getSpellRecipe().getNumberOf()){
					delayLeft = getSpellRecipe().getDelay();
					IVector2 temp = getNewTarget(gameObject);
					SpellObject o = SpellObjectFactory.create(getSpellRecipe(),temp,gameObject);
					addSpellObject(o);
					changedAngle -= getSpellRecipe().getAngelIntervall()/(getSpellRecipe().getNumberOf()-1);//sets the new angle
					count++;
				}
				delayLeft -=dTime/60;
			}
			if (count == getSpellRecipe().getNumberOf()){
				setDone(true);
			}
			gameObject.setState(State.STANDING);
		}
	}
	private void setAngle(IGameObject object){
		angle = getTarget().cpy().sub(object.getPosition()).angle();
	}
	private IVector2 getNewTarget(IGameObject object){
		IVector2 temp = getTarget().cpy();
		temp.sub(object.getPosition());//gets the vector that is the vector between the target and the characters position
		temp.setAngle(changedAngle);//Sets the angle of the vector so that we can acquire a new target position with a 
		//slightly tilted angle
		temp.nor();
		temp.mul(getSpellRecipe().getRange());//multiplies it so that it is the range
		temp.add(object.getPosition());//acquires the new target
		return temp;
	}

}
