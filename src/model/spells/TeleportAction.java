package model.spells;

import model.core.GameObject;
import model.core.IGameObject;
import services.math.IVector2;

/**
 * An action that teleports the caster
 * @author Olliver
 *
 */
public class TeleportAction extends CastSpellAction {

	public TeleportAction() {
		// TODO Auto-generated constructor stub
	}

	public TeleportAction(SpellSlot spell, IVector2 target) {
		super(spell, target,true,false,false,1);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void perform(float dTime, GameObject gameObject) {
		IVector2 temp = getTarget().cpy().sub(gameObject.getPosition());
		if(temp.len() < getSpellRecipe().getRange()){
			gameObject.setPosition(getTarget());
			setDone(true);
		}else{
			temp.nor();
			temp.mul(getSpellRecipe().getRange());
			temp.add(gameObject.getPosition());
			gameObject.setPosition(temp);
			setDone(true);
		}

	}
	@Override
	public boolean isDone(IGameObject gameObject) {
		if(done){
			getSpellSlot().useSpell();
		}
		return done;
	}
}
