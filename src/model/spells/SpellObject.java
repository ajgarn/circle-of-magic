package model.spells;

import java.util.LinkedList;
import java.util.List;

import model.core.CircleShape;
import model.core.GameObject;
import model.core.IAction;
import model.core.ICollidable;
import model.core.IGameObject;
import model.core.IShape;
import model.core.SpellEffect;
import model.core.SpellNames;
import model.core.State;
import services.math.IVector2;
/**
 * A representation of a SpellObject in the world. Holds data that is associated with this object
 * @author Olliver
 * @author Anton Jansson
 */

public class SpellObject extends GameObject implements ICollidable, ISpellObject{
	protected List<GameObject> alreadyHit = new LinkedList<GameObject>();
	protected IGameObject caster;
	private transient final IShape shape;
	protected transient final SpellRecipe spell;
	private final SpellNames spellName;
	
	protected SpellObject(){
		super();
		caster = null;
		spell = null;
		shape = new CircleShape(1);
		spellName = SpellNames.BLINK;
	}
	public SpellObject(SpellRecipe spell, IGameObject caster, IAction action) {
		this(spell, caster);
		addAction(action);
	}
	public SpellObject(SpellRecipe spell, IGameObject caster, IVector2 target) {
		super(target, 0, spell.getState(), spell.getSpeed(), 10);
		this.spell = spell;
		this.caster = caster;
		this.spellName = spell.getName();
		shape = new CircleShape(spell.getDiameter());
		addAction(new DurationAction(spell));
	}
	public SpellObject(SpellRecipe spell, IGameObject caster){
		super(caster.getPosition(), 0, spell.getState(), spell.getSpeed(), 10);
		this.spell = spell;
		this.caster = caster;
		this.spellName = spell.getName();
		shape = new CircleShape(spell.getDiameter());
	}
	/* (non-Javadoc)
	 * @see model.spells.ISpellObject#getSpellName()
	 */
	@Override
	public SpellNames getSpellName() {
		return spellName;
	}
	/* (non-Javadoc)
	 * @see model.spells.ISpellObject#isDead()
	 */
	@Override
	public boolean isDead() {
		if(getState() == State.DYING){
			if(getActions().isEmpty()){
				kill();
			}
			return getActions().isEmpty();
		}else if(getActions().isEmpty()){
			setState(State.DYING);
			addAction(new DurationAction(0.1f));
		}
		return false;
	}
	@Override
	public void kill() {
		getActions().clear();
		super.kill();
	}
	@Override
	public boolean isColliding(ICollidable object) {
		if (getState() == State.DYING || object.equals(caster)) {
			return false;
		}
		if (object instanceof SpellObject && caster.equals(((SpellObject) object).caster)) {
			return false;
		}
		return getShape().intersects(object.getShape());
	}
	@Override
	public void collision(ICollidable object) {
		if (object instanceof ISpellAffectable) {
			List<SpellEffect> effects = new LinkedList<SpellEffect>();
			for(SpellEffect e : spell.getSpellEffects()){
				SpellEffect effect = e.copy();
				effect.addCaster(caster);
				effects.add(effect);
			}
			((ISpellAffectable) object).addSpellEffect(effects);
			kill();
		} else if (object instanceof SpellObject) {
			SpellObject so = (SpellObject) object;
			int compare = spell.compareTo(so.spell);
			if (compare < 0) {
				this.kill();
			} else if (compare > 0) {
				so.kill();
			} else {
				this.kill();
				so.kill();
			}
		}
	}
	@Override
	public IShape getShape() {
		shape.setPosition(getPosition());
		return shape;
	}
	/* (non-Javadoc)
	 * @see model.spells.ISpellObject#getCaster()
	 */
	@Override
	public IGameObject getCaster(){
		return caster;
	}
	/* (non-Javadoc)
	 * @see model.spells.ISpellObject#refresh(model.core.IGameObject)
	 */
	@Override
	public void refresh(IGameObject o){
		super.refresh(o);
		if(o instanceof SpellObject){
			ISpellObject s = (ISpellObject)o;
			this.caster = s.getCaster();
		}
	}
	public void setCaster(IGameObject caster) {
		this.caster = caster;
	}
	@Override
	public void heal(int heal) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void damage(int damage, IGameObject damager) {
		// TODO Auto-generated method stub
		
	}
}
