package model.spells;

import model.core.GameObject;
import model.core.IGameObject;
import model.core.SpellEffect;
import model.core.SpellNames;

/**
 * A spelleffect that damages once
 * @author Olliver
 *
 */
public class DamageEffect extends SpellEffect {

	private int damage;
	public DamageEffect(){
		super();
	}
	public DamageEffect(SpellNames spellName,float duration,int damage){
		this(spellName,duration,damage,null);
	}
	public DamageEffect(SpellNames spellName,float duration, int damage, IGameObject caster) {
		super(spellName,duration,caster);
		this.damage = damage;
	}

	@Override
	public void perform(GameObject object, float dTime) {
		if(!isAlreadyPerformed()){
			object.damage(damage,caster);
			timePassed(dTime);
			alreadyPerformed();
		}else{
			timePassed(dTime);
		}
	}

	@Override
	public boolean isDone() {
		return getDuration() <= 0;
		
	}
	@Override
	public SpellEffect copy() {
		return new DamageEffect(getSpellName(),duration, damage, caster);
	}

}
