package model.spells;

import model.core.GameObject;
import model.core.IGameObject;
import model.core.SpellEffect;
import model.core.SpellNames;
/**
 * a spelleffect that heals over time
 * @author Olliver
 *
 */
public class HoTEffect extends SpellEffect {
	private int heal;
	private float timeElapsed;
	private float intervall;
	public HoTEffect() {
		// TODO Auto-generated constructor stub
	}
	public HoTEffect(SpellNames spellName,float duration, int heal, int intervall) {
		this(spellName,duration,heal,intervall,null);
	}
	public HoTEffect(SpellNames spellName,float duration,int heal, float intervall, IGameObject caster) {
		this(spellName,duration,heal,intervall,0,caster);
	}
	public HoTEffect(SpellNames spellName,float duration,int heal, float intervall, float timeElapsed, IGameObject caster) {
		super(spellName,duration,caster);
		this.heal = heal;
		this.intervall = intervall;
		this.timeElapsed = timeElapsed;
	}

	@Override
	public void perform(GameObject object, float dTime) {
		timeElapsed += dTime/60;
		timePassed(dTime);
		if(timeElapsed > intervall){
			object.heal(heal);
			timeElapsed = 0;
		}

	}

	@Override
	public boolean isDone() {
		return duration <= 0;
	}

	@Override
	public SpellEffect copy() {
		return new HoTEffect(getSpellName(),duration,heal,intervall,timeElapsed,caster);
	}

}
