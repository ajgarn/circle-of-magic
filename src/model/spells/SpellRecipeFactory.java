package model.spells;

import java.util.LinkedList;
import java.util.List;

import model.core.SpellEffect;
import model.core.SpellNames;
import model.core.State;

/**
 * Factory for SpellRecipes.
 * 
 * @author Alexander H�renstam
 * @author Olliver
 */

public class SpellRecipeFactory {
	public static SpellRecipe createSpellRecipe(SpellNames name){
		List<SpellEffect> spellEffects = new LinkedList<SpellEffect>();
		switch(name){
		case FIREBALL: 
			return createFireball(spellEffects,name);
		case FROSTBOLT:
			return createFrostbolt(spellEffects,name);
		case BOOMERANG:
			return createBoomerang(spellEffects, name);
//		case BOWLINGROCK:
//			not yet implemented
//			return createBowlingRock(spellEffects, name);
		case FIRESPRAY:
			return createFirespray(spellEffects, name);
//		case GRAVITYBOLT:
//			not yet implemented
//			return createGravitybolt(spellEffects, name);
		case BLINK:
			return createBlink(spellEffects, name);
		case ENFLAME:
			return createEnflame(spellEffects, name);
		case BOUNCING:
			return createBouncing(spellEffects, name);
		case LIGHTNINGBOLT:
			return createLightningbolt(spellEffects, name);
		default:
			return new SpellRecipe();
		}
	}
	private static SpellRecipe createFireball(List<SpellEffect> spellEffects, SpellNames name){
		spellEffects.add(new DamageEffect(name,0,8));
		spellEffects.add(new DoTEffect(name, 4, 4, 2));
		return new SpellRecipe(name, 3f,1,2f, 0.4f, 18f, 18f, 
			false, true, spellEffects, State.CASTTARGETSPELL1, "A simple fireball spell",1,0,0,0,1);
	}
	private static SpellRecipe createFrostbolt(List<SpellEffect> spellEffects, SpellNames name){
		spellEffects.add(new DamageEffect(name,0,4));
		spellEffects.add(new SlowEffect(name,4f, 0.6f));
		return new SpellRecipe(name, 10f,1, 2f, 0.4f, 20f, 20f, false, true, spellEffects, State.CASTTARGETSPELL1, "A simple frostbolt spell that slows",
				1, 0, 0, 0,1);
	}
	private static SpellRecipe createLightningbolt(List<SpellEffect> spellEffects, SpellNames name){
		spellEffects.add(new DamageEffect(name,0,8));
		spellEffects.add(new StunEffect(name,1.5f));
		return new SpellRecipe(name, 15f,1,2f, 0.4f, 18f, 18f, 
			false, true, spellEffects, State.CASTTARGETSPELL1, "A simple LightningBolt spell that stuns",1,0,0,0,3);
	}
	private static SpellRecipe createBouncing(List<SpellEffect> spellEffects, SpellNames name){
		spellEffects.add(new DamageEffect(name,0,4));
		return new SpellRecipe(name, 20f,1, 2f, 0.4f, 20f, 22f, false, true, spellEffects, State.CASTTARGETSPELL1, "A bouncing spell"
				, 1, 0, 0, 0,3);
	}
	private static SpellRecipe createEnflame(List<SpellEffect> spellEffects, SpellNames name){
		spellEffects.add(new DamageEffect(name,0f,10));
		return new SpellRecipe(name, 10f,1, 11f, 0.3f, 0, 0, false, false, spellEffects, State.CASTAOESPELL, 
				"An AOE fire spell", 1, 0, 0, 0.5f,1);
	}
	private static SpellRecipe createFirespray(List<SpellEffect> spellEffects, SpellNames name){
		spellEffects.add(new DamageEffect(name,0,3));
		return new SpellRecipe(name, 18f,1, 2f, 0.3f, 14f, 20f, false, true, spellEffects, State.CASTTARGETSPELL0,
				"Hurls multiple FireBalls at the opponents", 7, 45, 0, 0,2);
	}
	private static SpellRecipe createBlink(List<SpellEffect> spellEffects, SpellNames name){
		return new SpellRecipe(name, 10f,1, 0, 0, 10f, 0, false, true, spellEffects, State.CASTNOTARGETSPELL,
				"Blinks to the location", 0, 0, 0, 0,2);
	}
	
	private static SpellRecipe createBoomerang(List<SpellEffect> spellEffects, SpellNames name){
		spellEffects.add(new DamageEffect(name,0,10));
		return new SpellRecipe(name, 15f,2, 3f, 0.3f, 15f, 15f, false, true, spellEffects, State.CASTTARGETSPELL0,
				"A boomerang spell that casts a boomerang that returns to the caster",
				1, 0, 0, 0,2);
	}
	private static SpellRecipe createBowlingRock(List<SpellEffect> spellEffects, SpellNames name){
		spellEffects.add(new DamageEffect(name,0,3));
		return new SpellRecipe(name, 30f,1, 4f, 0.3f, 30f, 10f, false, true, spellEffects, State.CASTTARGETSPELL0, 
				"Hurls a huge rock at your opponents", 1, 0, 0, 0,3);
	}
	private static SpellRecipe createGravitybolt(List<SpellEffect> spellEffects, SpellNames name){
		spellEffects.add(new DamageEffect(name,0, 3));
		return new SpellRecipe(name, 20f,1, 2f, 0.4f, 20f, 18f, false, true, spellEffects, State.CASTTARGETSPELL1,
				"A gravity ball that pulls objects towards it", 1, 0, 0, 0,3);
	}
}
