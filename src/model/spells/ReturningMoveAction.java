package model.spells;

import model.core.GameObject;
import model.core.MoveAction;
import model.core.Vector2;
import services.math.IVector2;
/**
 * A moveaction that returns an object to the gameobject that is put in perform
 * @author Olliver
 *
 */
public class ReturningMoveAction extends MoveAction {
	@SuppressWarnings("unused")
	private ReturningMoveAction(){
		this(new Vector2());
	}
	public ReturningMoveAction(IVector2 newPosition) {
		super(newPosition);
		
	}
	@Override
	public void perform(float dTime, GameObject obj){
		super.perform(dTime, obj);
		if(obj instanceof ISpellObject){
			ISpellObject spellobj = (ISpellObject)obj;
			newPosition = spellobj.getCaster().getPosition();
		}
	}

}
