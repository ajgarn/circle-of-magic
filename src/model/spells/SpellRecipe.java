package model.spells;

import java.util.LinkedList;
import java.util.List;

import model.core.SpellEffect;
import model.core.SpellNames;
import model.core.State;
import services.network.INetworkObject;

/**
 * A representation of a Spell. Holds all the data for a spell.
 * 
 * @author Olliver
 * @author Alexander
 * @author Anton Jansson
 */

public class SpellRecipe implements INetworkObject, Comparable<SpellRecipe> {
	/**
	* Cooldown 1 = 1 sec.
	 */
	private final float cooldown;
	/**
	 * Diameter of the spell object in the world.
	 */
	private final float diameter;
	/**
	 * The power of the spell
	 */
	private final int power;
	/**
	/**
	 * castingTIme 1 = 1 sec.
	 */
	private final float castingTime;
	/**
	 * 20 range ~ half screen. (1 ~ character's size).
	 */
	private final float range;
	/**
	 * Has to click on a character in order to fire.
	 */
	private final boolean requiresTarget;
	/**
	 * Has to aim the spell, if false (cast on yourself).
	 */
	private final boolean requiresAim;
	/**
	 * Player state after spellcast, I.E. ENUM; Casting/Standing/Walking.
	 */
	private final State state;
	/**
	 * Buffs or debuffs affecting player hit.
	 */
	private final List<SpellEffect> spellEffects;
	/**
	 * Speed for the SpellObject in the world.
	 */
	private final float speed;
	/**
	 * Name of spell.
	 */
	private final SpellNames spellName;
	/**
	 * Number of spellObjects to be created
	 */
	private final int numberOf;
	/**
	 * The angle Intervall of the Spells
	 */
	private final float angleIntervall;
	/**
	 * The amount of time between when the Spells are created
	 */
	private final float delay;
	/**
	 * Description of spell.
	 */
	private final String description;
	/**
	 * The duration of the spell
	 */
	private final float duration;
	/**
	 * The cost of the spell
	 */
	private final int cost;
	public SpellRecipe(){
		this(SpellNames.BLINK,0,0,1,0,0,0,false,false,null,State.CASTTARGETSPELL0,"",1,0,0,0,1);
	}
	/**
	 * Setting all final values for a spell.
	 * @param spellName
	 * @param cooldown
	 * @param damage
	 * @param diameter
	 * @param castingTime
	 * @param range
	 * @param speed
	 * @param requiresTarget
	 * @param requiresAim
	 * @param spellEffects
	 * @param state
	 * @param description
	 * @param numberOf
	 * @param angleIntervall
	 * @param delay
	 */
	public SpellRecipe(SpellNames spellName, float cooldown,int power, float diameter, float castingTime, float range, float speed, boolean requiresTarget, boolean requiresAim, List<SpellEffect> spellEffects, State state, String description,int numberOf,float angleIntervall,float delay,float duration, int cost){
		this.cooldown = cooldown;
		this.diameter = diameter;
		this.power = power;
		this.castingTime = castingTime;
		this.range = range;
		this.speed = speed;
		this.requiresTarget = requiresTarget;
		this.requiresAim = requiresAim;
		this.spellEffects = spellEffects;
		this.state = state;
		this.spellName = spellName;
		this.description = description;
		this.numberOf = numberOf;
		this.angleIntervall = angleIntervall;
		this.delay = delay;
		this.duration = duration;
		this.cost = cost;
	}

	public int getNumberOf(){
		return numberOf;
	}
	public float getAngelIntervall(){
		return angleIntervall;
	}
	public float getDelay(){
		return delay;
	}
	/**
	 * Returns the speed for the SpellObject for this spell.
	 */
	public float getSpeed() {
		return speed;
	}
	/**
	 * Returns state for mage after spell cast.
	 * @return
	 */
	public State getState(){
		return state;
	}
	/**
	 * Returns cooldown of the spell.
	 * @return
	 */
	public float getCooldown(){
		return cooldown;
	}
	/**
	 * Returns damage of the spell.
	 * @return
	 */
	public int getPower(){
		return power;
	}
	/**
	 * Returns castingtime of spell.
	 * @return
	 */
	public float getCastingTime(){
		return castingTime;
	}
	/**
	 * Returns the range of spell.
	 * @return
	 */
	public float getRange(){
		return range;
	}
	/**
	 * Get whether the spell has to be aimed.
	 * @return
	 */
	public boolean getRequiresAim(){
		return requiresAim;
	}
	/**
	 * Return whether the spell requires a target to launch.
	 * @return
	 */
	public boolean getRequiresTarget(){
		return requiresTarget;
	}
	/**
	 * 
	 */
	public List<SpellEffect> getSpellEffects(){
		List<SpellEffect> effects = new LinkedList<SpellEffect>();
		for(SpellEffect e: spellEffects){
			effects.add(e.copy());
		}
		return effects;
	}
	/**
	 * Returns the spell name.
	 * @return
	 */
	public SpellNames getName(){
		return spellName;
	}
	/**
	 * Get a String description of a spell.
	 * @return
	 */
	public String getDescription() {
		return description;
	}
		@Override
	public int compareTo(SpellRecipe o) {
		return getPower() - o.getPower();
	}
	/**
	 * Get the diameter of the spell object in the world;
	 * @return
	 */
	public float getDiameter() {
		return diameter;
	}
	public float getDuration(){
		return duration;
	}
	public int getCost(){
		return cost;
	}
}
