package model.spells;

import model.core.Action;
import model.core.GameObject;
import model.core.IGameObject;
import model.core.SpellNames;

/**
 * An action that delays an object
 * @author Olliver
 *
 */
public class DurationAction extends Action {
	private SpellNames name;
	private float timeLeft;
	public DurationAction() {
		// TODO Auto-generated constructor stub
	}
	public DurationAction(SpellRecipe spell){
		this(spell,false,false,false,0);
	}
	public DurationAction(SpellRecipe spell,boolean clearLowerPriority, boolean clearActions, boolean canInterruptAction, int priority){
		super(clearLowerPriority, clearActions, canInterruptAction, priority);
		timeLeft = spell.getDuration();
		name = spell.getName();
	}
	public DurationAction(float time){
		timeLeft = time;
	}

	@Override
	public void perform(float dTime, GameObject gameObject) {
		timeLeft -= dTime/60;
		
	}
	

	@Override
	public boolean isDone(IGameObject gameObject) {
		return timeLeft <=0;
	}
	@Override
	public boolean equals(Object o){
		return super.equals(o) && name == ((DurationAction)o).name;
	}

}
