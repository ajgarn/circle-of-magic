package model.spells;

import model.core.GameObject;
import model.core.State;
import services.math.IVector2;
/**
 * An action for an AOE Spell
 * @author Olliver
 *
 */
public class AOEAction extends CastSpellAction {

	public AOEAction() {
		// TODO Auto-generated constructor stub
	}

	public AOEAction(SpellSlot spell, IVector2 target) {
		super(spell, target,false,false,false,1);
		// TODO Auto-generated constructor stub
	}
	public AOEAction(SpellSlot spell){
		super(spell, null, false, false, false, 1);
	}

	@Override
	public void perform(float dTime, GameObject gameObject) {
		if(getTarget() == null){
			setTarget(gameObject.getPosition());
		}
		gameObject.setState(State.CASTAOESPELL);
		SpellObject o = SpellObjectFactory.create(getSpellRecipe(),getTarget(),gameObject);
		addSpellObject(o);
		setDone(true);
		
	}

}
