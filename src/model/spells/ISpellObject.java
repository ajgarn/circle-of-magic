package model.spells;

import model.core.IGameObject;
import model.core.SpellNames;
/**
 * An interface for the spellobjects in the world. Holds the methods that are the same
 * @author Olliver
 *
 */
public interface ISpellObject extends IGameObject {

	public abstract SpellNames getSpellName();

	public abstract IGameObject getCaster();

}