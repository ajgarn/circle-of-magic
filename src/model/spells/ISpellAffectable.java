package model.spells;

import java.util.List;

import model.core.SpellEffect;


/**
 * Interface for all objects that can be affected by spells.
 * @author Anton Jansson
 */
public interface ISpellAffectable {
	/**
	 * Add a SpellEffect to the this object.
	 * @param spelleffect The SpellEffect to add.
	 */
	void addSpellEffect(SpellEffect spelleffect);
	/**
	 * Add a list of SpellEffects to this object.
	 * @param spellEffects The list of SpellEffects to add.
	 */
	void addSpellEffect(List<SpellEffect> spellEffects);
}
