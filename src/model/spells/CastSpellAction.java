package model.spells;

import java.util.LinkedList;
import java.util.List;

import model.core.Action;
import model.core.GameObject;
import model.core.IGameObject;
import model.core.IGameObjectCreatorAction;
import services.math.IVector2;

/**
 * An action that is performed when a spell is cast
 * @author Olliver
 * @author Anton Jansson
 *
 */
public abstract class CastSpellAction extends Action implements IGameObjectCreatorAction {
	private SpellSlot spell;
	private float timeLeft;
	protected boolean done = false;
	private IVector2 target;
	// Only server is creating the objects, thus transient.
	private transient final List<SpellObject> spellObjects;
	
	public CastSpellAction(){
		this(null, null,false,false,false,1);
	}
	public CastSpellAction(SpellSlot spell, IVector2 target,boolean clearLowerPriority, boolean clearActions, boolean canInterruptAction, int priority){
		super(clearLowerPriority, clearActions, canInterruptAction, priority);
		this.spell = spell;
		this.target = target;
		spellObjects = new LinkedList<SpellObject>();
		if (spell != null) {
			setTimeLeft(spell.getSpellRecipe().getCastingTime());
		}
	}
	@Override
	public abstract void perform(float dTime, GameObject gameObject);
	@Override
	public boolean isDone(IGameObject gameObject) {
		if(done){
			spell.useSpell();
		}
		return done;
	}
	protected void setTimeLeft(float time){
		timeLeft = time;
	}
	public float getTimeLeft(){
		return timeLeft;
	}
	public IVector2 getTarget(){
		if(target == null){
			return target;
		}
		return target.cpy();
	}
	public SpellRecipe getSpellRecipe(){
		return spell.getSpellRecipe();
	}
	protected void setDone(boolean done){
		this.done = done;
	}
	protected void addSpellObject(SpellObject object){
		spellObjects.add(object);
	}
	protected void setTarget(IVector2 target){
		this.target = target.cpy();
	}
	/**
	 * Returns all SpellObjects that should be added to the world,
	 * and clears them from the list.
	 * @return List of SpellObjects.
	 */
	@Override
	public List<? extends SpellObject> pullGameObjects(){
		List<? extends SpellObject> objects = new LinkedList<SpellObject>(spellObjects);
		spellObjects.clear();
		return objects;
	}
	public SpellSlot getSpellSlot(){
		return spell;
	}
	public void updateSpellSlot(SpellSlot[] slots){
		for(SpellSlot slot : slots){
			if(slot != null && spell.getSpellRecipe().getName() == slot.getSpellRecipe().getName()){
				spell = slot;
				break;
			}
		}
	}
	@Override
	public boolean equals(Object o){
		if(this == o){
			return true;
		}else if(o == null || getClass() != o.getClass()){
			return false;
		}
		return getSpellRecipe().getName() == ((CastSpellAction)o).getSpellRecipe().getName();
	}

}
