package model.spells;

import java.util.LinkedList;
import java.util.List;

import model.core.GameObject;
import model.core.IAction;
import model.core.ICollidable;
import model.core.IGameObject;
import model.core.SpellEffect;
import services.math.IVector2;
/**
 * A spellobject that represents an AOESpell
 * @author Olliver
 *
 */
public class AOESpellObject extends SpellObject {

	public AOESpellObject() {
		// TODO Auto-generated constructor stub
	}

	public AOESpellObject(SpellRecipe spell, IGameObject caster, IAction action) {
		super(spell, caster, action);
		// TODO Auto-generated constructor stub
	}

	public AOESpellObject(SpellRecipe spell, IGameObject caster, IVector2 target) {
		super(spell, caster, target);
		// TODO Auto-generated constructor stub
	}

	public AOESpellObject(SpellRecipe spell, IGameObject caster) {
		super(spell, caster);
		// TODO Auto-generated constructor stub
	}
	@Override
	public void collision(ICollidable object) {
		if (object instanceof ISpellAffectable){
			if(!alreadyHit.isEmpty())
			for(IGameObject o: alreadyHit){
				if(o.equals((IGameObject)object)){
					return;
				}
			}
			List<SpellEffect> effects = new LinkedList<SpellEffect>();
			for(SpellEffect e : spell.getSpellEffects()){
				SpellEffect effect = e.copy();
				effect.addCaster(caster);
				effects.add(effect);
			}
			((ISpellAffectable) object).addSpellEffect(effects);
			alreadyHit.add((GameObject)object);
		} else if (object instanceof ISpellObject) {
			SpellObject so = (SpellObject) object;
			int compare = spell.compareTo(so.spell);
			if (compare < 0) {
				this.kill();
			} else if (compare > 0) {
				so.kill();
			} else {
				this.kill();
				so.kill();
			}
		}
	}

}
