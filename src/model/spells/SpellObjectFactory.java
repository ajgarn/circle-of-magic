package model.spells;

import model.core.IGameObject;
import model.core.MoveAction;
import services.math.IVector2;

/**
 * Factory for creating {@link SpellObject}s.
 * @author Anton Jansson
 */
public class SpellObjectFactory {

	/**
	 * Create a {@link SpellObject}.
	 * @param spell The SpellRecipe
	 * @param vector Target position.
	 * @return
	 */
	public static SpellObject create(SpellRecipe spell, IVector2 target, IGameObject caster) {
		
		switch (spell.getName()) {
		case FIREBALL:
		case FROSTBOLT:
		case FIRESPRAY:
		case LIGHTNINGBOLT:
			return new SpellObject(spell, caster, new MoveAction(target));
		case BOUNCING:
			return new BouncingSpellObject(spell, caster, new MoveAction(target));
		case BOOMERANG:
			return new ReturningSpellObject(spell, caster, new MoveAction(target));
		case ENFLAME:
			return new AOESpellObject(spell, caster, target);
		default:
			return null;
		}
		
	}
	
}
