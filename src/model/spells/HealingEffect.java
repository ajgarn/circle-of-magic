package model.spells;

import model.core.GameObject;
import model.core.IGameObject;
import model.core.SpellEffect;
import model.core.SpellNames;
/**
 * A spelleffect that heals once
 * @author Olliver
 *
 */
public class HealingEffect extends SpellEffect {
	private int heal;
	public HealingEffect() {
		// TODO Auto-generated constructor stub
	}
	public HealingEffect(SpellNames spellName,float duration, int heal ) {
		this(spellName,duration,heal,null);
	}
	public HealingEffect(SpellNames spellName,float duration, int heal, IGameObject caster) {
		super(spellName,duration,caster);
		this.heal = heal;
	}

	@Override
	public void perform(GameObject object, float dTime) {
		if(!isAlreadyPerformed()){
			object.heal(heal);
			timePassed(dTime);
			alreadyPerformed();
		}else{
			timePassed(dTime);
		}

	}

	@Override
	public boolean isDone() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public SpellEffect copy() {
		return new HealingEffect(getSpellName(),duration,heal,caster);
	}

}
