package model.spells;

import java.util.LinkedList;
import java.util.List;

import model.core.GameObject;
import model.core.IAction;
import model.core.ICollidable;
import model.core.IGameObject;
import model.core.MoveAction;
import model.core.SpellEffect;
/**
 * A spellobject that returns to the caster
 * @author Olliver
 *
 */
public class ReturningSpellObject extends SpellObject {

	private boolean alreadyCleared = false;
	private boolean returning = false;
	public ReturningSpellObject() {
		// TODO Auto-generated constructor stub
	}

	public ReturningSpellObject(SpellRecipe spell, IGameObject caster,
			MoveAction action) {
		super(spell, caster, action);
		// TODO Auto-generated constructor stub
	}
	@Override
	public boolean isColliding(ICollidable object) {
		if(!alreadyHit.isEmpty()){
			for(IGameObject o: alreadyHit){
				if(o.equals((IGameObject)object)){
					return false;
				}
			}
		}
		return super.isColliding(object);
	}
	@Override
	public void collision(ICollidable object) {
		if (object instanceof ISpellAffectable){
			List<SpellEffect> effects = new LinkedList<SpellEffect>();
			for(SpellEffect e : spell.getSpellEffects()){
				SpellEffect effect = e.copy();
				effect.addCaster(caster);
				effects.add(effect);
			}
			((ISpellAffectable) object).addSpellEffect(effects);
			alreadyHit.add((GameObject)object);
		} else if (object instanceof ISpellObject) {
			SpellObject so = (SpellObject) object;
			int compare = spell.compareTo(so.spell);
			if (compare < 0) {
				this.kill();
			} else if (compare > 0) {
				so.kill();
			} else {
				this.kill();
				so.kill();
			}
		}
	}
	@Override
	public void update(float dTime){
		if (!getActions().isEmpty()) {
			IAction a = getActions().get(0);
			if(a.isDone(this)){
				removeAction(a);
				if(caster == null){
					kill();
				}
				if(!returning){
					addAction(new ReturningMoveAction(caster.getPosition()));
					returning = true;
				}
			}else{
				a.perform(dTime, this);
			}
		}
		if(!getActions().isEmpty()){
			IAction action = getActions().get(0);
			if(action instanceof ReturningMoveAction){
				if(!alreadyCleared){
					alreadyHit.clear();
					alreadyCleared = true;
				}
			}
		}
		if(getActions().isEmpty()){
			kill();
		}
	}

}
