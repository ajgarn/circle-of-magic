package model.spells;

import model.core.GameObject;
import model.core.IGameObject;
import model.core.SpellEffect;
import model.core.SpellNames;
/**
 * A spelleffect that stuns the opponent
 * @author Olliver
 *
 */
public class StunEffect extends SpellEffect {
	public StunEffect() {
	}
	public StunEffect(SpellNames spellName,float duration ) {
		this(spellName, duration,null);
	}
	public StunEffect(SpellNames spellName,float duration, IGameObject caster) {
		super(spellName, duration, caster);
	}

	@Override
	public void perform(GameObject object, float dTime) {
		if(!isAlreadyPerformed()){
			object.addAction(new StunnedAction(duration));
			alreadyPerformed();
		}
		timePassed(dTime);
	}

	@Override
	public boolean isDone() {
		return duration <= 0;
	}

	@Override
	public SpellEffect copy() {
		return new StunEffect(getSpellName(), duration, caster);
	}

}
