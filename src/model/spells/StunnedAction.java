package model.spells;
/**
 * An action that keeps the object stunned
 * @Author Olliver
 */
public class StunnedAction extends DurationAction {

	/**
	 * An action that keeps the object stunned
	 */
	public StunnedAction() {
		// TODO Auto-generated constructor stub
	}

	public StunnedAction(SpellRecipe spell) {
		super(spell,false,true,true,2);
		// TODO Auto-generated constructor stub
	}

	public StunnedAction(float time) {
		super(time);
		// TODO Auto-generated constructor stub
	}

}
