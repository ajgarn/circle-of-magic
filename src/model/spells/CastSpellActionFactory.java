package model.spells;

import services.math.IVector2;

/**
 * A factory for creating CastSpellActions
 * @author Olliver
 *
 */
public class CastSpellActionFactory {
	public static CastSpellAction create(SpellSlot spell, IVector2 target){
		switch(spell.getSpellRecipe().getName()){
		case FIREBALL:
		case FROSTBOLT:
		case LIGHTNINGBOLT:
		case GRAVITYBOLT:
		case FIRESPRAY:
		case BOOMERANG:
			return new ProjectileAction(spell,target);
		case ENFLAME:
			return new AOEAction(spell);
		case BLINK:
			return new TeleportAction(spell, target);
		case BOUNCING:
			return new ProjectileAction(spell, target);
		default:
			throw new IllegalArgumentException("That spell has no defiend castSpellAction");
		}
	}
}
