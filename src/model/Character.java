package model;

import java.util.LinkedList;
import java.util.List;

import model.core.CircleShape;
import model.core.GameObject;
import model.core.GameObjectListener;
import model.core.ICollidable;
import model.core.IGameObject;
import model.core.IShape;
import model.core.SpellEffect;
import model.core.SpellNames;
import model.core.State;
import model.spells.ISpellAffectable;
import services.math.IVector2;

/**
 * An abstract representation of a Character in the game. Holds the data for the character
 * @author Olliver
 * @author Anton Jansson
 */
public abstract class Character extends GameObject implements ICollidable, ISpellAffectable, ICharacter{

	private int health;
	protected List<SpellEffect> spelleffects;
	private transient final IShape shape = new CircleShape(2);

	protected Character() {}
	
	public Character(IVector2 position, float direction, State state, int health, float speed, int rotation){
		super(position,direction,state,speed,rotation);
		this.health = health;
		this.spelleffects = new LinkedList<SpellEffect>();
	}
	@Override
	public void addSpellEffect(SpellEffect spelleffect){
		spelleffects.add(spelleffect.copy());
		spellEffectAdded(this, spelleffect);
	}
	/**
	 * When a spellEffect has been added. Notify listeners
	 * @param spellEffect The added spellEffect
	 */
	public void spellEffectAdded(IGameObject source,SpellEffect spellEffect){
		for(GameObjectListener l : listeners){
			l.effectAdded(source, spellEffect);
		}
	}
	@Override
	public void addSpellEffect(List<SpellEffect> spelleffects){
		for(SpellEffect effect:spelleffects){
			this.spelleffects.add(effect.copy());
			spellEffectAdded(this, effect);
		}
	}
	public void removeSpellEffect(SpellEffect spelleffect){
		spelleffects.remove(spelleffect); 
		spellEffectRemoved(this, spelleffect);
	}
	/**
	 * When a spellEffect has been removed. Notify listeners
	 * @param spellEffect The removed spellEffect
	 */
	public void spellEffectRemoved(IGameObject source,SpellEffect spellEffect){
		for(GameObjectListener l : listeners){
			l.effectRemoved(source,spellEffect);
		}
	}
	/* (non-Javadoc)
	 * @see model.ICharacter#getHealth()
	 */
	@Override
	public int getHealth(){
		return health;
	}
	/* (non-Javadoc)
	 * @see model.ICharacter#getSpellEffects()
	 */
	@Override
	public List<SpellEffect> getSpellEffects(){
		return spelleffects;
	}
	@Override
	public boolean isDead(){
		return getHealth() <= 0;
	}
	public void kill(){
		health = 0;
		super.kill();
	}
		@Override
	public boolean isColliding(ICollidable object) {
		if (object instanceof Character) {
			return false;
		}
		return getShape().intersects(object.getShape());
	}

	@Override
	public void collision(ICollidable object) {
		// TODO: Handle collisions in Character?
	}

	@Override
	public IShape getShape() {
		shape.setPosition(getPosition());
		return shape;
	}
	@Override
	public void damage(int damage, IGameObject damager){
		health -= damage;
		for(GameObjectListener l : listeners){
			l.damaged(damager, this,damage);
		}
		if(isDead()){
			for(GameObjectListener l : listeners){
				l.killed(damager, this);
			}
			kill();
		}
	}
	@Override
	public void update(float time) {
		super.update(time);
		if(getActions().isEmpty()){	
			setState(State.STANDING);
		}
		if(!spelleffects.isEmpty()){
			java.util.Iterator<SpellEffect> it = spelleffects.iterator();
			while(it.hasNext()){
				SpellEffect effect = it.next();
				effect.perform(this, time);
				if(effect.isDone()){
					it.remove();
					for(GameObjectListener l : listeners){
						l.effectRemoved(this, effect);
					}
				}
			}
		}
	}
	@Override
	public void refresh(IGameObject o){
		super.refresh(o);
		if(o instanceof Character){
			ICharacter c = (ICharacter)o;
			this.health = c.getHealth();
			this.spelleffects = c.getSpellEffects();
		}
	}
	/* (non-Javadoc)
	 * @see model.ICharacter#getMaxHealth()
	 */
	@Override
	public int getMaxHealth() {
		return 100;
	}
	@Override
	public void heal(int heal){
		if(heal+health > getMaxHealth()){
			health = getMaxHealth();
		}else{
			health += heal;
		}

	}
	@Override
	public boolean isAffectedBySpell(SpellNames spellName) {
		for(SpellEffect spellEffect: spelleffects){
			if(spellEffect.getSpellName().equals(spellName)){
				return true;
			}
		}
		return false;
	}
}
