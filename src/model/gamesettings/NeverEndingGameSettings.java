package model.gamesettings;

import model.AbstractGame;
import model.AbstractGameSettings;

public class NeverEndingGameSettings extends AbstractGameSettings {
	
	public NeverEndingGameSettings() {
		super(1);
	}

	@Override
	protected int getRoundSpellPoints() {
		return 0;
	}

	@Override
	protected boolean shouldEndRoundInternal(AbstractGame game) {
		return false;
	}
	
	@Override
	public String toString() {
		return "Never ending game";
	}
}
