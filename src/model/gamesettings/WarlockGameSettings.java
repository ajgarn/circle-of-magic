/**
 * 
 */
package model.gamesettings;

import model.AbstractGame;
import model.AbstractGameSettings;

/**
 * @author Alexander H�renstam.
 *
 */
public class WarlockGameSettings extends AbstractGameSettings {
	//TODO implement subgamemodes:
	//Round, Deathmatch, King, Avatar, Ranked (RGC), Shuffle (team)
	//See Warlock gamemodes.
	
	public WarlockGameSettings() {
		this(10);
	}
	
	public WarlockGameSettings(int rounds) {
		super(rounds);
	}

	@Override
	protected int getRoundSpellPoints() {
		return 1;
	}

	@Override
	public boolean shouldEndRoundInternal(AbstractGame game) {
		return false;
	}
	
	@Override
	public String toString() {
		return "Warlock";
	}
}
