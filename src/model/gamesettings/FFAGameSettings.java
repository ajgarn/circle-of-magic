package model.gamesettings;

import model.AbstractGame;
import model.AbstractGameSettings;
import model.GameHeader;
import model.core.IGameObject;

/**
 * Free for all. End round when all player characters are dead.
 * @author Alexander H�renstam
 * @author Anton Jansson
 */
public class FFAGameSettings extends AbstractGameSettings {
	
	public FFAGameSettings() {
		this(2);
	}
	
	public FFAGameSettings(int rounds) {
		super(rounds);
	}

	@Override
	public boolean canCreateGame(GameHeader header) {
		if (header.getPlayers().size() < 2) {
			setMessage("There must be at least two players to start the game.");
			return false;
		}
		return true;
	}

	@Override
	protected int getRoundSpellPoints() {
		return 1;
	}

	@Override
	public boolean shouldEndRoundInternal(AbstractGame game) {
		int sum = 0;
		for (IGameObject character : game.getPlayerCharacters()) {
			if (character != null && !character.isDead()) {
				sum++;
			}
		}
		return sum <= 1;
	}
	
	@Override
	public String toString() {
		return "Free For All";
	}
}
