package model;

import services.network.INetworkObject;

/**
 * Class holding statistics for a specific player.
 * @author Alexander H�renstam.
 *
 */
public class PlayerStatistics implements INetworkObject{
	int killingBlows=0;
	int assists=0;
	int deaths=0;
	int damageDone=0;
	int damagePerSecond=0;
	int healingDone=0;
	int healingPerSecond=0;
	int healingReceived=0;
	int damageTaken=0;
	int spellsFired=0;
	int spellsHit=0;
	int crowdControl=0;
	int msDelay=0;
	
	/**
	 * Empty constructor for kryonet.
	 */
	public PlayerStatistics(){
		
	}
	/**
	 * @return the spellsHit
	 */
	public int getSpellsHit() {
		return spellsHit;
	}
	/**
	 * @param spellsHit the spellsHit to set
	 */
	public void setSpellsHit(int spellsHit) {
		this.spellsHit = spellsHit;
	}
	/**
	 * @return the killingBlows
	 */
	public int getKillingBlows() {
		return killingBlows;
	}
	/**
	 * @param killingBlows the killingBlows to set
	 */
	public void addKillingBlows() {
		killingBlows=killingBlows+1;
	}
	/**
	 * @return the assists
	 */
	public int getAssists() {
		return assists;
	}
	/**
	 * @param assists the assists to set
	 */
	public void addAssist() {
		this.assists = assists+1;
	}
	/**
	 * @return the deaths
	 */
	public int getDeaths() {
		return deaths;
	}
	/**
	 * @param deaths the deaths to set
	 */
	public void addDeath() {
		this.deaths = deaths+1;
	}
	/**
	 * @return the damageDone
	 */
	public int getDamageDone() {
		return damageDone;
	}
	/**
	 * @param damageDone the damageDone to set
	 */
	public void addDamageDone(int damageDone) {
		this.damageDone += damageDone;
	}
	/**
	 * @return the damagePerSecond
	 */
	public int getDamagePerSecond() {
		return damagePerSecond;
	}
	/**
	 * @param damagePerSecond the damagePerSecond to set
	 */
	public void setDamagePerSecond(int damagePerSecond) {
		this.damagePerSecond = damagePerSecond;
	}
	/**
	 * @return the healingDone
	 */
	public int getHealingDone() {
		return healingDone;
	}
	/**
	 * @param healingDone the healingDone to set
	 */
	public void addHealingDone(int healingDone) {
		this.healingDone = healingDone;
	}
	/**
	 * @return the healingPerSecond
	 */
	public int getHealingPerSecond() {
		return healingPerSecond;
	}
	/**
	 * @param healingPerSecond the healingPerSecond to set
	 */
	public void setHealingPerSecond(int healingPerSecond) {
		this.healingPerSecond = healingPerSecond;
	}
	/**
	 * @return the healingReceived
	 */
	public int getHealingReceived() {
		return healingReceived;
	}
	/**
	 * @param healingReceived the healingReceived to set
	 */
	public void setHealingReceived(int healingReceived) {
		this.healingReceived = healingReceived;
	}
	/**
	 * @return the damageTaken
	 */
	public int getDamageTaken() {
		return damageTaken;
	}
	/**
	 * @param damageTaken the damageTaken to set
	 */
	public void setDamageTaken(int damageTaken) {
		this.damageTaken = damageTaken;
	}
	/**
	 * @return the spellsFired
	 */
	public int getSpellsFired() {
		return spellsFired;
	}
	/**
	 * @param spellsFired the spellsFired to set
	 */
	public void addSpellFired() {
		this.spellsFired = spellsFired+1;
	}
	/**
	 * @return the spellAccuracy
	 */
	public float getSpellAccuracy() {
		return spellsHit;
	}
	/**
	 * @param spellAccuracy the spellAccuracy to set
	 */
	public void addSpellHit() {
		this.spellsHit = spellsHit+1;
	}
	
	public float getAccuracy(){
		return spellsHit/spellsFired;
	}
	/**
	 * @return the crowdControl
	 */
	public int CrowdControl() {
		return crowdControl;
	}
	/**
	 * @param crowdControl the crowdControl to set
	 */
	public void addCrowdControl(int crowdControl) {
		this.crowdControl = this.crowdControl+crowdControl;
	}
	/**
	 * @return the msDelay
	 */
	public int getMsDelay() {
		return msDelay;
	}
	/**
	 * @param msDelay the msDelay to set
	 */
	public void setMsDelay(int msDelay) {
		this.msDelay = msDelay;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PlayerStatistics [killingBlows=" + killingBlows + ", assists="
				+ assists + ", deaths=" + deaths + ", damageDone=" + damageDone
				+ ", damagePerSecond=" + damagePerSecond + ", healingDone="
				+ healingDone + ", healingPerSecond=" + healingPerSecond
				+ ", healingReceived=" + healingReceived + ", damageTaken="
				+ damageTaken + ", spellsFired=" + spellsFired
				+ ", spellAccuracy=" + spellsHit + ", crowdControl="
				+ crowdControl + ", msDelay=" + msDelay + "]";
	}
	public String print(){
		return "Killingblows : " + killingBlows + " Deaths: " + deaths +" Damage done: " + damageDone;
	}
}
