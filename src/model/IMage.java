package model;

import model.core.ColorNames;
/**
 * An interface for the mages in the world. Holds the methods that are the same
 * @author Olliver
 *
 */
public interface IMage extends ICharacter{

	public abstract ColorNames getColor();

}