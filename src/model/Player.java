package model;

import model.core.ColorNames;
import model.core.Constants;
import model.core.SpellNames;
import model.spells.SpellRecipeFactory;
import model.spells.SpellSlot;
import services.network.INetworkObject;

/**
 * The player in Circle of Magic.
 * @author Anton Jansson
 */
public class Player implements INetworkObject, Cloneable {
	
	/**
	 * Status for each Player. Holds if the player is ready 
	 * or has loaded the game.
	 * @author Anton Jansson
	 */
	public static enum Status implements INetworkObject {
		DISCONNECTED, IDLE, LOADED, READY, WAITING;
	}
	private static final int STARTING_SPELL_POINTS = 10;
	private String name;
	private int characterID;
	/** Points used for buying spells. */
	private int spellPoints = STARTING_SPELL_POINTS;
	private Status status;
	private final int accountID;
	private ColorNames color;
	private SpellSlot[] spellSlots = new SpellSlot[Constants.NUM_OF_SPELLSLOT];
	private PlayerStatistics statistics;

	@SuppressWarnings("unused")
	private Player() {
		this(null);
	}
	
	private Player(String name){
		this(name, 0);
	}
	
	public Player(String name, int accountID){
		this.name = name;
		this.setStatus(Status.IDLE);
		this.accountID = accountID;
		this.statistics = new PlayerStatistics();
		reset();
	}

	/**
	 * Add more spell points to this player. Spell points are used for
	 * buying spells.
	 * @param spellPoints The amount of points to add.
	 */
	public void addSpellPoints(int spellPoints) {
		this.spellPoints += spellPoints;
	}
	
	@Override
	public Player clone() {
		Player p = new Player(name, accountID);
		p.characterID = characterID;
		p.color = color;
		p.spellPoints = spellPoints;
		p.spellSlots = spellSlots.clone();
		p.statistics = statistics;
		p.status = status;
		return p;
	}
	
	/**
	 * Compare two players. Two players are equal if they have the
	 * same account ID.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		
		Player p = (Player) obj;
		return accountID == p.accountID && name.equals(p.name);
	}
	
	/**
	 * Returns the character ID.
	 */
	public int getCharacterID() {
		return characterID;
	}

	/**
	 * Returns the color of the player's character.
	 */
	public ColorNames getColorName() {
		return color;
	}
	
	/**
	 * Returns the name of the player.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the amount of spell points this player has. Spell points are
	 * used for buying spells.
	 */
	public int getSpellPoints() {
		return spellPoints;
	}
	
	/**
	 * Returns the SpellSlot with the specified index.
	 * @param spell The index of the spell slot.
	 * @return The SpellSlot object.
	 */
	public SpellSlot getSpellSlot(Integer spell){
		return spellSlots[spell];
	}
	
	/**
	 * Returns the spell slots for this player.
	 * @return An array of SpellSlot.
	 */
	public SpellSlot[] getSpellSlots(){
		return spellSlots;
	}

	/**
	 * Returns the status of this player.
	 */
	public Status getStatus() {
		return status;
	}
	
	@Override
	public int hashCode() {
		return 19 * ((Integer) accountID).hashCode() + 23 * name.hashCode();
	}

	/**
	 * Reset the player state, cooldown for spells etc.
	 * Called when a new round is started.
	 */
	public void resetAfterRound() {
		for (SpellSlot spell : spellSlots) {
			if (spell != null) {
				spell.resetCooldown();
			}
		}
	}
	
	/**
	 * Set the character ID for this player.
	 * @param characterID The ID of the Character.
	 */
	public void setCharacterID(int characterID) {
		this.characterID = characterID;
	}
	
	/**
	 * Set the color for this player.
	 * @param color The color to set.
	 */
	public void setColor(ColorNames color) {
		this.color = color;
	}

	/**
	 * Set the name for this player.
	 * @param name The name of the player.
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Set the spellSlot on this mage
	 * @param spellSlot The spellSlot to set
	 * @param slot The the slot to set the spell. 
	 * 0 <= slot < 8
	 */
	public void setSpellSlot(SpellSlot spellSlot, int slot){
		if(this.spellSlots[slot] == null){
			this.spellSlots[slot] = spellSlot;
		}else{
			spellSlots[slot].refresh(spellSlot);
		}
	}

	/**
	 * Set the status of this player.
	 * @param status The new status.
	 */
	public void setStatus(Status status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return "Player[" + name + ", character=" + characterID +"]";
	}
	
	public void update(float dTime){
		for(SpellSlot slot: spellSlots){
			if(slot != null){
				slot.update(dTime);
			}
		}
	}
	public PlayerStatistics getStatistics(){
		return statistics;
	}

	/**
	 * Refresh this player with the specified data.
	 * @param player The updated player.
	 */
	public void refresh(Player player) {
		spellPoints = player.spellPoints;
		status = player.status;
		characterID = player.characterID;
		refreshStatistics(player.statistics);
		for (int i = 0; i < spellSlots.length; i++) {
			if(spellSlots[i] != null && player.spellSlots[i] != null){
				spellSlots[i].refresh(player.spellSlots[i]);				
			}else{
				spellSlots[i] = player.spellSlots[i];				
			}
		}
	}
	
	/**
	 * Refresh the player statistics.
	 * @param statistics The updated statistics.
	 */
	public void refreshStatistics(PlayerStatistics statistics) {
		this.statistics = statistics;
	}
	
	/**
	 * Get the first empty spellslot.
	 * @return The index of the first empty
	 * spellslot, or -1 if no empty were found
	 */
	public int getFirstEmptySpellSlotIndex(){
		int i = 0;
		for(; i < Constants.NUM_OF_SPELLSLOT; i++){
			if(getSpellSlot(i) == null){
				break;
			}
		}
		return i < Constants.NUM_OF_SPELLSLOT? i:-1;
	}
	/**
	 * Buy a spellSlot and add it to the player,
	 * if the player has an empty slot.
	 * Lower the spellPoints.
	 * @param spellSlot
	 * @param slot The slot to set the spell.
	 * If slot is busy, find the first empty slot.
	 */
	public void buy(SpellSlot spellSlot, int slot){
		if(getSpellSlot(slot) != null){
			slot = getFirstEmptySpellSlotIndex();
		}
		if(slot > 0){
			spellPoints -= spellSlot.getSpellRecipe().getCost();
			setSpellSlot(spellSlot, slot);
		}
	}
	public boolean hasSpellSlot(SpellSlot spellSlot){
		for(SpellSlot slot : getSpellSlots()){
			if(slot != null && slot.getSpellRecipe().getName() == spellSlot.getSpellRecipe().getName()){
				return true;
			}
		}
		return false;
	}
	/**
	 * Removes all the spellSlots from this player
	 */
	public void reset(){
		for(int i = 0; i < spellSlots.length; i++){
			spellSlots[i] = null;
		}
		spellPoints = STARTING_SPELL_POINTS;
		this.statistics = new PlayerStatistics();
		setSpellSlot(new SpellSlot(SpellRecipeFactory.createSpellRecipe(SpellNames.FIREBALL)), 0);
	}
}
