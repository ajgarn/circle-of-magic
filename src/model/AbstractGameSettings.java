package model;

import model.Player.Status;
import model.core.Constants;
import services.network.INetworkObject;

/**
 * Class holding the settings for a game.
 * 
 * @author Alexander H�renstam.
 * @author Anton Jansson
 */
public abstract class AbstractGameSettings implements INetworkObject {

	private int totalRounds;
	private int totalBuyingTime;
	
	private int currentRound;
	private float roundTime;
	private String message = "";
	
	@SuppressWarnings("unused")
	private AbstractGameSettings() {
		this(1);
	}
	
	/**
	 * @param rounds Amount of rounds for the game.
	 */
	public AbstractGameSettings(int rounds) {
		this(rounds, 10);
	}
	
	/**
	 * @param rounds Amount of rounds for the game.
	 * @param buyingTime Time in seconds the players are allowed to buy spells.
	 */
	public AbstractGameSettings(int rounds, int buyingTime) {
		this.totalRounds = rounds;
		this.totalBuyingTime = buyingTime;
	}
	
	/**
	 * Returns whether the game can be started or not.
	 * <p>If the game cannot be started the error message can be retrieved by
	 * {@link #getMessage()}.
	 * @param header The game header to test.
	 */
	public boolean canCreateGame(GameHeader header) {
		return !header.getPlayers().isEmpty();
	}
	
	/**
	 * Returns the time left for the buying state, until the game automatically will start.
	 * @return The time in seconds.
	 */
	public float getBuyingTimeLeft() {
		return totalBuyingTime - getRoundTime();
	}
	
	/**
	 * Returns the current round.
	 * @return The number of the current round.
	 */
	public int getCurrentRound() {
		return currentRound;
	}
	
	/**
	 * Returns the time the current round has passed.
	 * @return The time in seconds.
	 */
	public float getRoundTime() {
		return roundTime / Constants.FPS;
	}
	
	/**
	 * Returns the message, e.g. error message when trying to create the game.
	 * @return A message as a string.
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * Returns the amount of spell points to add to each player before each 
	 * buying each round.
	 * <p>This can be customized for each round by getting the current round.
	 * The rounds starts at 1, and is during BUYING state the same as the
	 * next coming PLAYING round.
	 * @see #getCurrentRound()
	 */
	protected abstract int getRoundSpellPoints();

	/**
	 * Get the total amount of rounds for this game.
	 * @return The amount of rounds.
	 */
	public int getTotalRounds() {
		return totalRounds;
	}

	/**
	 * Update the settings of this GameSetting.
	 * @param gameSettings The new settings.
	 */
	public void refresh(AbstractGameSettings gameSettings) {
		this.totalRounds = gameSettings.totalRounds;
		this.totalBuyingTime = gameSettings.totalBuyingTime;
	}
	
	/**
	 * Set the message for this game setting, e.g. an error message.
	 * @param message The message as a string.
	 */
	protected void setMessage(String message) {
		this.message = message;
	}
	/**
	 * Sets amounts of rounds for game.
	 */
	public void setRounds(int rounds){
		this.totalRounds=rounds;
	}
	
	/**
	 * Setup game before it starts.
	 * @param game The game to setup.
	 */
	public void setupGame(AbstractGame game) {
		switch (game.getState()) {
		case BUYING:
			startBuyingRound(game);
			break;
		case PLAYING:
			startPlayingRound(game);
			break;
		case ENDED:
			break;
		}
	}
	
	/**
	 * Returns whether to end the game.
	 */
	public boolean shouldEndGame() {
		return currentRound > totalRounds;
	}
	
	/**
	 * Returning whether to end the current round of this game.
	 * @param game The server game.
	 */
	public boolean shouldEndRound(AbstractGame game) {
		switch (game.getState()) {
		case BUYING:
			return getBuyingTimeLeft() <= 0
					|| game.getHeader().allPlayersHaveStatus(Status.READY);
		case PLAYING:
			return shouldEndRoundInternal(game);
		default:
			return false;
		}
	}

	/**
	 * Returning whether to end the current round of this game.
	 * @param game The server game.
	 */
	protected abstract boolean shouldEndRoundInternal(AbstractGame game);
	
	/**
	 * Start a new round, i.e. resetting the timer.
	 */
	protected void startRound() {
		roundTime = 0;
	}
	
	/**
	 * Ends the current round.
	 * @param game The server game.
	 */
	public void startBuyingRound(AbstractGame game) {
		startRound();
		currentRound++;
		// Add spellpoints to all players.
		for (Player player : game.getPlayers()) {
			player.addSpellPoints(getRoundSpellPoints());
		}
	}
	
	/**
	 * Ends the current round.
	 * @param game The server game.
	 */
	public void startPlayingRound(AbstractGame game) {
		startRound();
	}
	
	public void update(float dTime) {
		roundTime += dTime;
	}
	
	@Override
	public String toString() {
		return getClass().getCanonicalName();
	}
}
