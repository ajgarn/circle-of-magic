package model;

import java.util.List;

import model.core.IGameObject;
import model.core.SpellEffect;
import model.core.SpellNames;

/**
 * An interface for the characters in the world. Holds the methods that are the same
 * @author Olliver
 *
 */
public interface ICharacter extends IGameObject{

	public abstract int getHealth();

	public abstract List<SpellEffect> getSpellEffects();

	/**
	 * Get the max health from this character
	 * @return The max health
	 */
	public abstract int getMaxHealth();
	/**
	 * Get whenever this gameObject has a spellEffect from
	 * from a specific spell.
	 * @param spellName The spell to check. 
	 * @return true if any spellEffect comes from
	 * the specified spell, false otherwise.
	 */
	public boolean isAffectedBySpell(SpellNames spellName);

}