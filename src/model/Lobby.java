package model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import model.Player.Status;
import model.core.ColorNames;
import services.network.INetworkObject;


/**
 * Lobby for holding players before a game is started.
 * @author Anton Jansson
 */
public class Lobby implements INetworkObject {
	
	private final GameHeader game;
	private Account owner;
	private final AbstractGameSettings settings;
	
	/** Whether the game has started loading. */
	private boolean isLoadingGame = false;
	
	@SuppressWarnings("unused")
	private Lobby() {
		this(null, null, null);
	}
	
	public Lobby(GameHeader game, Account owner, AbstractGameSettings settings) {
		this.game = game;
		this.owner = owner;
		this.settings = settings;
	}
	
	/**
	 * Closes this lobby, i.e. removing all players and removing the owner.
	 */
	private void close() {
		getPlayers().clear();
		owner = null;
	}
	
	public GameHeader getHeader() {
		return game;
	}

	public Account getOwner() {
		return owner;
	}
	
	/**
	 * Returns the colors used for all players within this lobby.
	 * @return All colors in a collection.
	 */
	private Collection<ColorNames> getPlayerColors() {
		Collection<ColorNames> colors = new ArrayList<ColorNames>();
		for (Player p : getPlayers()) {
			colors.add(p.getColorName());
		}
		return colors;
	}
	
	public Collection<Player> getPlayers() {
		return game.getPlayers();
	}
	
	public AbstractGameSettings getGameSettings() {
		return settings;
	}
	
	/**
	 * Add a player to this lobby.
	 * @param account The account who owns the player.
	 */
	public void connectPlayer(Account account) {
		if (!isLoading()) {
			Player p = account.getPlayer();
			p.setStatus(Status.WAITING);
			p.setColor(ColorNames.getRandomColor(getPlayerColors()));
			game.getPlayers().add(p);
		}
	}
	
	/**
	 * Remove a player from this lobby.
	 * <p>If the owner disconnects all players is removed from the lobby.
	 * @param account The account who owns the player.
	 */
	public void disconnectPlayer(Account account) {
		if (account.equals(owner)) {
			close();
		} else {
			game.getPlayers().remove(account.getPlayer());
		}
	}
	
	/**
	 * Returns whether all players have loaded completely and 
	 * are ready to begin playing.
	 * @return <code>true</code> if the game should start, 
	 * <code>false</code> otherwise.
	 */
	public boolean allPlayersLoaded() {
		return game.allPlayersHaveStatus(Status.LOADED);
	}
	
	/**
	 * Returns whether all players are ready and the game can be 
	 * created, i.e. start loading.
	 * @return <code>true</code> if all players are ready, 
	 * <code>false</code> otherwise.
	 */
	public boolean allPlayersReady() {
		return game.allPlayersHaveStatus(Status.READY) || game.allPlayersHaveStatus(Status.LOADED);
	}
	
	/**
	 * Returns whether the game can be started from this lobby or not.
	 * This depends of the specified game settings and if all players are ready.
	 * <p>If the game cannot be started the error message can be retrieved by
	 * {@link #getGameSettings()}.
	 */
	public boolean canLoadGame() {
		if (!settings.canCreateGame(game)) {
			return false;
		} else if (!allPlayersReady()) {
			settings.setMessage("Waiting for all players to get ready.");
			return false;
		}
		settings.setMessage("");
		return true;
	}
	
	/**
	 * Returns whether the lobby is empty or not.
	 */
	public boolean isEmpty() {
		return getPlayers().isEmpty();
	}
	
	/**
	 * Returns whether all players have connected and the game has 
	 * or should start loading.
	 */
	public boolean isLoading() {
		return isLoadingGame;
	}

	/**
	 * Make the game for this lobby ready to load.
	 * No more players can connect.
	 * @return Whether the game is started loading.
	 */
	public boolean loadGame() {
		isLoadingGame = canLoadGame();
		return isLoadingGame;
	}

	public void setOwner(Account owner) {
		this.owner = owner;
	}
	
	/**
	 * Two lobbies are equal if their GameHeaders are equal.
	 * <p>{@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		return game.equals(((Lobby) obj).game);
	}
	
	@Override
	public int hashCode() {
		return 13 * game.hashCode();
	}
	
	@Override
	public String toString() {
		return "Lobby[" + game.getName() + " (" + game.getID() + ") "
				+ getPlayers().size() + " players]";
	}

	/**
	 * Updates the setting of the game for this lobby. Only the
	 * owner of the lobby can change the lobby settings.
	 * <p>If the passed lobby is loading this lobby will run the
	 * method {@link #loadGame()}.
	 * @param lobby The lobby with the new settings.
	 * @param account The account who has requested the update.
	 */
	public void update(Lobby lobby, Account account) {
		if (owner == null || account != null && account.equals(owner)) {
			if (lobby.isLoading()) {
				loadGame();
			}
			
			List<Player> players = new ArrayList<Player>(lobby.getPlayers());
			for (Player p : getPlayers()) {
				int index = players.indexOf(p);
				if (index >= 0) {		// Update player
					p.refresh(players.get(index));
					players.remove(index);
				} else {				// Remove player
					getPlayers().remove(p);
				}
			}
			for (Player p : players) {	// Add player
				getPlayers().add(p);
			}
			
			// If owner has disconnected, close the lobby.
			if (!getPlayers().contains(owner.getPlayer())) {
				close();
			}
			
			settings.refresh(lobby.getGameSettings());
		}
	}
}
