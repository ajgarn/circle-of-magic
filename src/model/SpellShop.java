package model;

import java.util.LinkedList;
import java.util.List;

import model.core.SpellNames;
import model.spells.SpellRecipeFactory;
import model.spells.SpellSlot;
/**
 * A model representation of a spellshop. Holds all the spellslots available
 * @author Olliver
 *
 */
public class SpellShop {
	private List<SpellSlot> spellSlots = new LinkedList<SpellSlot>();
	public SpellShop() {
		for(SpellNames name: SpellNames.values()){
			this.add(new SpellSlot(SpellRecipeFactory.createSpellRecipe(name)));
		}
	}
	
	public void add(SpellSlot slot){
		spellSlots.add(slot);
	}
	public void remove(SpellSlot slot){
		spellSlots.remove(slot);
	}
	public List<SpellSlot> getSpellSlots(){
		return this.copy();
	}
	private List<SpellSlot> copy(){
		List<SpellSlot> temp = new LinkedList<SpellSlot>();
		for(SpellSlot slot:spellSlots){
			temp.add(new SpellSlot(slot));
		}
		return temp;
	}
}
