package model;

import java.util.ArrayList;
import java.util.Collection;

import model.core.IGameObject;
import services.network.INetworkObject;

/**
 * Abstract game representation used by both server and client. Holds a world
 * and general information about the game.
 * @author Anton Jansson
 *
 */
public abstract class AbstractGame {
	
	public enum GameState implements INetworkObject {
		BUYING, ENDED, PLAYING;
	}
	private final GameHeader header;
	private GameState state;
	private AbstractGameSettings settings;
	private World world;
	
	/**
	 * Constructor for AbstractGame.
	 * @param header Header information for the game.
	 */
	public AbstractGame(GameHeader header) {
		this.header = header;
		this.state = GameState.BUYING;
		this.world = new World();
	}
	
	/**
	 * Get basic information for this game, e.g. game ID, host address.
	 * @return The information as a GameHeader.
	 */
	public GameHeader getHeader() {
		return header;
	}
	
	/**
	 * Returns all players in this game.
	 * @return A collection of the players.
	 */
	public Collection<Player> getPlayers() {
		return header.getPlayers();
	}
	/**
	 * Get the player that controls a specific 
	 * gameObject.
	 * @param gameObjectId Teh id of the gameObject
	 * that the player controls
	 * @return The player if the gameObject has a
	 * player that controls it. Null otherwise
	 */
	public Player getPlayer(Integer gameObjectId){
		for(Player p: getPlayers()){
			if(p.getCharacterID() == gameObjectId){
				return p;
			}
		}
		return null;
	}

	/**
	 * Returns all of the players' characters.
	 * @return A collection of gameObjects.
	 */
	public Collection<IGameObject> getPlayerCharacters() {
		Collection<IGameObject> characters = new ArrayList<IGameObject>();
		for (Player p : header.getPlayers()) {
			characters.add(world.getGameObject(p.getCharacterID()));
		}
		return characters;
	}

	/**
	 * Returns the settings for this game.
	 * @return {@link AbstractGameSettings}
	 */
	public AbstractGameSettings getSettings() {
		return settings;
	}

	/**
	 * Returns the current game state.
	 */
	public GameState getState() {
		return state;
	}
	
	
	/**
	 * Returns the game world.
	 */
	public World getWorld() {
		return world;
	}

	/**
	 * Set the settings for this game.
	 * @param settings
	 */
	protected void setSettings(AbstractGameSettings settings) {
		this.settings = settings;
	}

	/**
	 * Set the state of the game.
	 * @param state The new game state.
	 */
	public void setState(GameState state) {
		this.state = state;
	}
	
	/**
	 * Set a new world.
	 * @param world
	 */
	public void setWorld(World world) {
		this.world = world;
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + "[" + header.getName() + " ("
				+ header.getID() + ") " + getPlayers().size() + " players]";
	}
	
	/**
	 * Updates all objects in game.
	 * @param dTime
	 */
	public void update(float dTime){
		if (world != null) {
			world.update(dTime);
		}
		if (settings != null) {
			settings.update(dTime);
		}
	}
}
