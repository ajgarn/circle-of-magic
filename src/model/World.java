package model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import model.core.GameObject;
import model.core.IGameObject;
import services.network.INetworkObject;

/**
 * A model representation of the gaming world. Holds all objects in the game
 * @author Anton Jansson
 * @author Robin Gr�nberg
 */
public class World implements INetworkObject {
	private final Map<Integer,IGameObject> gameObjects,gameObjectsAdded;
	private final List<Integer> gameObjectsRemoved;
	private int unusedIndex = 1;
	private float radius;
	private float minRadius;
	
	public World(){
		gameObjects = new HashMap<Integer, IGameObject>();
		gameObjectsAdded = new HashMap<Integer, IGameObject>();
		gameObjectsRemoved = new ArrayList<Integer>(10);
		radius = 35;
		minRadius = 10;
	}
	/**
	 * Add a GameObject to the world.
	 * @param object The object to be added.
	 * @return The index of the object created
	 */
	public int addGameObject(GameObject object) {
		int index = getUnusedIndex();
		setGameObject(index, object);
		return index;
	}
	public Collection<IGameObject> getGameObjects(){
		return gameObjects.values();
	}
	public Set<Integer> getGameObjectKeys(){
		Set<Integer> s = new HashSet<Integer>(gameObjects.keySet());
		synchronized (gameObjectsAdded) {
			s.addAll(gameObjectsAdded.keySet());
		}
		return s;
	}
	public IGameObject getGameObject(int index){
		synchronized (gameObjectsAdded){
			IGameObject o = gameObjectsAdded.get(index);
			if(o == null){
				return gameObjects.get(index);
			}else{
				return o;
			}
		}
	}	
	public Map<Integer, IGameObject> getGameObjectsMap(){
		Map<Integer, IGameObject> temp = new HashMap<Integer, IGameObject>(gameObjects);
		temp.putAll(gameObjectsAdded);
		return temp;
	}
	
	/**
	 * 
	 * @param object
	 * @return The index of the object or -1 if no match is found.
	 */
	public int getGameObjectIndex(IGameObject object){
		synchronized (gameObjectsAdded) {
			for (Integer index : gameObjectsAdded.keySet()){
				if (gameObjectsAdded.get(index).equals(object)){
					return index;
				}
			}
		}
		synchronized (gameObjects) {
			for (Integer index : gameObjects.keySet()){
				if (gameObjects.get(index).equals(object)){
					return index;
				}
			}
		}
		return -1;
	}
	public void removeGameObject(int index) {
		synchronized (gameObjectsRemoved) {
			gameObjectsRemoved.add(index);
		}
	}
	public void removeGameObject(IGameObject object){
		removeGameObject(getGameObjectIndex(object));
	}
	public void setGameObject(Integer index, IGameObject gameObject) {
		synchronized (gameObjectsAdded) {
			gameObjectsAdded.put(index, gameObject);
		}
	}
	public void update(float dTime){
		//Add new objects
		synchronized (gameObjectsAdded) {
			for(Integer index:gameObjectsAdded.keySet()){
				gameObjects.put(index, gameObjectsAdded.get(index));
			}
		}
		killDeadGameObjects();
		//remove old ones
		synchronized (gameObjectsRemoved) {
			for(Integer index:gameObjectsRemoved){
				gameObjects.remove(index);
			}
			gameObjectsRemoved.clear();
		}
		synchronized (gameObjectsAdded) {
			gameObjectsAdded.clear();
		}
		for(IGameObject gameObject:getGameObjects()){
			gameObject.update(dTime);
		}
		if(!(radius <= minRadius)){
			radius -=0.004f * dTime;
		}
	}
	public float getRadius(){
		return radius;
	}
	/**
	 * Checks gameWorld and remove deleted objects.
	 */
	private void killDeadGameObjects(){
		for (Integer index:getGameObjectKeys()){
			IGameObject object = getGameObject(index);
			if(object.isDead()){
				removeGameObject(index);
			}
		}
	}
	private int getUnusedIndex() {
		return unusedIndex++;
	}
}
