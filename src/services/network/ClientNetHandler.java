package services.network;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import services.logger.Log;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.rmi.ObjectSpace;

/**
 * Handles the server communication for the client.
 * <p>
 * This supports multiple client connections simultaneously by using
 * {@link #getClient(InetAddress)} or {@link #getClient(String)}.
 * 
 * @author Anton Jansson
 */
public class ClientNetHandler extends AbstractNetHandler implements IClientNetHandler {
	
	private static IClientNetHandler instance;
	
	private final Client mainClient;
	/** List for using multiple client connections simultaneously. */
	private final Collection<Client> multiConnectionClients;
	
	/**
	 * Creates a ClientNetHandler and starts the client.
	 */
	private ClientNetHandler(NetworkSettings settings) {
		super(settings);
		multiConnectionClients = new ArrayList<Client>();
		mainClient = new Client(settings.getWriteBufferSize(), 
				settings.getObjectBufferSize());
		mainClient.addListener(clientListener);
		getSettings().registerKryoClasses(mainClient.getKryo());
		mainClient.start();
	}
	
	@Override
	public void connectHost(InetAddress host) throws IOException {
		if (!mainClient.isConnected() || !host.equals(mainClient.getRemoteAddressTCP().getAddress())) {
			try {
				connectHost(mainClient, host);
			} catch (IOException e) {
				Log.error("Could not connect to host " + host, e);
				throw e;
			}
		}
	}
	
	@Override
	public void connectHost(String host) throws IOException {
		try {
			connectHost(InetAddress.getByName(host));
		} catch (UnknownHostException e) {
			Log.error(e.getMessage(), e);
			throw new IOException(e);
		}
	}
	
	/**
	 * Connects this client to a specific server host.
	 * @param client The client to connect
	 * @param host The address of the host to connect to.
	 * @throws IOException if the client could not be opened or connecting times out.
	 */
	private void connectHost(Client client, InetAddress host) throws IOException {
		client.connect(getSettings().getTimeout(), host, getSettings().getTCPPort(),
				getSettings().getUDPPort());
	}

	@Override
	public void disconnect() {
		mainClient.close();
	}
	
	@Override
	public Collection<InetAddress> getAvailableHosts() {
		return mainClient.discoverHosts(getSettings().getUDPPort(), getSettings().getTimeout());
	}
	
	@Override
	public Client getClient(InetAddress host) {
		synchronized (multiConnectionClients) {			
			Iterator<Client> it = multiConnectionClients.iterator();
			while (it.hasNext()) {
				Client c = it.next();
				if (!c.isConnected()) {
					c.stop();
					it.remove();
				} else if (c.getRemoteAddressTCP().getAddress().equals(host)) {
					return c;
				}
			}
		}
		Client c = newClient();
		try {
			connectHost(c, host);
		} catch (IOException e) {}
		return c;
	}
	
	@Override
	public Client getClient(String host) {
		try {
			return getClient(InetAddress.getByName(host));
		} catch (UnknownHostException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public Client getCurrentClient() {
		return mainClient;
	}
	
	public synchronized static IClientNetHandler getInstance(NetworkSettings settings) {
		if (instance == null) {
			instance = new ClientNetHandler(settings);
		}
		return instance;
	}

	@Override
	public int getPing() {
		return mainClient.getReturnTripTime();
	}
	
	@Override
	public <T> T getRemoteObject(int objectID, Class<T> iface) {
		return getRemoteObject(mainClient, objectID, iface);
	}

	/**
	 * Creates a new client, registers all kryo classes and starts the client.
	 * @return The created client.
	 */
	private Client newClient() {
		Client c = new Client();
		getSettings().registerKryoClasses(c.getKryo());
		c.start();
		synchronized (multiConnectionClients) {			
			multiConnectionClients.add(c);
		}
		return c;
	}
	
	@Override
	public <T> void send(T object) {
		mainClient.sendTCP(new NetHolder<T>(object));
	}
	
	@Override
	public void updatePing() {
		mainClient.updateReturnTripTime();
	}
	
	/**
	 * Listener for a Kryonet client.
	 */
	private Listener clientListener = new Listener() {
		
		@Override
		public void received(Connection connection, Object object) {
			super.received(connection, object);
			
			if (object instanceof NetHolder<?>) {
				NetHolder<?> holder = (NetHolder<?>) object;
				sendToListeners(connection.getID(), holder.get(), holder.getTime());
			} else if (object instanceof ObjectSpace.InvokeMethodResult) {
			} else if (object instanceof FrameworkMessage.KeepAlive) {
			} else if (object instanceof FrameworkMessage.Ping) {
			} else {
				Log.error("[ClientNetHandler] Received an undefined object: " + object);
			}
		}
	};
}
