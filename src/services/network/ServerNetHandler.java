package services.network;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Enumeration;

import services.logger.Log;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import com.esotericsoftware.kryonet.rmi.ObjectSpace;

/**
 * Handles the client communication for the server.
 * @author Anton Jansson
 */
public class ServerNetHandler extends AbstractNetHandler implements IServerNetHandler {

	private Server server;

	/**
	 * Create a ServerNetHandler.
	 * <p><b>Note:</b> The server must be started to accept client connections.
	 * @see #start()
	 */
	public ServerNetHandler(NetworkSettings settings) {
		super(settings);
	}
	
	@Override
	public InetAddress getAddress() {
		try {
			Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
			// Iterates over network cards and to find IP addresses.
			// Returns a non loop-back address.
			while (en.hasMoreElements()) {
					Enumeration<InetAddress> addresses = en.nextElement().getInetAddresses();
					while(addresses.hasMoreElements()){
						InetAddress addr = addresses.nextElement();
						if(!addr.isLoopbackAddress()){
							return addr;
						}
					}
			}
			return InetAddress.getByAddress(new byte[]{127,0,0,1});
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public <T> void send(final Collection<Integer> connectionIDs, T object) {
		if (server == null) {
			throw new IllegalStateException("Server is not started.");
		}
		final NetHolder<T> holder = new NetHolder<T>(object);
		Connection[] connections = server.getConnections();
		for (int i = 0, n = connections.length; i < n; i++) {
			final Connection connection = connections[i];
			if (connectionIDs.contains(connection.getID())) {
				new Thread(new Runnable() {
					@Override
					public void run() {
						connection.sendUDP(holder);
					}
				}).start();
			}
		}
	}
	
	@Override
	public <T> void send(int connectionID, T object) {
		server.sendToUDP(connectionID, new NetHolder<T>(object));
	}
	
	@Override
	public synchronized void start() {
		if (server == null || server.getUpdateThread() == null || 
				!server.getUpdateThread().isAlive()) {
			if (server != null) {
				server.stop();
			}
			server = new Server(getSettings().getWriteBufferSize(),
					getSettings().getObjectBufferSize());
			server.addListener(serverListener);
			getSettings().registerKryoClasses(server.getKryo());
			try {
				server.bind(getSettings().getTCPPort(), getSettings().getUDPPort());
			} catch (IOException e) {
				// TODO Handle when ports are unavailable.
				System.err.println("Starting server: " + e.getMessage());
			}
			server.start();
		}
	}
	
	@Override
	public synchronized void stop() {
		if (server != null) {
			server.stop();
			server = null;
		}
	}

	/**
	 * Listener for a Kryonet server.
	 */
	private Listener serverListener = new Listener() {
		@Override
		public void received(Connection connection, Object object) {
			if (object instanceof NetHolder<?>) {
				NetHolder<?> holder = (NetHolder<?>) object;
				sendToListeners(connection.getID(), holder.get(), holder.getTime());
			} else if (object instanceof ObjectSpace.InvokeMethod) {
			} else if (object instanceof FrameworkMessage.KeepAlive) {
			} else if (object instanceof FrameworkMessage.Ping) {
			} else {
				Log.error("[ServerNetHandler] Received an undefined object: " + object);
			}
		}
		
		@Override
		public void connected(Connection connection) {
			super.connected(connection);
			
			getObjectSpace().addConnection(connection);
		}
		
		@Override
		public void disconnected(Connection connection) {
			super.disconnected(connection);
			
			getObjectSpace().removeConnection(connection);
			sendToListeners(connection.getID(), new DisconnectRequest(),
					System.currentTimeMillis());
		}
		
		@Override
		public void idle(Connection connection) {
			super.idle(connection);
		}
	};
}
