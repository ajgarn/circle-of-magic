package services.network;

import services.logger.Log.Level;

import com.esotericsoftware.minlog.Log;
import com.esotericsoftware.minlog.Log.Logger;


/**
 * Helper class for network communication package.
 * @author Anton Jansson
 *
 */
class NetHelper {
	// TODO: What to do with this class, making package modular?
	
	/**
	 * Makes the {@link Log} send its messages to {@link services.logger.Log}.
	 */
	public static void setupNetworkLog() {
		Log.setLogger(new Logger() {
			private Level level;
			@Override
			public void log(int minlogLevel, String category, String message, Throwable ex) {
				switch (minlogLevel) {
				case Log.LEVEL_DEBUG:
					level = Level.DEBUG;
					break;
				case Log.LEVEL_INFO:
					level = Level.DEBUG;
					break;
				case Log.LEVEL_WARN:
					level = Level.WARNING;
					break;
				case Log.LEVEL_ERROR:
					level = Level.FATAL;
					break;
				default:
					level = Level.NONE;
					break;
				}
				
				super.log(minlogLevel, category, message, ex);
			}
			
			@Override
			protected void print(String message) {
				services.logger.Log.log(level, message, null);
			}
		});
	}
}
