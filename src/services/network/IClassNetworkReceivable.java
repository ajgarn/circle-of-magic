package services.network;

/**
 * Interface for classes that can receive objects from the network.
 * Implemented by {@link AbstractClassNetReceiver}.
 * @author Anton Jansson
 * 
 * @param <S> The type of the listeners for this interface.
 * @see INetworkReceivable
 */
public interface IClassNetworkReceivable<S> extends INetworkReceivable {
	/**
	 * Add a listener for when an object is received from the network.
	 * @param cls The class to listen for.
	 * @param listener The listener for the class.
	 */
	<T> void addListener(Class<T> cls, S listener);
	/**
	 * Remove a listener for when an object is received from the network.
	 * @param cls The class to remove the listener for.
	 * @param listener The listener to remove.
	 */
	<T> void removeListener(Class<T> cls, S listener);
}
