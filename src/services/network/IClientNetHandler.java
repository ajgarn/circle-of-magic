package services.network;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Collection;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.rmi.TimeoutException;

/**
 * Interface for network communication on the client side.
 * @author Anton Jansson
 * 
 * @see INetworkReceivable
 */
public interface IClientNetHandler extends INetworkReceivable {
	/**
	 * Connects this client to a specific host.
	 * @param host The host to connect to.
	 * @throws IOException 
	 */
	void connectHost(InetAddress host) throws IOException;
	/**
	 * Connects this client to a specific host.
	 * @param host The host to connect to.
	 * @throws IOException 
	 */
	void connectHost(String host) throws IOException;
	/**
	 * Disconnects the current connection.
	 */
	void disconnect();
	/**
	 * Searches the network for running hosts and returns all that are found.
	 * @return The running hosts as a collection of InetAddress.
	 */
	Collection<InetAddress> getAvailableHosts();
	/**
	 * Get a {@link Client} connected to the specified host. This offers the
	 * network handler to support multiple connections at the same time.
	 * @param host The host to connect to.
	 * @return The client connected to the host.
	 */
	Client getClient(InetAddress host);
	/**
	 * Get a client connected to the specified host. This offers the
	 * network handler to support multiple connections at the same time.
	 * @param host The host to connect to.
	 * @return The client connected to the host or null if no host can be found
	 * with the specified hostname.
	 */
	Client getClient(String host);
	/**
	 * Get the client used for the current connection to the host.
	 * @return The client.
	 */
	Client getCurrentClient();
	/**
	 * Returns the ping response time for the current connection.
	 * @return The ping time in milliseconds or -1 if ping has never been called.
	 */
	int getPing();
	/**
	 * Returns an object from the remote connection. An object space is
	 * used for remote invocation of objects, to allow the remote connection
	 * to access objects using a specified ID.
	 * 
	 * @param objectID The ID of the remote object.
	 * @param iface The type of the remote object as an interface.
	 * @return The remote object
	 * @throws TimeoutException
	 */
	<T> T getRemoteObject(int objectID, Class<T> iface);
	/**
	 * Send an object to the server.
	 * <p>
	 * All objects that will be sent must implement {@link INetworkObject}
	 * or be manually registered for network communication.
	 * // TODO: Fix javadoc above.
	 * @param object The object to send.
	 */
	<T> void send(T object);
	/**
	 * Update the ping response time by sending a ping request to 
	 * the current connection.
	 */
	void updatePing();
}
