package services.network;

import java.util.ArrayList;
import java.util.Collection;

import services.logger.Log;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.rmi.ObjectSpace;

/**
 * Abstract class for NetHandlers on the server and the client.
 * <p>
 * Controls e.g. the listener registration and notification. Listeners that can
 * be registered must implement {@link INetworkListener}.
 * 
 * @author Anton Jansson
 * @see INetworkReceivable
 */
abstract class AbstractNetHandler implements INetworkReceivable {

	private final NetworkSettings settings;
	private final ObjectSpace objectSpace;
	private final Collection<INetworkListener> listeners =
			new ArrayList<INetworkListener>();
	
	/**
	 * Create an AbstractNetHandler.
	 * @param settings The settings for this network handler.
	 */
	public AbstractNetHandler(NetworkSettings settings) {
		System.setProperty("java.net.preferIPv4Stack", "true");		// Use IPv4
		this.settings = settings;
		this.objectSpace = new ObjectSpace();
		NetHelper.setupNetworkLog();
	}
	
	@Override
	public void addListener(INetworkListener listener) {
		listeners.add(listener);
	}
	
	/**
	 * Returns the object space for this network handler. An object space is Add
	 * a comment to this line used for remote invocation of objects, to allow
	 * the remote connection to access objects using a specified ID.
	 * 
	 * @return The object space.
	 */
	protected ObjectSpace getObjectSpace() {
		return objectSpace;
	}
	
	@Override
	public <T> T getRemoteObject(Connection connection, int objectID,
			Class<T> iface) {
		return ObjectSpace.getRemoteObject(connection, objectID, iface);
	}

	/**
	 * Returns the settings for this network handler.
	 * @return The settings.
	 */
	protected NetworkSettings getSettings() {
		return settings;
	}
	
	@Override
	public void registerRemoteObject(int objectID, Object object) {
		objectSpace.register(objectID, object);
	}
	
	@Override
	public void removeListener(INetworkListener listener) {
		listeners.add(listener);
	}
	
	/**
	 * Sends the received object to all listeners.
	 * @param connectionID The network connection ID.
	 * @param object The received object.
	 * @param timeSent The time the object was sent.
	 */
	protected <T> void sendToListeners(int connectionID, T object, long timeSent) {
		if (!listeners.isEmpty()) {
			for (INetworkListener l : listeners) {
				l.networkReceived(connectionID, object, timeSent);
			}
		} else {
			Log.warning("[" + getClass().getSimpleName() + "] No listeners registered.");
		}
	}
}
