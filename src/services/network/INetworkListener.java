package services.network;

/**
 * Listener for all objects being received from the network.
 * @author Anton Jansson
 *
 * @see IClassNetworkListener
 */
public interface INetworkListener extends IClassNetworkListener<Object>{}
