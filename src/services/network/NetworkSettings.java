package services.network;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import services.file.ClassEnumerator;

import com.esotericsoftware.kryo.Kryo;

/**
 * Settings for the server and client {@link INetHandler}, specifying objects to
 * register for sending over network, TCP/UPD ports, timeout etc.
 * <p>
 * There are two ways to register objects that should be sent over network:
 * <ol>
 * <li>Let the class implement {@link INetworkObject} and register the package
 * that the class is within, using {@link #registerPackage(Package)}.</li>
 * <li>Register the class itself by using {@link #registerClass(Class)}.</li>
 * </ol>
 * 
 * @author Anton Jansson
 * 
 */
public class NetworkSettings {

	private int tcpPort = 54555;
	private int udpPort = 54777;
	private int timeout = 2000;
	private int writeBufferSize = 16384;
	private int objectBufferSize = 2 * 2048;
	
	private final Collection<Class<?>> classRegistration;
	private final Collection<Package> packageRegistration;
	private final Map<Class<?>, Integer> classIDRegistration;
	
	public NetworkSettings() {
		classRegistration = new HashSet<Class<?>>();
		packageRegistration = new HashSet<Package>();
		classIDRegistration = new HashMap<Class<?>, Integer>();
	}

	public int getTCPPort() {
		return tcpPort;
	}

	public int getUDPPort() {
		return udpPort;
	}

	public int getTimeout() {
		return timeout;
	}

	public int getWriteBufferSize() {
		return writeBufferSize;
	}

	public int getObjectBufferSize() {
		return objectBufferSize;
	}
	
	/**
	 * Register a class to be sent over network.
	 * @param cls The class that will be sent over network.
	 */
	public void registerClass(Class<?> cls) {
		classRegistration.add(cls);
	}
	
	/**
	 * Register a class to be sent over network, with a custom kryo registration ID.
	 * The ID will not be used literally, but as an offset from the next available ID
	 * on the kryo object.
	 * @param cls The class that will be sent over network.
	 * @param id The ID used by kryo registration.
	 * @see Kryo#register(Class, int)
	 */
	public void registerClass(Class<?> cls, int id) {
		classIDRegistration.put(cls, id);
	}
	
	/**
	 * Registers the classes to be sent over the network, between client and
	 * server.
	 */
	protected void registerKryoClasses(final Kryo kryo) {
		// Register classes with specified IDs first.
		registerOnKryo(kryo, classIDRegistration);
		
		// Register other classes on other (arbitrary) IDs.
		List<Class<?>> classes = new ArrayList<Class<?>>(classesToRegister());
		sortClassesByName(classes);		// Sort all classes equally on all platforms.
		registerOnKryo(kryo, classes);
	}
	
	/**
	 * Register a package containing {@link INetworkObject}s that needs to be
	 * registered for sending over network.
	 * @param pkg The package that will be registered.
	 */
	public void registerPackage(Package pkg) {
		packageRegistration.add(pkg);
	}

	/**
	 * Set the TCP port to use for this application.
	 * @param tcpPort TCP port (default 54555)
	 */
	public void setTCPPort(int tcpPort) {
		this.tcpPort = tcpPort;
	}

	/**
	 * Set the UDP port to use for this application.
	 * @param tcpPort UDP port (default 54777)
	 */
	public void setUDPPort(int udpPort) {
		this.udpPort = udpPort;
	}

	/**
	 * Set the timeout for network methods.
	 * @param timeout The timeout in milliseconds (default 2000).
	 */
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	/**
	 * Set the write buffer size.
	 * 
	 * <p>writeBufferSize - One buffer of this size is allocated for each connected client. 
	 * Objects are serialized to the write buffer where the bytes are queued until they 
	 * can be written to the socket. 
	 * 
	 * <p>Normally the socket is writable and the bytes are written immediately. If the 
	 * socket cannot be written to and enough serialized objects are queued to overflow 
	 * the buffer, then the connection will be closed. 
	 * 
	 * <p>The write buffer should be sized at least as large as the largest object that will 
	 * be sent, plus some head room to allow for some serialized objects to be queued in 
	 * case the buffer is temporarily not writable. The amount of head room needed is 
	 * dependent upon the size of objects being sent and how often they are sent.
	 * 
	 * @param writeBufferSize (default 16384)
	 */
	public void setWriteBufferSize(int writeBufferSize) {
		this.writeBufferSize = writeBufferSize;
	}
	/**
	 * Set the object buffer size.
	 * <p>
	 * objectBufferSize - Two (using only TCP) or four (using both TCP and UDP) buffers 
	 * of this size are allocated. These buffers are used to hold the bytes for a single 
	 * object graph until it can be sent over the network or deserialized.
	 * The object buffers should be sized at least as large as the largest object that 
	 * will be sent or received.
	 * 
	 * @param objectBufferSize (default 2048)
	 */
	public void setObjectBufferSize(int objectBufferSize) {
		this.objectBufferSize = objectBufferSize;
	}
	
	
	private Set<Class<?>> classesToRegister() {
		Set<Class<?>> classes = new HashSet<Class<?>>(classRegistration);
		registerCommonClasses(classes);
		for (Package p : packageRegistration) {
			for (Class<?> c : ClassEnumerator.getClassesForPackage(p)) {
				if (INetworkObject.class.isAssignableFrom(c)) {
					classes.add(c);
				}
			}
		}
		return classes;
	}
	
	private void registerCommonClasses(Collection<Class<?>> classes) {
		classes.add(NetHolder.class);
	}
	
	private void registerOnKryo(Kryo kryo, Collection<Class<?>> classes) {
		for (Class<?> c : classes) {
			kryo.register(c);
		}
	}
	
	/**
	 * Register classes with custom kryo registration IDs.
	 * @param classes The classes with a kryo registration ID.
	 */
	private void registerOnKryo(Kryo kryo, Map<Class<?>, Integer> classes) {
		int baseId = kryo.getNextRegistrationId();
		for (Entry<Class<?>, Integer> entry : classes.entrySet()) {
			kryo.register(entry.getKey(), baseId + entry.getValue());
		}
	}
	
	private void sortClassesByName(List<Class<?>> classes) {
		Collections.sort(classes, new Comparator<Class<?>>(){
			@Override
			public int compare(Class<?> c1, Class<?> c2) {
				return c1.getName().compareTo(c2.getName());
			}
		});
	}
}
