package services.network;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.esotericsoftware.kryonet.Connection;

import services.logger.Log;

/**
 * Decorator class for a network receivable, making listeners able to
 * register for events based on specific classes instead of all network
 * events.
 * <p>Extend this class and specify which listeners who can register,
 * to customize the data passed to listeners. This can be done by overriding
 * {@link #networkReceived(Object, int, Object, long)}.
 * @author Anton Jansson
 *
 * @param <S> The type of the listeners that can be added to this receiver.
 * @see IClassNetworkReceivable
 * @see INetworkReceivable
 */
public abstract class AbstractClassNetReceiver<S> implements IClassNetworkReceivable<S> {

	private final INetworkReceivable networkReceivable;
	private final Map<Class<?>, Collection<S>> classListeners = 
			new HashMap<Class<?>, Collection<S>>();
	
	/**
	 * Creates an AbstractClassNetReceiver.
	 * @param networkReceivable The network receivable to expand the
	 * functionality of.
	 */
	public AbstractClassNetReceiver(INetworkReceivable networkReceivable) {
		this.networkReceivable = networkReceivable;
		this.networkReceivable.addListener(new NetworkListener());
	}

	@Override
	public void addListener(INetworkListener listener) {
		networkReceivable.addListener(listener);
	}

	@Override
	public final <T> void addListener(Class<T> c, S listener) {
		Collection<S> l = classListeners.get(c);
		if (l == null) {
			l = new ArrayList<S>();
		}
		l.add(listener);
		classListeners.put(c, l);
	}
	
	/**
	 * Add all to a collection if the addition is not null.
	 * @param main The collection to add to.
	 * @param add The collection with the elements to add.
	 */
	private <T> void addNotNullCollection(Collection<T> main,
			Collection<? extends T> add) {
		if (add != null) {
			main.addAll(add);
		}
	}

	/**
	 * Get all listeners for a specific class.
	 * <p>Listeners registered on the class, a super class or any interface
	 * of the class or super class will be returned.
	 * @param cls The class to find listeners for.
	 * @return The listeners for the class as a collection.
	 */
	private final Collection<S> getListeners(Class<?> cls) {
		Collection<S> matchingListeners = new ArrayList<S>();
		while (cls != null) {
			addNotNullCollection(matchingListeners, classListeners.get(cls));
			for (Class<?> i : cls.getInterfaces()) {
				addNotNullCollection(matchingListeners, classListeners.get(i));
			}
			cls = cls.getSuperclass();
		}
		return matchingListeners;
	}

	@Override
	public <T> T getRemoteObject(Connection connection, int objectID,
			Class<T> iface) {
		return networkReceivable.getRemoteObject(connection, objectID, iface);
	}
	
	/**
	 * Called when an object is received from the network. Passes the
	 * received object to the specified listener.
	 * @param listener The listener to pass the received object to.
	 * @param connectionID The network connection ID.
	 * @param object The received object.
	 * @param time The time the object was sent (in milliseconds).
	 */
	protected abstract <T> void networkReceived(S listener, int connectionID, 
			T object, long time);

	@Override
	public void registerRemoteObject(int objectID, Object object) {
		networkReceivable.registerRemoteObject(objectID, object);
	}

	@Override
	public void removeListener(INetworkListener listener) {
		networkReceivable.removeListener(listener);
	}

	@Override
	public final <T> void removeListener(Class<T> c, S listener) {
		Collection<S> l = classListeners.get(c);
		if (l != null) {
			l.remove(listener);
		}
	}
	
	/**
	 * Called when an object is received from the network.
	 * Passes the object to all listeners registered on the object's class,
	 * super class or any interface it is implementing.
	 * <p>This method is calling {@link #networkReceived(Object, int, Object, long)}
	 * for every listener.
	 * @param connectionID The network connection ID.
	 * @param object The received object.
	 * @param time The time the object was sent as a long (in milliseconds).
	 */
	private void sendToListeners(int connectionID, Object object, long time) {
		Collection<S> listeners = getListeners(object.getClass());
		if (listeners != null && !listeners.isEmpty()) {
			sendToListeners(listeners, connectionID, object, time);
		} else {
			Log.warning("[" + getClass().getSimpleName() + "]: Unregistered "
					+ "NetHolder received: ("
					+ object.getClass().getSimpleName() + ") " + object);
		}
	}
	
	/**
	 * Called when an object is received from the network.
	 * Passes the object to all listeners passed to this method.
	 * <p>This method is calling {@link #networkReceived(Object, int, Object, long)}
	 * for every listener.
	 * @param listeners All listeners the received object will be passed to.
	 * @param connectionID The network connection ID.
	 * @param object The received object.
	 * @param time The time the object was sent as a long (in milliseconds).
	 */
	protected void sendToListeners(Collection<S> listeners, int connectionID,
			Object object, long time) {
		for (S listener : listeners) {
			networkReceived(listener, connectionID, object, time);
		}
	}

	/**
	 * The listener for network objects, hidden from outside.
	 * Calls {@link AbstractClassNetReceiver#sendToListeners(int, Object, long)}
	 * when an object is received.
	 * @author Anton Jansson
	 *
	 */
	private class NetworkListener implements INetworkListener {

		@Override
		public void networkReceived(int connectionID, Object object, long time) {
			sendToListeners(connectionID, object, time);
		}
		
	}
}