package services.network;

/**
 * Listener for when objects of specific classes are received from the network.
 * @author Anton Jansson
 *
 * @param <T> The specific class to listen for.
 */
public interface IClassNetworkListener<T> {
	/**
	 * An object of the specified type is received.
	 * @param connectionID The network connection ID.
	 * @param object The received object.
	 * @param timeSent The time the object was sent.
	 */
	public void networkReceived(int connectionID, T object, long timeSent);
}
