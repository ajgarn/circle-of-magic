package services.network;

/**
 * Holder for when a player disconnects.
 * @author Anton Jansson
 */
public class DisconnectRequest implements INetworkObject {}
