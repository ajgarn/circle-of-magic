package services.network;


/**
 * Holder for all objects that can be sent and received by
 * {@link INetworkReceivable}.
 * 
 * @author Anton Jansson
 * @param <T> The type that will be sent.
 */
public class NetHolder<T> implements INetworkObject {

	private final long timeSent;
	private final T value;
	
	@SuppressWarnings("unused")
	private NetHolder() {
		this(null);
	}
	
	/**
	 * Create a NetHolder object.
	 * @param object The object that will be sent.
	 */
	public NetHolder(T object) {
		this.value = object;
		this.timeSent = System.currentTimeMillis();
	}
	
	/**
	 * Get the passed object.
	 * @return The object passed in this holder.
	 */
	public T get() {
		return value;
	}
	
	/**
	 * Get the time when the content was sent.
	 * @return The time in milliseconds.
	 */
	public long getTime() {
		return timeSent;
	}
}
