package services.network;

/**
 * Classes that implements this interface can be sent over network,
 * between client and server. This is required for all these objects,
 * for network registration.
 * <p><b>Note</b>: Every class that will be sent must offer an empty
 * constructor. This constructor can be <code>private</code>.
 * @author Anton Jansson
 *
 */
public interface INetworkObject {}
