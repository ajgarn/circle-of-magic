package services.network;

import java.net.InetAddress;
import java.util.Collection;


/**
 * Interface for network communication on the server side.
 * @author Anton Jansson
 *
 * @see INetworkReceivable
 */
public interface IServerNetHandler extends INetworkReceivable {
	/**
	 * Get the IP address of the current host.
	 * @return The LAN IP address. if no lan IP was found, return localHost
	 */
	InetAddress getAddress();
	/**
	 * Sends an object to all specified players.
	 * <p>
	 * All objects that will be sent must implement {@link INetworkObject}
	 * or be manually registered for network communication.
	 * // TODO: Fix javadoc above.
	 * @param connectionIDs The connections to send the object to.
	 * @param holder Holder for the object to send.
	 */
	<T> void send(Collection<Integer> connectionIDs, T object);
	/**
	 * Send an object to a specific connected player.
	 * <p>
	 * All objects that will be sent must implement {@link INetworkObject}
	 * or be manually registered for network communication.
	 * // TODO: Fix javadoc above.
	 * @param connectionID The connections to send the object to.
	 * @param holder Holder for the object to send.
	 */
	<T> void send(int connectionID, T object);
	/**
	 * Starts the server thread, making it accessible by clients.
	 * start() can be called in whatever condition, e.g. after stop().
	 */
	void start();
	/**
	 * Stops the server thread, making it unavailable for clients.
	 */
	void stop();
}
