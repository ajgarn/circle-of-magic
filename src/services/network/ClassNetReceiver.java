package services.network;

/**
 * Decorator class for a network receivable, making listeners able to register
 * for events based on specific classes instead of all network events.
 * <p>
 * Listeners must implements {@link IClassNetworkListener}.
 * <p>
 * If the data passed to all listeners needs to be customized, extend the
 * abstract class {@link AbstractClassNetReceiver} instead.
 * 
 * @author Anton
 * @see AbstractClassNetReceiver
 * @see IClassNetworkListener
 */
public class ClassNetReceiver extends AbstractClassNetReceiver<IClassNetworkListener<?>> {

	/**
	 * Creates a ClassNetReceiver, making the network receivable accept
	 * listeners for specific classes.
	 * @param networkReceivable
	 */
	public ClassNetReceiver(INetworkReceivable networkReceivable) {
		super(networkReceivable);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected <T> void networkReceived(IClassNetworkListener<?> listener,
			int connectionID, T object, long time) {
		((IClassNetworkListener<T>) listener).networkReceived(connectionID, object, time);
	}
}
