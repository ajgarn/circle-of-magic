package services.network;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.rmi.TimeoutException;

/**
 * A network receivable that can receive objects from the network,
 * and offers listeners to register for when objects are received.
 * @author Anton Jansson
 *
 * @see INetworkListener
 */
public interface INetworkReceivable {
	/**
	 * Adds a listener on this network receivable.
	 * @param listener The listener to add.
	 */
	void addListener(INetworkListener listener);
	/**
	 * Returns an object from the remote connection. An object space is
	 * used for remote invocation of objects, to allow the remote connection
	 * to access objects using a specified ID.
	 * 
	 * @param connection The connection where the remote object exists.
	 * @param objectID The ID of the remote object.
	 * @param iface The type of the remote object as an interface.
	 * @return The remote object
	 * @throws TimeoutException
	 */
	<T> T getRemoteObject(Connection connection, int objectID, Class<T> iface);
	/**
	 * Register an object to be accessed by remote connections by an ID.
	 * @param objectID The ID to access the object.
	 * @param object The object to be accessible.
	 */
	void registerRemoteObject(int objectID, Object object);
	/**
	 * Removes a listener from this network receivable.
	 * @param listener The listener to remove.
	 */
	void removeListener(INetworkListener listener);
}