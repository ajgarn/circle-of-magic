package services.util;

import java.util.Random;

/**
 * Create random strings.
 * @author Anton Jansson
 *
 */
public class RandomStringUtils {
	 
    private static final String CHAR_LIST = 
        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
     
    /**
     * Creates a random string whose length is the number of characters specified.
     * @param length The length of the string.
     * @return A random string.
     */
    public static String randomString(int length) {
        StringBuffer randStr = new StringBuffer();
        Random r = new Random();
        for(int i = 0; i < length; i++){
            char ch = CHAR_LIST.charAt(r.nextInt(CHAR_LIST.length()));
            randStr.append(ch);
        }
        return randStr.toString();
    }
}