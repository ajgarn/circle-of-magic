package services.logger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import services.logger.Log.Level;

/**
 * Logs to the System console or terminal.
 * @author Anton Jansson
 *
 */
public class ConsoleLogger extends Logger {
	
	private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	/**
	 * Creates a ConsoleLogger with the default {@link Level#WARNING}.
	 */
	public ConsoleLogger() {
		this(Level.WARNING);
	}

	/**
	 * Creates a ConsoleLogger.
	 * @param level The lowest level to print.
	 */
	public ConsoleLogger(Level level) {
		super(level);
	}

	@Override
	public void handleMessage(Level level, Date time, String message, String stackTrace) {
		message = dateFormat.format(time) + " " + level + ": " +  message;
		if (stackTrace != null && !stackTrace.isEmpty()) {
			message += "\n" + stackTrace;
		}
		if (level.compareTo(Level.ERROR) < 0) {
			System.out.println(message);
		} else {
			System.err.println(message);
		}
	}

}
