package services.logger;

import java.util.Date;

import services.logger.Log.Level;

abstract class Logger implements ILogger {
	
	private Level level;

	/**
	 * Creates a new Logger.
	 * @param level The severity level for this Logger.
	 */
	public Logger(Level level) {
		this.level = level;
	}
	
	@Override
	public final Level getLevel() {
		return level;
	}
	
	/**
	 * {@inheritDoc}
	 * <p>This method is only called if {@link #shouldWrite(Level)} returns 
	 * <code>true</code>.
	 * @see #shouldWrite(Level)
	 */
	@Override
	public abstract void handleMessage(Level level, Date time, String message,
			String stackTrace);

	@Override
	public final void setLevel(Level level) {
		this.level = level;
	}
	
	@Override
	public final boolean shouldWrite(Level level) {
		return this.level.compareTo(level) <= 0;
	}
}
