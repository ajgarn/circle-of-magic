package services.logger;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import services.logger.Log.Level;
import services.net.URLReader;
import services.security.ICrypter;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonWriter.OutputType;

/**
 * Sends the logs to a web service via HTTP get.
 * @author Anton Jansson
 *
 */
public class WebLogger extends Logger {
	
	private final static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private final String address;
	private final String source;
	private final int time;
	private final ICrypter crypter;
	private final Collection<LogEvent> events;
	
	/**
	 * Create a WebLogger.
	 * <p>Uses no encryption.
	 * @param level The severity level for this Logger.
	 * @param name The name of this source.
	 * @param address HTTP address to the web service. The data will just be 
	 * appended to this address string.
	 * @param time Time in milliseconds to collect logging before sending all
	 * at the same time.
	 */
	public WebLogger(Level level, String name, String address, int time) {
		this(level, name, address, time, null);
	}

	/**
	 * Create a WebLogger.
	 * @param level The severity level for this Logger.
	 * @param name The name of the source for this logger.
	 * @param address HTTP address to the web service. The data will just be 
	 * appended to this address string.
	 * @param time Time in milliseconds to collect logging before sending all
	 * at the same time.
	 * @param crypter If the web service is using encryption.
	 */
	public WebLogger(Level level, String name, String address, int time, ICrypter crypter) {
		super(level);
		this.address = address;
		this.source = name;
		this.time = time;
		this.crypter = crypter;
		this.events = new ArrayList<LogEvent>();
		
		Thread t = new Thread(new WebSender());
		t.setDaemon(true);
		t.start();
	}

	@Override
	public void handleMessage(Level level, Date time, String message, String stackTrace) {
		if (stackTrace != null && stackTrace.isEmpty()) {
			stackTrace = null;
		}
		synchronized (events) {			
			events.add(new LogEvent(message, time, level.toString(), source, null, stackTrace));
		}
	}
	
	/**
	 * Sends the data to the web service.
	 * @author Anton Jansson
	 *
	 */
	private class WebSender implements Runnable {
		
		private final Json json;
		
		public WebSender() {
			json = new Json();
			json.setOutputType(OutputType.json);
		}

		@Override
		public void run() {
			while (!Thread.interrupted()) {
				try {
					Thread.sleep(time);
				} catch (InterruptedException e) {}
				
				if (!events.isEmpty()) {
					sendEvents();
				}
			}
		}

		public void sendEvents() {
			String data = null;
			synchronized (events) {				
				data = json.toJson(events, null, LogEvent.class);
				events.clear();
			}
			try {
				if (crypter != null) {
					data = crypter.encrypt(data);
				}
				data = URLEncoder.encode(data, "UTF-8");
				URLReader.postData(address, "data=" + data);
			} catch (Exception e) {
				Log.error("Could not send logs to web service.", e);
			}
		}
		
	}

	/**
	 * Holds a log event, which can be parsed to JSON.
	 * @author Anton Jansson
	 *
	 */
	private static class LogEvent {
		@SuppressWarnings("unused")
		private final String time;
		@SuppressWarnings("unused")
		private final String message;
		@SuppressWarnings("unused")
		private final String severity;
		@SuppressWarnings("unused")
		private final String source;
		@SuppressWarnings("unused")
		private final String stacktrace;
		@SuppressWarnings("unused")
		private final String tag;
		
		public LogEvent(String message, Date time, String severity, String source, String tag, String stackTrace) {
			this.time = dateFormat.format(time);
			this.message = message;
			this.severity = severity;
			this.source = source;
			this.stacktrace = (stackTrace != null) ? escape(stackTrace) : null;
			this.tag = tag;
		}
		
		private String escape(String raw) {
			raw = raw.replace("\\", "\\\\");
			raw = raw.replace("\t", "\\t");
			raw = raw.replace("\n", "\\n");
			raw = raw.replace("\r", "\\r");
			raw = raw.replace("\"", "\\\"");
			raw = raw.replace("\'", "\\'");
			return raw;
		}
	}
}
