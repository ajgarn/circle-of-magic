package services.logger;

import java.util.Date;

import services.logger.Log.Level;

/**
 * Interface for loggers being able to handle messages from {@link Log}.
 * @author Anton Jansson
 *
 */
public interface ILogger {
	/**
	 * Get the severity level set for this logger.
	 */
	Level getLevel();
	/**
	 * Handles a message on this Logger.
	 * @param level The severity level of the message.
	 * @param time The time the log was reported.
	 * @param message The message to send.
	 * @param stackTrace The stack trace for an exception.
	 */
	void handleMessage(Level level, Date time, String message, String stackTrace);
	/**
	 * Set the severity level for this level.
	 * @param level The severity level.
	 */
	void setLevel(Level level);
	/**
	 * Returns whether the message should be sent to the user or not.
	 * @param level The severity level to check.
	 * @return True if the message should be printed, false otherwise.
	 */
	boolean shouldWrite(Level level);
}
