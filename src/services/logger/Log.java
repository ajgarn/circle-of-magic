package services.logger;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

/**
 * Used for all logging in the Circle of Magic application.
 * @author Anton Jansson
 *
 */
public class Log {
	
	public enum Level {
		NONE, DEBUG, INFO, WARNING, ERROR, FATAL
	}
	
	private static Collection<ILogger> loggers = new HashSet<ILogger>();;
	
	public static void debug(String message) {
		debug(message, null);
	}
	
	public static void debug(String message, Throwable ex) {
		log(Level.DEBUG, message, ex);
	}
	
	public static void error(String message) {
		error(message, null);
	}
	
	public static void error(String message, Throwable ex) {
		log(Level.ERROR, message, ex);
	}
	
	public static void fatal(String message) {
		fatal(message, null);
	}
	
	public static void fatal(String message, Throwable ex) {
		log(Level.FATAL, message, ex);
	}
	
	public static void info(String message) {
		info(message, null);
	}
	
	public static void info(String message, Throwable ex) {
		log(Level.INFO, message, ex);
	}
	
	public static void warning(String message) {
		warning(message, null);
	}
	
	public static void warning(String message, Throwable ex) {
		log(Level.WARNING, message, ex);
	}
	
	/**
	 * Add a logger to handle messages.
	 * @param logger The logger.
	 */
	public static void addLogger(ILogger logger) {
		loggers.add(logger);
	}
	
	/**
	 * Remove a logger to handle messages.
	 * @param logger The logger.
	 */
	public static void removeLogger(ILogger logger) {
		loggers.remove(logger);
	}
	
	public static void log(Level level, String message, Throwable ex) {
		if (message == null || message.isEmpty()) {
			return;
		}
		
		// Stack trace
		StringWriter stackTraceWriter = null;
		if (ex != null) {
			stackTraceWriter = new StringWriter();
			PrintWriter pw = new PrintWriter(stackTraceWriter);
			ex.printStackTrace(pw);
		}
		
		for (ILogger logger : loggers) {
			if (logger.shouldWrite(level)) {
				logger.handleMessage(level, new Date(), message,
						(ex != null) ? stackTraceWriter.toString() : null);
			}
		}
	}
}
