package services.security;

import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Used for AES/CBC encryption.
 * 
 * @source http://blog.cwill-dev.com/2012/10/09/encryption-between-javaandroid-and-php/
 */
public class AESCrypter {
	
	private final IvParameterSpec ivspec;
	private final SecretKeySpec keyspec;
	private Cipher cipher;

	/**
	 * Creates an AESCrypter.
	 * @param secretkey The secret key for the encryption.
	 * @param iv The initialization vector.
	 */
	public AESCrypter(String secretkey, String iv) {
		ivspec = new IvParameterSpec(iv.getBytes());
		keyspec = new SecretKeySpec(secretkey.getBytes(), "AES");

		try {
			cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Encrypts a string.
	 * @param text The string to encrypt.
	 * @return The encrypted data.
	 * @throws Exception if the string is empty or cannot be encrypted.
	 * @see #decrypt(String)
	 */
	public byte[] encrypt(String text) throws Exception {
		if (text == null || text.length() == 0) {
			throw new Exception("Empty string");
		}
		byte[] encrypted = null;
		try {
			cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
			encrypted = cipher.doFinal(text.getBytes("UTF-8"));
		} catch (Exception e) {
			throw new Exception("[encrypt] " + e.getMessage());
		}
		return encrypted;
	}

	/**
	 * Decrypts a string.
	 * @param code The encrypted data.
	 * @return The decrypted data.
	 * @throws Exception if input data is empty or cannot be decrypted.
	 * @see #encrypt(String)
	 */
	public byte[] decrypt(byte[] code) throws Exception {
		if (code == null || code.length == 0) {
			throw new Exception("Empty string");
		}
		byte[] decrypted = null;
		try {
			cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);
			decrypted = cipher.doFinal(code);
		} catch (Exception e) {
			throw new Exception("[decrypt] " + e.getMessage());
		}
		return decrypted;
	}

	/**
	 * Converts a byte array to hexadecimal values in a string.
	 * @param data The data bytes.
	 * @return A string with the data formatted in hex values.
	 * @see #hexToBytes(String)
	 */
	public static String bytesToHex(byte[] data) {
		if (data == null) {
			return null;
		}
		int len = data.length;
		String str = "";
		for (int i = 0; i < len; i++) {
			if ((data[i] & 0xFF) < 16) {
				str = str + "0" + java.lang.Integer.toHexString(data[i] & 0xFF);
			} else {
				str = str + java.lang.Integer.toHexString(data[i] & 0xFF);
			}
		}
		return str;
	}

	/**
	 * Converts a string with hexadecimal values to a byte array.
	 * @param str The input string with hex values.
	 * @return A byte array with the data.
	 * @see #bytesToHex(byte[])
	 */
	public static byte[] hexToBytes(String str) {
		if (str == null) {
			return null;
		} else if (str.length() < 2) {
			return null;
		} else {
			int len = str.length() / 2;
			byte[] buffer = new byte[len];
			for (int i = 0; i < len; i++) {
				buffer[i] = (byte) Integer.parseInt(
						str.substring(i * 2, i * 2 + 2), 16);
			}
			return buffer;
		}
	}

}
