package services.security;

/**
 * Used for encryption and decryption of data.
 * @author Anton Jansson
 *
 */
public interface ICrypter {
	/**
	 * Encrypts a string.
	 * @param text The string encrypt.
	 * @return The encrypted code.
	 * @throws Exception if the text cannot be encrypted.
	 */
	String encrypt(String text) throws Exception;
	/**
	 * Decrypts a code.
	 * @param code The code to be decrypted.
	 * @return The decrypted string.
	 * @throws Exception if the code cannot be decrypted.
	 */
	String decrypt(String code) throws Exception;
}
