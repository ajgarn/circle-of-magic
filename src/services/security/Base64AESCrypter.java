package services.security;

import javax.xml.bind.DatatypeConverter;

/**
 * Uses AES encryption and encodes the encrypted code in base 64 format.
 * <p>
 * This class uses {@link AESCrypter} for encryption.
 * @author Anton Jansson
 *
 */
public class Base64AESCrypter implements ICrypter {

	private AESCrypter crypter;
	
	/**
	 * Creates a Base64AESCrypter. Parameters configure AES encryption.
	 * @param secretkey The secret key for the encryption.
	 * @param iv The initialization vector.
	 */
	public Base64AESCrypter(String secretKey, String iv) {
		crypter = new AESCrypter(secretKey, iv);
	}

	/**
	 * Encrypts the text using AES and converts the encoding to base 64.
	 */
	@Override
	public String encrypt(String text) throws Exception {
		byte[] encrypted = crypter.encrypt(text);
		return DatatypeConverter.printBase64Binary(encrypted);
	}

	/**
	 * Converts the encoding from base 64 and decrypts the code using AES .
	 */
	@Override
	public String decrypt(String code) throws Exception {
		byte[] bytes = DatatypeConverter.parseBase64Binary(code);
		return new String(crypter.decrypt(bytes), "UTF-8");
	}
	
}
