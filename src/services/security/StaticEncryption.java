package services.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Creates encrypted hashes of strings.
 * @author Anton Jansson
 *
 */
public class StaticEncryption {

	/**
	 * Encrypts a string with the SHA1 algorithm.
	 * 
	 * @param input  The input string.
	 * @return The SHA1 encryption of the string.
	 * @throws NoSuchAlgorithmException
	 *             if no Provider supports a MessageDigestSpi implementation for
	 *             the SHA1 algorithm.
	 */
	public static String sha1(String input) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA1");
        byte[] result = messageDigest.digest(input.getBytes());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }
         
        return sb.toString();
    }
}
