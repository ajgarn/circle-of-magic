package services.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Used for reading content from the World Wide Web.
 * @author Anton Jansson
 *
 */
public class URLReader {

	/**
	 * Read a web content as a string. For info of how the URL string 
	 * can be used, see {@link URL}.
	 * @param urlString The URL to the web content.
	 * @return The content as a string.
	 * @throws Exception if the web content cannot be reached.
	 */
	public static String readUrl(String urlString) throws Exception {
	    BufferedReader reader = null;
	    try {
	        URL url = new URL(urlString);
	        reader = new BufferedReader(new InputStreamReader(url.openStream()));
	        StringBuffer buffer = new StringBuffer();
	        int read;
	        char[] chars = new char[1024];
	        while ((read = reader.read(chars)) != -1)
	            buffer.append(chars, 0, read); 

	        return buffer.toString();
	    } finally {
	        if (reader != null)
	            reader.close();
	    }
	}
	
	/**
	 * Post data to a web page.
	 * @param urlString The web page URL.
	 * @param postParameters The parameters as a string, formatted like "param1=a&param2=b".
	 * @throws IOException if the web page cannot be reached.
	 */
	public static void postData(String urlString, String postParameters) throws IOException {
		HttpURLConnection connection = null;
		try {
			URL url = new URL(urlString);
			connection = (HttpURLConnection) url.openConnection();     
			
			connection.setUseCaches (false); 
			connection.setDoOutput(true);
			
			connection.setFixedLengthStreamingMode(postParameters.getBytes().length);
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			
			// Send request
			PrintWriter out = new PrintWriter(connection.getOutputStream());
			out.print(postParameters);
			out.close();
            
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
            	throw new IOException("HTTP response not OK: " + connection.getResponseMessage());
            }
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}
}
