package services.math;

public class LinearFunction implements IFunction {
	private float[] values;
	/**
	 * Creates values.length -n linear function between the values  in values.
	 * These values have the same distant between each other.
	 * @param values
	 */
	public LinearFunction(float[] values){
		this.values = values;
	}
	/**
	 * Get the approximate value between each values in this equation
	 * entering 0 will return values[0]
	 * entering 1 will return values[values.length-1]
	 */
	@Override
	public float getValue(float lifetime) {
		int numOfIntervals = values.length-1;
		int startIndex = (int)(lifetime*numOfIntervals);
		int endIndex = (startIndex +1)%values.length;
		lifetime -= startIndex / numOfIntervals;
		lifetime *= numOfIntervals;
		lifetime -= startIndex;
		float b =  (values[endIndex]-values[startIndex])*lifetime + values[startIndex];
		return b;
	}

}
