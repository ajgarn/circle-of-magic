package services.math;

public class SinFunction implements IFunction {
	float amp,period;
	public SinFunction(float amp, float period){
		this.amp = amp;
		this.period = period;
	}
	@Override
	public float getValue(float lifetime) {
		return (float)(amp*Math.sin(lifetime * period * Math.PI*2));
	}

}
