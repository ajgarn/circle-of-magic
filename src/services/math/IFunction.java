package services.math;

public interface IFunction {
	public float getValue(float lifetime);
}
