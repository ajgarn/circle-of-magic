package services.math;


/**
 * An interface between libgdx Vector2 an our model Vector2
 * @author Olliver
 *
 */
public interface IVector2{
	public float getX();
	public float getY();
	public void setX(float x);
	public void setY(float y);
	public void nor();
	public float angle();
	public float len();
	public IVector2 add(IVector2 v);
	public IVector2 sub(IVector2 v);
	public IVector2 mul(float m);
	public IVector2 cpy();
	public IVector2 setAngle(float angle);
	public IVector2 rotate(float angle);
}
