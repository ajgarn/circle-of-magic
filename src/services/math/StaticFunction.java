package services.math;


public class StaticFunction implements IFunction {
	private float value;
	public StaticFunction(float value){
		this.value = value;
	}
	@Override
	public float getValue(float lifetime) {
		return value;
	}

}
