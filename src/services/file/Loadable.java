package services.file;
/**
 * Anything that needs to load resources 
 * should implement this class
 * @author Robin
 *
 */
public interface Loadable {
	/**
	 * Load necessary resources for this Loadable object asynchronized.
	 * IMPORTANT: this method puts this loadable in the queue for
	 * loading. Must check if recources ha loaded with hasLoaded() method.
	 */
	public void load();
	/**
	 * Get whenever this Loadable has loaded all resources
	 * @return true if all recources has been loaded, false otherwise
	 */
	public boolean hasLoaded();
}
