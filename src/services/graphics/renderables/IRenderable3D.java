package services.graphics.renderables;


/**
 * A renderable 3d object that can be rendered, updated, killed and positioned 
 * @author Robin Gronberg
 *
 */
public interface IRenderable3D extends IRenderable{
	/**
	 * render this renderable on the screen
	 */
	public void render(boolean opaque);
}
