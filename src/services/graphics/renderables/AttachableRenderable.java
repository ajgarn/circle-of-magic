package services.graphics.renderables;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import services.graphics.core.Attachable;
import services.math.IVector2;

/**
 * A RenderableObject that can be attached with other renderable Objects
 * 
 * @author robin
 * 
 */
public class AttachableRenderable<T extends IRenderable> implements IRenderable, Attachable<T>{
	private List<T> renderables;

	public AttachableRenderable() {
		renderables = new CopyOnWriteArrayList<T>();
	}

	/**
	 * Attach a renderable to this GameObjectView. The attached renderable is
	 * rendered in any of the two methods renderMethods If a particleSystem is
	 * added. The particleSystem will be used during the RUNNING state.
	 * 
	 * @param r
	 *            The renderable to attach
	 */
	public void attach(T r) {
		renderables.add(r);
	}

	/**
	 * Removes the Renderable object from this Object instantly
	 * 
	 * @param r
	 *            The renderable to remove.
	 */
	public void detach(T r) {
		renderables.remove(r);
	}



	@Override
	public void update(float dTime) {
		for (T r : renderables) {
			r.update(dTime);
			if (r.isDead()) {
				detach(r);
			}
		}
	}

	/**
	 * Kills all particleSystems this GameObjectView has attached as well as its
	 * GameObject
	 */

	@Override
	public void kill() {
		for (IRenderable r : renderables) {
			r.kill();
		}
	}

	@Override
	public boolean isDead() {
		for (IRenderable r : renderables) {
			if (!r.isDead())
				return false;
		}
		return true;
	}

	@Override
	public void movePosition(IVector2 pos, float percent) {
		for (IRenderable r : renderables) {
			r.movePosition(pos, percent);
		}
	}

	@Override
	public void setPosition(IVector2 pos) {
		for (IRenderable r : renderables) {
			r.setPosition(pos);
		}
	}

	@Override
	public void setAngle(float angle) {
		for (IRenderable r : renderables) {
			r.setAngle(angle);
		}

	}

	@Override
	public Collection<T> get() {
		return renderables;
	}
}
