package services.graphics.renderables;

public class AttachableRenderable3D extends AttachableRenderable<IRenderable3D>
		implements IRenderable3D {

	@Override
	public void render(boolean opaque) {
		for(IRenderable3D r: get()){
			r.render(opaque);
		}
	}

}
