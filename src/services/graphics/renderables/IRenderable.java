package services.graphics.renderables;

import services.math.IVector2;
/**
 * Everything that can be rendered in some way by a Renderer is a IRenderableObject
 * @author Robin
 *
 */
public interface IRenderable {

	/**
	 * Kill this Renderable. 
	 */
	public abstract void kill();

	/**
	 * Once this Renderable is completely dead, this returns true
	 * @return true if compleately dead, false otherwise
	 */
	public abstract boolean isDead();

	/**
	 * Moves this IRenderableObject towards the position.
	 * @param pos The position to move towards to.
	 * @param percent Value between 0 and 1. 1 sets the position
	 * of this IRenderableObject to pos. 0.5 set the new position
	 * between the current position and pos.
	 */
	public abstract void movePosition(IVector2 pos, float percent);

	/**
	 * Set the position of this IRenderableObject instantly
	 * @param pos The new position this object should have
	 */
	public abstract void setPosition(IVector2 pos);

	/**
	 * Set the angle of this IRenderableObject instantly.
	 * @param angle The angle to set to
	 */
	public abstract void setAngle(float angle);

	/**
	 * Update this renderable. Everything that
	 * should be updated 60 times per second should
	 * this method update
	 * @param dTime
	 */
	public abstract void update(float dTime);

}