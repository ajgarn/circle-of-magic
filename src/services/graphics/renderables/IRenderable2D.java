package services.graphics.renderables;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * A renderable 2d object that can be rendered, updated, killed and positioned 
 * @author Robin Gronberg
 *
 */
public interface IRenderable2D extends IRenderable{
	/**
	 * render this renderable on the screen
	 */
	public void render(SpriteBatch batch);
}
