package services.graphics.core;
/**
 * Any class that should listen to an animation should implement this interface.
 * If the animation has ended, or animation requests a frameChange, this AnimationListener is notified
 * @author Robin Gronberg
 *
 */
public interface AnimationListener {
	/**
	 * When current animation has ended, this method is called
	 */
	public void endAnimation();
}
