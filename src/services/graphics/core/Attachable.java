package services.graphics.core;

import java.util.Collection;

/**
 * Something that can be attached with something
 * @author Robin
 *
 */
public interface Attachable<T> {
	/**
	 * Attach this object
	 * @param object
	 */
	public void attach(T object);
	/**
	 * Detach this object
	 * @param object
	 */
	public void detach(T object);
	/**
	 * Get all attached objects on this Attachable
	 * @return
	 */
	public Collection<T> get();
}
