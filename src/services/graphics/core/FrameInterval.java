package services.graphics.core;
/**
 * A class used to define an interval between frames
 * @author robin
 *
 */
public class FrameInterval {
	/**
	 * Create a new inderval between startFrame and endFrame
	 * @param startFrame
	 * @param endFrame
	 */
	public FrameInterval(int startFrame, int endFrame){
		this.startFrame = startFrame;
		this.endFrame = endFrame;
	}
	public int getStartFrame(){
		return startFrame;
	}
	public int getEndFrame(){
		return endFrame;
	}
	private int startFrame;
	private int endFrame;
	/**
	 * Get whenever specified frame is within this intervall
	 * @param frame
	 * @return true if it is, false otherwise.
	 */
	public boolean isWithinInterval(int frame){
		return frame >= startFrame && frame <= endFrame;
	}
	public boolean isBeyondInterval(int frame){
		return frame > endFrame;
	}
	public boolean isBelowInterval(int frame){
		return frame < startFrame;
	}
}
