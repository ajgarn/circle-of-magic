package services.graphics.core;


/**
 * This class handles different Animations.
 * @author Robin
 *
 */
public interface Animation {

	/**
	 * Add an Animations listener. This listener will be notified when a frame is set and 
	 * when this animation has ended
	 * @param listener The listener.
	 */
	public abstract void addAnimationListener(AnimationListener listener);

	/**
	 * Removes an Animation listener if it listens to this animation
	 * @param listener
	 */
	public abstract void removeAnimationListener(AnimationListener listener);

	public abstract String getNextAnimationName();

	public abstract int getCurrentFrame();

	public abstract boolean isTranssionAnimation();

	public abstract void setCurrentFrame(int frame);

	public abstract int getStartFrame();

	/**
	 * Get if this animtion can start play from current frame
	 * @param currentFrame the frame to check from
	 * @return true if it can, false otherwise
	 */
	public abstract boolean canStartAnimation(int currentFrame);

	/**
	 * Animation this animation as current animation. Notify all the listeners
	 * of the new frame.
	 * @param dTime the time between the updates. dTime = 1 should prepresent 60fps
	 * @param currentFrame The current frame of the model
	 */
	public abstract void animate(float dTime);

}