package services.graphics.core;
/**
 * An object that can be refreshed.
 * The refresh should synchronize the two different objects. 
 * Perhaps after an object was sent by network.
 * @author Robin
 *
 * @param <T> The type of object used to refresh this object
 */
public interface Refreshable<T> {
	/**
	 * Refresh this object. This object should synchronize its data to
	 * the same as object data. Usually used after a network update
	 * @param object
	 */
	public void refresh(T object);
}
