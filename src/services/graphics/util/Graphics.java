package services.graphics.util;



import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
/**
 * A tool class that fixes stuff with graphics,
 * like setup opengl and clear the frame. 
 * @author Robin Gronberg
 *
 */
public class Graphics{
	private Graphics(){	}
	
	public static void setupGL(){
		Gdx.gl.glEnable(GL10.GL_DEPTH_TEST);
		Gdx.gl.glEnable(GL10.GL_LIGHT0);
		Gdx.gl10.glLightfv(GL10.GL_LIGHT0, GL10.GL_POSITION, new float[]{1,1,5,0},0);
		Gdx.gl10.glLightfv(GL10.GL_LIGHT0, GL10.GL_AMBIENT, new float[]{2,2,2,1}, 0);
		Gdx.gl10.glEnable(GL10.GL_LINE_SMOOTH);
		Gdx.gl10.glHint(GL10.GL_LINE_SMOOTH_HINT, GL10.GL_DONT_CARE);
		Gdx.gl10.glLineWidth(3.5f);
		
	}
	public static void clear(){
		Gdx.gl.glClearColor(0, 0, 0, 0f);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);	
	}
}
