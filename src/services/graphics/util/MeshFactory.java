package services.graphics.util;

import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
/**
 * A factory that creates meshes that can be used to render
 * simple textures in the world.
 * @author Robin Gronberg
 *
 */
public class MeshFactory {
	public static Mesh createRectangle(float width,float height,float texCoordX,
			float texCoordY,float texCoordWidth,float texCoordHeight){
		Mesh mesh = new Mesh(true, 4, 6, new VertexAttribute(Usage.Position,
				3, "a_position"), new VertexAttribute(Usage.TextureCoordinates,
						2, "a_texCoord"));
		mesh.setVertices(new float[] { -0.5f * width, 0, 0.5f * height, texCoordX, texCoordY, 
				0.5f * width, 0, 0.5f * height,texCoordWidth, texCoordY, 
				0.5f * width, 0, -0.5f * height, texCoordWidth, texCoordHeight, 
				-0.5f * width, 0, -0.5f * height, texCoordX, texCoordHeight });
		mesh.setIndices(new short[] { 0, 1, 2, 0, 2, 3 });
		return mesh;
	}
	/**
	 * Create a Circle mesh along the x-y plane 
	 * @param radius The radius of the Circle
	 * @param numOfEdges Number of edges used.
	 * @return The mesh.
	 */
	public static Mesh createCircle(float radius,int numOfEdges){
		Mesh mesh = new Mesh(true, numOfEdges + 1, numOfEdges * 3, new VertexAttribute(Usage.Position,
				3, "a_position"));
		float[] vertices = new float[numOfEdges *3];
		short[] indices = new short[numOfEdges * 3];
		/* Loops through and bind vertices center(of the circle),vert[0],vert[1] -> 
		 * center vert[1],vert[2] -> center vert[2],vert[3] etc
		 * until all edges have been looped through. then binds center,last,0
		**/
		for(int i = 0; i < numOfEdges * 3; i+=3){
			vertices[i] = (float)Math.cos(2*Math.PI * i / (numOfEdges * 3)) * 0.5f * radius;
			vertices[i + 1] = 0;
			vertices[i + 2] = (float)Math.sin(2*Math.PI * i / (numOfEdges * 3)) * 0.5f * radius;
			indices[i] = (short)((numOfEdges - 1) * 3); // 0
			indices[i + 1] = (short)(i / 3);
			indices[i + 2] = (short)(i / 3 + 1);
		}
		//bind the end (center,last,0)
		indices[numOfEdges * 3 - 3] = (short)(numOfEdges); // 0
		indices[numOfEdges * 3 - 2] = (short)(numOfEdges - 1); 
		indices[numOfEdges * 3 - 1] = 0; 
		mesh.setVertices(vertices);
		mesh.setIndices(indices);
		return mesh;
	}
}
