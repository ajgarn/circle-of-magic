package services.graphics.util;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g3d.loaders.md2.MD2Loader;
import com.badlogic.gdx.graphics.g3d.materials.Material;
import com.badlogic.gdx.graphics.g3d.model.keyframe.KeyframedModel;
/**
 * A class that loads data from the res/ directory. If some data
 * are already loaded, and something is trying to load it again,
 * The same object are returned. 
 * @author Robin Gronberg
 * Used by everything that loads data from hard drive.
 */
public class ContentManager {
	private LoadingAsset<Texture> textures;
	private LoadingAsset<KeyframedModel> models;
	private LoadingAsset<BitmapFont> fonts;
	private MD2Loader md2Loader;
	private AssetManager manager;
	
	private ContentManager(){ 
		md2Loader = new MD2Loader();
		textures = new LoadingAsset<Texture>();
		models = new LoadingAsset<KeyframedModel>();
		fonts = new LoadingAsset<BitmapFont>();
		manager=new AssetManager();
	}
	private static ContentManager instance;
	public static ContentManager getInstance(){
		if(instance == null){
			instance = new ContentManager();
		}
		return instance;
	}
	public Texture getTexture(String location){
		location = "res/" + location;
		Texture.setEnforcePotImages(false);
		Texture texture = textures.get(location);
		if(texture == null){
			//model has never loaded before
			texture = new Texture(location);
			//texture = manager.get(location);
			textures.put(location, texture);
			//texture.setAssetManager(manager);
		}
		return texture;
	}
	public KeyframedModel getModel(String location){
		location = "res/" + location;
		KeyframedModel model = models.get(location);
		if(model == null){
			//model has never loaded before
			model = md2Loader.load(Gdx.files.internal(location),1);
			model.setMaterial(new Material());
			models.put(location, model);
		}
		return model;
	}
	public BitmapFont getFont(String location, String imageLocation){
		location = "res/" + location;
		imageLocation = "res/" + imageLocation;
		BitmapFont font = fonts.get(location);
		if(font == null){
			//model has never loaded before
			font = new BitmapFont(Gdx.files.internal(location), Gdx.files.internal(imageLocation),false);
			fonts.put(location, font);
		}
		return font;
	}
	/**
	 * Get progress of loading.
	 * @return float value [0] to [1].
	 */
	public float getProgress(){
		return manager.getProgress();
	}
	/**
	 * Putting an asset into the loading queue.
	 * @param desc AssetDescriptor
	 */
	public void load(AssetDescriptor desc){
		manager.load(desc);
	}
	private class LoadingAsset<T>{
		Map<String,T> assets;
		public LoadingAsset() {
			assets = new HashMap<String, T>();
		}
		public T get(String location){
			T asset = assets.get(location);
			return asset;
		}
		public T put(String location, T asset){
			return assets.put(location, asset);
		}
	}
}
