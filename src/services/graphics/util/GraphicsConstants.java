package services.graphics.util;
/**
 * Holds different graphic constants
 * @author Robin
 *
 */
public class GraphicsConstants {
	public static final int PREFERRED_SCREEN_WIDTH = 1920;
	public static final int PREFERRED_SCREEN_HEIGHT = 1080;
}
