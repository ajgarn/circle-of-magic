package services.graphics.particles;

import services.graphics.core.AnimationListener;
import services.graphics.particles.effects.IParticleEffect;
import services.graphics.particles.effects.Particle;
import services.graphics.renderables.IRenderable3D;
import services.math.IVector2;
/**
 * A ParticleSystem that holds a String as a string.
 * Mainly used to categories different ParticleSystems
 * @author robin
 *
 */
public class StateParticleSystem implements IParticleSystem,IRenderable3D{
	private final ParticleSystem particleSystem;
	private final String state;
	/**
	 * Create a new ParticleSystem.
	 * @param String The String this ParticleSystem shall have.
	 */
	public StateParticleSystem(String String, ParticleSystem particleSystem) {
		this.state = String;
		this.particleSystem = particleSystem;
	}
	@Override
	public void update(float dTime) {
		particleSystem.update(dTime);
	}
	public String getState(){
		return state;
	}
	@Override
	public void render(boolean opaque) {
		particleSystem.render(opaque);
	}
	@Override
	public void kill() {
		particleSystem.kill();
	}
	@Override
	public boolean isDead() {
		return particleSystem.isDead();
	}
	@Override
	public void movePosition(IVector2 pos,float percent) {
		particleSystem.movePosition(pos,percent);
	}
	@Override
	public void setPosition(IVector2 pos) {
		particleSystem.setPosition(pos);
	}
	@Override
	public void setAngle(float angle) {}
	@Override
	public void addParticle(Particle p) {
		particleSystem.addParticle(p);
	}
	@Override
	public void addParticleEffect(IParticleEffect e) {
		particleSystem.addParticleEffect(e);
		
	}
	@Override
	public void addAnimationListener(AnimationListener a) {
		particleSystem.addAnimationListener(a);
	}
	@Override
	public AbstractEmitter getEmitter() {
		return particleSystem.getEmitter();
	}
	@Override
	public void setEmitter(AbstractEmitter e) {
		particleSystem.setEmitter(e);
	}

}
