package services.graphics.particles;

import services.graphics.core.AnimationListener;
import services.graphics.particles.effects.IParticleEffect;
import services.graphics.particles.effects.Particle;


public interface IParticleSystem {
	/**
	 * Add a particle to the particle system at ParticleSystem position
	 * @param particle
	 */
	public void addParticle(Particle p);
	public void addParticleEffect(IParticleEffect e);
	/**
	 * if a animationListener wants to know when the animation has
	 * ended (ParticleSystem) add it here
	 * @param a The AnimationListener that 
	 */
	public void addAnimationListener(AnimationListener a);
	/**
	 * Get the emitter of this particle system
	 * @return The emitter
	 */
	public AbstractEmitter getEmitter();
	/**
	 * Set the emitter of this particle system
	 * @param e The new emitter
	 */
	public void setEmitter(AbstractEmitter e);
}
