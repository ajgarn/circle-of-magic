package services.graphics.particles;


import services.graphics.particles.effects.Particle;
import services.math.IFunction;

import com.badlogic.gdx.math.Vector3;
/**
 * Use this class if particle should Emitt in a circle
 * @author robin
 *
 */
public class CircleEmitter extends AbstractEmitter {
	private float particleMaxLifetime,particleMinLifetime,dAngle,angle,minSpeed,maxSpeed,distanceMin,distanceMax;
	public CircleEmitter(IFunction property, float particleMinLifetime,
			float particleManLifetime,float lifetime,float dAngle,
			float minSpeed,float maxSpeed) {
		this(property, particleMinLifetime,
			 particleManLifetime, lifetime, dAngle,
			 minSpeed, maxSpeed,0,0);
	}
	
	public CircleEmitter(IFunction property, float particleMinLifetime,
			float particleMaxLifetime,float lifetime,float dAngle,
			float minSpeed,float maxSpeed,float distanceMin,float distanceMax) {
		super(property,lifetime);
		this.particleMinLifetime = particleMinLifetime;
		this.particleMaxLifetime = particleMaxLifetime;
		this.distanceMin = distanceMin;
		this.distanceMax = distanceMax;
		this.minSpeed = minSpeed;
		this.maxSpeed = maxSpeed;
		this.dAngle = dAngle;
	}

	protected Particle spawnParticle() {
		angle += dAngle;
		float time = getRandom(particleMinLifetime, particleMaxLifetime);
		float speed = getRandom(minSpeed, maxSpeed);
		float distance = getRandom(distanceMin, distanceMax);
		Vector3 dir = new Vector3((float)Math.cos(angle), (float)Math.sin(angle),0);
		return new Particle(time,dir.cpy().mul(distance),dir.cpy().mul(speed));
	}
	public void setRadius(float radius){
		float oldRadius = (distanceMin + distanceMax) / 2;
		float deltaRadius = oldRadius - radius;
		distanceMax -= deltaRadius;
		distanceMin -= deltaRadius;
	}


}
