package services.graphics.particles.effects;


import com.badlogic.gdx.math.Vector3;

public interface IParticleEffect {
	/**
	 * Apply particle effects before rendering
	 * @param particle
	 */
	public void applyEffect(Particle particle,Vector3 particleSystemPosition);
}
