package services.graphics.particles.effects;


import services.graphics.util.MeshFactory;
import client.graphics.IWorldCamera;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.math.Vector3;

public class Particle implements Comparable<Particle>{
	private static Mesh mesh;
	private float lifetime,currentLifetime;
	
	private Vector3 position,direction;
	/**
	 * Create new Particle
	 * @param lifetime lifetime in seconds
	 * @param pos The position of the particle
	 * @param dir The direction of the particle
	 */
	public Particle(float lifetime,Vector3 pos,Vector3 dir){
		this.lifetime = lifetime;
		this.position = pos;
		this.direction = dir;
	}
	private static Mesh getMesh(){
		if(mesh == null){
			mesh = MeshFactory.createRectangle(0.2f, 0.2f,0,0,1,1);
		}
		return mesh;
	}
	/**
	 * Get lifetime in precent
	 * @return Lifetime in percent. 0 when emitted. 1 when dead.
	 */
	public float getCurrentLifetime(){
		return currentLifetime <= lifetime? currentLifetime/lifetime:0.99f;
	}
	public void update(float dTime){
		position.add(direction);
		currentLifetime += dTime/60;
	}
	public boolean isDead(){
		return currentLifetime > lifetime;
	}
	public void setDirection(Vector3 dir){
		this.direction = dir;
	}
	public Vector3 getDirection(){
		return direction.cpy();
	}
	public Vector3 getPosition(){
		return position.cpy();
	}
	public void addDirection(Vector3 dir){
		direction.add(dir);
	}
	public void addPosition(Vector3 pos){
		position.add(pos);
	}
	public void preRender(){
		Gdx.gl10.glPushMatrix();
		Gdx.gl10.glTranslatef(position.x, position.z, position.y);
		Gdx.gl10.glRotatef((float)(Math.atan2(direction.x, direction.y)*360/Math.PI / 2) - 90,0, 1, 0);		
	}
	/**
	 * Render particle at proper position. 
	 * any texture, scale or rotation should be applied before by
	 * an particleEffect
	 */
	public void render(){
		//TODO, fix to render with spritebatch instead of mesh render
		getMesh().render(GL10.GL_TRIANGLES);
	}
	public void postRender(){
		Gdx.gl10.glPopMatrix();
	}
	@Override
	public int compareTo(Particle o) {
		if(o.position.y < position.y){
			return -1;
		}else return 1;
	}
}
