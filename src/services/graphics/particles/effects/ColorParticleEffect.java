package services.graphics.particles.effects;


import services.math.IFunction;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;


public class ColorParticleEffect implements IParticleEffect {
	private IFunction red,green,blue,alpha;
	public ColorParticleEffect(IFunction red,IFunction green,IFunction blue, IFunction alpha){
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.alpha = alpha;
	}

	@Override
	public void applyEffect(Particle particle,Vector3 particleSystemPosition) {
		Gdx.gl10.glColor4f(red.getValue(particle.getCurrentLifetime()), 
				green.getValue(particle.getCurrentLifetime()), 
				blue.getValue(particle.getCurrentLifetime()),
				alpha.getValue(particle.getCurrentLifetime()));
	}
}
