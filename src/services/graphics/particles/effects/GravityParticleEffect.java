package services.graphics.particles.effects;


import services.math.IFunction;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class GravityParticleEffect implements IParticleEffect {
	private IFunction function;
	private Vector3 position;
	/**
	 * Accelerate the particle towards position by 1m/s * function value
	 * @param function The function to determine how much the particles should be affected during it's lifetime
	 * @param position The position to move towards as a vector3 (height will be 0)
	 */
	public GravityParticleEffect(IFunction function, Vector3 position){
		this.function = function;
		this.position = position;
	}
	/**
	 * Accelerate the particle towards position of the particleSystem by 1m/s * function value
	 * @param function The function to determine how much the particles should be affected during it's lifetime
	 * @param position The position to move towards as a vector3 (height will be 0)
	 */
	public GravityParticleEffect(IFunction function){
		this.function = function;
	}
	
	@Override
	public void applyEffect(Particle particle,Vector3 particleSystemPosition) {
		Vector3 dir;
		if(position != null)dir = particle.getPosition().sub(position).nor();
		else dir = particle.getPosition().sub(particleSystemPosition).nor();
		particle.addDirection(dir.mul(-function.getValue(particle.getCurrentLifetime())/60));

	}

}
