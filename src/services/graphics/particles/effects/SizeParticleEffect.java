package services.graphics.particles.effects;

import services.math.IFunction;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;


public class SizeParticleEffect implements IParticleEffect{
	private IFunction function;
	public SizeParticleEffect(IFunction function) {
		this.function = function;
	}
	@Override
	public void applyEffect(Particle particle,Vector3 particleSystemPosition) {
		float scale = function.getValue(particle.getCurrentLifetime());
		Gdx.gl10.glScalef(scale,scale,scale);
		//TODO restore after scale
	}

}
