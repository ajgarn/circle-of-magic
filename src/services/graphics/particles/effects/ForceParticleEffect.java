package services.graphics.particles.effects;

import services.math.IFunction;

import com.badlogic.gdx.math.Vector3;


public class ForceParticleEffect implements IParticleEffect {
	private IFunction function;
	private Vector3 direction;
	/**
	 * Accelerate the particle towards direction by 1m/s * function value
	 * @param function The function to determine how much the particles should be affected during it's lifetime
	 * @param direction The direction to move as a vector3
	 */
	public ForceParticleEffect(IFunction function, Vector3 direction){
		this.function = function;
		this.direction = direction;
	}
	@Override
	public void applyEffect(Particle particle,Vector3 particleSystemPosition){
		Vector3 dirTmp = direction.cpy();
		particle.addDirection(dirTmp.mul(function.getValue(particle.getCurrentLifetime())/60));
	}

}
