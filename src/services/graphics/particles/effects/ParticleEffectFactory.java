package services.graphics.particles.effects;

import services.math.IFunction;
import services.math.LinearFunction;
import services.math.StaticFunction;


public class ParticleEffectFactory {
	public static IParticleEffect createFireColorEffect(){
		IFunction red = new StaticFunction(1);
		IFunction green = new LinearFunction(new float[]{1,0});
		IFunction blue = new StaticFunction(0);
		IFunction alpha = new LinearFunction(new float[]{1,0});
		return new ColorParticleEffect(red, green, blue, alpha);
	}
	public static IParticleEffect createIceColorEffect(){
		IFunction red = new LinearFunction(new float[]{1,0});
		IFunction green = new LinearFunction(new float[]{1,0.5f});
		IFunction blue = new StaticFunction(1);
		IFunction alpha = new LinearFunction(new float[]{1,0});
		return new ColorParticleEffect(red, green, blue, alpha);
	}
	public static IParticleEffect createGreenColorEffect(){
		IFunction red = new LinearFunction(new float[]{1,0});
		IFunction green = new StaticFunction(1);
		IFunction blue = new LinearFunction(new float[]{1,0.5f});
		IFunction alpha = new LinearFunction(new float[]{1,0});
		return new ColorParticleEffect(red, green, blue, alpha);
	}
	public static IParticleEffect createBlueColorEffect(){
		IFunction red = new LinearFunction(new float[]{1,0});
		IFunction green = new LinearFunction(new float[]{1,0});
		IFunction blue = new StaticFunction(1);
		IFunction alpha = new LinearFunction(new float[]{1,0});
		return new ColorParticleEffect(red, green, blue, alpha);
	}
	public static IParticleEffect createSmokeColorEffect(){
		IFunction red,green,blue;
		red = green = blue = new LinearFunction(new float[]{0.5f,0});
		IFunction alpha = new LinearFunction(new float[]{1,0});
		return new ColorParticleEffect(red, green, blue, alpha);
	}
	public static IParticleEffect createParticleAttractorEffect(float speed){
		IFunction f = new LinearFunction(new float[]{0,0,0,0.5f*speed,speed,2*speed});
		return new GravityParticleEffect(f);
	}
}
