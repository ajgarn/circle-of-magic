package services.graphics.particles;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import services.graphics.core.AnimationListener;
import services.graphics.particles.effects.IParticleEffect;
import services.graphics.particles.effects.Particle;
import services.graphics.renderables.IRenderable3D;
import services.math.IVector2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;

public class ParticleSystem implements IParticleSystem,IRenderable3D{
	private final List<IParticleEffect>effects;
	private final Set<Particle> particles;
	private final Texture texture;
	
	private AbstractEmitter emitter;
	private Vector3 position;
	private boolean alive = true;
	
	public ParticleSystem(Texture texture,AbstractEmitter emitter,Vector3 pos){
		particles = new TreeSet<Particle>();
		effects = new ArrayList<IParticleEffect>(10);
		this.texture = texture;
		this.emitter = emitter;
		this.position = pos;
	}
	public AbstractEmitter getEmitter(){
		return emitter;
	}
	public void setEmitter(AbstractEmitter e){
		this.emitter = e;
	}
	@Override
	public void movePosition(IVector2 pos, float percent) {
		position.x = ((position.x * (1 - percent) + pos.getX() * percent));
		position.y = ((position.y * (1 - percent) + pos.getY() * percent));
	}
	@Override
	public void setPosition(IVector2 pos) {
		movePosition(pos, 1);	
	}
	public void addParticle(Particle particle) {
		particle.addPosition(new Vector3(position.x,position.y,position.z));
		particles.add(particle);
	}
	public void addParticleEffect(IParticleEffect e){
		effects.add(e);
	}
	/**
	 * Update the emitter and eventually emit particles
	 * @param dTime time between render calls
	 */
	public void update(float dTime) {
		for(Particle p : particles){
			p.update(dTime);
		}
		Iterator<Particle> t = particles.iterator();
		while(t.hasNext()){
			if(t.next().isDead()){
				t.remove();
			}
		}
		if(alive){
			emitter.update(dTime, this);
		}
	}
	/**
	 * This method calls applyEffect for each particle
	 * in the particle system as well as renders them
	 * @param dTime time between render calls
	 */
	public void render(boolean opaque) {
		if(!opaque){
			Gdx.gl10.glDisable(GL10.GL_LIGHTING);
			Gdx.gl10.glEnable(GL10.GL_BLEND);
			Gdx.gl10.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
			Gdx.gl10.glEnable(GL10.GL_DEPTH_TEST);
			Gdx.gl10.glDepthMask(false);
			if(texture != null){
				Gdx.gl10.glEnable(GL10.GL_TEXTURE_2D);
				texture.bind();
			}
			else
				Gdx.gl10.glDisable(GL10.GL_TEXTURE_2D);
			for(Particle p : particles){
				p.preRender();
				for(IParticleEffect effect:effects){
					effect.applyEffect(p,position.cpy());
				}
				p.render();
				p.postRender();
			}
			Gdx.gl10.glDepthMask(true);
		}
	}
	/**
	 * Kill this particleSystem
	 * a dead particleSystem cannot emit particles
	 */
	public void kill(){
		alive = false;
	}
	/**
	 * Check whenever this particleSystem is dead or not
	 * @return true if this system has no particles left
	 * and was killed before by kill() call. false otherwise
	 */
	public boolean isDead(){
		return !alive && particles.isEmpty();
	}
	@Override
	public void setAngle(float angle) {
		// TODO Auto-generated method stub
	}
	/**
	 * if a animationListener wants to know when the animation has
	 * ended (ParticleSystem) add it here
	 * @param a The AnimationListener that 
	 */
	public void addAnimationListener(AnimationListener a){
		emitter.addAnimationListener(a);
	}
}
