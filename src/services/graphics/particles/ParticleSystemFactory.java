package services.graphics.particles;

import services.graphics.particles.effects.ColorParticleEffect;
import services.graphics.particles.effects.ForceParticleEffect;
import services.graphics.particles.effects.ParticleEffectFactory;
import services.graphics.particles.effects.SizeParticleEffect;
import services.graphics.util.ContentManager;
import services.math.IFunction;
import services.math.LinearFunction;
import services.math.StaticFunction;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;

/**
 * A factory class that creates different particleSystems
 * 
 * @author Robin Gronberg
 * 
 */
public class ParticleSystemFactory {
	private static float HEIGHT = 2;
	private static float HEIGHT_OUTSIDE_MAP = -3;

	public static ParticleSystem createFire(float size, float intensity) {
		ParticleSystem p = createSmokeEffect(size, ContentManager.getInstance()
				.getTexture("particles/flameParticle.png"), intensity);
		p.addParticleEffect(ParticleEffectFactory.createFireColorEffect());
		return p;
	}

	public static ParticleSystem createBouncing(float size, float intensity) {
		ParticleSystem p = createTailEffect(
				size,
				0.5f,
				2,
				0.1f * size,
				ContentManager.getInstance().getTexture(
						"particles/flameParticle.png"),
				intensity);
		p.addParticleEffect(ParticleEffectFactory.createGreenColorEffect());
		return p;
	}

	public static ParticleSystem createLightning(float size, float intensity) {
		ParticleSystem p = createAllDirectionsEffect(size, ContentManager
				.getInstance().getTexture("particles/particleSpark.png"),
				intensity, 1.7f);
		p.addParticleEffect(ParticleEffectFactory.createBlueColorEffect());
		return p;
	}

	public static ParticleSystem createFrostbolt(float size, float intensity) {
		ParticleSystem p = createTailEffect(
				size,
				0.5f,
				2,
				0.1f * size,
				ContentManager.getInstance().getTexture(
						"particles/flameParticle.png"),
				intensity);
		p.addParticleEffect(ParticleEffectFactory.createIceColorEffect());
		return p;
	}

	public static ParticleSystem createExplostion(float size) {
		ParticleSystem p = createSplashEffect(size, ContentManager
				.getInstance().getTexture("particles/particle.png"));
		p.addParticleEffect(ParticleEffectFactory.createFireColorEffect());
		return p;
	}

	public static ParticleSystem createCircleExplostion(float size) {
		ParticleSystem p = createCircleExplotionEffect(size, ContentManager
				.getInstance().getTexture("particles/particle.png"));
		p.addParticleEffect(ParticleEffectFactory.createFireColorEffect());
		return p;
	}

	public static ParticleSystem createEnflameExplostion(float size) {
		ParticleSystem p = createEnflameEffect(size, ContentManager
				.getInstance().getTexture("particles/particle.png"));
		p.addParticleEffect(ParticleEffectFactory.createFireColorEffect());
		return p;
	}

	public static ParticleSystem createSmoke(float size) {
		ParticleSystem p = createSmokeEffect(size, ContentManager.getInstance()
				.getTexture("particles/particle.png"), 1);
		p.addParticleEffect(ParticleEffectFactory.createSmokeColorEffect());
		return p;
	}

	private static ParticleSystem createSmokeEffect(float size, Texture tex,
			float intensity) {
		IFunction pr = new StaticFunction(intensity);
		VectorEmitter e = new VectorEmitter(pr, 0.2f, 0.8f, 10, new Vector3(0,
				0, 0), new Vector3(0, 0, 0), new Vector3(-0.015f * size,
				-0.015f * size, 0.01f * size), new Vector3(0.015f * size,
				0.015f * size, 0.02f * size));
		ParticleSystem p = new ParticleSystem(tex, e, new Vector3(0, 0, HEIGHT));
		p.addParticleEffect(new SizeParticleEffect(new LinearFunction(
				new float[] { 2 * size, 5 * size })));
		return p;
	}

	private static ParticleSystem createSplashEffect(float size, Texture tex) {
		IFunction pr = new LinearFunction(new float[] { 180, 0, 0, 0, 0, 0, 0,
				0, 0 });
		VectorEmitter e = new VectorEmitter(pr, 0.5f, 0.8f, 0.5f, new Vector3(
				0, 0, 0), new Vector3(0, 0, 0), new Vector3(-0.08f * size,
				-0.08f * size, 0.08f * size), new Vector3(0.08f * size,
				0.08f * size, 0.08f * size));
		ParticleSystem p = new ParticleSystem(tex, e, new Vector3(0, 0, HEIGHT));
		p.addParticleEffect(new SizeParticleEffect(new LinearFunction(
				new float[] { 2 * size, 5 * size })));
		p.addParticleEffect(new ForceParticleEffect(new StaticFunction(-1f),
				new Vector3(0, 0, 1)));
		return p;
	}

	private static ParticleSystem createTailEffect(float size, float length,
			float radius, float shatter, Texture tex, float intensity) {
		IFunction f = new StaticFunction(intensity);
		VectorEmitter e = new VectorEmitter(f, length, length, 1, new Vector3(
				shatter * size, shatter * size, shatter * size), new Vector3(
				-shatter * size, -shatter * size, -shatter * size));
		ParticleSystem p = new ParticleSystem(tex, e, new Vector3(0, 0, HEIGHT));
		p.addParticleEffect(new SizeParticleEffect(new LinearFunction(
				new float[] { radius * size, 0 })));
		return p;
	}

	private static ParticleSystem createCircleExplotionEffect(float size,
			Texture tex) {
		IFunction f = new LinearFunction(new float[] { 50, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0, 0 });
		CircleEmitter e = new CircleEmitter(f, 1, 1.5f, 1, 0.1f, 0.1f, 0.1f);
		ParticleSystem p = new ParticleSystem(tex, e, new Vector3(0, 0, HEIGHT));
		p.addParticleEffect(new SizeParticleEffect(new LinearFunction(
				new float[] { 1 * size, 8 * size })));
		return p;
	}

	private static ParticleSystem createEnflameEffect(float size, Texture tex) {
		IFunction f = new LinearFunction(new float[] { 50, 0, 0, 0, 0, 0, 0 });
		CircleEmitter e = new CircleEmitter(f, 0.4f, 1f, 1, 0.1f, 0.1f, 0.1f,
				0, size);
		ParticleSystem p = new ParticleSystem(tex, e, new Vector3(0, 0, HEIGHT));
		p.addParticleEffect(new SizeParticleEffect(new LinearFunction(
				new float[] { 2 * size, 6 * size })));
		return p;
	}

	public static ParticleSystem createLavaFlowEffect(float size, Texture tex) {
		IFunction f = new StaticFunction(3);
		CircleEmitter e = new CircleEmitter(f, 1, 6, 10, 1f, 0.01f, 0.015f, 35,
				35);
		ParticleSystem p = new ParticleSystem(tex, e, new Vector3(0, 0, HEIGHT_OUTSIDE_MAP));
		p.addParticleEffect(new SizeParticleEffect(new LinearFunction(
				new float[] { 1 * size, 8 * size })));
		IFunction c = new StaticFunction(1);
		IFunction a = new LinearFunction(new float[] { 1, 0 });
		p.addParticleEffect(new ColorParticleEffect(c, c, c, a));
		return p;
	}

	public static ParticleSystem createAllDirectionsEffect(float size,
			Texture tex, float intensity, float speed) {
		IFunction pr = new StaticFunction(intensity);
		VectorEmitter e = new VectorEmitter(pr, 0.15f / speed, .11f / speed,
				10, new Vector3(0, 0, 0), new Vector3(0, 0, 0), new Vector3(
						-0.15f * size * speed, -0.15f * size * speed, -0.15f
								* size * speed), new Vector3(0.15f * size
						* speed, 0.15f * size * speed, 0.15f * size * speed));
		ParticleSystem p = new ParticleSystem(tex, e, new Vector3(0, 0, HEIGHT));
		p.addParticleEffect(new SizeParticleEffect(new LinearFunction(
				new float[] { 2 * size, 5 * size })));
		return p;
	}
}
