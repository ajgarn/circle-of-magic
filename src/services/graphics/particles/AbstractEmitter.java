package services.graphics.particles;

import java.util.ArrayList;
import java.util.List;

import services.graphics.core.AnimationListener;
import services.graphics.particles.effects.Particle;
import services.math.IFunction;


/**
 * An Emitter is something that emits particles in a
 * particleSystem. 
 * @author robin
 *
 */
public abstract class AbstractEmitter {
	private IFunction function;
	private float time,particlesLeft,lifetime;
	private final List<AnimationListener> listeners;
	private boolean isPaused = false;
	public AbstractEmitter(IFunction property,float lifetime){
		this.function = property;
		this.lifetime = lifetime;
		listeners = new ArrayList<AnimationListener>();
	}
	public void update(float dTime, IParticleSystem system){
		if(!isPaused){
			time+=(dTime/60);
			if(time >= lifetime){
				time%=lifetime;
				for(AnimationListener a:listeners){
					a.endAnimation();
				}
			}
			particlesLeft += Math.abs(function.getValue(time/lifetime)*dTime);
			while(particlesLeft >= 1){
				system.addParticle(spawnParticle());
				particlesLeft--;
			}
		}
	}
	protected float getRandom(float min, float max){
		return (float)Math.random()*(max-min) + min;
	}
	protected abstract Particle spawnParticle();
	public void addAnimationListener(AnimationListener a){
		listeners.add(a);
	}
	public void pause(){
		isPaused = true;
	}
	public void resume(){
		isPaused = false;
		
	}
	/**
	 * Set the intensity as a function;
	 * @param function The new intensity
	 */
	public void setIntensity(IFunction function){
		this.function = function;
	}
}
