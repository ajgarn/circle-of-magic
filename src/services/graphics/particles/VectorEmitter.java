package services.graphics.particles;


import services.graphics.particles.effects.Particle;
import services.math.IFunction;

import com.badlogic.gdx.math.Vector3;
/**
 * Use this Emitter if Particles should emitt with a max and min direction discribed as a Vector3
 * @author robin
 *
 */
public class VectorEmitter extends AbstractEmitter {
	private Vector3 maxPos,minPos,maxDir,minDir;
	private float particleMaxLifetime,particleMinLifetime;
	public VectorEmitter(IFunction property, float particleMinLifetime,
			float particleMaxLifetime,float lifetime) {
		super(property,lifetime);
		this.particleMinLifetime = particleMinLifetime;
		this.particleMaxLifetime = particleMaxLifetime;
		maxPos = minPos = maxDir = minDir = Vector3.Zero;
	}
	public VectorEmitter(IFunction property, float minLifetime,
			float maxLifetime,float lifetime,Vector3 maxPos,Vector3 minPos) {
		this(property, minLifetime, maxLifetime,lifetime);
		this.maxPos = maxPos;
		this.minPos = minPos;
	}
	public VectorEmitter(IFunction property, float minLifetime,
			float maxLifetime,float lifetime,Vector3 maxPos,Vector3 minPos,
			Vector3 maxDir,Vector3 minDir) {
		this(property, minLifetime,maxLifetime,lifetime ,maxPos,minPos);
		this.maxDir = maxDir;
		this.minDir = minDir;
	}

	@Override
	protected Particle spawnParticle() {
		float time = getRandom(particleMinLifetime, particleMaxLifetime);
		Vector3 pos = new Vector3(getRandom(minPos.x, maxPos.x), getRandom(minPos.y, maxPos.y), getRandom(minPos.z, maxPos.z));
		Vector3 dir = new Vector3(getRandom(minDir.x, maxDir.x), getRandom(minDir.y, maxDir.y), getRandom(minDir.z, maxDir.z));
		return new Particle(time,pos,dir);
	}

}
