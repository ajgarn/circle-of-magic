package services.graphics.models;



import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import services.graphics.core.Animation;
import services.graphics.core.AnimationListener;
import services.graphics.core.FrameInterval;
import services.graphics.renderables.IRenderable3D;
import services.math.IVector2;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.model.keyframe.KeyframedModel;
import com.badlogic.gdx.math.Vector3;
/**
 * This class is used to represent a 3d model in a 3d world space.
 * @author robin
 *
 */
public class Model implements AnimationListener,IRenderable3D{
	private final KeyframedModel model;
	private final Texture texture;
	private final Map<String, ModelAnimation> animations;
	private final boolean lightningEnabled,textureEnabled,shadowEnabled,lockRotation;
	
	private Color color;
	private boolean alive = true;
	private float angle;
	private Vector3 position;
	private Vector3 scale;
	private String currentAnimationName;
	private String desiredAnimationName;
	private Animation currentAnimation;
	/** 
	 * Create a new 3d model
	 * @param modelLoc Location of the model. The model should use the format .md2
	 * @param texLoc Location of the texture that should be mapped around the model. UV-mapping is used to mapp the texture. Texture should use format .png
	 * @param useTexture If the model should use the texture when rendering
	 * @param useLightning If the model should use existing lightning when rendered.
	 */
	public Model(KeyframedModel model,Texture texture,boolean useTexture,boolean useLightning,boolean useShadow,boolean lockRotation){
		this(model,texture,useTexture,useLightning,useShadow,lockRotation,0,new Vector3(0,0,0));
	}
	/** 
	 * Create a new 3d model
	 * @param modelLoc Location of the model. The model should use the format .md2
	 * @param texLoc Location of the texture that should be mapped around the model. UV-mapping is used to mapp the texture. Texture should use format .png
	 * @param useTexture If the model should use the texture when rendering
	 * @param useLightning If the model should use existing lightning when rendered.
	 * @param angle The angle this model should use when rendering.
	 * @param position The position this model should use when rendering.
	 */
	public Model(KeyframedModel model,Texture texture,
			boolean useTexture,boolean useLightning,boolean useShadow,
			boolean lockRotation, float angle, Vector3 position){
		this.model = model;
		this.texture = texture;
		this.lightningEnabled = useLightning;
		this.textureEnabled = useTexture;
		this.shadowEnabled = useShadow;
		this.lockRotation = lockRotation;
		this.animations = new HashMap<String, ModelAnimation>();
		this.angle = angle;
		this.position = position;
		this.color = new Color(1, 1, 1, 1);
		this.scale = new Vector3(1, 1, 1);
		currentAnimation = new ModelAnimation(new FrameInterval(1, 1), new FrameInterval[]{},1000000, "standardAnimation", false);
	}
	private void setAnimation(String name){
		Animation newAnimation = animations.get(name);
		if(newAnimation != null){
			currentAnimation = newAnimation;
			currentAnimationName = name;
			setFrame(newAnimation.getStartFrame());
		}
	}
	public void setColor(float r, float g, float b, float a){
		this.color = new Color(r, g, b, a);
	}
	public void setScale(float x, float y, float z){
		scale = new Vector3(x, y, z);
	}
	/**
	 * Sets the current frame. Exception is cast if frame doesn't exist.
	 */
	public void setFrame(int frame){
		model.setAnimation("frame" + frame, 0.1f, true);
		currentAnimation.setCurrentFrame(frame);
	}
	public int getFrame(){
		return currentAnimation.getCurrentFrame();
	}
	/**
	 * gets all animationsNames that has a next animation with the name "name"
	 * @param name The name
	 * @return A list of animation names
	 */
	private Collection<String> getPreviousAnimations(String name){
		Collection<String> allAnimations = animations.keySet();
		Collection<String> previousAnimations = new LinkedList<String>();
		for(String animationName: allAnimations){
			if(getAnimation(animationName).getNextAnimationName().equals(name))
				previousAnimations.add(animationName);
		}
		return previousAnimations;
	}
	/**
	 * Get the name of the current animation playing
	 * @return The name of the current animation
	 */
	public String getCurrentAnimationName(){
		return currentAnimationName;
	}
	/**
	 * Get the current animation playing
	 * @return The current animation
	 */
	public Animation getCurrentAnimation(){
		return currentAnimation;
	}
	/**
	 * 
	 * @param name
	 * @return
	 */
	public Animation getAnimation(String name){
		return animations.get(name);
	}
	/**
	 * Add a new animation to this model
	 * @param name The name this animation should be associated with
	 * @param animation The animation to add to this model
	 */
	public void addAnimation(String name,ModelAnimation animation){
		animations.put(name, animation);
		animation.addAnimationListener(this);
	}
	/**
	 * Play specified animation if animation exists and can be played. 
	 * If animation cannot be played from current frame/animation
	 * it tries to find an transition animation between current animation and the specified one.
	 * Play found animation if that animation can be played from current frame/animation.
	 * Otherwise, save it for later and try this method when current animation has ended.
	 * 
	 * Entering an empty string should kill this model.
	 * @param name Name of the animation to be played
	 * @param force if true, The animation is changed no matter what.
	 * @return true if animation is changed, false otherwise.
	 */
	public boolean playAnimation(String name, boolean force){
		boolean animationChanged = false;
		if(force){
			setAnimation(name);
			animationChanged = true;
		}
		else{
			Animation newAnimation = animations.get(name);
			if(newAnimation != null){
				if(newAnimation.canStartAnimation(getFrame())){ 
					//If new animation can play from current animation
					setAnimation(name);
					animationChanged = true;
				}else{
					//check if any other animations has to be played before
					//if animation found, try to play that one
					for(String animationName: getPreviousAnimations(name)){
						if(!name.equals(animationName) && getAnimation(animationName).isTranssionAnimation()){
							animationChanged = playAnimation(animationName, force);	
							if(animationChanged){
								break;
							}
						}
					}
				}
			}//ignore if GameObjectView doesn't have that animation
		}
		if(animationChanged){
			desiredAnimationName = "none";
		}else{
			//if no better animation was found, set it as desired animation and play it later
			desiredAnimationName = name;
		}
		return animationChanged;
	}
	/**
	 * When an animation has ended, call this method to decide witch animation to play next.
	 * If no animation is found or the next equals "". kill this model
	 * @param source The animation that was ended
	 */
	@Override
	public void endAnimation() {
		//Kill if no animation is left
		if(currentAnimation.getNextAnimationName().equals("")){
			kill();
		}
		//nothing has requested an animation change or next animation is the requested one
		if(desiredAnimationName.equals("none") || desiredAnimationName.equals(currentAnimation.getNextAnimationName())){
			playAnimation(currentAnimation.getNextAnimationName(), true);
		}else{
			boolean animationChanged = playAnimation(desiredAnimationName, false);
			//if desired animation cannot be played by any means
			//(cannot find any proper animation to play before)
			//skip animation and play the next one instead
			if(!animationChanged){
				playAnimation(currentAnimation.getNextAnimationName(), true);
			}
		}
	}
	/**
	 * Update this models current animation so that it animates correctly.
	 * @param dTime
	 */
	public void update(float dTime){
		if(currentAnimation != null){
			currentAnimation.animate(dTime);
			setFrame(currentAnimation.getCurrentFrame());
		}	
	}
	/**
	 * render this model
	 */
	public void render(boolean opaque){
		if(!isDead() && opaque){
			Gdx.gl10.glColor4f(color.r, color.g, color.b, color.a);
			if(lightningEnabled){
				Gdx.gl10.glEnable(GL10.GL_LIGHTING);
			}else{
				Gdx.gl10.glDisable(GL10.GL_LIGHTING);
			}
			if(textureEnabled){
				Gdx.gl10.glEnable(GL10.GL_TEXTURE_2D);
				texture.bind();
			}else
				Gdx.gl10.glDisable(GL10.GL_TEXTURE_2D);
			setFrame(getFrame());
			Gdx.gl10.glPushMatrix();
			Gdx.gl10.glTranslatef(position.x, position.z, position.y);
			Gdx.gl10.glScalef(scale.x, scale.y, scale.z);
			if(!lockRotation){
				Gdx.gl10.glRotatef(-90-angle, 0, 1, 0);
			}
			model.render();
			Gdx.gl10.glPopMatrix();
			Gdx.gl10.glPushMatrix();
			if(shadowEnabled){
				Gdx.gl10.glTranslatef(position.x, 0, position.y);
				Gdx.gl10.glScalef(scale.x, 0, scale.z);
				Gdx.gl10.glRotatef(30,1, 0, -1);
				if(!lockRotation){
					Gdx.gl10.glRotatef(-90-angle, 0, 1, 0);
				}
				Gdx.gl10.glDisable(GL10.GL_TEXTURE_2D);
				Gdx.gl10.glDisable(GL10.GL_LIGHTING);
				Gdx.gl10.glEnable(GL10.GL_BLEND);
				Gdx.gl10.glDepthMask(false);
				Gdx.gl10.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
				Gdx.gl10.glColor4f(0, 0, 0, 0.3f);
				model.render();
				Gdx.gl10.glDepthMask(true);
			}
			Gdx.gl10.glPopMatrix();
		}
	}
	@Override
	public void kill() {
		alive = false;
	}
	@Override
	public boolean isDead() {
		return !alive;
	}
	@Override
	public void movePosition(IVector2 pos, float percent) {
		position.x = (position.x * (1 - percent) + pos.getX() * percent);
		position.y = (position.y * (1 - percent) + pos.getY() * percent);
	}
	@Override
	public void setPosition(IVector2 pos) {
		movePosition(pos, 1);	
	}
	@Override
	public void setAngle(float angle) {
		this.angle = angle;
	}

}
