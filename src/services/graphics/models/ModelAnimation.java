package services.graphics.models;


import java.util.HashSet;
import java.util.Set;

import services.graphics.core.Animation;
import services.graphics.core.AnimationListener;
import services.graphics.core.FrameInterval;

/**
 * An animation for a model. If a 3d model should animate, this class keeps track of different animations
 * @author robin
 *
 */
public class ModelAnimation implements Animation {
	/**
	 * Create a new animation
	 * @param frameInterval The frame interval of this animation.
	 * @param startFrameIntervall The frame interval this animation can play from
	 * @param animationSpeed How fast this animation plays. animationSpeed = 1 <=> 60 frames per second
	 * @param nextAnimation When this animation has ended, this animation will start to play
	 */
	public ModelAnimation(FrameInterval frameInterval,FrameInterval[] startFrameIntervals,
			float animationSpeed,String nextAnimation,boolean transsionAnimation){
		this.frameInterval = frameInterval;
		this.startFrameIntervals = startFrameIntervals;
		this.animationSpeed = animationSpeed;
		this.nextAnimation = nextAnimation;
		this.transsionAnimation = transsionAnimation;
		listeners = new HashSet<AnimationListener>();
		currentFrame = frameInterval.getStartFrame();
	}
	private final FrameInterval frameInterval;
	private final FrameInterval[] startFrameIntervals;
	private final float animationSpeed;
	private final String nextAnimation;
	private final Set<AnimationListener> listeners;
	private final boolean transsionAnimation;
	private float time;
	private int currentFrame;
	/* (non-Javadoc)
	 * @see services.graphics.Animation#addAnimationListener(services.graphics.AnimationListener)
	 */
	@Override
	public void addAnimationListener(AnimationListener listener){
		listeners.add(listener);
	}
	/* (non-Javadoc)
	 * @see services.graphics.Animation#removeAnimationListener(services.graphics.AnimationListener)
	 */
	@Override
	public void removeAnimationListener(AnimationListener listener){
		listeners.remove(listener);
	}
	/* (non-Javadoc)
	 * @see services.graphics.Animation#getNextAnimationName()
	 */
	@Override
	public String getNextAnimationName(){
		return nextAnimation;
	}
	/* (non-Javadoc)
	 * @see services.graphics.Animation#getCurrentFrame()
	 */
	@Override
	public int getCurrentFrame(){
		return currentFrame;
	}
	/* (non-Javadoc)
	 * @see services.graphics.Animation#isTranssionAnimation()
	 */
	@Override
	public boolean isTranssionAnimation(){
		return transsionAnimation;
	}
	/* (non-Javadoc)
	 * @see services.graphics.Animation#setCurrentFrame(int)
	 */
	@Override
	public void setCurrentFrame(int frame){
		currentFrame = frame;
	}
	/* (non-Javadoc)
	 * @see services.graphics.Animation#getStartFrame()
	 */
	@Override
	public int getStartFrame(){
		return frameInterval.getStartFrame();
	}
	/* (non-Javadoc)
	 * @see services.graphics.Animation#canStartAnimation(int)
	 */
	@Override
	public boolean canStartAnimation(int currentFrame){
		for(FrameInterval interval: startFrameIntervals){
			if(interval.isWithinInterval(currentFrame)){
				return true;
			}
		}
		return false;
	}
	/* (non-Javadoc)
	 * @see services.graphics.Animation#animate(float)
	 */
	@Override
	public void animate(float dTime){
		time+=dTime;
		while(time > animationSpeed){
			currentFrame++;
			time -= animationSpeed;
		}
		if(!frameInterval.isWithinInterval(currentFrame)){//end animation
			for(AnimationListener listener: listeners){
				this.currentFrame = frameInterval.getStartFrame();
				listener.endAnimation();
			}
		}
		if(frameInterval.isBeyondInterval(currentFrame)){
			currentFrame = frameInterval.getEndFrame();
		}
	}
}
