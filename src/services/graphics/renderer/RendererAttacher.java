package services.graphics.renderer;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import services.graphics.core.Attachable;
import services.graphics.renderables.IRenderable;



/**
 * An implementation of the IRenderer. Cannot render anything without attaching
 * concrete IRenderer's. Structures, updates adds and removes the proper
 * IRenderables. May attach other IRenderer's to add functionality, like
 * Renderer2D and Renderer3D.
 * 
 * @author Robin
 * 
 */
public class RendererAttacher implements IRenderer, Attachable<IRenderer> {

	private List<IRenderer> renderers;
	private List<IRenderable> renderableObjects;

	public RendererAttacher() {
		this.renderers = new CopyOnWriteArrayList<IRenderer>();
		this.renderableObjects = new CopyOnWriteArrayList<IRenderable>();
	}

	@Override
	public void update(float dTime) {
		for (IRenderer r : renderers) {
			r.update(dTime);
		}
		for (IRenderable renderable : renderableObjects) {
			renderable.update(dTime);
			if (renderable.isDead()) {
				removeRenderableObject(renderable);
			}
		}
	}

	@Override
	public void removeRenderableObject(IRenderable renderable) {
		for (IRenderer r : renderers) {
			r.removeRenderableObject(renderable);
		}
		renderableObjects.remove(renderable);
	}

	@Override
	public void addRenderableObject(IRenderable renderable) {
		for (IRenderer r : renderers) {
			r.addRenderableObject(renderable);
		}
		renderableObjects.add(renderable);
	}

	public Collection<IRenderable> getRenderableObjects() {
		return renderableObjects;
	}

	@Override
	public void clearRenderableObjects() {
		for (IRenderer r : renderers) {
			r.clearRenderableObjects();
		}
		renderableObjects.clear();
	}

	@Override
	public void attach(IRenderer object) {
		renderers.add(object);
	}

	@Override
	public void detach(IRenderer object) {
		renderers.remove(object);
	}

	@Override
	public Collection<IRenderer> get() {
		return renderers;
	}

	@Override
	public void render() {
		for (IRenderer r : renderers) {
			r.render();
		}
	}

	@Override
	public void resize(int width, int height) {
		for(IRenderer r: renderers){
			r.resize(width, height);
		}
	}

}
