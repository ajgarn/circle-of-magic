package services.graphics.renderer;

import java.util.Collection;
import java.util.concurrent.CopyOnWriteArrayList;

import services.graphics.renderables.IRenderable;
import services.graphics.renderables.IRenderable2D;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * This class is an IRenderer That renders elements in the 3d world represented as 2d. This
 * adds 2d rendering functionality to an existing RendererAttacher.
 * 
 * Used by RendererAttacher
 * @author Robin
 * 
 */
public class Renderer2D implements IRenderer{
	
	private SpriteBatch batch;
	private Collection<IRenderable2D> renderableObjects;
	
	
	/**
	 * Create a Renderer3d.
	 */
	public Renderer2D() {
		batch = new SpriteBatch();
		renderableObjects = new CopyOnWriteArrayList<IRenderable2D>();
	}

	@Override
	public void update(float dTime) {
		//batch.setProjectionMatrix(new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()).combined);
	}

	@Override
	public void render() {
		batch.begin();
		for(IRenderable2D renderable : renderableObjects){
			renderable.render(batch);
		}
		batch.end();
	}

	@Override
	public void removeRenderableObject(IRenderable renderable) {
		if(renderable instanceof IRenderable2D){
			renderableObjects.remove((IRenderable2D)renderable);
		}
	}

	@Override
	public void addRenderableObject(IRenderable renderable) {
		if(renderable instanceof IRenderable2D){
			renderableObjects.add((IRenderable2D)renderable);
		}
	}

	@Override
	public Collection<IRenderable> getRenderableObjects() {
		return new CopyOnWriteArrayList<IRenderable>(renderableObjects);
	}

	@Override
	public void clearRenderableObjects() {
		renderableObjects.clear();
	}

	@Override
	public void resize(int width, int height) {
		batch = new SpriteBatch();
	}

}
