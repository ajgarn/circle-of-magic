package services.graphics.renderer;

import java.util.Collection;
import java.util.concurrent.CopyOnWriteArrayList;

import services.graphics.renderables.IRenderable;
import services.graphics.renderables.IRenderable3D;


/**
 * This class is a IRenderer That renders elements in the 3d world. This 
 * adds 3d functionality to an existing RendererAttacher.
 * 
 * Used by RendererAttacher
 * 
 * @author Robin
 * 
 */
public class Renderer3D implements IRenderer {
	
	private Collection<IRenderable3D> renderableObjects;
	
	
	/**
	 * Create a Renderer3d.
	 */
	public Renderer3D() {
		renderableObjects = new CopyOnWriteArrayList<IRenderable3D>();
	}

	@Override
	public void update(float dTime) {
		
	}

	@Override
	public void render() {
		for(IRenderable3D renderable : renderableObjects){
			renderable.render(true);
		}
		for(IRenderable3D renderable : renderableObjects){
			renderable.render(false);
		}
	}

	@Override
	public void removeRenderableObject(IRenderable renderable) {
		if(renderable instanceof IRenderable3D){
			renderableObjects.remove((IRenderable3D)renderable);
		}
	}

	@Override
	public void addRenderableObject(IRenderable renderable) {
		if(renderable instanceof IRenderable3D){
			renderableObjects.add((IRenderable3D)renderable);
		}
	}

	@Override
	public Collection<IRenderable> getRenderableObjects() {
		return new CopyOnWriteArrayList<IRenderable>(renderableObjects);
	}

	@Override
	public void clearRenderableObjects() {
		renderableObjects.clear();
	}

	@Override
	public void resize(int width, int height) {}

}
