package services.graphics.renderer;

import java.util.Collection;

import services.graphics.renderables.IRenderable;


/**
 * A simple class that holds renderable objects that can be rendered.
 * IRenderer uses the decorator pattern.
 * @author Robin
 *
 */
public interface IRenderer {
	/**
	 * Updates all the gameObjectViews. If a new world was sent from the network since
	 * the last update, refresh the world
	 * @param dTime
	 */
	public void update(float dTime);
	/**
	 * Renders all the IRenderableObjects this IRenderer has
	 */
	public void render();
	/**
	 * Called when the Application Frame i resized.
	 * @param width The width of the frame, in pixels
	 * @param height The height of the frame, in pixels
	 */
	public void resize(int width, int height);
	/**
	 * Removes the IRenderableObject
	 * @param object
	 */
	public void removeRenderableObject(IRenderable renderable);
	/**
	 * Add a specific IRenderableObject to this IRenderer
	 * @param renderable The IRenderableObject
	 */
	public void addRenderableObject(IRenderable renderable);
	/**
	 * Gets all the IRenderableObjects that this IRenderer has
	 * @return
	 */
	public Collection<IRenderable> getRenderableObjects();
	/**
	 * Removes all the IRenderable objects from this IRenderer
	 */
	public void clearRenderableObjects();
}
