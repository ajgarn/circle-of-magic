package server.network;

import java.net.URLEncoder;

import model.Account;
import services.logger.Log;
import services.net.URLReader;
import services.security.Base64AESCrypter;
import services.security.ICrypter;
import services.util.RandomStringUtils;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.SerializationException;
import common.application.WebServices;

/**
 * Authenticates the user using a web service.
 * @author Anton Jansson
 *
 */
public class WebAuthenticator implements IAuthenticator {
	
	private static final String DATA = "username=%s&password=%s";
	
	private final Json json;
	private final ICrypter crypter;
	
	public WebAuthenticator() {
		json = new Json();
		crypter = new Base64AESCrypter(WebServices.Security.SECRET_KEY,
				WebServices.Security.IV);
	}
	
	@Override
	public Account authenticate(String username, String password) {
		String response = null;
		
		try {
			String controlKey = RandomStringUtils.randomString(WebServices.Security.CONTROL_KEY_LENGTH);
			
			// Format query string
			String queryString = controlKey + String.format(DATA, username, password);
			
			String data = crypter.encrypt(queryString);
			data = URLEncoder.encode(data, "UTF-8");
			
			// Send web service request
			response = URLReader.readUrl(WebServices.AUTHENTICATION + data);
			
			response = crypter.decrypt(response);
			
			// Check control key
			String responseControlKey = response.substring(0,
					WebServices.Security.CONTROL_KEY_LENGTH);
			if (responseControlKey.equals(controlKey)) {
				response = response
						.substring(WebServices.Security.CONTROL_KEY_LENGTH,
								response.length());

				// Parse response as WordPress user
				WPUser user = json.fromJson(WPUser.class, response);
				
				return new Account(user.ID, user.display_name);
			} else {
				Log.warning("Control key from web response did not match the original."
						+ "(origninal=" + controlKey
						+ ", response=" + responseControlKey + ")");
			}
		} catch (SerializationException e) {
			// Login failed due to incorrect input data.
			Log.info("Login failure: " + response + " (username=" + username + ")");
		} catch (Exception e) {
			Log.fatal("Could not reach web service for authentication", e);
		}
		
		return null;
	}
	
	/**
	 * User account from Wordpress site.
	 * @author Anton Jansson
	 *
	 */
	private static class WPUser {
		private int ID;
		private String display_name;
	}

}
