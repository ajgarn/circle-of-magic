package server.network;

import model.Account;
import services.logger.Log;
import services.network.DisconnectRequest;

/**
 * Service for handling user authentication.
 * @author Anton Jansson
 *
 */
public class AuthenticationService implements IAuthenticationService {

	private static IAuthenticationService instance;
	
	private final AccountConnectionsStore accounts;
	private final IAuthenticator authenticator;
	private final ServerNetReceiver netHandler;
	
	private boolean allowLANAccounts = true;
	
	private AuthenticationService() {
		accounts = AccountConnectionsStore.getInstance();
		authenticator = new WebAuthenticator();
		netHandler = ServerNetReceiver.getInstance();
		netHandler.addListener(DisconnectRequest.class, disconnectionListener);
	}
	
	public static synchronized IAuthenticationService getInstance() {
		if (instance == null) {
			instance = new AuthenticationService();
		}
		return instance;
	}
	
	@Override
	public Account addAccount(Account account, int connectionId) {
		if ((!account.isLANAccount() || allowLANAccounts) &&
				accounts.getAccountID(account) < 0) {		// Account does not exist.
			accounts.add(account, connectionId);
			Log.info(account + " added to authentication service.");
			return account;
		}
		Log.debug(account + " not added to authentication service " +
				"(account already exists or LAN account not allowed).");
		return null;
	}
	
	@Override
	public Account login(String username, String password, int connectionId) {
		Account account = authenticator.authenticate(username, password);
		if (account != null) {
			logoutOldClient(account);
			accounts.add(account, connectionId);
			Log.info(account + " has logged in.");
		} else {
			Log.info("Login failed. Username: " + username);
		}
		return account;
	}

	@Override
	public void logout(Account account) {
		accounts.remove(account);
		Log.info(account + " has logged out.");
	}

	/**
	 * Disconnects old client authenticated with the specified account.
	 * @param account
	 */
	private void logoutOldClient(Account account) {
		int oldID = accounts.getAccountID(account);
		if (oldID >= 0) {
			accounts.remove(account);
			netHandler.send(oldID, new DisconnectRequest());
			Log.debug("Client (" + oldID + ") has been disconnected due to " +
					"authentication from other client for the same account.");
		}
	}
	
	/**
	 * Sets if this authentication service allows LAN accounts for clients.
	 * Default is <code>true></code>
	 * @param allowLANAccounts If LAN accounts is allowed or not.
	 */
	public void setAllowLANAccounts(boolean allowLANAccounts) {
		this.allowLANAccounts = allowLANAccounts;
	}

	/**
	 * Controls when a client has disconnected from the server.
	 */
	private IServerNetworkListener<DisconnectRequest> disconnectionListener =
			new IServerNetworkListener<DisconnectRequest>() {
		@Override
		public void networkReceived(Account account, DisconnectRequest object,
				long timeSent) {
			Log.debug(account + " has disconnected.");
			logout(account);
		}
	};

}
