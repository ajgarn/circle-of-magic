package server.network;

import model.core.Constants;

/**
 * Service for controlling application version and updates.
 * @author Anton Jansson
 *
 */
public class VersionControlService implements IVersionControlService {

	@Override
	public boolean requiresUpdate(float version) {
		return Constants.CLIENT_VERSION > version;
	}
}
