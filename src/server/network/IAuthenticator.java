package server.network;

import model.Account;

/**
 * Controls the user authentication for the game. This can be implemented to
 * use various authentication techniques.
 * 
 * @author Anton Jansson
 *
 */
interface IAuthenticator {
	/**
	 * Tries to authenticate the user with the specified username and password.
	 * Returns true if the user is authenticated, false otherwise.
	 * @param username The user's username.
	 * @param password The user's password.
	 * @return An account if the user is authenticated, <code>null</code> if not.
	 */
	Account authenticate(String username, String password);
}