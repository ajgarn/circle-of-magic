package server.network;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Account;


import server.database.DBAccess;
import services.logger.Log;
import services.security.StaticEncryption;

/**
 * Authenticates the user using direct access to an external database.
 * @author Anton Jansson
 *
 */
class DBAuthenticator implements IAuthenticator {
	
	private static final String SQL_USER = "SELECT * FROM Users WHERE user_login = ? AND user_pass = ?";
	
	@Override
	public Account authenticate(String username, String password) {
		Connection connection = DBAccess.getConnection();
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(SQL_USER);
			statement.setString(1, username);
			statement.setString(2, encryptPassword(password));
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				int id = resultSet.getInt("ID");
				String name = resultSet.getString("display_name");
				return new Account(id, name);
			}
		} catch (NullPointerException e) {
		} catch (SQLException e) {
			Log.error("Database request error.", e);
		} finally {
			try {
				statement.close();
				connection.close();
			} catch (NullPointerException e) {
			} catch (SQLException e) {}
		}
		return null;
	}
	
	private static String encryptPassword(String password) {
		try {
			return StaticEncryption.sha1(password);
		} catch (NoSuchAlgorithmException e) {
			Log.fatal("SHA1 algorithm not found while encrypting password in "
					+ DBAuthenticator.class.getSimpleName(), e);
			return null;
		}
	}
}
