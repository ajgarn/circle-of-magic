package server.network;

import model.Account;

/**
 * Listener for objects received from the network on the server side. 
 * The type that this will listen for is specified by the generic params.
 * @author Anton Jansson
 *
 * @param <T> The object type to listen for.
 */
public interface IServerNetworkListener<T> {
	/**
	 * An object of type {@link T} is received on this server.
	 * @param account The player this object is sent from.
	 * @param object The object received.
	 * @param timeSent The time the object was sent.
	 */
	void networkReceived(Account account, T object, long timeSent);
}
