package server.network;

import services.network.INetworkObject;
import model.Account;

/**
 * Interface for service handling user authentication.
 * @author Anton Jansson
 *
 */
public interface IAuthenticationService extends INetworkObject {
	/**
	 * Add a LAN account to map it with its connection. This does not require authentication.
	 * A LAN account can not connect to an online game. If the user already exists on this
	 * server, this method returns <code>null</code>.
	 * @param account The account to add.
	 * @param connectionId The ID of the network connection.
	 * @return The created account or <code>null</code> if the account already exists.
	 */
	Account addAccount(Account account, int connectionId);
	/**
	 * Try to authenticate the user with the specified data. If a user is
	 * authenticated the client is mapped with this user account.
	 * @param username The username of the account.
	 * @param password The password of the account.
	 * @param connectionId The ID of the client in the network connection.
	 * @return The authenticated account or <code>null</code> if authentication fails.
	 */
	Account login(String username, String password, int connectionId);
	/**
	 * Log out the specified account (with a connected client) from this server.
	 * @param account The account to log out.
	 */
	void logout(Account account);
}