package server.network;

/**
 * Interface for service which controls application version and updates.
 * @author Anton Jansson
 *
 */
public interface IVersionControlService {

	/**
	 * Returns whether the client must be updated or not.
	 * @param version The current client version.
	 * @return If the client must be updated or not.
	 */
	public abstract boolean requiresUpdate(float version);

}