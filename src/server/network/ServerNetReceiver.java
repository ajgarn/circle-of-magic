package server.network;

import java.net.InetAddress;
import java.util.Collection;

import common.network.NetworkRegistration;

import model.Account;

import services.logger.Log;
import services.network.AbstractClassNetReceiver;
import services.network.DisconnectRequest;
import services.network.IServerNetHandler;
import services.network.ServerNetHandler;


/**
 * Network receivable customized for the server side of Circle of Magic. This is
 * translating network connection IDs passed from network library to player
 * objects located in {@link PlayerConnectionsStore}, making
 * {@link IServerNetworkListener} to register as listeners.
 * 
 * @author Anton Jansson
 * 
 * @see AbstractClassNetReceiver
 * @see IServerNetworkListener
 */
public class ServerNetReceiver extends
		AbstractClassNetReceiver<IServerNetworkListener<?>> implements IServerNetHandler {
	
	private static ServerNetReceiver instance;
	
	private final AccountConnectionsStore accounts;
	private final IServerNetHandler handler;

	private ServerNetReceiver(IServerNetHandler handler) {
		super(handler);
		this.handler = handler;
		accounts = AccountConnectionsStore.getInstance();
	}
	
	@Override
	public InetAddress getAddress() {
		return handler.getAddress();
	}
	
	@Override
	protected <T> void networkReceived(IServerNetworkListener<?> listener,
			int connectionID, T object, long time) {
		// Never used, because overriding sendToListeners.
	}
	
	@SuppressWarnings("unchecked")
	private <T> void networkReceived(IServerNetworkListener<?> listener,
			Account account, T object, long time) {
		((IServerNetworkListener<T>) listener).networkReceived(
				account, object, time);
	}

	@Override
	public <T> void send(Collection<Integer> connectionIDs, T object) {
		handler.send(connectionIDs, object);
	}

	@Override
	public <T> void send(int connectionID, T object) {
		handler.send(connectionID, object);
	}
	
	public <T> void sendToAccount(Account account, T object) {
		handler.send(accounts.getAccountID(account), object);
	}

	public <T> void sendToAccounts(Collection<Account> accounts, T object) {
		handler.send(this.accounts.getAccountIDs(accounts), object);
	}
	
	@Override
	protected void sendToListeners(
			Collection<IServerNetworkListener<?>> listeners, int connectionID,
			Object object, long time) {
		Account account = accounts.getAccount(connectionID);
		if (account != null) {
			// Pass object to all listeners.
			for (IServerNetworkListener<?> listener : listeners) {
				networkReceived(listener, account, object, time);
			}
		} else {
			// TODO: Ask client to authenticate user.
			send(connectionID, new DisconnectRequest());
			Log.warning("[" + getClass().getSimpleName()
					+ "] Object received from logged out account: " + object);
			return;
		}
	}

	@Override
	public void start() {
		handler.start();
	}
	
	@Override
	public void stop() {
		handler.stop();
	}

	public static synchronized ServerNetReceiver getInstance() {
		if (instance == null) {
			instance = new ServerNetReceiver(new ServerNetHandler(
					NetworkRegistration.getNetworkSettings()));
		}
		return instance;
	}
}
