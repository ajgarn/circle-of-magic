package server.network;

import java.util.ArrayList;
import java.util.Collection;

import model.Account;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

/**
 * Stores the accounts for each network connection, to retrieve
 * accounts by connection ID or retrieve connection ID by account.
 * Used by network holder receivers and CoMServer.
 * @author Anton Jansson
 */
class AccountConnectionsStore {

	private static AccountConnectionsStore instance;
	// The Integer represents the ID of the network connection
	private final BiMap<Account, Integer> accounts = HashBiMap.create();

	private AccountConnectionsStore() {}
	
	public static synchronized AccountConnectionsStore getInstance() {
		if (instance == null) {
			instance = new AccountConnectionsStore();
		}
		return instance;
	}
	
	/**
	 * Returns a account with the specified ID.
	 * @param connectionID The ID of the account.
	 * @return The account with the ID or <code>null</code> if no account
	 * is found.
	 */
	public synchronized Account getAccount(int connectionID) {
		return accounts.inverse().get(connectionID);
	}

	/**
	 * Returns the connection ID for a account.
	 * @param account The account to get the id of.
	 * @return The id of the account or -1 if no match was found.
	 */
	public synchronized int getAccountID(Account account) {
		Integer id = accounts.get(account);
		return (id != null) ? id : -1;
	}
	
	/**
	 * Returns all account IDs in this game.
	 * @return A collection of the account IDs.
	 */
	public synchronized Collection<Integer> getAccountIDs(Collection<Account> accounts) {
		Collection<Integer> ids = new ArrayList<Integer>();
		for (Account p : accounts) {
			Integer id = this.accounts.get(p);
			if (id != null) {
				ids.add(id);
			}
		}
		return ids;
	}

	/**
	 * Adds a account to this store.
	 * @param account The account to add.
	 * @param connectionId The ID of the account.
	 */
	public synchronized void add(Account account, int connectionId) {
		if (account != null) {
			accounts.put(account, connectionId);
		}
	}

	/**
	 * Remove a account from this store.
	 * @param account The account to remove.
	 */
	public synchronized void remove(Account account) {
		if (account != null) {
			accounts.remove(account);
		}
	}
}
