package server.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import services.logger.Log;

/**
 * Controls the connection to the Circle of Magic database.
 * The database connection configuration is handled internally.
 * @author Anton Jansson
 * 
 */
public class DBAccess {
	
	// Database configuration
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB_URL = 
			"jdbc:mysql://mysql.yellowend.com/ajgarn_circleofmagic";
	
	// Database credentials
	private static final String USER = "ajgarn_sheepstud";
	private static final String PASS = "hu7UCh=7";

	/**
	 * Creates a DBAccess which controls the database connection.
	 * @param jdbcDriver the fully qualified name of the desired JDBC driver class.
	 * @param dbUrl a database url of the form jdbc:subprotocol:subname
	 * @param username the database user on whose behalf the connection is being made
	 * @param password the user's password
	 * @throws ClassNotFoundException if the JDBC driver cannot be found.
	 * @throws SQLException if a database access error occurs.
	 */
	private static Connection setupConnection(String jdbcDriver, String dbUrl,
			String username, String password) throws ClassNotFoundException,
			SQLException {
		Class.forName(jdbcDriver); // Load JDBC driver.
		return DriverManager.getConnection(dbUrl, username, password);
	}

	/**
	 * Creates and returns a new open database connection.
	 * @return The database connection or <code>null</code> if the 
	 * connection can not be created.
	 */
	public static Connection getConnection() {
		try {
			return setupConnection(JDBC_DRIVER, DB_URL, USER, PASS);
		} catch (ClassNotFoundException e) {
			Log.fatal("JDBC driver is not found.", e);
		} catch (SQLException e) {
			Log.fatal("Could not access database.", e);
		}
		return null;
	}

}