package server.model;

import java.util.Collection;

import model.AbstractGameSettings;
import model.Account;
import model.Lobby;

/**
 * Interface for the Circle of Magic server. 
 * @author Anton Jansson
 */
public interface ICoMServer {

	/**
	 * Creates a new Lobby on the server.
	 * @param title The title of the game.
	 * @param owner The owner of the lobby, who can make changes.
	 * @param settings The settings for the game.
	 * @return The lobby created.
	 */
	Lobby createLobby(String title, Account owner, AbstractGameSettings settings);

	/**
	 * Get all available GameSettings. Used when creating a lobby.
	 * @return A collection of {@link AbstractGameSettings}
	 */
	Collection<AbstractGameSettings> getGameSettings();

	/**
	 * Starts the loading of a game if all players are ready.
	 * If all players not are ready this method returns <code>false</code>.
	 * Notifies all connected clients.
	 * @param lobby The lobby to start loading.
	 * @return Whether the game has started loading or not.
	 */
	boolean loadGame(Lobby lobby);

	/**
	 * Starts a running server that clients can connect to.
	 */
	void start();

	/**
	 * Stops the server.
	 * All games are paused and all server connections are disconnected.
	 */
	void stop();
	
	/**
	 * Remove the specified lobby from the server.
	 * @param lobby The lobby to remove.
	 */
	void removeLobby(Lobby lobby);
}