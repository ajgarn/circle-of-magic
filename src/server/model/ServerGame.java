package server.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import services.math.IVector2;

import model.AbstractGame;
import model.AbstractGameSettings;
import model.Character;
import model.GameHeader;
import model.GameObjectFactory;
import model.ICharacter;
import model.IMage;
import model.Player;
import model.Player.Status;
import model.World;
import model.core.GameObject;
import model.core.GameObjectListener;
import model.core.IAction;
import model.core.ICollidable;
import model.core.IGameObject;
import model.core.IGameObjectCreatorAction;
import model.core.SpellEffect;
import model.core.State;
import model.core.Vector2;
import model.spells.CastSpellAction;

/**
 * A server game representation, extension of {@link AbstractGame}.
 * Maps all players within a game with its own character.
 * Used on the server.
 * @author Anton Jansson
 */
public class ServerGame extends AbstractGame {
	
	/**
	 * Stores all GameObjects that have changed and must be sent to all clients. 
	 * <p><i>GameObject ID</i> as key and <i>GameObject</i> as value.
	 */
	private final Map<Integer, GameObject> changedGameObjects;
	private final List<ISendWorldListener> sendWorldListeners;


	/**
	 * Creates a server game.
	 * @param header The header information for this game.
	 * @param settings The settings for the game.
	 * @throws IllegalArgumentException If the game cannot be created due to
	 * specific game settings.
	 */
	public ServerGame(GameHeader header, AbstractGameSettings settings)
			throws IllegalArgumentException {
		super(header);
		if (settings == null) {
			throw new IllegalArgumentException("Settings cannot be null.");
		} else if (!settings.canCreateGame(header)) {
			throw new IllegalArgumentException(settings.getMessage());
		}
		
		this.setSettings(settings);
		settings.setupGame(this);
		changedGameObjects = new HashMap<Integer, GameObject>();
		sendWorldListeners = new ArrayList<ISendWorldListener>();
	}
	
	/**
	 * Add an action to a specific player character.
	 * @param action The action to add.
	 * @param player The ID of the player.
	 */
	public void addAction(IAction action, Player player) {
		GameObject object = (GameObject) getWorld().getGameObject(player.getCharacterID());
		if (object != null) {
			object.addAction(action);
			addChangedGameObject(player.getCharacterID(), object);
			if (action instanceof CastSpellAction && object instanceof IMage) {
				CastSpellAction cast = (CastSpellAction) action;
				cast.updateSpellSlot(player.getSpellSlots());
			}
		}
	}
	
	/**
	 * Add GameObject that has been changed since last client update.
	 * Next model update this GameObject is sent to the clients.
	 * @param gameObjectID ID of the object in the world.
	 * @param gameObject The GameObject that has changed.
	 */
	private void addChangedGameObject(int gameObjectID, GameObject gameObject) {
		changedGameObjects.put(gameObjectID, gameObject);
	}
	
	/**
	 * Adds a listener for when the world has to be sent.
	 * @param listener
	 */
	public void addSendWorldListener(ISendWorldListener listener) {
		sendWorldListeners.add(listener);
	}
	
	/**
	 * Generate a character for this player.
	 * @param player The player object.
	 */
	public void createCharacter(Player player,IVector2 position) {
		if (getWorld() != null) {
			Character character = GameObjectFactory.createMage(player.getColorName(),position);
			character.addListener(gameStatisticsListener);
			int characterID = getWorld().addGameObject(character);
			player.setCharacterID(characterID);
			addChangedGameObject(characterID, character);
		}
	}

	/**
	 * Find all IGameObjectCreatorActions and creates GameObjects if needed.
	 * @pre getWorld() != null
	 */
	private void createGameObjects() {
		for (IGameObject gameObject : getWorld().getGameObjects()) {
			if (!gameObject.getActions().isEmpty()) {
				IAction action = gameObject.getActions().get(0);
				if (action instanceof IGameObjectCreatorAction) {
					List<? extends GameObject> objects = 
							((IGameObjectCreatorAction) action).pullGameObjects();
					for (GameObject object : objects){
						int index = getWorld().addGameObject(object);
						addChangedGameObject(index, object);
					}
				}
			}
		}
	}
	
	/**
	 * Remove a player from this game.
	 * @param player The player object.
	 */
	public void disconnectPlayer(Player player) {
		player.setStatus(Status.DISCONNECTED);
		if (getHeader().allPlayersHaveStatus(Status.DISCONNECTED)) {
			endGame();
		}
		/*GameObject character = getWorld().getGameObject(player.getCharacterID());
		if (character != null) {
			character.kill();
			addChangedGameObject(player.getCharacterID(), character);
		}
		getPlayers().remove(player);*/
	}

	/**
	 * End this game.
	 */
	private void endGame() {
		setState(GameState.ENDED);
		sendWorldToAll();
		for (Player p : getPlayers()) {
			p.reset();
		}
	}
	
	private Player getPlayer(IGameObject o){
		return getPlayer(getWorld().getGameObjectIndex(o));
	}
	
	/**
	 * Find all collisions and call the collision methods if any found.
	 * @pre getWorld() != null
	 */
	private void handleCollisions() {
		Collection<ICollidable> collidables = new ArrayList<ICollidable>();
		for (IGameObject go : getWorld().getGameObjects()) {
			if (go instanceof ICollidable) {
				collidables.add((ICollidable) go);
			}
		}
		
		int i = 0;
		for (ICollidable o1 : collidables) {
			int j = 0;
			for (ICollidable o2 : collidables) {
				if (i == j) {
					break;
				}
				if (o1.isColliding(o2) && o2.isColliding(o1)) {
					o1.collision(o2);
					o2.collision(o1);
					addChangedGameObject(getWorld().getGameObjectIndex((IGameObject) o1),
							(GameObject) o1);
					addChangedGameObject(getWorld().getGameObjectIndex((IGameObject) o2),
							(GameObject) o2);
				}
				j++;
			}
			i++;
		}
		
		// Stopping character from moving outside of the world.
		for (IGameObject o : getWorld().getGameObjects()){
			if(o instanceof ICharacter && o.getPosition().len() > getWorld().getRadius()){
				IVector2 v = o.getPosition().cpy();
				v.nor();
				IVector2 sub = v.cpy().mul(0.1f);
				v.mul(getWorld().getRadius());
				v.sub(sub);
				((GameObject)o).setPosition(v);
				addChangedGameObject(getWorld().getGameObjectIndex(o), (GameObject)o);
			}
		}
	}

	/**
	 * End round or game if supposed to, according to GameSettings.
	 * @see AbstractGameSettings
	 */
	private void handleRoundsUpdate(float dTime) {
		if (getSettings().shouldEndGame()) {
			endGame();
		} else if (getSettings().shouldEndRound(this)) {
			switch (getState()) {
			case BUYING:
				startPlayingState();
				break;
			case PLAYING:
				startBuyingState();
				break;
			case ENDED:
				break;
			}
			
		}
	}
	
	/**
	 * Resets the world, creates all mages and maps them to the players.
	 */
	private void newRound() {
		World world = new World();
		setWorld(world);
		int i = 0;
		for (Player player : getPlayers()) {
			player.resetAfterRound();
			float angle = (float)i++ / getPlayers().size() * (float)Math.PI * 2;
			createCharacter(player,new Vector2((float)Math.cos(angle), 
					(float)Math.sin(angle)).mul(world.getRadius() - 5));
		}
		
		changedGameObjects.clear();
		sendWorldToAll();
	}

	private void respawnDeadPlayers() {
		for (Player p : getPlayers()) {
			IGameObject oldCharacter = getWorld().getGameObject(p.getCharacterID());
			if (oldCharacter != null && oldCharacter.isDead()) {
				createCharacter(p, oldCharacter.getPosition());
				sendWorldToAll();
			}
		}
	}
	
	/**
	 * Notifies the {@link ISendWorldListener}s to send the updated GameObjects.
	 */
	private void sendChangedObjects() {
		if (!changedGameObjects.isEmpty()) {
			for (ISendWorldListener listener : sendWorldListeners) {
				listener.sendChangedGameObjects(this, changedGameObjects);
			}
			changedGameObjects.clear();
		}
	}
	
	/**
	 * Notifies the {@link ISendWorldListener}s to send the GameSettings.
	 */
	private void sendGameSettings() {
		for (ISendWorldListener listener : sendWorldListeners) {
			listener.sendPlayers(this);
		}
	}
	
	/**
	 * Notifies the {@link ISendWorldListener}s to send the world to all players.
	 */
	private void sendWorldToAll() {
		for (ISendWorldListener listener : sendWorldListeners) {
			listener.sendWorld(this);
		}
	}
	
	/**
	 * Starts the game, to make it run the update loop.
	 */
	public void start() {
		newRound();
	}
	
	private void startBuyingState() {
		getSettings().startBuyingRound(this);
		if (!getSettings().shouldEndGame()) {
			setState(GameState.BUYING);
			for (Player player : getPlayers()) {
				player.setStatus(Status.WAITING);
			}
			newRound();
		}
	}
	
	private void startPlayingState() {
		getSettings().startPlayingRound(this);
		setState(GameState.PLAYING);
		newRound();
	}
	
	@Override
	public void update(float dTime) {
		if (getWorld() == null) {
			return;
		}
		for(Player p: getPlayers()){
			p.update(dTime);
		}
		createGameObjects();
		super.update(dTime);
		handleCollisions();
		
		if (getState() == GameState.BUYING) {		// Players respawn during buying time.
			respawnDeadPlayers();
		}
		
		handleRoundsUpdate(dTime);
		
		// Send world if something has been changed
		sendChangedObjects();
	}
	
	private GameObjectListener gameStatisticsListener = new GameObjectListener() {
		@Override
		public void effectAdded(IGameObject source,SpellEffect effect) {
		}
	
		@Override
		public void effectRemoved(IGameObject source,SpellEffect effect) {
		}
	
		@Override
		public void actionAdded(IGameObject source,IAction action) {
		}
	
		@Override
		public void actionRemoved(IGameObject source,IAction action) {
		}
	
		@Override
		public void died(IGameObject source) {	}
	
		@Override
		public void stateChanged(IGameObject source,State oldState, State newState) {
		}
	
		@Override
		public void killed(IGameObject killer, IGameObject killed) {
			Player killerPlayer = getPlayer(killer);
			Player killedPlayer = getPlayer(killed);
			if (killerPlayer != null) {
				killerPlayer.getStatistics().addKillingBlows();
			}
			if (killedPlayer != null) {
				killedPlayer.getStatistics().addDeath();
			}
			sendGameSettings();
		}
	
		@Override
		public void damaged(IGameObject damager, IGameObject damaged, int damage) {
			Player p = getPlayer(damager);
			if (p != null) {
				p.getStatistics().addDamageDone(damage);
			}
			sendGameSettings();
		}
	};
}
