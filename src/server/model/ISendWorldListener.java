package server.model;

import java.util.Map;

import model.core.IGameObject;

/**
 * Listener to send the world to all clients.
 * Used between CoMServer and ServerGame.
 * @author Anton Jansson
 */
interface ISendWorldListener {
	/**
	 * Send the changed GameObjects to all clients when the listener is called.
	 */
	void sendChangedGameObjects(ServerGame game, Map<Integer, ? extends IGameObject> gameObjects);
	/**
	 * Send the statistics to all clients when the listener is called.
	 */
	void sendPlayers(ServerGame game);
	/**
	 * Send the world to all clients when the listener is called.
	 */
	void sendWorld(ServerGame game);
}
