package server.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import model.AbstractGame;
import model.AbstractGame.GameState;
import model.AbstractGameSettings;
import model.Account;
import model.GameHeader;
import model.Lobby;
import model.Player;
import model.Player.Status;
import model.core.Constants;
import model.core.IAction;
import model.core.IGameObject;
import model.gamesettings.NeverEndingGameSettings;
import server.network.AuthenticationService;
import server.network.IServerNetworkListener;
import server.network.ServerNetReceiver;
import server.network.VersionControlService;
import services.file.ClassEnumerator;
import services.logger.Log;
import services.network.DisconnectRequest;
import services.network.ServerNetHandler;
import services.util.ArrayListBiMultimap;

import common.network.holders.GameHeadersHolder;
import common.network.holders.GameObjectsHolder;
import common.network.holders.JoinLobbyHolder;
import common.network.holders.PlayersHolder;
import common.network.holders.WorldHolder;

/**
 * Server for Circle of Magic. Holds games and lobbies with its connected players
 * and updates all its games in a separate thread. 
 * Has a {@link ServerNetHandler} and handles events sent by clients.
 * @author Anton Jansson
 */
public class CoMServer implements ISendWorldListener, ICoMServer {

	private static CoMServer instance;
	
	private final List<ServerGame> games;
	private final List<Lobby> lobbies;
	private final ServerNetReceiver netHandler;
	private Thread updateThread;
	/**
	 * Mapping each account with a connected game.
	 * To faster find the relevant game and accounts for the game.
	 */
	private final ArrayListBiMultimap<ServerGame, Account> gameAccounts;
	/**
	 * Mapping each account with a connected lobby.
	 * To faster find the relevant lobby and accounts for the lobby.
	 */
	private final ArrayListBiMultimap<Lobby, Account> lobbyAccounts;
	/** The number of update loops this server does per second */
	private int ups;
	
	private CoMServer() {
		games = new CopyOnWriteArrayList<ServerGame>();
		lobbies = new CopyOnWriteArrayList<Lobby>();
		//gameAccounts = new ConcurrentHashMap<Account, ServerGame>();
		//lobbyAccounts = new ConcurrentHashMap<Account, Lobby>();
		gameAccounts = new ArrayListBiMultimap<ServerGame, Account>();
		lobbyAccounts = new ArrayListBiMultimap<Lobby, Account>();
		netHandler = ServerNetReceiver.getInstance();
		
		netHandler.registerRemoteObject(Constants.IDs.AUTHENTICATION_SERVICE, 
				AuthenticationService.getInstance());
		netHandler.registerRemoteObject(Constants.IDs.GAMES_HOLDER,
				new GameHeadersHolder(games, lobbies));
		netHandler.registerRemoteObject(Constants.IDs.VERSION_CONTROL_SERVICE, 
				new VersionControlService());
		
		netHandler.addListener(IAction.class, new ActionListener());
		netHandler.addListener(DisconnectRequest.class, new DisconnectedListener());
		netHandler.addListener(JoinLobbyHolder.class, new JoinLobbyListener());
		netHandler.addListener(Lobby.class, new LobbyListener());
		netHandler.addListener(Player.class, new PlayerListener());
		netHandler.addListener(Player.Status.class, new PlayerStatusListener());
	}

	private void actionReceived(IAction action, Account account) {
		Log.debug("Action received (" + action.getClass().getSimpleName() 
				+ ") for player (" + account + ")");
		ServerGame game = gameAccounts.getKey(account);
		if (game != null) {
			game.addAction(action, account.getPlayer());
		}
	}
	
	/**
	 * Creates a new game on the server and removes the lobby from the server.
	 * Sends the world to all connected players.
	 * @param lobby The lobby holding the game details.
	 */
	public void createGame(Lobby lobby) {
		if (lobby == null) {
			return;
		}
		ServerGame game = new ServerGame(lobby.getHeader(), 
				lobby.getGameSettings());
		game.addSendWorldListener(this);
		lobbies.remove(lobby);
		games.add(game);
		Collection<Account> accounts = lobbyAccounts.removeAll(lobby);
		gameAccounts.putAll(game, accounts);
		game.start();
		Log.info("Created game: " + game);
	}
	
	@Override
	public Lobby createLobby(String title, Account owner, AbstractGameSettings settings) {
		if (title == null) {
			throw new IllegalArgumentException("Title cannot be null.");
		}
		if (settings == null) {
			throw new IllegalArgumentException("Settings cannot be null.");
		}
		if (owner != null) {
			owner.getPlayer().setStatus(Status.WAITING);
		}
		Lobby lobby = new Lobby(new GameHeader(getUnusedGameID(), title, 
				netHandler.getAddress()), owner, settings);
		lobbies.add(lobby);
		Log.info("Created lobby: " + lobby);
		return lobby;
	}
	
	/**
	 * Get a Game by its ID.
	 * @param id The game ID.
	 * @return The game or <code>null</code> if no game with the ID exists.
	 */
	public ServerGame getGame(int id) {
		for (ServerGame game : games) {
			if (id == game.getHeader().getID()) {
				return game;
			}
		}
		return null;
	}
	
	/**
	 * Get all games on this server.
	 * @return A collection of AbstractGame objects.
	 */
	public Collection<? extends AbstractGame> getGames() {
		return games;
	}
	
	@Override
	public Collection<AbstractGameSettings> getGameSettings() {
		Collection<AbstractGameSettings> gamesettings = 
				new ArrayList<AbstractGameSettings>();
		Collection<Class<?>> classes = ClassEnumerator.getClassesForPackage(
				NeverEndingGameSettings.class.getPackage());
		
		for (Class<?> c : classes) {
			if (AbstractGameSettings.class.isAssignableFrom(c)) {
				try {
					gamesettings.add((AbstractGameSettings) c.newInstance());
				} catch (InstantiationException e) {
					Log.error("GameSettings class cannot be instantiated: " 
							+ c + " (" + e.getMessage() + ")");
				} catch (IllegalAccessException e) {
					Log.error("GameSettings class cannot be instantiated: " 
							+ c + " (" + e.getMessage() + ")");
				}
			}
		}
		return gamesettings;
	}
	
	public static synchronized CoMServer getInstance() {
		if (instance == null) {
			instance = new CoMServer();
		}
		return instance;
	}
	
	/**
	 * Get all lobbies for this server.
	 * @return A collection of Lobby objects.
	 */
	public Collection<Lobby> getLobbies() {
		return lobbies;
	}
	
	/**
	 * Get a Lobby by its ID.
	 * @param id The GameHeader ID.
	 * @return The lobby or <code>null</code> if no lobby with the ID exists.
	 */
	private Lobby getLobby(int id) {
		for (Lobby lobby : lobbies) {
			if (id == lobby.getHeader().getID()) {
				return lobby;
			}
		}
		return null;
	}
	
	/**
	 * Returns an unused game ID.
	 * @return A game ID for a new game.
	 */
	private int getUnusedGameID() {
		int gameID = 0, lobbyID = 0;
		if (!games.isEmpty()) {
			gameID = games.get(games.size() - 1).getHeader().getID() + 1;
		}
		if(!lobbies.isEmpty()){
			lobbyID = lobbies.get(lobbies.size() - 1).getHeader().getID() + 1;
		}
		return (gameID > lobbyID) ? gameID : lobbyID;
	}
	
	/**
	 * Get the number of updates loops this server perform per second
	 */
	public int getUps(){
		return ups;
	}
	
	@Override
	public boolean loadGame(Lobby lobby) {
		boolean isStarted = lobby.loadGame();
		if (isStarted) {
			sendLobby(lobby);
			Log.debug("Lobby starts loading: " + lobby);
		}
		return isStarted;
	}

	private void lobbyReceived(Lobby lobby, Account account) {
		int index = lobbies.indexOf(lobby);
		Lobby lob = null;
		if (index >= 0) {
			lob = lobbies.get(index);
			lob.update(lobby, account);
			Log.debug("Lobby updated: " + lobby);
		} else {
			if (account.equals(lobby.getOwner())) {
				lobby.setOwner(account);
			}
			lob = createLobby(lobby.getHeader().getName(), lobby.getOwner(),
					lobby.getGameSettings());
			playerConnected(lob.getHeader(), account);
		}
		sendLobby(lob);
	}

	void playerConnected(GameHeader gameHeader, Account account) {
		if (gameHeader == null || account == null) {
			return;
		}
		Lobby lobby = getLobby(gameHeader.getID());
		if (lobby != null) {
			lobby.connectPlayer(account);
			lobbyAccounts.put(lobby, account);
			Log.debug("Player connected to lobby (" + lobby + "): " + account);
			sendLobby(lobby);
		}
	}

	void playerDisconnected(Account account) {
		if (account == null) {
			return;
		}
		ServerGame game = gameAccounts.getKey(account);
		Lobby lobby = lobbyAccounts.getKey(account);
		if (game != null) {
			game.disconnectPlayer(account.getPlayer());
			Log.debug("Player disconnected from game (" + game + "): " + account);
		}
		if (lobby != null) {
			lobbyAccounts.remove(lobby, account);
			lobby.disconnectPlayer(account);
			Log.debug("Player disconnected from lobby (" + lobby + "): " + account);
			if (lobby.isEmpty()) {
				removeLobby(lobby);
			}
			sendLobby(lobby);
		}
	}

	private void playerReceived(Account account, Player updatedPlayer) {
		account.getPlayer().refresh(updatedPlayer);
		Log.debug("Player updated: " + account);
		sendToAll(gameAccounts.get(gameAccounts.getKey(account)), account.getPlayer());
	};
	
	private void playerStatus(Account account, Status status) {
		account.getPlayer().setStatus(status);
		Log.debug("Player got status: " + status + " (" + account + ")");
		switch (status) {
		case DISCONNECTED:
			playerDisconnected(account);
			break;
		case READY:
		case WAITING:
			Lobby lobby = lobbyAccounts.getKey(account);
			if (lobby != null) {
				sendLobby(lobby);
			}
			break;
		default:
			break;
		}
	}
	
	/**
	 * Remove the specified game from the server.
	 * @param game The game to remove.
	 */
	public void removeGame(AbstractGame game) {
		if (game != null) {
			games.remove(game);
			gameAccounts.removeAll(game);
			Log.info("Removed game: " + game);
		}
	}
	
	/**
	 * Remove the specified game from the server.
	 * @param gameID The ID of the game.
	 */
	public void removeGame(int gameID) {
		removeGame(getGame(gameID));
	}

	@Override
	public void removeLobby(Lobby lobby) {
		if (lobby != null) {
			lobby.getPlayers().clear();
			lobbies.remove(lobby);
			Collection<Account> accounts = lobbyAccounts.removeAll(lobby);
			Log.info("Removed lobby: " + lobby);
			sendToAll(accounts, lobby);
		}
	}
	
	public <T> void sendToAll(Collection<Account> accounts, T object) {
		Log.debug("Sends to all: " + object.getClass().getSimpleName() 
				+ " (" + Arrays.toString(accounts.toArray()) + ")");
		netHandler.sendToAccounts(accounts, object);
	}
	
	public <T> void sendToPlayer(Account account, T object) {
		Log.debug("Sends to player: " + object.getClass().getSimpleName() 
				+ " (" + account + ")");
		netHandler.sendToAccount(account, object);
	}
	
	@Override
	public void sendChangedGameObjects(ServerGame game, 
			Map<Integer, ? extends IGameObject> gameObjects) {
		sendToAll(gameAccounts.get(game), new GameObjectsHolder(gameObjects));
	}

	@Override
	public void sendPlayers(ServerGame game) {
		sendToAll(gameAccounts.get(game), new PlayersHolder(game.getPlayers()));
	}

	private void sendLobby(Lobby lobby) {
		lobby.canLoadGame();		// Update message.
		sendToAll(lobbyAccounts.get(lobby), lobby);
	}
	
	@Override
	public void sendWorld(ServerGame game) {
		sendToAll(gameAccounts.get(game), new WorldHolder(game.getWorld(), game.getPlayers(), 
				game.getState(), game.getSettings()));
	}
	
	@Override
	public void start() {
		netHandler.start();
		if (updateThread == null || !updateThread.isAlive()) {
			updateThread = new Thread(updateRunnable);
			updateThread.setDaemon(true);
			updateThread.start();
		}
		Log.info("Started server.");
	}
	
	@Override
	public void stop() {
		if (updateThread != null) {
			updateThread.interrupt();
		}
		netHandler.stop();
		Log.info("Stopped server.");
	}
	
	private class ActionListener implements IServerNetworkListener<IAction> {
		@Override
		public void networkReceived(Account account, IAction action,
				long timeSent) {
			actionReceived(action, account);
		}
	}
	private class DisconnectedListener implements IServerNetworkListener<DisconnectRequest> {
		@Override
		public void networkReceived(Account account, DisconnectRequest request,
				long timeSent) {
			playerDisconnected(account);
		}
	}
	private class JoinLobbyListener implements IServerNetworkListener<JoinLobbyHolder> {
		@Override
		public void networkReceived(Account account,
				JoinLobbyHolder holder, long timeSent) {
			playerConnected(holder.getGameHeader(), account);
		}
	}
	private class LobbyListener implements IServerNetworkListener<Lobby> {
		@Override
		public void networkReceived(Account account, Lobby lobby, long timeSent) {
			lobbyReceived(lobby, account);
		}
	}
	private class PlayerListener implements IServerNetworkListener<Player> {
		@Override
		public void networkReceived(Account account, Player updatedPlayer,
				long timeSent) {
			playerReceived(account, updatedPlayer);
		}
	}
	private class PlayerStatusListener implements IServerNetworkListener<Player.Status> {
		@Override
		public void networkReceived(Account account, Status status,
				long timeSent) {
			playerStatus(account, status);
		}
	}
	
	/**
	 * Runnable for updating the server representation of the model each frame.
	 */
	private Runnable updateRunnable = new Runnable() {
		
		/** Milliseconds the game updates plus the Thread.sleep() in each loop should take. */
		private static final float UPDATE_TIME = 1000f / Constants.SERVER_FPS;
		private float deltaTime = 1;
		private long lastLoopTime = 0;
		private float sleep = UPDATE_TIME;
		/** Timer to decide when one second has passed*/
		private long upsTimer = 0;
		/** Updates per second timer*/
		private int upsCounter = 0;
		
		private void update() {
			for (ServerGame game : games) {
				if (game.getState() != GameState.ENDED) {
					game.update(deltaTime);
				} else {
					removeGame(game);		// Can remove inside loop thanks to CopyOnWriteArrayList.
				}
			}
			for (Lobby lobby : lobbies) {
				if (lobby.getOwner() == null) {
					removeLobby(lobby);
				} else if (lobby.canLoadGame() && lobby.allPlayersLoaded()) {
					createGame(lobby);
				}
			}
		}
		
		private void fpsRegulation() {
			long now = System.nanoTime();
			// timeDiff should be UPDATE_TIME.
			float timeDiffMillis = (float) ((now - lastLoopTime) / 1e6);
			lastLoopTime = now;
			deltaTime = timeDiffMillis / UPDATE_TIME;
			
			// Updates per second counter.
			upsTimer += timeDiffMillis;
			upsCounter++;
			if(upsTimer >= 1000){
				ups = upsCounter;
				upsTimer %= 1000;
				upsCounter = 0;
			}
			
			// Sleep
			sleep += UPDATE_TIME - timeDiffMillis;
			if (sleep >= 0) {
				try {
					Thread.sleep((int) sleep, (int) ((sleep % 1) * 1e6));
				} catch (InterruptedException e) {
					return;
				}
			}
		}

		
		@Override
		public void run() {
			lastLoopTime = System.nanoTime();
			
			while (!Thread.interrupted()) {
				update();
				
				fpsRegulation();
			}
		}
	};
} 