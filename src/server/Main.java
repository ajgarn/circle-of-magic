package server;

import java.util.Scanner;

import model.AbstractGame;
import model.AbstractGameSettings;
import model.Lobby;
import server.model.CoMServer;
import services.logger.ConsoleLogger;
import services.logger.ILogger;
import services.logger.Log;
import services.logger.Log.Level;
import services.logger.WebLogger;
import services.security.Base64AESCrypter;

import common.application.UncaughtExceptionLogger;
import common.application.WebServices;

/**
 * Starts and controls a server via command line.
 * Used for running a server without the graphical user interface.
 * @author Anton Jansson
 */
public class Main {

	// GENERAL COMMANDS (1ST)
	private static final String EXIT 	= "exit";
	private static final String GAME 	= "game";
	private static final String HELP 	= "help";
	private static final String LOBBY 	= "lobby";
	private static final String LOG 	= "log";
	private static final String SERVER 	= "server";
	private static final String STATUS 	= "status";
	private static final String UPS 	= "ups";
	
	// SUB COMMANDS (2ND)
	private static final String CREATE	= "create";
	private static final String DISABLE	= "disable";
	private static final String ENABLE	= "enable";
	private static final String GETALL		= "get all";
	private static final String LOAD	= "load";
	private static final String REMOVE	= "remove";
	private static final String SETTINGS= "settings";
	private static final String START	= "start";
	private static final String STOP	= "stop";
	
	private static CoMServer server;
	private static ILogger logger;
	
	public Main() {
		server = CoMServer.getInstance();
		server.getGameSettings();		// Otherwise server.start() is throwing
										// NullPointerException, due to registration.
		logger = new ConsoleLogger(Level.INFO);
		Log.addLogger(logger);
		Log.addLogger(new WebLogger(Level.DEBUG, "Server", WebServices.LOGGING,
				1000, new Base64AESCrypter(WebServices.Security.SECRET_KEY,
						WebServices.Security.IV)));

		// Log uncaught exceptions.
		Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionLogger());
		
		server.start();
		run();
	}
	
	/**
	 * Handle a command line from terminal.
	 * @param line The command line from terminal.
	 */
	private void commandLine(String line) {
		String[] lineSplit = line.trim().split(" ", 2);
		String command = lineSplit[0].trim().toLowerCase();
		String args = (lineSplit.length > 1) ? lineSplit[1] : null;
		
		executeCommand(command, args);
	}
	
	private void executeCommand(String command, String args) {
		if (command == null || command.isEmpty()) {
			return;
		}
		
		switch (command) {
		case EXIT:
			System.exit(1);
			return;
		case GAME:
			if (args.contains(LOAD)) {
				Commands.loadGame(args);
			} else if (args.contains(GETALL)) {
				Commands.getGames();
			} else if (args.contains(REMOVE)) {
				Commands.removeGame(args);
			} else if (args.contains(SETTINGS)) {
				Commands.getGameSettings();
			}
			break;
		case HELP:
			Commands.help();
			break;
		case LOBBY:
			if (args.contains(CREATE)) {
				Commands.createLobby(args);
			} else if (args.contains(GETALL)) {
				Commands.getLobbies();
			} else if (args.contains(REMOVE)) {
				Commands.removeLobby(args);
			}
			break;
		case LOG:
			Commands.setLogging(args);
			break;
		case SERVER:
			if (args.contains(START)) {
				Commands.startServer();
			} else if (args.contains(STOP)) {
				Commands.stopServer();
			}
			break;
		case STATUS:
			Commands.status();
			break;
		case UPS:
			Commands.ups();
			break;
		default: print("Unknown command " + command);
		}
	}
	
	private static void print(String text) {
		System.out.println(text);
	}
	
	private void run() {
		Scanner sc = new Scanner(System.in);
		print("Circle of Magic Server");
		print(" - Get available commands by writing: " + HELP);
		while (sc.hasNext()) {
			try {
				commandLine(sc.nextLine());
			} catch (IllegalArgumentException e) {
				print("Command not found.");
			} catch (Exception e) {
				print("Error: " + e.getMessage() + " (" + e.getClass().toString() + ")");
			}
		}
		sc.close();
	}

	public static void main(String[] args) {
		new Main();
	}
	
	
	/**
	 * Class holding all server commands.
	 * @author Anton Jansson
	 */
	private static class Commands {

		public static void createLobby(String args) {
			String[] split = removeArgs(args, CREATE).split(" ", 2);
			createLobby(split[0], split[1]);
		}

		public static void help() {
			print("All available commands:");
			printHelp(EXIT, "Stops the server and exits.");
			printHelp(GAME, new String[]{ LOAD, "<lobbyID>"}, "Create a game. Pass the ID of the lobby.");
			printHelp(GAME, new String[]{ REMOVE, "<gameID>"}, "Remove a game.");
			printHelp(GAME, SETTINGS, "List all GameSettings.");
			printHelp(HELP, "List all commands with descriptions.");
			printHelp(LOBBY, new String[]{ CREATE, "<GameSetting>", "<Title>"}, 
					"Create a new lobby. Pass the title and a GameSettings name.");
			printHelp(LOBBY, GETALL, "Print all lobbies on this server. ID is within the brackets (id).");
			printHelp(LOBBY, new String[] {REMOVE, "<lobbyID>"},
					"Remove a lobby.");
			printHelp(LOG, DISABLE, "Disables the log.");
			printHelp(LOG, new String[]{ ENABLE, "<logmode>"}, "Enables the log. " +
					"Log modes: debug, info, warning, error, fatal");
			printHelp(SERVER, START, "Starts the server.");
			printHelp(SERVER, STOP, "Stops the server.");
			printHelp(STATUS, "Lists lobbies and games on this server.");
			printHelp(UPS, "Returns the update loops per second.");
		}

		public static void getGames() {
			print("Games on this server (" + server.getGames().size() + "):");
			for (AbstractGame g : server.getGames()) {
				print("- " + g.getHeader());
			}
		}

		public static void getGameSettings() {
			print("Available GameSettings:");
			for (AbstractGameSettings s : server.getGameSettings()) {
				print("- " + s.getClass().getSimpleName());
			}
		}

		public static void getLobbies() {
			print("Lobbies on this server (" + server.getLobbies().size() + "):");
			for (Lobby lobby : server.getLobbies()) {
				print("- " + lobby);
			}
		}

		public static void loadGame(String args) {
			try {
				int gameID = Integer.parseInt(removeArgs(args, LOAD));
				Lobby lobby = getLobbyByID(gameID);
				if (lobby != null) {
					if (server.loadGame(lobby)) {
						print("Game " + gameID + " loaded.");
					} else {
						print("Game " + gameID + " NOT loaded.");
					}
				} else {
					print("Could not find lobby");
				}
			} catch (NumberFormatException e) {
				print("Invalid number format: " + e.getMessage());
			}
		}

		public static void removeGame(String args) {
			try {
				int gameID = Integer.parseInt(removeArgs(args, REMOVE));
				server.removeGame(gameID);
			} catch (NumberFormatException e) {
				print("Invalid number format: " + e.getMessage());
			}
		}

		public static void removeLobby(String args) {
			int lobbyID = Integer.parseInt(removeArgs(args, REMOVE));
			server.removeLobby(getLobbyByID(lobbyID));
		}
		
		public static void setLogging(String args) {
			if (args.contains(DISABLE)) {
				logger.setLevel(Level.NONE);
			} else if (args.contains(ENABLE)) {
				args = removeArgs(args, ENABLE).toLowerCase();
				if (args.contains("debug")) {
					logger.setLevel(Level.DEBUG);
				} else if (args.contains("error")) {
					logger.setLevel(Level.ERROR);
				} else if (args.contains("fatal")) {
					logger.setLevel(Level.FATAL);
				} else if (args.contains("info")) {
					logger.setLevel(Level.INFO);
				} else if (args.contains("warning")) {
					logger.setLevel(Level.WARNING);
				} else {
					print("Invalid log mode. Log modes: debug, info, warning, error, fatal");
				}
			}
		}
		
		public static void startServer() {
			server.start();
		}
		
		public static void status() {
			getLobbies();
			getGames();
		}
		
		public static void stopServer() {
			server.stop();
		}

		public static void ups() {
			print("Ups: " + server.getUps());
		}

		
		// Private helper methods
		private static void createLobby(String settings, String name) {
			AbstractGameSettings settingsClass = getGameSettingsByString(settings);
			if (settingsClass == null) {
				print("Undefined GameSettings type. List all GameSettings via the "
						+ "command: " + GAME + " " + SETTINGS);
			} else {
				server.createLobby(name, null, settingsClass);
			}
		}
		
		private static AbstractGameSettings getGameSettingsByString(String className) {
			for (AbstractGameSettings s : server.getGameSettings()) {
				if (s.getClass().getSimpleName().equals(className)) {
					return s;
				}
			}
			return null;
		}

		private static Lobby getLobbyByID(int id) {
			for (Lobby lobby : server.getLobbies()) {
				if (lobby.getHeader().getID() == id) {
					return lobby;
				}
			}
			return null;
		}
		
		private static void printHelp(String command, String desc) {
			System.out.format("%-38s%-50s%n", command, desc);
		}
		private static void printHelp(String command, String arg, String desc) {
			printHelp(command, new String[] { arg }, desc);
		}
		private static void printHelp(String command, String[] args, String desc) {
			String cmd = command.toString();
			for (String arg : args) {
				cmd += " " + arg;
			}
			printHelp(cmd, desc);
		}
		
		/**
		 * Returns the command line without the given command.
		 * @param raw The raw command line.
		 * @param target The command that should be removed.
		 * @return The rest of the line.
		 */
		private static String removeArgs(String raw, String target) {
			return raw.replace(target, "").trim();
		}
	}

}
