package model.spells;

import static org.junit.Assert.assertTrue;

import java.util.List;

import model.Mage;
import model.core.ColorNames;
import model.core.SpellNames;
import model.core.State;
import model.core.Vector2;

import org.junit.Test;


public class CastSpellActionTest {

	@Test
	public void test() {
		CastSpellAction action = new ProjectileAction(new SpellSlot(SpellRecipeFactory.createSpellRecipe(SpellNames.FIREBALL)), new Vector2(10, 10));
		action.perform(120, new Mage(new Vector2(0, 0), 0f, State.STANDING, 100, 10f, ColorNames.BLACK));
		List<? extends SpellObject> objects =	action.pullGameObjects();
		assertTrue( !objects.isEmpty());
		
	}

}
