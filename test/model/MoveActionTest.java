package model;

import static org.junit.Assert.assertTrue;
import model.core.ColorNames;
import model.core.MoveAction;
import model.core.State;
import model.core.Vector2;

import org.junit.Test;


public class MoveActionTest {

	@Test
	public void testPerform() {
		Mage caster = new Mage(new Vector2(0,0),(float)0,State.STANDING,100,(float)8,ColorNames.BLACK);
		MoveAction move = new MoveAction(new Vector2(10, 10));
		caster.addAction(move);
		for(int i = 0; i < 1000; i++){
			caster.update(1);
		}
		assertTrue(move.isDone(caster));
	}

}
