package model.core;

import static org.junit.Assert.assertTrue;
import model.GameObjectFactory;
import model.Mage;
import model.spells.CastSpellAction;
import model.spells.CastSpellActionFactory;
import model.spells.SpellRecipeFactory;
import model.spells.SpellSlot;

import org.junit.Test;


public class GameObjectTest {

	@Test
	public void testLookAt() {
		GameObject object = new Mage(new Vector2(0,0), 0f, State.STANDING, 100, 10f, ColorNames.BLACK);
		float temp = 10 -object.getDirection();
		while(!(temp < object.getRotationSpeed() && temp > -object.getRotationSpeed())){
			object.lookAt(10);
			temp = 10 -object.getDirection();
		}
		assertTrue(temp < object.getRotationSpeed() && temp > -object.getRotationSpeed());
		
	}
	@Test
	public void testAddAction(){
		GameObject object = GameObjectFactory.createMage(ColorNames.getRandomColor(), new Vector2());
		object.addAction(CastSpellActionFactory.create(new SpellSlot(SpellRecipeFactory.createSpellRecipe(SpellNames.FIREBALL)), new Vector2(10,10)));
		object.addAction(CastSpellActionFactory.create(new SpellSlot(SpellRecipeFactory.createSpellRecipe(SpellNames.FIREBALL)), new Vector2(10,10)));
		object.addAction(CastSpellActionFactory.create(new SpellSlot(SpellRecipeFactory.createSpellRecipe(SpellNames.FIREBALL)), new Vector2(10,10)));
		object.addAction(new MoveAction(new Vector2(20,20)));
		assertTrue(object.getActions().size() == 2 && object.getActions().get(0) instanceof CastSpellAction && object.getActions().get(1) instanceof MoveAction);
	}

}
