package server.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import model.AbstractGame;
import model.AbstractGameSettings;
import model.Account;
import model.Lobby;
import model.core.GameObject;
import model.gamesettings.NeverEndingGameSettings;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import server.network.AuthenticationService;
import services.logger.ConsoleLogger;
import services.logger.Log;
import services.logger.Log.Level;
import services.network.ClientNetHandler;
import services.network.IClassNetworkReceivable;
import services.network.IClientNetHandler;
import services.network.NetworkSettings;
import client.model.CoMClient;
import client.network.IClientNetworkListener;

import common.network.holders.GameObjectsHolder;
import common.network.holders.JoinLobbyHolder;
import common.network.holders.WorldHolder;

public class TestCoMServer {
	
	private static CoMServer server;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		server = CoMServer.getInstance();
		Log.addLogger(new ConsoleLogger(Level.WARNING));
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	@Test
	public void testCreateGame() {
		int numGames = server.getGames().size();
		Lobby lobby = createLobby();
		server.start();
		server.playerConnected(lobby.getHeader(), new Account(1, ""));
		server.createGame(lobby);
		server.stop();
		assertTrue("Game is not created.", server.getGames().size() == numGames + 1);
		server.removeGame(lobby.getHeader().getID());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testCreateGameIllegalSettings() {
		server.start();
		server.createGame(createLobby());
		server.stop();
	}
	
	@Test
	public void testCreateGameNull() {
		server.createGame(null);
	}

	@Test
	public void testCreateLobby() throws InterruptedException {
		Lobby lo1 = createLobby();
		Lobby lo2 = createLobby();
		
		assertTrue("Lobbies are not created", server.getLobbies().contains(lo1)
				&& server.getLobbies().contains(lo2));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testCreateLobbyNull() {
		int size = server.getLobbies().size();
		try {
			server.createLobby(null, null, null);
		} catch (IllegalArgumentException e) {
			throw e;
		} finally {
			assertTrue("Null-valued lobby is added to server lobbies.",
					server.getLobbies().size() == size);
		}
	}
	
	@Test
	public void testGetGameSettings() {
		Collection<AbstractGameSettings> settings = server.getGameSettings();
		for (AbstractGameSettings s : settings) {
			assertTrue("Class " + s + " is not subclass of AbstractGameSettings.",
					s instanceof AbstractGameSettings);
			assertFalse(s.getClass().getCanonicalName() + " should not be member of the collection.",
					s.getClass().equals(AbstractGameSettings.class));
		}
	}
	
	@Test
	public void testLobbyJoined() throws InterruptedException {
		Lobby lobby = createLobby();
		server.start();
		joinLobby(lobby);
		server.stop();
		Thread.sleep(40);
		assertTrue("Player is not added to lobby.", lobby.getPlayers().size() == 1);
	}

	@Test
	public void testPlayerConnected() throws InterruptedException {
		Lobby lobby = createLobby();
		server.start();
		server.playerConnected(lobby.getHeader(), new Account(1, "Name"));
		server.stop();
		Thread.sleep(40);
		assertTrue("Player is not added to lobby.", lobby.getPlayers().size() == 1);
	}

	@Test
	public void testPlayerDisconnected() throws InterruptedException {
		Lobby lobby = createLobby();
		Account a1 = new Account(1, "Name 1");
		Account a2 = new Account(2, "Name 2");
		
		server.start();
		server.playerConnected(lobby.getHeader(), a1);
		server.playerConnected(lobby.getHeader(), a2);
		assertTrue("Player is not added to lobby.", lobby.getPlayers().size() == 2);
		server.playerDisconnected(a1);
		Thread.sleep(10);
		assertTrue("Player not removed from lobby. (size=" + lobby.getPlayers().size() + ")",
				lobby.getPlayers().size() == 1);
		
		server.createGame(lobby);
		AbstractGame game = server.getGame(lobby.getHeader().getID());
		server.playerDisconnected(a2);
		server.stop();
		assertTrue("Game has not ended or has not been removed.", 
				!server.getGames().contains(game));
	}

	@Test
	public void testRemoveGame() {
		server.removeGame(10);		// Test IndexOutOfBounds
		
		Lobby lobby = createLobby();
		server.start();
		server.playerConnected(lobby.getHeader(), new Account(1, ""));
		server.createGame(lobby);
		server.stop();
		int numGames = server.getGames().size();
		server.removeGame(lobby.getHeader().getID());
		
		assertTrue("Game is not removed.", server.getGames().size() == numGames - 1);
	}
	


	@Test
	public void testRemoveLobby() {
		server.removeLobby(null);
		Lobby lo1 = createLobby();
		int size = server.getLobbies().size();
		server.start();
		server.removeLobby(lo1);
		server.stop();
		
		assertTrue("Lobby is not removed", !server.getLobbies().contains(lo1)
				&& server.getLobbies().size() == size - 1);
	}

	@Test
	public void testSendChangedGameObjects() throws InterruptedException {
		final AtomicBoolean result = new AtomicBoolean(false);
		
		IClientNetworkListener<GameObjectsHolder> goListener = new IClientNetworkListener<GameObjectsHolder>() {
			@Override
			public void networkReceived(GameObjectsHolder object, long timeSent) {
				result.set(true);
			}
		};
		IClientNetworkListener<Lobby> lobbyListener = new IClientNetworkListener<Lobby>() {
			@Override
			public void networkReceived(Lobby object, long timeSent) {}
		};
		IClientNetworkListener<WorldHolder> worldListener = new IClientNetworkListener<WorldHolder>() {
			@Override
			public void networkReceived(WorldHolder object, long timeSent) {}
		};

		IClassNetworkReceivable<IClientNetworkListener<?>> netReceivable = CoMClient
				.getInstance().getNetworkReceivable();
		netReceivable.addListener(GameObjectsHolder.class, goListener);
		netReceivable.addListener(Lobby.class, lobbyListener);
		netReceivable.addListener(WorldHolder.class, worldListener);
		
		Lobby lobby = createLobby();
		server.start();
		server.playerConnected(lobby.getHeader(), new Account(3, "Player 1"));
		joinLobby(lobby);
		Thread.sleep(100);		// Wait for player to join.
		server.createGame(lobby);
		server.sendChangedGameObjects(server.getGame(lobby.getHeader().getID()),
				new HashMap<Integer, GameObject>());
		Thread.sleep(200);
		server.removeGame(lobby.getHeader().getID());
		server.stop();
		
		netReceivable.removeListener(GameObjectsHolder.class, goListener);
		netReceivable.removeListener(Lobby.class, lobbyListener);
		netReceivable.removeListener(WorldHolder.class, worldListener);
		
		assertTrue("ChangedGameObjectsHolder is not received.", result.get());
	}

	@Test
	public void testSendWorld() throws InterruptedException {
		final AtomicInteger result = new AtomicInteger(0);
		
		IClientNetworkListener<WorldHolder> worldListener = new IClientNetworkListener<WorldHolder>() {
			@Override
			public void networkReceived(WorldHolder object, long timeSent) {
				result.incrementAndGet();
			}
		};
		IClientNetworkListener<Lobby> lobbyListener = new IClientNetworkListener<Lobby>() {
			@Override
			public void networkReceived(Lobby object, long timeSent) {}
		};

		IClassNetworkReceivable<IClientNetworkListener<?>> netReceivable = CoMClient
				.getInstance().getNetworkReceivable();
		netReceivable.addListener(WorldHolder.class, worldListener);
		netReceivable.addListener(Lobby.class, lobbyListener);
		
		Lobby lobby = createLobby();
		server.start();
		joinLobby(lobby);
		Thread.sleep(50);				// Wait for player to join.
		server.createGame(lobby);		// Sends the world once.
		Thread.sleep(200);
		assertTrue("World is not received when game is created.", result.get() == 1);
		
		server.sendWorld(server.getGame((lobby.getHeader().getID())));
		server.removeGame(lobby.getHeader().getID());
		Thread.sleep(200);
		server.stop();
		
		netReceivable.removeListener(WorldHolder.class, worldListener);
		netReceivable.removeListener(Lobby.class, lobbyListener);
		
		assertTrue("World is not received of sendWorldToPlayer().", result.get() == 2);
	}

	@Test
	public void testStart() throws InterruptedException {
		server.start();
		server.start();		// Multiple calls not giving exception.
		
		Lobby lobby = createLobby();
		server.playerConnected(lobby.getHeader(), new Account(1, "1"));
		server.sendToAll(Arrays.asList(new Account(1, ""), new Account(2, "")), lobby);
		server.removeLobby(lobby);
		server.stop();
	}

	@Test (expected = IllegalStateException.class)
	public void testStop() {
		server.stop();
		server.stop();
		
		Lobby lobby = createLobby();
		try {
			server.sendWorld(server.getGame((lobby.getHeader().getID())));
		} catch (IllegalStateException e) {
			throw e;
		} finally {
			server.removeLobby(lobby);
		}
	}

	private Lobby createLobby() {
		return server.createLobby("Test game", new Account(0, ""), 
				new NeverEndingGameSettings());
	}

	public void joinLobby(Lobby lobby) {
		IClientNetHandler client = ClientNetHandler.getInstance(new NetworkSettings());
		try {
			client.connectHost(lobby.getHeader().getHost());
		} catch (IOException e) {}
		AuthenticationService.getInstance().login("sheepstudios", "hu7UCh=7", 
				client.getCurrentClient().getID());
		client.send(new JoinLobbyHolder(lobby.getHeader()));
	}
}
