package server.network;

import static org.junit.Assert.assertTrue;

import model.Account;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import server.network.DBAuthenticator;



/**
 * 
 * @author Anton Jansson
 *
 */
public class TestDBAuthenticator {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void testAuthenticate() {
		IAuthenticator authenticator = new DBAuthenticator();
		Account acc1 = authenticator.authenticate("sheepstudios", "hu7UCh=7");
		Account acc2 = authenticator.authenticate("sheepstudios", "");
		assertTrue("Wrong username and password", acc1 != null);
		assertTrue("Should be wrong username or password but is not", acc2 == null);
	}

}
