package services.network;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import model.World;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import services.network.INetworkListener;
import services.network.IServerNetHandler;
import services.network.NetHolder;
import services.network.NetworkSettings;
import services.network.ServerNetHandler;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

import common.network.NetworkRegistration;
import common.network.holders.WorldHolder;

/**
 * 
 * @author Anton Jansson
 *
 */
public class TestServerNetHandler {
	
	/**
	 * Specify the time in milliseconds to wait for object to be
	 * sent over network.
	 */
	private static final int OBJECT_RECEIVED_WAIT = 100;
	
	private static IServerNetHandler netHandler;
	private static NetworkSettings settings;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		settings = NetworkRegistration.getNetworkSettings();
		netHandler = new ServerNetHandler(settings);
		netHandler.start();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void testGetAddress() {
		InetAddress addr = netHandler.getAddress();
		
		assertNotNull("Address is null", addr);
		assertFalse("Address is loopback IP", addr.isLoopbackAddress());
	}
	
	@Test
	public void testReceiveObject() throws IOException, InterruptedException {
		final AtomicBoolean result = new AtomicBoolean(false);
		netHandler.addListener(new INetworkListener() {
			@Override
			public void networkReceived(int connectionID, Object object, long time) {
				if (object instanceof WorldHolder) {
					result.set(true);
				}
			}
		});
		Client client = createAndConnectClient();
		client.sendTCP(new NetHolder<WorldHolder>(
				new WorldHolder(new World(), null, null, null)));
		Thread.sleep(OBJECT_RECEIVED_WAIT);
		assertTrue("Object is not received or handled by server.", result.get());
	}

	@Test
	public void testSendToAll() throws IOException, InterruptedException {
		final AtomicInteger result = new AtomicInteger(0);
		Client cl1 = createAndConnectClient();
		Client cl2 = createAndConnectClient();
		cl1.addListener(new Listener() {
			@Override
			public void received(Connection connection, Object object) {
				if (object.getClass() == NetHolder.class) {
					result.incrementAndGet();
				}
			}
		});
		cl2.addListener(new Listener() {
			@Override
			public void received(Connection connection, Object object) {
				if (object.getClass() == NetHolder.class) {
					result.incrementAndGet();
				}
			}
		});

		Collection<Integer> players = Arrays.asList(cl1.getID(), cl2.getID());
		netHandler.send(players, new NetHolder<WorldHolder>(
				new WorldHolder(new World(), null, null, null)));
		Thread.sleep(OBJECT_RECEIVED_WAIT);
		assertTrue("World is not received by client.", result.get() == 2);
	}

	@Test
	public void testSendToPlayer() throws IOException, InterruptedException {
		final AtomicInteger result = new AtomicInteger(0);
		Client client = createAndConnectClient();
		client.addListener(new Listener() {
			@Override
			public void received(Connection connection, Object object) {
				if (object.getClass() == NetHolder.class) {
					result.incrementAndGet();
				}
			}
		});
		
		// Increment result only on first call.
		netHandler.send(client.getID(), new NetHolder<WorldHolder>(
				new WorldHolder(new World(), null, null, null)));
		netHandler.send(client.getID() + 1, new NetHolder<WorldHolder>(
				new WorldHolder(new World(), null, null, null)));
		
		Thread.sleep(OBJECT_RECEIVED_WAIT);
		assertTrue("World not send or sent to multiple clients.", result.get() == 1);
	}
	
	@Test
	public void testStart() throws IOException {
		netHandler.start();
		netHandler.start();
		netHandler.stop();
		netHandler.start();
		createAndConnectClient();
	}
	
	@Test(expected = ConnectException.class)
	public void testStop() throws IOException {
		netHandler.stop();
		netHandler.stop();
		try {
			createAndConnectClient();
		} catch (IOException e) {
			if (e.getCause() instanceof ConnectException) {
				throw new ConnectException(e.getCause().getMessage());
			} else {
				throw e;
			}
		} finally {
			netHandler.start();
		}
	}
	
	
	// Private helper methods
	
	/**
	 * Creates a {@link Client}, registers kryo classes, starts the client
	 * and connects it to the server.
	 * @return
	 * @throws IOException
	 */
	private Client createAndConnectClient() throws IOException {
		final Client client = new Client();
		settings.registerKryoClasses(client.getKryo());
		client.start();
		client.connect(settings.getTimeout(), InetAddress.getLoopbackAddress(), 
				settings.getTCPPort(), settings.getUDPPort());
		return client;
	}

}
