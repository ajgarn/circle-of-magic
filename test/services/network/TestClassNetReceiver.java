package services.network;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;

import common.network.NetworkRegistration;

import services.network.ClassNetReceiver;
import services.network.ClientNetHandler;
import services.network.IClassNetworkListener;
import services.network.IClientNetHandler;
import services.network.IServerNetHandler;
import services.network.NetworkSettings;
import services.network.ServerNetHandler;

public class TestClassNetReceiver {

	@Test
	public void testNetworkReceived() throws InterruptedException {
		NetworkSettings settings = NetworkRegistration.getNetworkSettings();
		IServerNetHandler server = new ServerNetHandler(settings);
		IClientNetHandler client = ClientNetHandler.getInstance(settings);
		ClassNetReceiver receiver = new ClassNetReceiver(server);
		final AtomicInteger result = new AtomicInteger(0);
		
		receiver.addListener(String.class, new IClassNetworkListener<String>() {
			@Override
			public void networkReceived(int connectionID, String object,
					long timeSent) {
				result.incrementAndGet();
			}
		});
		
		server.start();
		try {
			client.connectHost(server.getAddress());
		} catch (IOException e) {}
		client.send("Text");
		client.send(new Integer(1));
		
		Thread.sleep(20);
		server.stop();
		
		assertTrue("Value not received by listener", result.get() == 1);
	}

}
