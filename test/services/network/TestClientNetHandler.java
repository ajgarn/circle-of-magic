package services.network;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicBoolean;

import model.AbstractGame;
import model.GameHeader;
import model.Lobby;
import model.World;
import model.gamesettings.NeverEndingGameSettings;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import common.network.NetworkRegistration;
import common.network.holders.WorldHolder;

import services.network.ClientNetHandler;
import services.network.IClientNetHandler;
import services.network.INetworkListener;
import services.network.IServerNetHandler;
import services.network.NetworkSettings;
import services.network.ServerNetHandler;

/**
 * @author Anton Jansson
 */
public class TestClientNetHandler {
	/**
	 * Specify the time in milliseconds to wait for object to be
	 * sent over network.
	 */
	private static final int OBJECT_RECEIVED_WAIT = 200;
	
	private static IClientNetHandler clientNetHandler;
	private static IServerNetHandler serverNetHandler;
	private static final Collection<AbstractGame> games = new ArrayList<AbstractGame>();
	private static final Collection<Lobby> lobbies = new ArrayList<Lobby>();

	@BeforeClass
	public static void setUpClass() throws Exception {
		NetworkSettings settings = NetworkRegistration.getNetworkSettings();
		clientNetHandler = ClientNetHandler.getInstance(settings);
		serverNetHandler = new ServerNetHandler(settings);
		serverNetHandler.start();
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
		serverNetHandler.stop();
	}
	
	@Before
	public void setUp() {
		games.clear();
		lobbies.clear();
		
		games.add(new AbstractGame(new GameHeader(1, "Test", InetAddress.getLoopbackAddress())) {});
		lobbies.add(new Lobby(new GameHeader(1, "Test", InetAddress.getLoopbackAddress()), 
				null, new NeverEndingGameSettings()));
		lobbies.add(new Lobby(new GameHeader(2, "Test", InetAddress.getLoopbackAddress()), 
				null, new NeverEndingGameSettings()));
	}
	
	/*@Test
	public void testGetAvailableGamesAndLobbiesLAN() {
		Collection<GameHeader> games = clientNetHandler.getAvailableGames();
		
		// TODO: Implement more test result possibilities.
		assertNotNull("Returns a null reference", games);
		assertFalse("The list is empty", games.isEmpty());
	}
	
	@Test
	public void testGetAvailableGamesAndLobbiesInetAddress() {
		Collection<GameHeader> result = clientNetHandler.getAvailableGames(serverNetHandler.getAddress());
		
		assertTrue("The list contains " + result.size() + " instead of " + lobbies.size(),
				result.size() == lobbies.size());
	}
	
	@Test
	public void testGetAvailableGamesAndLobbiesString() {
		Collection<GameHeader> result = clientNetHandler.getAvailableGames("localhost");
		
		assertTrue("The list contains " + result.size() + " instead of " + lobbies.size(),
				result.size() == lobbies.size());
	}*/
	
	/*@Test
	public void testReceiveObject() throws IOException, InterruptedException {
		final AtomicBoolean result = new AtomicBoolean(false);
		WorldHolderReceiver worldReceiver = new WorldHolderReceiver(clientNetHandler);
		worldReceiver.addWorldReceivedListener(new IWorldReceivedListener() {
			@Override
			public void worldReceived(World world) {
				result.set(true);
			}
			@Override
			public void changedGameObjectsReceived(
					Map<Integer, GameObject> gameObjects, long timeSent) {}
		});
		Log.DEBUG();
		clientNetHandler.connectHost(serverNetHandler.getAddress());
		serverNetHandler.send(Arrays.asList(0,1,2,3,4,5,6,7,8,9), new WorldHolder(new World()));	// Get client ID?
		Thread.sleep(OBJECT_RECEIVED_WAIT);
		assertTrue("Object is not received or handled by client.", result.get());
	}*/
	
	@Test
	public void testSend() throws InterruptedException {
		final AtomicBoolean result = new AtomicBoolean(false);
		serverNetHandler.addListener(new INetworkListener() {
			@Override
			public void networkReceived(int connectionID, Object object, long time) {
				if (object instanceof WorldHolder) {
					result.set(true);
				}
			}
		});
		
		try {
			clientNetHandler.connectHost(serverNetHandler.getAddress());
		} catch (IOException e) {}
		clientNetHandler.send(new WorldHolder(new World(), null, null, null));
		Thread.sleep(OBJECT_RECEIVED_WAIT);
		assertTrue("Object not received by server.", result.get());
	}
}
