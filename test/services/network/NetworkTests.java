package services.network;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestClassNetReceiver.class, TestClientNetHandler.class,
		TestServerNetHandler.class })
public class NetworkTests {

}
