CLAIMS
© Alexander Härenstam, Anton Jansson, Olliver Mattsson, Robin Grönberg.

AVAILABILITY
You may until further notice freely use this software in any way you'd like.

PROGRESS
By contributing to this source code, you grant the original copyright-holders full ownership of your contributions. This means that you give up both your economic and moral rights to your contributions.